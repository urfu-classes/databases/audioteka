USE MASTER;

IF NOT db_id('kb301_dolgorukov') IS NULL
    ALTER DATABASE kb301_dolgorukov SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
    DROP DATABASE IF EXISTS kb301_dolgorukov;
GO

CREATE DATABASE kb301_dolgorukov;
GO

USE kb301_dolgorukov;
GO

IF EXISTS (SELECT * FROM sys.schemas WHERE name = 'ind')
    DROP TABLE IF EXISTS ind.tracks;
    DROP TABLE IF EXISTS ind.albums;
    DROP TABLE IF EXISTS ind.authors;
    DROP TABLE IF EXISTS ind.summary_genres;
    DROP TABLE IF EXISTS ind.genres;

    DROP FUNCTION IF EXISTS ind.cost_album;
    DROP FUNCTION IF EXISTS ind.cost_album_cheapest;
    DROP FUNCTION IF EXISTS ind.durations_author;
    DROP FUNCTION IF EXISTS ind.duration_minimum;
    DROP FUNCTION IF EXISTS ind.artists_with_duration_minimum;
    DROP FUNCTION IF EXISTS ind.duration_album;
    DROP FUNCTION IF EXISTS ind.count_tracks;
    DROP FUNCTION IF EXISTS ind.authors_and_durations_of_albums;

    DROP TRIGGER IF EXISTS ind.on_insert_track;

    DROP SCHEMA IF EXISTS ind;
GO

CREATE SCHEMA ind;
GO

-- Таблица «Авторы».
CREATE TABLE ind.authors (
    id INT PRIMARY KEY,
    name NVARCHAR(128)
);
GO

-- Таблица «Жанры».
CREATE TABLE ind.genres (
    id TINYINT PRIMARY KEY,
    name NVARCHAR(64)
);
GO

-- Таблица «Альбомы».
CREATE TABLE ind.albums (
    id_album INT PRIMARY KEY,
    title NVARCHAR(128),
    author INT FOREIGN KEY REFERENCES ind.authors(id),
);
GO

-- Таблица «Треки».
CREATE TABLE ind.tracks (
    id INT PRIMARY KEY,
    title NVARCHAR(128),
    album INT FOREIGN KEY REFERENCES ind.albums(id_album),
    genre TINYINT FOREIGN KEY REFERENCES ind.genres(id),
    duration INT,
    bytes INT,
    cost DECIMAL(5, 2)
);
GO

-- Таблица «Итоги по жанрам».
CREATE TABLE ind.summary_genres (
    id_genre TINYINT FOREIGN KEY REFERENCES ind.genres(id),
    count_track INT,
    size_summa BIGINT
);
GO

-- Функция вычисления цены альбома.
CREATE FUNCTION ind.cost_album (
    @id_album INT
) RETURNS DECIMAL(8, 2)
AS
BEGIN
    DECLARE @total DECIMAL(8, 2) = 0;

    SELECT @total = sum(cost) FROM ind.tracks
    WHERE ind.tracks.album = @id_album;

    RETURN @total;
END;
GO

-- Функция поиска цены самого дешёвого альбома у исполнителя.
CREATE FUNCTION ind.cost_album_cheapest (
    @name_author NVARCHAR(256)
) RETURNS INT
AS
BEGIN
    DECLARE @cost DECIMAL(8, 2) = 0;

    SELECT @cost = min(ind.cost_album(id_album)) FROM ind.albums
    JOIN ind.authors
    ON ind.albums.author = ind.authors.id AND
       ind.authors.name = @name_author;

    RETURN @cost;
END;
GO

-- Построение отношения таблицы, где у каждого автора единого жанра указывается
-- суммарная длительность его треков.
CREATE FUNCTION ind.durations_author (
    @genre NVARCHAR(64)
) RETURNS TABLE
AS
    RETURN
    SELECT sum(duration) AS duration,
           ind.authors.id AS author
    FROM ind.tracks
    JOIN ind.albums ON ind.albums.id_album = ind.tracks.album
    JOIN ind.authors ON ind.albums.author = ind.authors.id
    JOIN ind.genres ON ind.tracks.genre = ind.genres.id AND
                       ind.genres.name = @genre
    GROUP BY ind.authors.id;
GO

-- Наименьшая суммарная продолжительность треков в жанре.
CREATE FUNCTION ind.duration_minimum (
    @genre NVARCHAR(64)
) RETURNS INT
AS
BEGIN
    DECLARE @duration INT = 0;
    SELECT @duration = min(duration) FROM ind.durations_author(@genre);

    RETURN @duration;
END;
GO

-- Вычисление авторов в жанре, имеющих наименьшую суммарную продолжительность в
-- жанре.
CREATE FUNCTION ind.artists_with_duration_minimum (
    @genre NVARCHAR(64)
) RETURNS TABLE
AS
    RETURN
    SELECT duration / 1000 as N'Продолжительность',
           ind.authors.name as N'Название'
    FROM ind.durations_author(@genre) AS da
    JOIN ind.authors ON da.author = ind.authors.id
    WHERE duration = ind.duration_minimum(@genre);
GO

-- Триггер, обновляющий информацию в таблице с итогами.
CREATE TRIGGER ind.on_insert_track ON ind.tracks AFTER INSERT
AS
BEGIN
    UPDATE ind.summary_genres SET
        count_track += (
            SELECT count(*) FROM inserted
            WHERE ind.summary_genres.id_genre = genre
        )

        , size_summa += (
            SELECT sum(cast(bytes AS BIGINT))
            FROM inserted
            WHERE ind.summary_genres.id_genre = inserted.genre
        )
    FROM inserted

    UPDATE ind.summary_genres SET
        size_summa /= (1024 * 1024)
END;
GO

CREATE FUNCTION ind.duration_album (
    @id_album INT
) RETURNS BIGINT
AS
BEGIN
    DECLARE @duration_tracks_total BIGINT = 0;

    SELECT
        @duration_tracks_total = sum(duration)
    FROM ind.tracks
    WHERE ind.tracks.album = @id_album;

    RETURN @duration_tracks_total / (1000 * 60);
END;
GO

CREATE FUNCTION ind.count_tracks(
    @id_album INT
) RETURNS INT
AS
BEGIN
    DECLARE @count INT = 0;

    SELECT
        @count = count(*)
    FROM ind.tracks
    WHERE ind.tracks.album = @id_album;

    RETURN @count;
END;
GO

CREATE FUNCTION ind.authors_and_durations_of_albums ()
RETURNS TABLE
AS
    RETURN
    SELECT
        ind.albums.title as [Название альбома]
        , ind.authors.[name] as [Исполнитель]
        , ind.duration_album(id_album) as [Продолжительность, мин.]
        , ind.count_tracks(id_album) as [Количество треков]
    FROM ind.albums
    JOIN ind.authors
    ON ind.albums.author = ind.authors.id;
GO


INSERT INTO ind.authors(id, name) VALUES
(1, N'AC/DC')
, (2, N'Accept')
, (3, N'Aerosmith')
, (4, N'Alanis Morissette')
, (5, N'Alice In Chains')
, (6, N'Antônio Carlos Jobim')
, (7, N'Apocalyptica')
, (8, N'Audioslave')
, (9, N'BackBeat')
, (10, N'Billy Cobham')
, (11, N'Black Label Society')
, (12, N'Black Sabbath')
, (13, N'Body Count')
, (14, N'Bruce Dickinson')
, (15, N'Buddy Guy')
, (16, N'Caetano Veloso')
, (17, N'Chico Buarque')
, (18, N'Chico Science & NaГ§ГЈo Zumbi')
, (19, N'Cidade Negra')
, (20, N'Claudio Zoli')
, (21, N'Various Artists')
, (22, N'Led Zeppelin')
, (23, N'Frank Zappa & Captain Beefheart')
, (24, N'Marcos Valle')
, (25, N'Milton Nascimento & Bebeto')
, (26, N'Azymuth')
, (27, N'Gilberto Gil')
, (28, N'JoГЈo Gilberto')
, (29, N'Bebel Gilberto')
, (30, N'Jorge Vercilo')
, (31, N'Baby Consuelo')
, (32, N'Ney Matogrosso')
, (33, N'Luiz Melodia')
, (34, N'Nando Reis')
, (35, N'Pedro LuГ­s & A Parede')
, (36, N'O Rappa')
, (37, N'Ed Motta')
, (38, N'Banda Black Rio')
, (39, N'Fernanda Porto')
, (40, N'Os Cariocas')
, (41, N'Elis Regina')
, (42, N'Milton Nascimento')
, (43, N'A Cor Do Som')
, (44, N'Kid Abelha')
, (45, N'Sandra De So')
, (46, N'Jorge Ben')
, (47, N'Hermeto Pascoal')
, (48, N'BarГЈo Vermelho')
, (49, N'Edson, DJ Marky & DJ Patife Featuring Fernanda Porto')
, (50, N'Metallica')
, (51, N'Queen')
, (52, N'Kiss')
, (53, N'Spyro Gyra')
, (54, N'Green Day')
, (55, N'David Coverdale')
, (56, N'Gonzaguinha')
, (57, N'Os Mutantes')
, (58, N'Deep Purple')
, (59, N'Santana')
, (60, N'Santana Feat. Dave Matthews')
, (61, N'Santana Feat. Everlast')
, (62, N'Santana Feat. Rob Thomas')
, (63, N'Santana Feat. Lauryn Hill & Cee-Lo')
, (64, N'Santana Feat. The Project G&B')
, (65, N'Santana Feat. Mano')
, (66, N'Santana Feat. Eagle-Eye Cherry')
, (67, N'Santana Feat. Eric Clapton')
, (68, N'Miles Davis')
, (69, N'Gene Krupa')
, (70, N'Toquinho & Vinicius')
, (71, N'Vinocius De Moraes & Baden Powell')
, (72, N'Vinicius De Moraes')
, (73, N'Vinicius E Qurteto Em Cy')
, (74, N'Vinicius E Odette Lara')
, (75, N'Vinicius, Toquinho & Quarteto Em Cy')
, (76, N'Creedence Clearwater Revival')
, (77, N'CГЎssia Eller')
, (78, N'Def Leppard')
, (79, N'Dennis Chambers')
, (80, N'Djavan')
, (81, N'Eric Clapton')
, (82, N'Faith No More')
, (83, N'Falamansa')
, (84, N'Foo Fighters')
, (85, N'Frank Sinatra')
, (86, N'Funk Como Le Gusta')
, (87, N'Godsmack')
, (88, N'Guns N'' Roses')
, (89, N'Incognito')
, (90, N'Iron Maiden')
, (91, N'James Brown')
, (92, N'Jamiroquai')
, (93, N'JET')
, (94, N'Jimi Hendrix')
, (95, N'Joe Satriani')
, (96, N'Jota Quest')
, (97, N'JoГЈo Suplicy')
, (98, N'Judas Priest')
, (99, N'LegiГЈo Urbana')
, (100, N'Lenny Kravitz')
, (101, N'Lulu Santos')
, (102, N'Marillion')
, (103, N'Marisa Monte')
, (104, N'Marvin Gaye')
, (105, N'Men At Work')
, (106, N'Motorhead')
, (107, N'Motorhead & Girlschool')
, (108, N'MГґnica Marianno')
, (109, N'Motley CrГјe')
, (110, N'Nirvana')
, (111, N'O TerГ§o')
, (112, N'Olodum')
, (113, N'Os Paralamas Do Sucesso')
, (114, N'Ozzy Osbourne')
, (115, N'Page & Plant')
, (116, N'Passengers')
, (117, N'Paul D''Ianno')
, (118, N'Pearl Jam')
, (119, N'Peter Tosh')
, (120, N'Pink Floyd')
, (121, N'Planet Hemp')
, (122, N'R.E.M. Feat. Kate Pearson')
, (123, N'R.E.M. Feat. KRS-One')
, (124, N'R.E.M.')
, (125, N'Raimundos')
, (126, N'Raul Seixas')
, (127, N'Red Hot Chili Peppers')
, (128, N'Rush')
, (129, N'Simply Red')
, (130, N'Skank')
, (131, N'Smashing Pumpkins')
, (132, N'Soundgarden')
, (133, N'Stevie Ray Vaughan & Double Trouble')
, (134, N'Stone Temple Pilots')
, (135, N'System Of A Down')
, (136, N'Terry Bozzio, Tony Levin & Steve Stevens')
, (137, N'The Black Crowes')
, (138, N'The Clash')
, (139, N'The Cult')
, (140, N'The Doors')
, (141, N'The Police')
, (142, N'The Rolling Stones')
, (143, N'The Tea Party')
, (144, N'The Who')
, (145, N'Tim Maia')
, (146, N'TitГЈs')
, (147, N'Battlestar Galactica')
, (148, N'Heroes')
, (149, N'Lost')
, (150, N'U2')
, (151, N'UB40')
, (152, N'Van Halen')
, (153, N'Velvet Revolver')
, (154, N'Whitesnake')
, (155, N'Zeca Pagodinho')
, (156, N'The Office')
, (157, N'Dread Zeppelin')
, (158, N'Battlestar Galactica (Classic)')
, (159, N'Aquaman')
, (160, N'Christina Aguilera featuring BigElf')
, (161, N'Aerosmith & Sierra Leone''s Refugee Allstars')
, (162, N'Los Lonely Boys')
, (163, N'Corinne Bailey Rae')
, (164, N'Dhani Harrison & Jakob Dylan')
, (165, N'Jackson Browne')
, (166, N'Avril Lavigne')
, (167, N'Big & Rich')
, (168, N'Youssou N''Dour')
, (169, N'Black Eyed Peas')
, (170, N'Jack Johnson')
, (171, N'Ben Harper')
, (172, N'Snow Patrol')
, (173, N'Matisyahu')
, (174, N'The Postal Service')
, (175, N'Jaguares')
, (176, N'The Flaming Lips')
, (177, N'Jack''s Mannequin & Mick Fleetwood')
, (178, N'Regina Spektor')
, (179, N'Scorpions')
, (180, N'House Of Pain')
, (181, N'Xis')
, (182, N'Nega Gizza')
, (183, N'Gustavo & Andres Veiga & Salazar')
, (184, N'Rodox')
, (185, N'Charlie Brown Jr.')
, (186, N'Pedro LuГ­s E A Parede')
, (187, N'Los Hermanos')
, (188, N'Mundo Livre S/A')
, (189, N'Otto')
, (190, N'Instituto')
, (191, N'NaГ§ГЈo Zumbi')
, (192, N'DJ Dolores & Orchestra Santa Massa')
, (193, N'Seu Jorge')
, (194, N'Sabotage E Instituto')
, (195, N'Stereo Maracana')
, (196, N'Cake')
, (197, N'Aisha Duo')
, (198, N'Habib KoitГ© and Bamada')
, (199, N'Karsh Kale')
, (200, N'The Posies')
, (201, N'Luciana Souza/Romero Lubambo')
, (202, N'Aaron Goldberg')
, (203, N'Nicolaus Esterhazy Sinfonia')
, (204, N'Temple of the Dog')
, (205, N'Chris Cornell')
, (206, N'Alberto Turco & Nova Schola Gregoriana')
, (207, N'Richard Marlow & The Choir of Trinity College, Cambridge')
, (208, N'English Concert & Trevor Pinnock')
, (209, N'Anne-Sophie Mutter, Herbert Von Karajan & Wiener Philharmoniker')
, (210, N'Hilary Hahn, Jeffrey Kahane, Los Angeles Chamber Orchestra & Margaret Batjer')
, (211, N'Wilhelm Kempff')
, (212, N'Yo-Yo Ma')
, (213, N'Scholars Baroque Ensemble')
, (214, N'Academy of St. Martin in the Fields & Sir Neville Marriner')
, (215, N'Academy of St. Martin in the Fields Chamber Ensemble & Sir Neville Marriner')
, (216, N'Berliner Philharmoniker, Claudio Abbado & Sabine Meyer')
, (217, N'Royal Philharmonic Orchestra & Sir Thomas Beecham')
, (218, N'Orchestre RГ©volutionnaire et Romantique & John Eliot Gardiner')
, (219, N'Britten Sinfonia, Ivor Bolton & Lesley Garrett')
, (220, N'Chicago Symphony Chorus, Chicago Symphony Orchestra & Sir Georg Solti')
, (221, N'Sir Georg Solti & Wiener Philharmoniker')
, (222, N'Academy of St. Martin in the Fields, John Birch, Sir Neville Marriner & Sylvia McNair')
, (223, N'London Symphony Orchestra & Sir Charles Mackerras')
, (224, N'Barry Wordsworth & BBC Concert Orchestra')
, (225, N'Herbert Von Karajan, Mirella Freni & Wiener Philharmoniker')
, (226, N'Eugene Ormandy')
, (227, N'Luciano Pavarotti')
, (228, N'Leonard Bernstein & New York Philharmonic')
, (229, N'Boston Symphony Orchestra & Seiji Ozawa')
, (230, N'Aaron Copland & London Symphony Orchestra')
, (231, N'Ton Koopman')
, (232, N'Sergei Prokofiev & Yuri Temirkanov')
, (233, N'Chicago Symphony Orchestra & Fritz Reiner')
, (234, N'Orchestra of The Age of Enlightenment')
, (235, N'Emanuel Ax, Eugene Ormandy & Philadelphia Orchestra')
, (236, N'James Levine')
, (237, N'Berliner Philharmoniker & Hans Rosbaud')
, (238, N'Maurizio Pollini')
, (239, N'Academy of St. Martin in the Fields, Sir Neville Marriner & William Bennett')
, (240, N'Gustav Mahler')
, (241, N'Felix Schmidt, London Symphony Orchestra & Rafael FrГјhbeck de Burgos')
, (242, N'Edo de Waart & San Francisco Symphony')
, (243, N'Antal DorГЎti & London Symphony Orchestra')
, (244, N'Choir Of Westminster Abbey & Simon Preston')
, (245, N'Michael Tilson Thomas & San Francisco Symphony')
, (246, N'Chor der Wiener Staatsoper, Herbert Von Karajan & Wiener Philharmoniker')
, (247, N'The King''s Singers')
, (248, N'Berliner Philharmoniker & Herbert Von Karajan')
, (249, N'Sir Georg Solti, Sumi Jo & Wiener Philharmoniker')
, (250, N'Christopher O''Riley')
, (251, N'Fretwork')
, (252, N'Amy Winehouse')
, (253, N'Calexico')
, (254, N'Otto Klemperer & Philharmonia Orchestra')
, (255, N'Yehudi Menuhin')
, (256, N'Philharmonia Orchestra & Sir Neville Marriner')
, (257, N'Academy of St. Martin in the Fields, Sir Neville Marriner & Thurston Dart')
, (258, N'Les Arts Florissants & William Christie')
, (259, N'The 12 Cellists of The Berlin Philharmonic')
, (260, N'Adrian Leaper & Doreen de Feis')
, (261, N'Roger Norrington, London Classical Players')
, (262, N'Charles Dutoit & L''Orchestre Symphonique de MontrГ©al')
, (263, N'Equale Brass Ensemble, John Eliot Gardiner & Munich Monteverdi Orchestra and Choir')
, (264, N'Kent Nagano and Orchestre de l''OpГ©ra de Lyon')
, (265, N'Julian Bream')
, (266, N'Martin Roscoe')
, (267, N'Goteborgs Symfoniker & Neeme JГ¤rvi')
, (268, N'Itzhak Perlman')
, (269, N'Michele Campanella')
, (270, N'Gerald Moore')
, (271, N'Mela Tenenbaum, Pro Musica Prague & Richard Kapp')
, (272, N'Emerson String Quartet')
, (273, N'C. Monteverdi, Nigel Rogers - Chiaroscuro; London Baroque; London Cornett & Sackbu')
, (274, N'Nash Ensemble')
, (275, N'Philip Glass Ensemble');
GO

INSERT INTO ind.genres(id, name) VALUES
(1, 'Rock')
, (2, 'Jazz')
, (3, 'Metal')
, (4, 'Alternative & Punk')
, (5, 'Rock And Roll')
, (6, 'Blues')
, (7, 'Latin')
, (8, 'Reggae')
, (9, 'Pop')
, (10, 'Soundtrack')
, (11, 'Bossa Nova')
, (12, 'Easy Listening')
, (13, 'Heavy Metal')
, (14, 'R&B/Soul')
, (15, 'Electronica/Dance')
, (16, 'World')
, (17, 'Hip Hop/Rap')
, (18, 'Science Fiction')
, (19, 'TV Shows')
, (20, 'Sci Fi & Fantasy')
, (21, 'Drama')
, (22, 'Comedy')
, (23, 'Alternative')
, (24, 'Classical')
, (25, 'Opera')

INSERT ind.summary_genres
SELECT id, 0, 0 FROM ind.genres;
GO

INSERT INTO ind.albums(id_album, title, author) VALUES
(1, N'For Those About To Rock We Salute You', 1)
, (2, N'Balls to the Wall', 2)
, (3, N'Restless and Wild', 2)
, (4, N'Let There Be Rock', 1)
, (5, N'Big Ones', 3)
, (6, N'Jagged Little Pill', 4)
, (7, N'Facelift', 5)
, (8, N'Warner 25 Anos', 6)
, (9, N'Plays Metallica By Four Cellos', 7)
, (10, N'Audioslave', 8)
, (11, N'Out Of Exile', 8)
, (12, N'BackBeat Soundtrack', 9)
, (13, N'The Best Of Billy Cobham', 10)
, (14, N'Alcohol Fueled Brewtality Live! [Disc 1]', 11)
, (15, N'Alcohol Fueled Brewtality Live! [Disc 2]', 11)
, (16, N'Black Sabbath', 12)
, (17, N'Black Sabbath Vol. 4 (Remaster)', 12)
, (18, N'Body Count', 13)
, (19, N'Chemical Wedding', 14)
, (20, N'The Best Of Buddy Guy - The Millenium Collection', 15)
, (21, N'Prenda Minha',  16)
, (22, N'Sozinho Remix Ao Vivo', 16)
, (23, N'Minha Historia', 17)
, (24, N'Afrociberdelia', 18)
, (25, N'Da Lama Ao Caos', 18)
, (26, N'AcГєstico MTV [Live]', 19)
, (27, N'Cidade Negra - Hits', 19)
, (28, N'Na Pista', 20)
, (29, N'AxГ© Bahia 2001', 21)
, (30, N'BBC Sessions [Disc 1] [Live]', 22)
, (31, N'Bongo Fury', 23)
, (32, N'Carnaval 2001', 21)
, (33, N'Chill: Brazil (Disc 1)', 24)
, (34, N'Chill: Brazil (Disc 2)', 6)
, (35, N'Garage Inc. (Disc 1)', 50)
, (36, N'Greatest Hits II', 51)
, (37, N'Greatest Kiss', 52)
, (38, N'Heart of the Night', 53)
, (39, N'International Superhits', 54)
, (40, N'Into The Light', 55)
, (41, N'Meus Momentos', 56)
, (42, N'Minha HistГіria', 57)
, (43, N'MK III The Final Concerts [Disc 1]', 58)
, (44, N'Physical Graffiti [Disc 1]', 22)
, (45, N'Sambas De Enredo 2001', 21)
, (46, N'Supernatural', 59)
, (47, N'The Best of Ed Motta', 37)
, (48, N'The Essential Miles Davis [Disc 1]', 68)
, (49, N'The Essential Miles Davis [Disc 2]', 68)
, (50, N'The Final Concerts (Disc 2)', 58)
, (51, N'Up An'' Atom', 69)
, (52, N'VinГ­cius De Moraes - Sem Limite', 70)
, (53, N'Vozes do MPB', 21)
, (54, N'Chronicle, Vol. 1', 76)
, (55, N'Chronicle, Vol. 2', 76)
, (56, N'CГЎssia Eller - ColeГ§ГЈo Sem Limite [Disc 2]', 77)
, (57, N'CГЎssia Eller - Sem Limite [Disc 1]', 77)
, (58, N'Come Taste The Band', 58)
, (59, N'Deep Purple In Rock', 58)
, (60, N'Fireball', 58)
, (61, N'Knocking at Your Back Door: The Best Of Deep Purple in the 80''s', 58)
, (62, N'Machine Head', 58)
, (63, N'Purpendicular', 58)
, (64, N'Slaves And Masters', 58)
, (65, N'Stormbringer', 58)
, (66, N'The Battle Rages On', 58)
, (67, N'Vault: Def Leppard''s Greatest Hits', 78)
, (68, N'Outbreak',  79)
, (69, N'Djavan Ao Vivo - Vol. 02', 80)
, (70, N'Djavan Ao Vivo - Vol. 1', 80)
, (71, N'Elis Regina-Minha HistГіria', 41)
, (72, N'The Cream Of Clapton', 81)
, (73, N'Unplugged', 81)
, (74, N'Album Of The Year', 82)
, (75, N'Angel Dust', 82)
, (76, N'King For A Day Fool For A Lifetime', 82)
, (77, N'The Real Thing', 82)
, (78, N'Deixa Entrar', 83)
, (79, N'In Your Honor [Disc 1]', 84)
, (80, N'In Your Honor [Disc 2]', 84)
, (81, N'One By One', 84)
, (82, N'The Colour And The Shape', 84)
, (83, N'My Way: The Best Of Frank Sinatra [Disc 1]', 85)
, (84, N'Roda De Funk', 86)
, (85, N'As CanГ§Гµes de Eu Tu Eles', 27)
, (86, N'Quanta Gente Veio Ver (Live)', 27)
, (87, N'Quanta Gente Veio ver--BГґnus De Carnaval', 27)
, (88, N'Faceless', 87)
, (89, N'American Idiot', 54)
, (90, N'Appetite for Destruction', 88)
, (91, N'Use Your Illusion I', 88)
, (92, N'Use Your Illusion II', 88)
, (93, N'Blue Moods', 89)
, (94, N'A Matter of Life and Death', 90)
, (95, N'A Real Dead One', 90)
, (96, N'A Real Live One', 90)
, (97, N'Brave New World', 90)
, (98, N'Dance Of Death', 90)
, (99, N'Fear Of The Dark', 90)
, (100, N'Iron Maiden', 90)
, (101, N'Killers', 90)
, (102, N'Live After Death', 90)
, (103, N'Live At Donington 1992 (Disc 1)', 90)
, (104, N'Live At Donington 1992 (Disc 2)', 90)
, (105, N'No Prayer For The Dying', 90)
, (106, N'Piece Of Mind', 90)
, (107, N'Powerslave', 90)
, (108, N'Rock In Rio [CD1]', 90)
, (109, N'Rock In Rio [CD2]', 90)
, (110, N'Seventh Son of a Seventh Son', 90)
, (111, N'Somewhere in Time', 90)
, (112, N'The Number of The Beast', 90)
, (113, N'The X Factor', 90)
, (114, N'Virtual XI', 90)
, (115, N'Sex Machine', 91)
, (116, N'Emergency On Planet Earth', 92)
, (117, N'Synkronized', 92)
, (118, N'The Return Of The Space Cowboy', 92)
, (119, N'Get Born', 93)
, (120, N'Are You Experienced?', 94)
, (121, N'Surfing with the Alien (Remastered)', 95)
, (122, N'Jorge Ben Jor 25 Anos', 46)
, (123, N'Jota Quest-1995', 96)
, (124, N'Cafezinho', 97)
, (125, N'Living After Midnight', 98)
, (126, N'Unplugged [Live]', 52)
, (127, N'BBC Sessions [Disc 2] [Live]', 22)
, (128, N'Coda', 22)
, (129, N'Houses Of The Holy', 22)
, (130, N'In Through The Out Door', 22)
, (131, N'IV', 22)
, (132, N'Led Zeppelin I', 22)
, (133, N'Led Zeppelin II', 22)
, (134, N'Led Zeppelin III', 22)
, (135, N'Physical Graffiti [Disc 2]', 22)
, (136, N'Presence', 22)
, (137, N'The Song Remains The Same (Disc 1)', 22)
, (138, N'The Song Remains The Same (Disc 2)', 22)
, (139, N'A TempestadeTempestade Ou O Livro Dos Dias', 99)
, (140, N'Mais Do Mesmo', 99)
, (141, N'Greatest Hits', 100)
, (142, N'Lulu Santos - RCA 100 Anos De MГєsica - ГЃlbum 01', 101)
, (143, N'Lulu Santos - RCA 100 Anos De MГєsica - ГЃlbum 02', 101)
, (144, N'Misplaced Childhood', 102)
, (145, N'Barulhinho Bom', 103)
, (146, N'Seek And Shall Find: More Of The Best (1963-1981)', 104)
, (147, N'The Best Of Men At Work', 105)
, (148, N'Black Album', 50)
, (149, N'Garage Inc. (Disc 2)', 50)
, (150, N'Kill ''Em All', 50)
, (151, N'Load', 50)
, (152, N'Master Of Puppets', 50)
, (153, N'ReLoad', 50)
, (154, N'Ride The Lightning', 50)
, (155, N'St. Anger', 50)
, (156, N'...And Justice For All', 50)
, (157, N'Miles Ahead', 68)
, (158, N'Milton Nascimento Ao Vivo', 42)
, (159, N'Minas', 42)
, (160, N'Ace Of Spades', 106)
, (161, N'Demorou...', 108)
, (162, N'Motley Crue Greatest Hits', 109)
, (163, N'From The Muddy Banks Of The Wishkah [Live]', 110)
, (164, N'Nevermind', 110)
, (165, N'Compositores', 111)
, (166, N'Olodum', 112)
, (167, N'AcГєstico MTV', 113)
, (168, N'Arquivo II', 113)
, (169, N'Arquivo Os Paralamas Do Sucesso', 113)
, (170, N'Bark at the Moon (Remastered)', 114)
, (171, N'Blizzard of Ozz', 114)
, (172, N'Diary of a Madman (Remastered)', 114)
, (173, N'No More Tears (Remastered)', 114)
, (174, N'Tribute', 114)
, (175, N'Walking Into Clarksdale', 115)
, (176, N'Original Soundtracks 1', 116)
, (177, N'The Beast Live', 117)
, (178, N'Live On Two Legs [Live]', 118)
, (179, N'Pearl Jam', 118)
, (180, N'Riot Act', 118)
, (181, N'Ten', 118)
, (182, N'Vs.', 118)
, (183, N'Dark Side Of The Moon', 120)
, (184, N'Os CГЈes Ladram Mas A Caravana NГЈo PГЎra', 121)
, (185, N'Greatest Hits I', 51)
, (186, N'News Of The World', 51)
, (187, N'Out Of Time', 122)
, (188, N'Green', 124)
, (189, N'New Adventures In Hi-Fi', 124)
, (190, N'The Best Of R.E.M.: The IRS Years', 124)
, (191, N'Cesta BГЎsica', 125)
, (192, N'Raul Seixas', 126)
, (193, N'Blood Sugar Sex Magik', 127)
, (194, N'By The Way', 127)
, (195, N'Californication', 127)
, (196, N'Retrospective I (1974-1980)', 128)
, (197, N'Santana - As Years Go By', 59)
, (198, N'Santana Live', 59)
, (199, N'Maquinarama', 130)
, (200, N'O Samba PoconГ©', 130)
, (201, N'Judas 0: B-Sides and Rarities', 131)
, (202, N'Rotten Apples: Greatest Hits', 131)
, (203, N'A-Sides', 132)
, (204, N'Morning Dance', 53)
, (205, N'In Step', 133)
, (206, N'Core', 134)
, (207, N'Mezmerize', 135)
, (208, N'[1997] Black Light Syndrome', 136)
, (209, N'Live [Disc 1]', 137)
, (210, N'Live [Disc 2]', 137)
, (211, N'The Singles', 138)
, (212, N'Beyond Good And Evil', 139)
, (213, N'Pure Cult: The Best Of The Cult (For Rockers, Ravers, Lovers & Sinners) [UK]', 139)
, (214, N'The Doors', 140)
, (215, N'The Police Greatest Hits', 141)
, (216, N'Hot Rocks, 1964-1971 (Disc 1)', 142)
, (217, N'No Security', 142)
, (218, N'Voodoo Lounge', 142)
, (219, N'Tangents', 143)
, (220, N'Transmission', 143)
, (221, N'My Generation - The Very Best Of The Who', 144)
, (222, N'Serie Sem Limite (Disc 1)', 145)
, (223, N'Serie Sem Limite (Disc 2)', 145)
, (224, N'AcГєstico', 146)
, (225, N'Volume Dois', 146)
, (226, N'Battlestar Galactica: The Story So Far', 147)
, (227, N'Battlestar Galactica, Season 3', 147)
, (228, N'Heroes, Season 1', 148)
, (229, N'Lost, Season 3', 149)
, (230, N'Lost, Season 1', 149)
, (231, N'Lost, Season 2', 149)
, (232, N'Achtung Baby', 150)
, (233, N'All That You Can''t Leave Behind', 150)
, (234, N'B-Sides 1980-1990', 150)
, (235, N'How To Dismantle An Atomic Bomb', 150)
, (236, N'Pop', 150)
, (237, N'Rattle And Hum', 150)
, (238, N'The Best Of 1980-1990', 150)
, (239, N'War', 150)
, (240, N'Zooropa', 150)
, (241, N'UB40 The Best Of - Volume Two [UK]', 151)
, (242, N'Diver Down', 152)
, (243, N'The Best Of Van Halen, Vol. I', 152)
, (244, N'Van Halen', 152)
, (245, N'Van Halen III', 152)
, (246, N'Contraband', 153)
, (247, N'Vinicius De Moraes', 72)
, (248, N'Ao Vivo [IMPORT]', 155)
, (249, N'The Office, Season 1', 156)
, (250, N'The Office, Season 2', 156)
, (251, N'The Office, Season 3', 156)
, (252, N'Un-Led-Ed', 157)
, (253, N'Battlestar Galactica (Classic), Season 1', 158)
, (254, N'Aquaman', 159)
, (255, N'Instant Karma: The Amnesty International Campaign to Save Darfur', 150)
, (256, N'Speak of the Devil', 114)
, (257, N'20th Century Masters - The Millennium Collection: The Best of Scorpions', 179)
, (258, N'House of Pain', 180)
, (259, N'Radio Brasil (O Som da Jovem Vanguarda) - Seleccao de Henrique Amaro', 36)
, (260, N'Cake: B-Sides and Rarities', 196)
, (261, N'LOST, Season 4', 149)
, (262, N'Quiet Songs', 197)
, (263, N'Muso Ko', 198)
, (264, N'Realize', 199)
, (265, N'Every Kind of Light', 200)
, (266, N'Duos II', 201)
, (267, N'Worlds', 202)
, (268, N'The Best of Beethoven', 203)
, (269, N'Temple of the Dog', 204)
, (270, N'Carry On', 205)
, (271, N'Revelations', 8)
, (272, N'Adorate Deum: Gregorian Chant from the Proper of the Mass', 206)
, (273, N'Allegri: Miserere', 207)
, (274, N'Pachelbel: Canon & Gigue', 208)
, (275, N'Vivaldi: The Four Seasons', 209)
, (276, N'Bach: Violin Concertos', 210)
, (277, N'Bach: Goldberg Variations', 211)
, (278, N'Bach: The Cello Suites', 212)
, (279, N'Handel: The Messiah (Highlights)', 213)
, (280, N'The World of Classical Favourites', 214)
, (281, N'Sir Neville Marriner: A Celebration', 215)
, (282, N'Mozart: Wind Concertos', 216)
, (283, N'Haydn: Symphonies 99 - 104', 217)
, (284, N'Beethoven: Symhonies Nos. 5 & 6', 218)
, (285, N'A Soprano Inspired', 219)
, (286, N'Great Opera Choruses', 220)
, (287, N'Wagner: Favourite Overtures', 221)
, (288, N'FaurГ©: Requiem, Ravel: Pavane & Others', 222)
, (289, N'Tchaikovsky: The Nutcracker', 223)
, (290, N'The Last Night of the Proms', 224)
, (291, N'Puccini: Madama Butterfly - Highlights', 225)
, (292, N'Holst: The Planets, Op. 32 & Vaughan Williams: Fantasies', 226)
, (293, N'Pavarotti''s Opera Made Easy', 227)
, (294, N'Great Performances - Barber''s Adagio and Other Romantic Favorites for Strings', 228)
, (295, N'Carmina Burana', 229)
, (296, N'A Copland Celebration, Vol. I', 230)
, (297, N'Bach: Toccata & Fugue in D Minor', 231)
, (298, N'Prokofiev: Symphony No.1', 232)
, (299, N'Scheherazade', 233)
, (300, N'Bach: The Brandenburg Concertos', 234)
, (301, N'Chopin: Piano Concertos Nos. 1 & 2', 235)
, (302, N'Mascagni: Cavalleria Rusticana', 236)
, (303, N'Sibelius: Finlandia', 237)
, (304, N'Beethoven Piano Sonatas: Moonlight & Pastorale', 238)
, (305, N'Great Recordings of the Century - Mahler: Das Lied von der Erde', 240)
, (306, N'Elgar: Cello Concerto & Vaughan Williams: Fantasias', 241)
, (307, N'Adams, John: The Chairman Dances', 242)
, (308, N'Tchaikovsky: 1812 Festival Overture, Op.49, Capriccio Italien & Beethoven: Wellington''s Victory', 243)
, (309, N'Palestrina: Missa Papae Marcelli & Allegri: Miserere', 244)
, (310, N'Prokofiev: Romeo & Juliet', 245)
, (311, N'Strauss: Waltzes', 226)
, (312, N'Berlioz: Symphonie Fantastique', 245)
, (313, N'Bizet: Carmen Highlights', 246)
, (314, N'English Renaissance', 247)
, (315, N'Handel: Music for the Royal Fireworks (Original Version 1749)', 208)
, (316, N'Grieg: Peer Gynt Suites & Sibelius: PellГ©as et MГ©lisande', 248)
, (317, N'Mozart Gala: Famous Arias', 249)
, (318, N'SCRIABIN: Vers la flamme', 250)
, (319, N'Armada: Music from the Courts of England and Spain', 251)
, (320, N'Mozart: Symphonies Nos. 40 & 41', 248)
, (321, N'Back to Black', 252)
, (322, N'Frank', 252)
, (323, N'Carried to Dust (Bonus Track Version)', 253)
, (324, N'Beethoven: Symphony No. 6 ''Pastoral'' Etc.', 254)
, (325, N'Bartok: Violin & Viola Concertos', 255)
, (326, N'Mendelssohn: A Midsummer Night''s Dream', 256)
, (327, N'Bach: Orchestral Suites Nos. 1 - 4', 257)
, (328, N'Charpentier: Divertissements, Airs & Concerts', 258)
, (329, N'South American Getaway', 259)
, (330, N'GГіrecki: Symphony No. 3', 260)
, (331, N'Purcell: The Fairy Queen', 261)
, (332, N'The Ultimate Relexation Album', 262)
, (333, N'Purcell: Music for the Queen Mary', 263)
, (334, N'Weill: The Seven Deadly Sins', 264)
, (335, N'J.S. Bach: Chaconne, Suite in E Minor, Partita in E Major & Prelude, Fugue and Allegro', 265)
, (336, N'Prokofiev: Symphony No.5 & Stravinksy: Le Sacre Du Printemps', 248)
, (337, N'Szymanowski: Piano Works, Vol. 1', 266)
, (338, N'Nielsen: The Six Symphonies', 267)
, (339, N'Great Recordings of the Century: Paganini''s 24 Caprices', 268)
, (340, N'Liszt - 12 Г‰tudes D''Execution Transcendante', 269)
, (341, N'Great Recordings of the Century - Shubert: Schwanengesang, 4 Lieder', 270)
, (342, N'Locatelli: Concertos for Violin, Strings and Continuo, Vol. 3', 271)
, (343, N'Respighi:Pines of Rome', 226)
, (344, N'Schubert: The Late String Quartets & String Quintet (3 CD''s)', 272)
, (345, N'Monteverdi: L''Orfeo', 273)
, (346, N'Mozart: Chamber Music', 274)
, (347, N'Koyaanisqatsi (Soundtrack from the Motion Picture)', 275);
GO

INSERT INTO ind.tracks(id, title, album, genre, duration, bytes, cost)
    SELECT 1, N'For Those About To Rock (We Salute You)', 1, 1, 343719, 11170334, 101
UNION ALL
    SELECT 2, N'Balls to the Wall', 2, 1, 342562, 5510424, 161
UNION ALL
    SELECT 3, N'Fast As a Shark', 3, 1, 230619, 3990994, 96
UNION ALL
    SELECT 4, N'Restless and Wild', 3, 1, 252051, 4331779, 133
UNION ALL
    SELECT 5, N'Princess of the Dawn', 3, 1, 375418, 6290521, 172
UNION ALL
    SELECT 6, N'Put The Finger On You', 1, 1, 205662, 6713451, 136
UNION ALL
    SELECT 7, N'Let''s Get It Up', 1, 1, 233926, 7636561, 58
UNION ALL
    SELECT 8, N'Inject The Venom', 1, 1, 210834, 6852860, 169
UNION ALL
    SELECT 9, N'Snowballed', 1, 1, 203102, 6599424, 89
UNION ALL
    SELECT 10, N'Evil Walks', 1, 1, 263497, 8611245, 122
UNION ALL
    SELECT 11, N'C.O.D.', 1, 1, 199836, 6566314, 160
UNION ALL
    SELECT 12, N'Breaking The Rules', 1, 1, 263288, 8596840, 161
UNION ALL
    SELECT 13, N'Night Of The Long Knives', 1, 1, 205688, 6706347, 76
UNION ALL
    SELECT 14, N'Spellbound', 1, 1, 270863, 8817038, 83
UNION ALL
    SELECT 15, N'Go Down', 4, 1, 331180, 10847611, 184
UNION ALL
    SELECT 16, N'Dog Eat Dog', 4, 1, 215196, 7032162, 88
UNION ALL
    SELECT 17, N'Let There Be Rock', 4, 1, 366654, 12021261, 61
UNION ALL
    SELECT 18, N'Bad Boy Boogie', 4, 1, 267728, 8776140, 139
UNION ALL
    SELECT 19, N'Problem Child', 4, 1, 325041, 10617116, 100
UNION ALL
    SELECT 20, N'Overdose', 4, 1, 369319, 12066294, 50
UNION ALL
    SELECT 21, N'Hell Ain''t A Bad Place To Be', 4, 1, 254380, 8331286, 141
UNION ALL
    SELECT 22, N'Whole Lotta Rosie', 4, 1, 323761, 10547154, 186
UNION ALL
    SELECT 23, N'Walk On Water', 5, 1, 295680, 9719579, 160
UNION ALL
    SELECT 24, N'Love In An Elevator', 5, 1, 321828, 10552051, 200
UNION ALL
    SELECT 25, N'Rag Doll', 5, 1, 264698, 8675345, 123
UNION ALL
    SELECT 26, N'What It Takes', 5, 1, 310622, 10144730, 65
UNION ALL
    SELECT 27, N'Dude (Looks Like A Lady)', 5, 1, 264855, 8679940, 176
UNION ALL
    SELECT 28, N'Janie''s Got A Gun', 5, 1, 330736, 10869391, 136
UNION ALL
    SELECT 29, N'Cryin''', 5, 1, 309263, 10056995, 128
UNION ALL
    SELECT 30, N'Amazing', 5, 1, 356519, 11616195, 69
UNION ALL
    SELECT 31, N'Blind Man', 5, 1, 240718, 7877453, 148
UNION ALL
    SELECT 32, N'Deuces Are Wild', 5, 1, 215875, 7074167, 104
UNION ALL
    SELECT 33, N'The Other Side', 5, 1, 244375, 7983270, 148
UNION ALL
    SELECT 34, N'Crazy', 5, 1, 316656, 10402398, 64
UNION ALL
    SELECT 35, N'Eat The Rich', 5, 1, 251036, 8262039, 166
UNION ALL
    SELECT 36, N'Angel', 5, 1, 307617, 9989331, 194
UNION ALL
    SELECT 37, N'Livin'' On The Edge', 5, 1, 381231, 12374569, 98
UNION ALL
    SELECT 38, N'All I Really Want', 6, 1, 284891, 9375567, 164
UNION ALL
    SELECT 39, N'You Oughta Know', 6, 1, 249234, 8196916, 129
UNION ALL
    SELECT 40, N'Perfect', 6, 1, 188133, 6145404, 74
UNION ALL
    SELECT 41, N'Hand In My Pocket', 6, 1, 221570, 7224246, 148
UNION ALL
    SELECT 42, N'Right Through You', 6, 1, 176117, 5793082, 182
UNION ALL
    SELECT 43, N'Forgiven', 6, 1, 300355, 9753256, 179
UNION ALL
    SELECT 44, N'You Learn', 6, 1, 239699, 7824837, 195
UNION ALL
    SELECT 45, N'Head Over Feet', 6, 1, 267493, 8758008, 175
UNION ALL
    SELECT 46, N'Mary Jane', 6, 1, 280607, 9163588, 83
UNION ALL
    SELECT 47, N'Ironic', 6, 1, 229825, 7598866, 96
UNION ALL
    SELECT 48, N'Not The Doctor', 6, 1, 227631, 7604601, 178
UNION ALL
    SELECT 49, N'Wake Up', 6, 1, 293485, 9703359, 125
UNION ALL
    SELECT 50, N'You Oughta Know (Alternate)', 6, 1, 491885, 16008629, 197
UNION ALL
    SELECT 51, N'We Die Young', 7, 1, 152084, 4925362, 153
UNION ALL
    SELECT 52, N'Man In The Box', 7, 1, 286641, 9310272, 170
UNION ALL
    SELECT 53, N'Sea Of Sorrow', 7, 1, 349831, 11316328, 73
UNION ALL
    SELECT 54, N'Bleed The Freak', 7, 1, 241946, 7847716, 116
UNION ALL
    SELECT 55, N'I Can''t Remember', 7, 1, 222955, 7302550, 158
UNION ALL
    SELECT 56, N'Love, Hate, Love', 7, 1, 387134, 12575396, 160
UNION ALL
    SELECT 57, N'It Ain''t Like That', 7, 1, 277577, 8993793, 95
UNION ALL
    SELECT 58, N'Sunshine', 7, 1, 284969, 9216057, 150
UNION ALL
    SELECT 59, N'Put You Down', 7, 1, 196231, 6420530, 169
UNION ALL
    SELECT 60, N'Confusion', 7, 1, 344163, 11183647, 116
UNION ALL
    SELECT 61, N'I Know Somethin (Bout You)', 7, 1, 261955, 8497788, 183
UNION ALL
    SELECT 62, N'Real Thing', 7, 1, 243879, 7937731, 144
UNION ALL
    SELECT 63, N'Desafinado', 8, 2, 185338, 5990473, 124
UNION ALL
    SELECT 64, N'Garota De Ipanema', 8, 2, 285048, 9348428, 74
UNION ALL
    SELECT 65, N'Samba De Uma Nota SГі (One Note Samba)', 8, 2, 137273, 4535401, 149
UNION ALL
    SELECT 66, N'Por Causa De VocГЄ', 8, 2, 169900, 5536496, 195
UNION ALL
    SELECT 67, N'Ligia', 8, 2, 251977, 8226934, 75
UNION ALL
    SELECT 68, N'Fotografia', 8, 2, 129227, 4198774, 180
UNION ALL
    SELECT 69, N'Dindi (Dindi)', 8, 2, 253178, 8149148, 157
UNION ALL
    SELECT 70, N'Se Todos Fossem Iguais A VocГЄ (Instrumental)', 8, 2, 134948, 4393377, 80
UNION ALL
    SELECT 71, N'Falando De Amor', 8, 2, 219663, 7121735, 110
UNION ALL
    SELECT 72, N'Angela', 8, 2, 169508, 5574957, 119
UNION ALL
    SELECT 73, N'Corcovado (Quiet Nights Of Quiet Stars)', 8, 2, 205662, 6687994, 165
UNION ALL
    SELECT 74, N'Outra Vez', 8, 2, 126511, 4110053, 75
UNION ALL
    SELECT 75, N'O Boto (BГґto)', 8, 2, 366837, 12089673, 74
UNION ALL
    SELECT 76, N'Canta, Canta Mais', 8, 2, 271856, 8719426, 108
UNION ALL
    SELECT 77, N'Enter Sandman', 9, 3, 221701, 7286305, 192
UNION ALL
    SELECT 78, N'Master Of Puppets', 9, 3, 436453, 14375310, 163
UNION ALL
    SELECT 79, N'Harvester Of Sorrow', 9, 3, 374543, 12372536, 142
UNION ALL
    SELECT 80, N'The Unforgiven', 9, 3, 322925, 10422447, 94
UNION ALL
    SELECT 81, N'Sad But True', 9, 3, 288208, 9405526, 142
UNION ALL
    SELECT 82, N'Creeping Death', 9, 3, 308035, 10110980, 56
UNION ALL
    SELECT 83, N'Wherever I May Roam', 9, 3, 369345, 12033110, 188
UNION ALL
    SELECT 84, N'Welcome Home (Sanitarium)', 9, 3, 350197, 11406431, 186
UNION ALL
    SELECT 85, N'Cochise', 10, 1, 222380, 5339931, 102
UNION ALL
    SELECT 86, N'Show Me How to Live', 10, 1, 277890, 6672176, 163
UNION ALL
    SELECT 87, N'Gasoline', 10, 1, 279457, 6709793, 107
UNION ALL
    SELECT 88, N'What You Are', 10, 1, 249391, 5988186, 188
UNION ALL
    SELECT 89, N'Like a Stone', 10, 1, 294034, 7059624, 144
UNION ALL
    SELECT 90, N'Set It Off', 10, 1, 263262, 6321091, 198
UNION ALL
    SELECT 91, N'Shadow on the Sun', 10, 1, 343457, 8245793, 124
UNION ALL
    SELECT 92, N'I am the Highway', 10, 1, 334942, 8041411, 112
UNION ALL
    SELECT 93, N'Exploder', 10, 1, 206053, 4948095, 178
UNION ALL
    SELECT 94, N'Hypnotize', 10, 1, 206628, 4961887, 152
UNION ALL
    SELECT 95, N'Bring''em Back Alive', 10, 1, 329534, 7911634, 88
UNION ALL
    SELECT 96, N'Light My Way', 10, 1, 303595, 7289084, 127
UNION ALL
    SELECT 97, N'Getaway Car', 10, 1, 299598, 7193162, 186
UNION ALL
    SELECT 98, N'The Last Remaining Light', 10, 1, 317492, 7622615, 79
UNION ALL
    SELECT 99, N'Your Time Has Come', 11, 4, 255529, 8273592, 50
UNION ALL
    SELECT 100, N'Out Of Exile', 11, 4, 291291, 9506571, 141
UNION ALL
    SELECT 101, N'Be Yourself', 11, 4, 279484, 9106160, 54
UNION ALL
    SELECT 102, N'Doesn''t Remind Me', 11, 4, 255869, 8357387, 178
UNION ALL
    SELECT 103, N'Drown Me Slowly', 11, 4, 233691, 7609178, 98
UNION ALL
    SELECT 104, N'Heaven''s Dead', 11, 4, 276688, 9006158, 129
UNION ALL
    SELECT 105, N'The Worm', 11, 4, 237714, 7710800, 167
UNION ALL
    SELECT 106, N'Man Or Animal', 11, 4, 233195, 7542942, 79
UNION ALL
    SELECT 107, N'Yesterday To Tomorrow', 11, 4, 273763, 8944205, 56
UNION ALL
    SELECT 108, N'Dandelion', 11, 4, 278125, 9003592, 52
UNION ALL
    SELECT 109, N'#1 Zero', 11, 4, 299102, 9731988, 64
UNION ALL
    SELECT 110, N'The Curse', 11, 4, 309786, 10029406, 74
UNION ALL
    SELECT 111, N'Money', 12, 5, 147591, 2365897, 95
UNION ALL
    SELECT 112, N'Long Tall Sally', 12, 5, 106396, 1707084, 61
UNION ALL
    SELECT 113, N'Bad Boy', 12, 5, 116088, 1862126, 75
UNION ALL
    SELECT 114, N'Twist And Shout', 12, 5, 161123, 2582553, 94
UNION ALL
    SELECT 115, N'Please Mr. Postman', 12, 5, 137639, 2206986, 136
UNION ALL
    SELECT 116, N'C''Mon Everybody', 12, 5, 140199, 2247846, 73
UNION ALL
    SELECT 117, N'Rock ''N'' Roll Music', 12, 5, 141923, 2276788, 157
UNION ALL
    SELECT 118, N'Slow Down', 12, 5, 163265, 2616981, 102
UNION ALL
    SELECT 119, N'Roadrunner', 12, 5, 143595, 2301989, 80
UNION ALL
    SELECT 120, N'Carol', 12, 5, 143830, 2306019, 90
UNION ALL
    SELECT 121, N'Good Golly Miss Molly', 12, 5, 106266, 1704918, 150
UNION ALL
    SELECT 122, N'20 Flight Rock', 12, 5, 107807, 1299960, 117
UNION ALL
    SELECT 123, N'Quadrant', 13, 2, 261851, 8538199, 108
UNION ALL
    SELECT 124, N'Snoopy''s search-Red baron', 13, 2, 456071, 15075616, 113
UNION ALL
    SELECT 125, N'Spanish moss-"A sound portrait"-Spanish moss', 13, 2, 248084, 8217867, 184
UNION ALL
    SELECT 126, N'Moon germs', 13, 2, 294060, 9714812, 117
UNION ALL
    SELECT 127, N'Stratus', 13, 2, 582086, 19115680, 123
UNION ALL
    SELECT 128, N'The pleasant pheasant', 13, 2, 318066, 10630578, 115
UNION ALL
    SELECT 129, N'Solo-Panhandler', 13, 2, 246151, 8230661, 64
UNION ALL
    SELECT 130, N'Do what cha wanna', 13, 2, 274155, 9018565, 163
UNION ALL
    SELECT 131, N'Intro/ Low Down', 14, 3, 323683, 10642901, 156
UNION ALL
    SELECT 132, N'13 Years Of Grief', 14, 3, 246987, 8137421, 66
UNION ALL
    SELECT 133, N'Stronger Than Death', 14, 3, 300747, 9869647, 126
UNION ALL
    SELECT 134, N'All For You', 14, 3, 235833, 7726948, 117
UNION ALL
    SELECT 135, N'Super Terrorizer', 14, 3, 319373, 10513905, 70
UNION ALL
    SELECT 136, N'Phoney Smile Fake Hellos', 14, 3, 273606, 9011701, 64
UNION ALL
    SELECT 137, N'Lost My Better Half', 14, 3, 284081, 9355309, 137
UNION ALL
    SELECT 138, N'Bored To Tears', 14, 3, 247327, 8130090, 51
UNION ALL
    SELECT 139, N'A.N.D.R.O.T.A.Z.', 14, 3, 266266, 8574746, 190
UNION ALL
    SELECT 140, N'Born To Booze', 14, 3, 282122, 9257358, 166
UNION ALL
    SELECT 141, N'World Of Trouble', 14, 3, 359157, 11820932, 114
UNION ALL
    SELECT 142, N'No More Tears', 14, 3, 555075, 18041629, 88
UNION ALL
    SELECT 143, N'The Begining... At Last', 14, 3, 365662, 11965109, 125
UNION ALL
    SELECT 144, N'Heart Of Gold', 15, 3, 194873, 6417460, 111
UNION ALL
    SELECT 145, N'Snowblind', 15, 3, 420022, 13842549, 127
UNION ALL
    SELECT 146, N'Like A Bird', 15, 3, 276532, 9115657, 181
UNION ALL
    SELECT 147, N'Blood In The Wall', 15, 3, 284368, 9359475, 171
UNION ALL
    SELECT 148, N'The Beginning...At Last', 15, 3, 271960, 8975814, 163
UNION ALL
    SELECT 149, N'Black Sabbath', 16, 3, 382066, 12440200, 125
UNION ALL
    SELECT 150, N'The Wizard', 16, 3, 264829, 8646737, 123
UNION ALL
    SELECT 151, N'Behind The Wall Of Sleep', 16, 3, 217573, 7169049, 122
UNION ALL
    SELECT 152, N'N.I.B.', 16, 3, 368770, 12029390, 183
UNION ALL
    SELECT 153, N'Evil Woman', 16, 3, 204930, 6655170, 154
UNION ALL
    SELECT 154, N'Sleeping Village', 16, 3, 644571, 21128525, 54
UNION ALL
    SELECT 155, N'Warning', 16, 3, 212062, 6893363, 103
UNION ALL
    SELECT 156, N'Wheels Of Confusion / The Straightener', 17, 3, 494524, 16065830, 51
UNION ALL
    SELECT 157, N'Tomorrow''s Dream', 17, 3, 192496, 6252071, 135
UNION ALL
    SELECT 158, N'Changes', 17, 3, 286275, 9175517, 129
UNION ALL
    SELECT 159, N'FX', 17, 3, 103157, 3331776, 85
UNION ALL
    SELECT 160, N'Supernaut', 17, 3, 285779, 9245971, 164
UNION ALL
    SELECT 161, N'Snowblind', 17, 3, 331676, 10813386, 169
UNION ALL
    SELECT 162, N'Cornucopia', 17, 3, 234814, 7653880, 96
UNION ALL
    SELECT 163, N'Laguna Sunrise', 17, 3, 173087, 5671374, 133
UNION ALL
    SELECT 164, N'St. Vitus Dance', 17, 3, 149655, 4884969, 78
UNION ALL
    SELECT 165, N'Under The Sun/Every Day Comes and Goes', 17, 3, 350458, 11360486, 137
UNION ALL
    SELECT 166, N'Smoked Pork', 18, 4, 47333, 1549074, 121
UNION ALL
    SELECT 167, N'Body Count''s In The House', 18, 4, 204251, 6715413, 112
UNION ALL
    SELECT 168, N'Now Sports', 18, 4, 4884, 161266, 95
UNION ALL
    SELECT 169, N'Body Count', 18, 4, 317936, 10489139, 111
UNION ALL
    SELECT 170, N'A Statistic', 18, 4, 6373, 211997, 118
UNION ALL
    SELECT 171, N'Bowels Of The Devil', 18, 4, 223216, 7324125, 95
UNION ALL
    SELECT 172, N'The Real Problem', 18, 4, 11650, 387360, 128
UNION ALL
    SELECT 173, N'KKK Bitch', 18, 4, 173008, 5709631, 98
UNION ALL
    SELECT 174, N'D Note', 18, 4, 95738, 3067064, 123
UNION ALL
    SELECT 175, N'Voodoo', 18, 4, 300721, 9875962, 145
UNION ALL
    SELECT 176, N'The Winner Loses', 18, 4, 392254, 12843821, 176
UNION ALL
    SELECT 177, N'There Goes The Neighborhood', 18, 4, 350171, 11443471, 155
UNION ALL
    SELECT 178, N'Oprah', 18, 4, 6635, 224313, 194
UNION ALL
    SELECT 179, N'Evil Dick', 18, 4, 239020, 7828873, 178
UNION ALL
    SELECT 180, N'Body Count Anthem', 18, 4, 166426, 5463690, 110
UNION ALL
    SELECT 181, N'Momma''s Gotta Die Tonight', 18, 4, 371539, 12122946, 197
UNION ALL
    SELECT 182, N'Freedom Of Speech', 18, 4, 281234, 9337917, 54
UNION ALL
    SELECT 183, N'King In Crimson', 19, 3, 283167, 9218499, 133
UNION ALL
    SELECT 184, N'Chemical Wedding', 19, 3, 246177, 8022764, 87
UNION ALL
    SELECT 185, N'The Tower', 19, 3, 285257, 9435693, 51
UNION ALL
    SELECT 186, N'Killing Floor', 19, 3, 269557, 8854240, 185
UNION ALL
    SELECT 187, N'Book Of Thel', 19, 3, 494393, 16034404, 124
UNION ALL
    SELECT 188, N'Gates Of Urizen', 19, 3, 265351, 8627004, 69
UNION ALL
    SELECT 189, N'Jerusalem', 19, 3, 402390, 13194463, 153
UNION ALL
    SELECT 190, N'Trupets Of Jericho', 19, 3, 359131, 11820908, 118
UNION ALL
    SELECT 191, N'Machine Men', 19, 3, 341655, 11138147, 64
UNION ALL
    SELECT 192, N'The Alchemist', 19, 3, 509413, 16545657, 96
UNION ALL
    SELECT 193, N'Realword', 19, 3, 237531, 7802095, 79
UNION ALL
    SELECT 194, N'First Time I Met The Blues', 20, 6, 140434, 4604995, 77
UNION ALL
    SELECT 195, N'Let Me Love You Baby', 20, 6, 175386, 5716994, 81
UNION ALL
    SELECT 196, N'Stone Crazy', 20, 6, 433397, 14184984, 100
UNION ALL
    SELECT 197, N'Pretty Baby', 20, 6, 237662, 7848282, 65
UNION ALL
    SELECT 198, N'When My Left Eye Jumps', 20, 6, 235311, 7685363, 100
UNION ALL
    SELECT 199, N'Leave My Girl Alone', 20, 6, 204721, 6859518, 147
UNION ALL
    SELECT 200, N'She Suits Me To A Tee', 20, 6, 136803, 4456321, 148
UNION ALL
    SELECT 201, N'Keep It To Myself (Aka Keep It To Yourself)', 20, 6, 166060, 5487056, 151
UNION ALL
    SELECT 202, N'My Time After Awhile', 20, 6, 182491, 6022698, 86
UNION ALL
    SELECT 203, N'Too Many Ways (Alternate)', 20, 6, 135053, 4459946, 139
UNION ALL
    SELECT 204, N'Talkin'' ''Bout Women Obviously', 20, 6, 589531, 19161377, 95
UNION ALL
    SELECT 205, N'Jorge Da CapadГіcia', 21, 7, 177397, 5842196, 172
UNION ALL
    SELECT 206, N'Prenda Minha', 21, 7, 99369, 3225364, 109
UNION ALL
    SELECT 207, N'MeditaГ§ГЈo', 21, 7, 148793, 4865597, 98
UNION ALL
    SELECT 208, N'Terra', 21, 7, 482429, 15889054, 116
UNION ALL
    SELECT 209, N'Eclipse Oculto', 21, 7, 221936, 7382703, 106
UNION ALL
    SELECT 210, N'Texto "Verdade Tropical"', 21, 7, 84088, 2752161, 161
UNION ALL
    SELECT 211, N'Bem Devagar', 21, 7, 133172, 4333651, 196
UNION ALL
    SELECT 212, N'DrГЈo', 21, 7, 156264, 5065932, 107
UNION ALL
    SELECT 213, N'Saudosismo', 21, 7, 144326, 4726981, 158
UNION ALL
    SELECT 214, N'Carolina', 21, 7, 181812, 5924159, 166
UNION ALL
    SELECT 215, N'Sozinho', 21, 7, 190589, 6253200, 200
UNION ALL
    SELECT 216, N'Esse Cara', 21, 7, 223111, 7217126, 182
UNION ALL
    SELECT 217, N'Mel', 21, 7, 294765, 9854062, 140
UNION ALL
    SELECT 218, N'Linha Do Equador', 21, 7, 299337, 10003747, 195
UNION ALL
    SELECT 219, N'Odara', 21, 7, 141270, 4704104, 93
UNION ALL
    SELECT 220, N'A Luz De Tieta', 21, 7, 251742, 8507446, 104
UNION ALL
    SELECT 221, N'AtrГЎs Da Verd-E-Rosa SГі NГЈo Vai Quem JГЎ Morreu', 21, 7, 307252, 10364247, 148
UNION ALL
    SELECT 222, N'Vida Boa', 21, 7, 281730, 9411272, 136
UNION ALL
    SELECT 223, N'Sozinho (Hitmakers Classic Mix)', 22, 7, 436636, 14462072, 77
UNION ALL
    SELECT 224, N'Sozinho (Hitmakers Classic Radio Edit)', 22, 7, 195004, 6455134, 68
UNION ALL
    SELECT 225, N'Sozinho (CaГЄdrum ''n'' Bass)', 22, 7, 328071, 10975007, 155
UNION ALL
    SELECT 226, N'Carolina', 23, 7, 163056, 5375395, 114
UNION ALL
    SELECT 227, N'Essa MoГ§a Ta Diferente', 23, 7, 167235, 5568574, 113
UNION ALL
    SELECT 228, N'Vai Passar', 23, 7, 369763, 12359161, 165
UNION ALL
    SELECT 229, N'Samba De Orly', 23, 7, 162429, 5431854, 69
UNION ALL
    SELECT 230, N'Bye, Bye Brasil', 23, 7, 283402, 9499590, 149
UNION ALL
    SELECT 231, N'Atras Da Porta', 23, 7, 189675, 6132843, 108
UNION ALL
    SELECT 232, N'Tatuagem', 23, 7, 172120, 5645703, 183
UNION ALL
    SELECT 233, N'O Que SerГЎ (ГЂ Flor Da Terra)', 23, 7, 167288, 5574848, 102
UNION ALL
    SELECT 234, N'Morena De Angola', 23, 7, 186801, 6373932, 156
UNION ALL
    SELECT 235, N'Apesar De VocГЄ', 23, 7, 234501, 7886937, 89
UNION ALL
    SELECT 236, N'A Banda', 23, 7, 132493, 4349539, 102
UNION ALL
    SELECT 237, N'Minha Historia', 23, 7, 182256, 6029673, 78
UNION ALL
    SELECT 238, N'Com AГ§Гєcar E Com Afeto', 23, 7, 175386, 5846442, 161
UNION ALL
    SELECT 239, N'Brejo Da Cruz', 23, 7, 214099, 7270749, 99
UNION ALL
    SELECT 240, N'Meu Caro Amigo', 23, 7, 260257, 8778172, 148
UNION ALL
    SELECT 241, N'Geni E O Zepelim', 23, 7, 317570, 10342226, 93
UNION ALL
    SELECT 242, N'Trocando Em MiГєdos', 23, 7, 169717, 5461468, 97
UNION ALL
    SELECT 243, N'Vai Trabalhar Vagabundo', 23, 7, 139154, 4693941, 158
UNION ALL
    SELECT 244, N'Gota D''ГЎgua', 23, 7, 153208, 5074189, 158
UNION ALL
    SELECT 245, N'ConstruГ§ГЈo / Deus Lhe Pague', 23, 7, 383059, 12675305, 165
UNION ALL
    SELECT 246, N'Mateus Enter', 24, 7, 33149, 1103013, 175
UNION ALL
    SELECT 247, N'O CidadГЈo Do Mundo', 24, 7, 200933, 6724966, 136
UNION ALL
    SELECT 248, N'Etnia', 24, 7, 152555, 5061413, 103
UNION ALL
    SELECT 249, N'Quilombo Groove [Instrumental]', 24, 7, 151823, 5042447, 188
UNION ALL
    SELECT 250, N'MacГґ', 24, 7, 249600, 8253934, 64
UNION ALL
    SELECT 251, N'Um Passeio No Mundo Livre', 24, 7, 240091, 7984291, 171
UNION ALL
    SELECT 252, N'Samba Do Lado', 24, 7, 227317, 7541688, 120
UNION ALL
    SELECT 253, N'Maracatu AtГґmico', 24, 7, 284264, 9670057, 101
UNION ALL
    SELECT 254, N'O Encontro De Isaac Asimov Com Santos Dumont No CГ©u', 24, 7, 99108, 3240816, 171
UNION ALL
    SELECT 255, N'Corpo De Lama', 24, 7, 232672, 7714954, 71
UNION ALL
    SELECT 256, N'Sobremesa', 24, 7, 240091, 7960868, 156
UNION ALL
    SELECT 257, N'Manguetown', 24, 7, 194560, 6475159, 113
UNION ALL
    SELECT 258, N'Um SatГ©lite Na CabeГ§a', 24, 7, 126615, 4272821, 91
UNION ALL
    SELECT 259, N'BaiГЈo Ambiental [Instrumental]', 24, 7, 152659, 5198539, 74
UNION ALL
    SELECT 260, N'Sangue De Bairro', 24, 7, 132231, 4415557, 171
UNION ALL
    SELECT 261, N'Enquanto O Mundo Explode', 24, 7, 88764, 2968650, 114
UNION ALL
    SELECT 262, N'Interlude Zumbi', 24, 7, 71627, 2408550, 161
UNION ALL
    SELECT 263, N'CrianГ§a De Domingo', 24, 7, 208222, 6984813, 105
UNION ALL
    SELECT 264, N'Amor De Muito', 24, 7, 175333, 5881293, 173
UNION ALL
    SELECT 265, N'Samidarish [Instrumental]', 24, 7, 272431, 8911641, 178
UNION ALL
    SELECT 266, N'Maracatu AtГґmico [Atomic Version]', 24, 7, 273084, 9019677, 89
UNION ALL
    SELECT 267, N'Maracatu AtГґmico [Ragga Mix]', 24, 7, 210155, 6986421, 92
UNION ALL
    SELECT 268, N'Maracatu AtГґmico [Trip Hop]', 24, 7, 221492, 7380787, 177
UNION ALL
    SELECT 269, N'Banditismo Por Uma Questa', 25, 7, 307095, 10251097, 116
UNION ALL
    SELECT 270, N'Banditismo Por Uma Questa', 25, 7, 243644, 8147224, 99
UNION ALL
    SELECT 271, N'Rios Pontes & Overdrives', 25, 7, 286720, 9659152, 55
UNION ALL
    SELECT 272, N'Cidade', 25, 7, 216346, 7241817, 92
UNION ALL
    SELECT 273, N'Praiera', 25, 7, 183640, 6172781, 68
UNION ALL
    SELECT 274, N'Samba Makossa', 25, 7, 271856, 9095410, 112
UNION ALL
    SELECT 275, N'Da Lama Ao Caos', 25, 7, 251559, 8378065, 71
UNION ALL
    SELECT 276, N'Maracatu De Tiro Certeiro', 25, 7, 88868, 2901397, 163
UNION ALL
    SELECT 277, N'Salustiano Song', 25, 7, 215405, 7183969, 140
UNION ALL
    SELECT 278, N'Antene Se', 25, 7, 248372, 8253618, 87
UNION ALL
    SELECT 279, N'Risoflora', 25, 7, 105586, 3536938, 86
UNION ALL
    SELECT 280, N'Lixo Do Mangue', 25, 7, 193253, 6534200, 130
UNION ALL
    SELECT 281, N'Computadores Fazem Arte', 25, 7, 404323, 13702771, 147
UNION ALL
    SELECT 282, N'Girassol', 26, 8, 249808, 8327676, 158
UNION ALL
    SELECT 283, N'A Sombra Da Maldade', 26, 8, 230922, 7697230, 181
UNION ALL
    SELECT 284, N'Johnny B. Goode', 26, 8, 254615, 8505985, 126
UNION ALL
    SELECT 285, N'Soldado Da Paz', 26, 8, 194220, 6455080, 160
UNION ALL
    SELECT 286, N'Firmamento', 26, 8, 222145, 7402658, 138
UNION ALL
    SELECT 287, N'Extra', 26, 8, 304352, 10078050, 185
UNION ALL
    SELECT 288, N'O ErГЄ', 26, 8, 236382, 7866924, 181
UNION ALL
    SELECT 289, N'Podes Crer', 26, 8, 232280, 7747747, 125
UNION ALL
    SELECT 290, N'A Estrada', 26, 8, 248842, 8275673, 186
UNION ALL
    SELECT 291, N'Berlim', 26, 8, 207542, 6920424, 165
UNION ALL
    SELECT 292, N'JГЎ Foi', 26, 8, 221544, 7388466, 196
UNION ALL
    SELECT 293, N'Onde VocГЄ Mora?', 26, 8, 256026, 8502588, 148
UNION ALL
    SELECT 294, N'Pensamento', 26, 8, 173008, 5748424, 192
UNION ALL
    SELECT 295, N'ConciliaГ§ГЈo', 26, 8, 257619, 8552474, 113
UNION ALL
    SELECT 296, N'Realidade Virtual', 26, 8, 195239, 6503533, 161
UNION ALL
    SELECT 297, N'Mensagem', 26, 8, 225332, 7488852, 75
UNION ALL
    SELECT 298, N'A Cor Do Sol', 26, 8, 231392, 7663348, 111
UNION ALL
    SELECT 299, N'Onde VocГЄ Mora?', 27, 8, 298396, 10056970, 140
UNION ALL
    SELECT 300, N'O ErГЄ', 27, 8, 206942, 6950332, 87
UNION ALL
    SELECT 301, N'A Sombra Da Maldade', 27, 8, 285231, 9544383, 74
UNION ALL
    SELECT 302, N'A Estrada', 27, 8, 282174, 9344477, 138
UNION ALL
    SELECT 303, N'Falar A Verdade', 27, 8, 244950, 8189093, 199
UNION ALL
    SELECT 304, N'Firmamento', 27, 8, 225488, 7507866, 193
UNION ALL
    SELECT 305, N'Pensamento', 27, 8, 192391, 6399761, 176
UNION ALL
    SELECT 306, N'Realidade Virtual', 27, 8, 240300, 8069934, 122
UNION ALL
    SELECT 307, N'Doutor', 27, 8, 178155, 5950952, 65
UNION ALL
    SELECT 308, N'Na Frente Da TV', 27, 8, 289750, 9633659, 128
UNION ALL
    SELECT 309, N'Downtown', 27, 8, 239725, 8024386, 96
UNION ALL
    SELECT 310, N'SГЎbado A Noite', 27, 8, 267363, 8895073, 162
UNION ALL
    SELECT 311, N'A Cor Do Sol', 27, 8, 273031, 9142937, 137
UNION ALL
    SELECT 312, N'Eu TambГ©m Quero Beijar', 27, 8, 211147, 7029400, 60
UNION ALL
    SELECT 313, N'Noite Do Prazer', 28, 7, 311353, 10309980, 78
UNION ALL
    SELECT 314, N'ГЂ Francesa', 28, 7, 244532, 8150846, 147
UNION ALL
    SELECT 315, N'Cada Um Cada Um (A Namoradeira)', 28, 7, 253492, 8441034, 61
UNION ALL
    SELECT 316, N'Linha Do Equador', 28, 7, 244715, 8123466, 193
UNION ALL
    SELECT 317, N'Amor Demais', 28, 7, 254040, 8420093, 67
UNION ALL
    SELECT 318, N'FГ©rias', 28, 7, 264202, 8731945, 103
UNION ALL
    SELECT 319, N'Gostava Tanto De VocГЄ', 28, 7, 230452, 7685326, 59
UNION ALL
    SELECT 320, N'Flor Do Futuro', 28, 7, 275748, 9205941, 76
UNION ALL
    SELECT 321, N'Felicidade Urgente', 28, 7, 266605, 8873358, 60
UNION ALL
    SELECT 322, N'Livre Pra Viver', 28, 7, 214595, 7111596, 58
UNION ALL
    SELECT 323, N'Dig-Dig, Lambe-Lambe (Ao Vivo)', 29, 9, 205479, 6892516, 108
UNION ALL
    SELECT 324, N'PererГЄ', 29, 9, 198661, 6643207, 56
UNION ALL
    SELECT 325, N'TriboTchan', 29, 9, 194194, 6507950, 100
UNION ALL
    SELECT 326, N'Tapa Aqui, Descobre Ali', 29, 9, 188630, 6327391, 96
UNION ALL
    SELECT 327, N'Daniela', 29, 9, 230791, 7748006, 153
UNION ALL
    SELECT 328, N'Bate Lata', 29, 9, 206733, 7034985, 196
UNION ALL
    SELECT 329, N'Garotas do Brasil', 29, 9, 210155, 6973625, 82
UNION ALL
    SELECT 330, N'Levada do Amor (Ailoviu)', 29, 9, 190093, 6457752, 91
UNION ALL
    SELECT 331, N'Lavadeira', 29, 9, 214256, 7254147, 168
UNION ALL
    SELECT 332, N'Reboladeira', 29, 9, 210599, 7027525, 55
UNION ALL
    SELECT 333, N'Г‰ que Nessa EncarnaГ§ГЈo Eu Nasci Manga', 29, 9, 196519, 6568081, 50
UNION ALL
    SELECT 334, N'Reggae Tchan', 29, 9, 206654, 6931328, 193
UNION ALL
    SELECT 335, N'My Love', 29, 9, 203493, 6772813, 73
UNION ALL
    SELECT 336, N'Latinha de Cerveja', 29, 9, 166687, 5532564, 74
UNION ALL
    SELECT 337, N'You Shook Me', 30, 1, 315951, 10249958, 88
UNION ALL
    SELECT 338, N'I Can''t Quit You Baby', 30, 1, 263836, 8581414, 65
UNION ALL
    SELECT 339, N'Communication Breakdown', 30, 1, 192653, 6287257, 195
UNION ALL
    SELECT 340, N'Dazed and Confused', 30, 1, 401920, 13035765, 147
UNION ALL
    SELECT 341, N'The Girl I Love She Got Long Black Wavy Hair', 30, 1, 183327, 5995686, 114
UNION ALL
    SELECT 342, N'What is and Should Never Be', 30, 1, 260675, 8497116, 196
UNION ALL
    SELECT 343, N'Communication Breakdown(2)', 30, 1, 161149, 5261022, 183
UNION ALL
    SELECT 344, N'Travelling Riverside Blues', 30, 1, 312032, 10232581, 128
UNION ALL
    SELECT 345, N'Whole Lotta Love', 30, 1, 373394, 12258175, 69
UNION ALL
    SELECT 346, N'Somethin'' Else', 30, 1, 127869, 4165650, 101
UNION ALL
    SELECT 347, N'Communication Breakdown(3)', 30, 1, 185260, 6041133, 77
UNION ALL
    SELECT 348, N'I Can''t Quit You Baby(2)', 30, 1, 380551, 12377615, 129
UNION ALL
    SELECT 349, N'You Shook Me(2)', 30, 1, 619467, 20138673, 79
UNION ALL
    SELECT 350, N'How Many More Times', 30, 1, 711836, 23092953, 139
UNION ALL
    SELECT 351, N'Debra Kadabra', 31, 1, 234553, 7649679, 193
UNION ALL
    SELECT 352, N'Carolina Hard-Core Ecstasy', 31, 1, 359680, 11731061, 130
UNION ALL
    SELECT 353, N'Sam With The Showing Scalp Flat Top', 31, 1, 171284, 5572993, 193
UNION ALL
    SELECT 354, N'Poofter''s Froth Wyoming Plans Ahead', 31, 1, 183902, 6007019, 186
UNION ALL
    SELECT 355, N'200 Years Old', 31, 1, 272561, 8912465, 138
UNION ALL
    SELECT 356, N'Cucamonga', 31, 1, 144483, 4728586, 177
UNION ALL
    SELECT 357, N'Advance Romance', 31, 1, 677694, 22080051, 183
UNION ALL
    SELECT 358, N'Man With The Woman Head', 31, 1, 88894, 2922044, 73
UNION ALL
    SELECT 359, N'Muffin Man', 31, 1, 332878, 10891682, 193
UNION ALL
    SELECT 360, N'Vai-Vai 2001', 32, 10, 276349, 9402241, 162
UNION ALL
    SELECT 361, N'X-9 2001', 32, 10, 273920, 9310370, 80
UNION ALL
    SELECT 362, N'Gavioes 2001', 32, 10, 282723, 9616640, 154
UNION ALL
    SELECT 363, N'Nene 2001', 32, 10, 284969, 9694508, 179
UNION ALL
    SELECT 364, N'Rosas De Ouro 2001', 32, 10, 284342, 9721084, 86
UNION ALL
    SELECT 365, N'Mocidade Alegre 2001', 32, 10, 282488, 9599937, 56
UNION ALL
    SELECT 366, N'Camisa Verde 2001', 32, 10, 283454, 9633755, 64
UNION ALL
    SELECT 367, N'Leandro De Itaquera 2001', 32, 10, 274808, 9451845, 136
UNION ALL
    SELECT 368, N'Tucuruvi 2001', 32, 10, 287921, 9883335, 107
UNION ALL
    SELECT 369, N'Aguia De Ouro 2001', 32, 10, 284160, 9698729, 75
UNION ALL
    SELECT 370, N'Ipiranga 2001', 32, 10, 248293, 8522591, 98
UNION ALL
    SELECT 371, N'Morro Da Casa Verde 2001', 32, 10, 284708, 9718778, 102
UNION ALL
    SELECT 372, N'Perola Negra 2001', 32, 10, 281626, 9619196, 171
UNION ALL
    SELECT 373, N'Sao Lucas 2001', 32, 10, 296254, 10020122, 67
UNION ALL
    SELECT 374, N'Guanabara', 33, 7, 247614, 8499591, 181
UNION ALL
    SELECT 375, N'Mas Que Nada', 33, 7, 248398, 8255254, 106
UNION ALL
    SELECT 376, N'VГґo Sobre o Horizonte', 33, 7, 225097, 7528825, 132
UNION ALL
    SELECT 377, N'A Paz', 33, 7, 263183, 8619173, 71
UNION ALL
    SELECT 378, N'Wave (Vou te Contar)', 33, 7, 271647, 9057557, 63
UNION ALL
    SELECT 379, N'ГЃgua de Beber', 33, 7, 146677, 4866476, 111
UNION ALL
    SELECT 380, N'Samba da BenГ§aco', 33, 7, 282200, 9440676, 98
UNION ALL
    SELECT 381, N'Pode Parar', 33, 7, 179408, 6046678, 83
UNION ALL
    SELECT 382, N'Menino do Rio', 33, 7, 262713, 8737489, 102
UNION ALL
    SELECT 383, N'Ando Meio Desligado', 33, 7, 195813, 6547648, 65
UNION ALL
    SELECT 384, N'MistГ©rio da RaГ§a', 33, 7, 184320, 6191752, 189
UNION ALL
    SELECT 385, N'All Star', 33, 7, 176326, 5891697, 67
UNION ALL
    SELECT 386, N'Menina Bonita', 33, 7, 237087, 7938246, 113
UNION ALL
    SELECT 387, N'Pescador de IlusГµes', 33, 7, 245524, 8267067, 131
UNION ALL
    SELECT 388, N'ГЂ Vontade (Live Mix)', 33, 7, 180636, 5972430, 192
UNION ALL
    SELECT 389, N'Maria FumaГ§a', 33, 7, 141008, 4743149, 107
UNION ALL
    SELECT 390, N'Sambassim (dj patife remix)', 33, 7, 213655, 7243166, 191
UNION ALL
    SELECT 391, N'Garota De Ipanema', 34, 7, 279536, 9141343, 160
UNION ALL
    SELECT 392, N'Tim Tim Por Tim Tim', 34, 7, 213237, 7143328, 139
UNION ALL
    SELECT 393, N'Tarde Em ItapoГЈ', 34, 7, 313704, 10344491, 98
UNION ALL
    SELECT 394, N'Tanto Tempo', 34, 7, 170292, 5572240, 70
UNION ALL
    SELECT 395, N'Eu Vim Da Bahia - Live', 34, 7, 157988, 5115428, 136
UNION ALL
    SELECT 396, N'AlГґ AlГґ Marciano', 34, 7, 238106, 8013065, 119
UNION ALL
    SELECT 397, N'Linha Do Horizonte', 34, 7, 279484, 9275929, 108
UNION ALL
    SELECT 398, N'Only A Dream In Rio', 34, 7, 371356, 12192989, 136
UNION ALL
    SELECT 399, N'Abrir A Porta', 34, 7, 271960, 8991141, 140
UNION ALL
    SELECT 400, N'Alice', 34, 7, 165982, 5594341, 133
UNION ALL
    SELECT 401, N'Momentos Que Marcam', 34, 7, 280137, 9313740, 168
UNION ALL
    SELECT 402, N'Um Jantar Pra Dois', 34, 7, 237714, 7819755, 117
UNION ALL
    SELECT 403, N'Bumbo Da Mangueira', 34, 7, 270158, 9073350, 153
UNION ALL
    SELECT 404, N'Mr Funk Samba', 34, 7, 213890, 7102545, 151
UNION ALL
    SELECT 405, N'Santo Antonio', 34, 7, 162716, 5492069, 181
UNION ALL
    SELECT 406, N'Por VocГЄ', 34, 7, 205557, 6792493, 102
UNION ALL
    SELECT 407, N'SГі Tinha De Ser Com VocГЄ', 34, 7, 389642, 13085596, 127
UNION ALL
    SELECT 408, N'Free Speech For The Dumb', 35, 3, 155428, 5076048, 114
UNION ALL
    SELECT 409, N'It''s Electric', 35, 3, 213995, 6978601, 123
UNION ALL
    SELECT 410, N'Sabbra Cadabra', 35, 3, 380342, 12418147, 134
UNION ALL
    SELECT 411, N'Turn The Page', 35, 3, 366524, 11946327, 183
UNION ALL
    SELECT 412, N'Die Die My Darling', 35, 3, 149315, 4867667, 190
UNION ALL
    SELECT 413, N'Loverman', 35, 3, 472764, 15446975, 99
UNION ALL
    SELECT 414, N'Mercyful Fate', 35, 3, 671712, 21942829, 116
UNION ALL
    SELECT 415, N'Astronomy', 35, 3, 397531, 13065612, 170
UNION ALL
    SELECT 416, N'Whiskey In The Jar', 35, 3, 305005, 9943129, 153
UNION ALL
    SELECT 417, N'Tuesday''s Gone', 35, 3, 545750, 17900787, 74
UNION ALL
    SELECT 418, N'The More I See', 35, 3, 287973, 9378873, 58
UNION ALL
    SELECT 419, N'A Kind Of Magic', 36, 1, 262608, 8689618, 194
UNION ALL
    SELECT 420, N'Under Pressure', 36, 1, 236617, 7739042, 77
UNION ALL
    SELECT 421, N'Radio GA GA', 36, 1, 343745, 11358573, 91
UNION ALL
    SELECT 422, N'I Want It All', 36, 1, 241684, 7876564, 89
UNION ALL
    SELECT 423, N'I Want To Break Free', 36, 1, 259108, 8552861, 80
UNION ALL
    SELECT 424, N'Innuendo', 36, 1, 387761, 12664591, 91
UNION ALL
    SELECT 425, N'It''s A Hard Life', 36, 1, 249417, 8112242, 96
UNION ALL
    SELECT 426, N'Breakthru', 36, 1, 249234, 8150479, 166
UNION ALL
    SELECT 427, N'Who Wants To Live Forever', 36, 1, 297691, 9577577, 153
UNION ALL
    SELECT 428, N'Headlong', 36, 1, 273057, 8921404, 86
UNION ALL
    SELECT 429, N'The Miracle', 36, 1, 294974, 9671923, 101
UNION ALL
    SELECT 430, N'I''m Going Slightly Mad', 36, 1, 248032, 8192339, 154
UNION ALL
    SELECT 431, N'The Invisible Man', 36, 1, 238994, 7920353, 50
UNION ALL
    SELECT 432, N'Hammer To Fall', 36, 1, 220316, 7255404, 176
UNION ALL
    SELECT 433, N'Friends Will Be Friends', 36, 1, 248920, 8114582, 155
UNION ALL
    SELECT 434, N'The Show Must Go On', 36, 1, 263784, 8526760, 154
UNION ALL
    SELECT 435, N'One Vision', 36, 1, 242599, 7936928, 100
UNION ALL
    SELECT 436, N'Detroit Rock City', 37, 1, 218880, 7146372, 82
UNION ALL
    SELECT 437, N'Black Diamond', 37, 1, 314148, 10266007, 96
UNION ALL
    SELECT 438, N'Hard Luck Woman', 37, 1, 216032, 7109267, 67
UNION ALL
    SELECT 439, N'Sure Know Something', 37, 1, 242468, 7939886, 180
UNION ALL
    SELECT 440, N'Love Gun', 37, 1, 196257, 6424915, 146
UNION ALL
    SELECT 441, N'Deuce', 37, 1, 185077, 6097210, 66
UNION ALL
    SELECT 442, N'Goin'' Blind', 37, 1, 216215, 7045314, 189
UNION ALL
    SELECT 443, N'Shock Me', 37, 1, 227291, 7529336, 74
UNION ALL
    SELECT 444, N'Do You Love Me', 37, 1, 214987, 6976194, 112
UNION ALL
    SELECT 445, N'She', 37, 1, 248346, 8229734, 200
UNION ALL
    SELECT 446, N'I Was Made For Loving You', 37, 1, 271360, 9018078, 113
UNION ALL
    SELECT 447, N'Shout It Out Loud', 37, 1, 219742, 7194424, 198
UNION ALL
    SELECT 448, N'God Of Thunder', 37, 1, 255791, 8309077, 95
UNION ALL
    SELECT 449, N'Calling Dr. Love', 37, 1, 225332, 7395034, 175
UNION ALL
    SELECT 450, N'Beth', 37, 1, 166974, 5360574, 68
UNION ALL
    SELECT 451, N'Strutter', 37, 1, 192496, 6317021, 137
UNION ALL
    SELECT 452, N'Rock And Roll All Nite', 37, 1, 173609, 5735902, 121
UNION ALL
    SELECT 453, N'Cold Gin', 37, 1, 262243, 8609783, 187
UNION ALL
    SELECT 454, N'Plaster Caster', 37, 1, 207333, 6801116, 91
UNION ALL
    SELECT 455, N'God Gave Rock ''n'' Roll To You', 37, 1, 320444, 10441590, 89
UNION ALL
    SELECT 456, N'Heart of the Night', 38, 2, 273737, 9098263, 134
UNION ALL
    SELECT 457, N'De La Luz', 38, 2, 315219, 10518284, 96
UNION ALL
    SELECT 458, N'Westwood Moon', 38, 2, 295627, 9765802, 112
UNION ALL
    SELECT 459, N'Midnight', 38, 2, 266866, 8851060, 155
UNION ALL
    SELECT 460, N'Playtime', 38, 2, 273580, 9070880, 118
UNION ALL
    SELECT 461, N'Surrender', 38, 2, 287634, 9422926, 123
UNION ALL
    SELECT 462, N'Valentino''s', 38, 2, 296124, 9848545, 176
UNION ALL
    SELECT 463, N'Believe', 38, 2, 310778, 10317185, 120
UNION ALL
    SELECT 464, N'As We Sleep', 38, 2, 316865, 10429398, 195
UNION ALL
    SELECT 465, N'When Evening Falls', 38, 2, 298135, 9863942, 73
UNION ALL
    SELECT 466, N'J Squared', 38, 2, 288757, 9480777, 127
UNION ALL
    SELECT 467, N'Best Thing', 38, 2, 274259, 9069394, 155
UNION ALL
    SELECT 468, N'Maria', 39, 4, 167262, 5484747, 178
UNION ALL
    SELECT 469, N'Poprocks And Coke', 39, 4, 158354, 5243078, 109
UNION ALL
    SELECT 470, N'Longview', 39, 4, 234083, 7714939, 152
UNION ALL
    SELECT 471, N'Welcome To Paradise', 39, 4, 224208, 7406008, 97
UNION ALL
    SELECT 472, N'Basket Case', 39, 4, 181629, 5951736, 51
UNION ALL
    SELECT 473, N'When I Come Around', 39, 4, 178364, 5839426, 110
UNION ALL
    SELECT 474, N'She', 39, 4, 134164, 4425128, 150
UNION ALL
    SELECT 475, N'J.A.R. (Jason Andrew Relva)', 39, 4, 170997, 5645755, 111
UNION ALL
    SELECT 476, N'Geek Stink Breath', 39, 4, 135888, 4408983, 193
UNION ALL
    SELECT 477, N'Brain Stew', 39, 4, 193149, 6305550, 89
UNION ALL
    SELECT 478, N'Jaded', 39, 4, 90331, 2950224, 107
UNION ALL
    SELECT 479, N'Walking Contradiction', 39, 4, 151170, 4932366, 168
UNION ALL
    SELECT 480, N'Stuck With Me', 39, 4, 135523, 4431357, 58
UNION ALL
    SELECT 481, N'Hitchin'' A Ride', 39, 4, 171546, 5616891, 69
UNION ALL
    SELECT 482, N'Good Riddance (Time Of Your Life)', 39, 4, 153600, 5075241, 51
UNION ALL
    SELECT 483, N'Redundant', 39, 4, 198164, 6481753, 127
UNION ALL
    SELECT 484, N'Nice Guys Finish Last', 39, 4, 170187, 5604618, 159
UNION ALL
    SELECT 485, N'Minority', 39, 4, 168803, 5535061, 69
UNION ALL
    SELECT 486, N'Warning', 39, 4, 221910, 7343176, 101
UNION ALL
    SELECT 487, N'Waiting', 39, 4, 192757, 6316430, 199
UNION ALL
    SELECT 488, N'Macy''s Day Parade', 39, 4, 213420, 7075573, 135
UNION ALL
    SELECT 489, N'Into The Light', 40, 1, 76303, 2452653, 158
UNION ALL
    SELECT 490, N'River Song', 40, 1, 439510, 14359478, 50
UNION ALL
    SELECT 491, N'She Give Me ...', 40, 1, 252551, 8385478, 191
UNION ALL
    SELECT 492, N'Don''t You Cry', 40, 1, 347036, 11269612, 128
UNION ALL
    SELECT 493, N'Love Is Blind', 40, 1, 344999, 11409720, 183
UNION ALL
    SELECT 494, N'Slave', 40, 1, 291892, 9425200, 163
UNION ALL
    SELECT 495, N'Cry For Love', 40, 1, 293015, 9567075, 92
UNION ALL
    SELECT 496, N'Living On Love', 40, 1, 391549, 12785876, 121
UNION ALL
    SELECT 497, N'Midnight Blue', 40, 1, 298631, 9750990, 191
UNION ALL
    SELECT 498, N'Too Many Tears', 40, 1, 359497, 11810238, 175
UNION ALL
    SELECT 499, N'Don''t Lie To Me', 40, 1, 283585, 9288007, 175
UNION ALL
    SELECT 500, N'Wherever You May Go', 40, 1, 239699, 7803074, 126
UNION ALL
    SELECT 501, N'Grito De Alerta', 41, 7, 202213, 6539422, 165
UNION ALL
    SELECT 502, N'NГЈo DГЎ Mais Pra Segurar (Explode CoraГ§ГЈo)', 41, 7, 219768, 7083012, 123
UNION ALL
    SELECT 503, N'ComeГ§aria Tudo Outra Vez', 41, 7, 196545, 6473395, 74
UNION ALL
    SELECT 504, N'O Que Г‰ O Que Г‰ ?', 41, 7, 259291, 8650647, 133
UNION ALL
    SELECT 505, N'Sangrando', 41, 7, 169717, 5494406, 105
UNION ALL
    SELECT 506, N'Diga LГЎ, CoraГ§ГЈo', 41, 7, 255921, 8280636, 105
UNION ALL
    SELECT 507, N'Lindo Lago Do Amor', 41, 7, 249678, 8353191, 154
UNION ALL
    SELECT 508, N'Eu Apenas Queria Que VoГ§ГЄ Soubesse', 41, 7, 155637, 5130056, 145
UNION ALL
    SELECT 509, N'Com A Perna No Mundo', 41, 7, 227448, 7747108, 143
UNION ALL
    SELECT 510, N'E Vamos ГЂ Luta', 41, 7, 222406, 7585112, 63
UNION ALL
    SELECT 511, N'Um Homem TambГ©m Chora (Guerreiro Menino)', 41, 7, 207229, 6854219, 161
UNION ALL
    SELECT 512, N'Comportamento Geral', 41, 7, 181577, 5997444, 119
UNION ALL
    SELECT 513, N'Ponto De InterrogaГ§ГЈo', 41, 7, 180950, 5946265, 69
UNION ALL
    SELECT 514, N'Espere Por Mim, Morena', 41, 7, 207072, 6796523, 155
UNION ALL
    SELECT 515, N'Meia-Lua Inteira', 23, 7, 222093, 7466288, 153
UNION ALL
    SELECT 516, N'Voce e Linda', 23, 7, 242938, 8050268, 190
UNION ALL
    SELECT 517, N'Um Indio', 23, 7, 195944, 6453213, 75
UNION ALL
    SELECT 518, N'Podres Poderes', 23, 7, 259761, 8622495, 69
UNION ALL
    SELECT 519, N'Voce Nao Entende Nada - Cotidiano', 23, 7, 421982, 13885612, 165
UNION ALL
    SELECT 520, N'O Estrangeiro', 23, 7, 374700, 12472890, 117
UNION ALL
    SELECT 521, N'Menino Do Rio', 23, 7, 147670, 4862277, 75
UNION ALL
    SELECT 522, N'Qualquer Coisa', 23, 7, 193410, 6372433, 189
UNION ALL
    SELECT 523, N'Sampa', 23, 7, 185051, 6151831, 181
UNION ALL
    SELECT 524, N'Queixa', 23, 7, 299676, 9953962, 97
UNION ALL
    SELECT 525, N'O Leaozinho', 23, 7, 184398, 6098150, 140
UNION ALL
    SELECT 526, N'Fora Da Ordem', 23, 7, 354011, 11746781, 164
UNION ALL
    SELECT 527, N'Terra', 23, 7, 401319, 13224055, 189
UNION ALL
    SELECT 528, N'Alegria, Alegria', 23, 7, 169221, 5497025, 200
UNION ALL
    SELECT 529, N'Balada Do Louco', 42, 4, 241057, 7852328, 196
UNION ALL
    SELECT 530, N'Ando Meio Desligado', 42, 4, 287817, 9484504, 84
UNION ALL
    SELECT 531, N'Top Top', 42, 4, 146938, 4875374, 162
UNION ALL
    SELECT 532, N'Baby', 42, 4, 177188, 5798202, 193
UNION ALL
    SELECT 533, N'A E O Z', 42, 4, 518556, 16873005, 170
UNION ALL
    SELECT 534, N'Panis Et Circenses', 42, 4, 125152, 4069688, 139
UNION ALL
    SELECT 535, N'ChГЈo De Estrelas', 42, 4, 284813, 9433620, 83
UNION ALL
    SELECT 536, N'Vida De Cachorro', 42, 4, 195186, 6411149, 111
UNION ALL
    SELECT 537, N'Bat Macumba', 42, 4, 187794, 6295223, 175
UNION ALL
    SELECT 538, N'Desculpe Babe', 42, 4, 170422, 5637959, 142
UNION ALL
    SELECT 539, N'Rita Lee', 42, 4, 189257, 6270503, 84
UNION ALL
    SELECT 540, N'Posso Perder Minha Mulher, Minha MГЈe, Desde Que Eu Tenha O Rock And Roll', 42, 4, 222955, 7346254, 99
UNION ALL
    SELECT 541, N'Banho De Lua', 42, 4, 221831, 7232123, 157
UNION ALL
    SELECT 542, N'Meu Refrigerador NГЈo Funciona', 42, 4, 382981, 12495906, 152
UNION ALL
    SELECT 543, N'Burn', 43, 1, 453955, 14775708, 138
UNION ALL
    SELECT 544, N'Stormbringer', 43, 1, 277133, 9050022, 163
UNION ALL
    SELECT 545, N'Gypsy', 43, 1, 339173, 11046952, 85
UNION ALL
    SELECT 546, N'Lady Double Dealer', 43, 1, 233586, 7608759, 86
UNION ALL
    SELECT 547, N'Mistreated', 43, 1, 758648, 24596235, 174
UNION ALL
    SELECT 548, N'Smoke On The Water', 43, 1, 618031, 20103125, 118
UNION ALL
    SELECT 549, N'You Fool No One', 43, 1, 804101, 26369966, 148
UNION ALL
    SELECT 550, N'Custard Pie', 44, 1, 253962, 8348257, 54
UNION ALL
    SELECT 551, N'The Rover', 44, 1, 337084, 11011286, 170
UNION ALL
    SELECT 552, N'In My Time Of Dying', 44, 1, 666017, 21676727, 179
UNION ALL
    SELECT 553, N'Houses Of The Holy', 44, 1, 242494, 7972503, 189
UNION ALL
    SELECT 554, N'Trampled Under Foot', 44, 1, 336692, 11154468, 106
UNION ALL
    SELECT 555, N'Kashmir', 44, 1, 508604, 16686580, 198
UNION ALL
    SELECT 556, N'Imperatriz', 45, 7, 339173, 11348710, 190
UNION ALL
    SELECT 557, N'Beija-Flor', 45, 7, 327000, 10991159, 70
UNION ALL
    SELECT 558, N'Viradouro', 45, 7, 344320, 11484362, 79
UNION ALL
    SELECT 559, N'Mocidade', 45, 7, 261720, 8817757, 69
UNION ALL
    SELECT 560, N'Unidos Da Tijuca', 45, 7, 338834, 11440689, 71
UNION ALL
    SELECT 561, N'Salgueiro', 45, 7, 305920, 10294741, 53
UNION ALL
    SELECT 562, N'Mangueira', 45, 7, 298318, 9999506, 179
UNION ALL
    SELECT 563, N'UniГЈo Da Ilha', 45, 7, 330945, 11100945, 143
UNION ALL
    SELECT 564, N'Grande Rio', 45, 7, 307252, 10251428, 129
UNION ALL
    SELECT 565, N'Portela', 45, 7, 319608, 10712216, 89
UNION ALL
    SELECT 566, N'Caprichosos', 45, 7, 351320, 11870956, 127
UNION ALL
    SELECT 567, N'TradiГ§ГЈo', 45, 7, 269165, 9114880, 112
UNION ALL
    SELECT 568, N'ImpГ©rio Serrano', 45, 7, 334942, 11161196, 171
UNION ALL
    SELECT 569, N'Tuiuti', 45, 7, 259657, 8749492, 133
UNION ALL
    SELECT 570, N'(Da Le) Yaleo', 46, 1, 353488, 11769507, 87
UNION ALL
    SELECT 571, N'Love Of My Life', 46, 1, 347820, 11634337, 118
UNION ALL
    SELECT 572, N'Put Your Lights On', 46, 1, 285178, 9394769, 196
UNION ALL
    SELECT 573, N'Africa Bamba', 46, 1, 282827, 9492487, 59
UNION ALL
    SELECT 574, N'Smooth', 46, 1, 298161, 9867455, 188
UNION ALL
    SELECT 575, N'Do You Like The Way', 46, 1, 354899, 11741062, 81
UNION ALL
    SELECT 576, N'Maria Maria', 46, 1, 262635, 8664601, 62
UNION ALL
    SELECT 577, N'Migra', 46, 1, 329064, 10963305, 126
UNION ALL
    SELECT 578, N'Corazon Espinado', 46, 1, 276114, 9206802, 96
UNION ALL
    SELECT 579, N'Wishing It Was', 46, 1, 292832, 9771348, 60
UNION ALL
    SELECT 580, N'El Farol', 46, 1, 291160, 9599353, 102
UNION ALL
    SELECT 581, N'Primavera', 46, 1, 378618, 12504234, 198
UNION ALL
    SELECT 582, N'The Calling', 46, 1, 747755, 24703884, 127
UNION ALL
    SELECT 583, N'SoluГ§ГЈo', 47, 7, 247431, 8100449, 73
UNION ALL
    SELECT 584, N'Manuel', 47, 7, 230269, 7677671, 174
UNION ALL
    SELECT 585, N'Entre E OuГ§a', 47, 7, 286302, 9391004, 130
UNION ALL
    SELECT 586, N'Um Contrato Com Deus', 47, 7, 202501, 6636465, 113
UNION ALL
    SELECT 587, N'Um Jantar Pra Dois', 47, 7, 244009, 8021589, 79
UNION ALL
    SELECT 588, N'Vamos DanГ§ar', 47, 7, 226194, 7617432, 132
UNION ALL
    SELECT 589, N'Um Love', 47, 7, 181603, 6095524, 191
UNION ALL
    SELECT 590, N'Seis Da Tarde', 47, 7, 238445, 7935898, 199
UNION ALL
    SELECT 591, N'Baixo Rio', 47, 7, 198008, 6521676, 89
UNION ALL
    SELECT 592, N'Sombras Do Meu Destino', 47, 7, 280685, 9161539, 144
UNION ALL
    SELECT 593, N'Do You Have Other Loves?', 47, 7, 295235, 9604273, 169
UNION ALL
    SELECT 594, N'Agora Que O Dia Acordou', 47, 7, 323213, 10572752, 145
UNION ALL
    SELECT 595, N'JГЎ!!!', 47, 7, 217782, 7103608, 114
UNION ALL
    SELECT 596, N'A Rua', 47, 7, 238027, 7930264, 123
UNION ALL
    SELECT 597, N'Now''s The Time', 48, 2, 197459, 6358868, 197
UNION ALL
    SELECT 598, N'Jeru', 48, 2, 193410, 6222536, 142
UNION ALL
    SELECT 599, N'Compulsion', 48, 2, 345025, 11254474, 74
UNION ALL
    SELECT 600, N'Tempus Fugit', 48, 2, 231784, 7548434, 58
UNION ALL
    SELECT 601, N'Walkin''', 48, 2, 807392, 26411634, 110
UNION ALL
    SELECT 602, N'''Round Midnight', 48, 2, 357459, 11590284, 120
UNION ALL
    SELECT 603, N'Bye Bye Blackbird', 48, 2, 476003, 15549224, 157
UNION ALL
    SELECT 604, N'New Rhumba', 48, 2, 277968, 9018024, 82
UNION ALL
    SELECT 605, N'Generique', 48, 2, 168777, 5437017, 100
UNION ALL
    SELECT 606, N'Summertime', 48, 2, 200437, 6461370, 174
UNION ALL
    SELECT 607, N'So What', 48, 2, 564009, 18360449, 87
UNION ALL
    SELECT 608, N'The Pan Piper', 48, 2, 233769, 7593713, 141
UNION ALL
    SELECT 609, N'Someday My Prince Will Come', 48, 2, 544078, 17890773, 176
UNION ALL
    SELECT 610, N'My Funny Valentine (Live)', 49, 2, 907520, 29416781, 125
UNION ALL
    SELECT 611, N'E.S.P.', 49, 2, 330684, 11079866, 81
UNION ALL
    SELECT 612, N'Nefertiti', 49, 2, 473495, 15478450, 156
UNION ALL
    SELECT 613, N'Petits Machins (Little Stuff)', 49, 2, 487392, 16131272, 173
UNION ALL
    SELECT 614, N'Miles Runs The Voodoo Down', 49, 2, 843964, 27967919, 184
UNION ALL
    SELECT 615, N'Little Church (Live)', 49, 2, 196101, 6273225, 100
UNION ALL
    SELECT 616, N'Black Satin', 49, 2, 316682, 10529483, 156
UNION ALL
    SELECT 617, N'Jean Pierre (Live)', 49, 2, 243461, 7955114, 154
UNION ALL
    SELECT 618, N'Time After Time', 49, 2, 220734, 7292197, 147
UNION ALL
    SELECT 619, N'Portia', 49, 2, 378775, 12520126, 175
UNION ALL
    SELECT 620, N'Space Truckin''', 50, 1, 1196094, 39267613, 175
UNION ALL
    SELECT 621, N'Going Down / Highway Star', 50, 1, 913658, 29846063, 114
UNION ALL
    SELECT 622, N'Mistreated (Alternate Version)', 50, 1, 854700, 27775442, 166
UNION ALL
    SELECT 623, N'You Fool No One (Alternate Version)', 50, 1, 763924, 24887209, 182
UNION ALL
    SELECT 624, N'Jeepers Creepers', 51, 2, 185965, 5991903, 123
UNION ALL
    SELECT 625, N'Blue Rythm Fantasy', 51, 2, 348212, 11204006, 107
UNION ALL
    SELECT 626, N'Drum Boogie', 51, 2, 191555, 6185636, 165
UNION ALL
    SELECT 627, N'Let Me Off Uptown', 51, 2, 187637, 6034685, 127
UNION ALL
    SELECT 628, N'Leave Us Leap', 51, 2, 182726, 5898810, 156
UNION ALL
    SELECT 629, N'Opus No.1', 51, 2, 179800, 5846041, 77
UNION ALL
    SELECT 630, N'Boogie Blues', 51, 2, 204199, 6603153, 178
UNION ALL
    SELECT 631, N'How High The Moon', 51, 2, 201430, 6529487, 169
UNION ALL
    SELECT 632, N'Disc Jockey Jump', 51, 2, 193149, 6260820, 136
UNION ALL
    SELECT 633, N'Up An'' Atom', 51, 2, 179565, 5822645, 122
UNION ALL
    SELECT 634, N'Bop Boogie', 51, 2, 189596, 6093124, 80
UNION ALL
    SELECT 635, N'Lemon Drop', 51, 2, 194089, 6287531, 75
UNION ALL
    SELECT 636, N'Coronation Drop', 51, 2, 176222, 5899898, 123
UNION ALL
    SELECT 637, N'Overtime', 51, 2, 163030, 5432236, 165
UNION ALL
    SELECT 638, N'Imagination', 51, 2, 289306, 9444385, 97
UNION ALL
    SELECT 639, N'Don''t Take Your Love From Me', 51, 2, 282331, 9244238, 60
UNION ALL
    SELECT 640, N'Midget', 51, 2, 217025, 7257663, 79
UNION ALL
    SELECT 641, N'I''m Coming Virginia', 51, 2, 280163, 9209827, 94
UNION ALL
    SELECT 642, N'Payin'' Them Dues Blues', 51, 2, 198556, 6536918, 110
UNION ALL
    SELECT 643, N'Jungle Drums', 51, 2, 199627, 6546063, 148
UNION ALL
    SELECT 644, N'Showcase', 51, 2, 201560, 6697510, 194
UNION ALL
    SELECT 645, N'Swedish Schnapps', 51, 2, 191268, 6359750, 168
UNION ALL
    SELECT 646, N'Samba Da BГЄnГ§ГЈo', 52, 11, 409965, 13490008, 61
UNION ALL
    SELECT 647, N'Pot-Pourri N.Вє 4', 52, 11, 392437, 13125975, 80
UNION ALL
    SELECT 648, N'Onde Anda VocГЄ', 52, 11, 168437, 5550356, 101
UNION ALL
    SELECT 649, N'Samba Da Volta', 52, 11, 170631, 5676090, 184
UNION ALL
    SELECT 650, N'Canto De Ossanha', 52, 11, 204956, 6771624, 104
UNION ALL
    SELECT 651, N'Pot-Pourri N.Вє 5', 52, 11, 219898, 7117769, 152
UNION ALL
    SELECT 652, N'Formosa', 52, 11, 137482, 4560873, 169
UNION ALL
    SELECT 653, N'Como Г‰ Duro Trabalhar', 52, 11, 226168, 7541177, 114
UNION ALL
    SELECT 654, N'Minha Namorada', 52, 11, 244297, 7927967, 97
UNION ALL
    SELECT 655, N'Por Que SerГЎ', 52, 11, 162142, 5371483, 121
UNION ALL
    SELECT 656, N'Berimbau', 52, 11, 190667, 6335548, 82
UNION ALL
    SELECT 657, N'Deixa', 52, 11, 179826, 5932799, 81
UNION ALL
    SELECT 658, N'Pot-Pourri N.Вє 2', 52, 11, 211748, 6878359, 59
UNION ALL
    SELECT 659, N'Samba Em PrelГєdio', 52, 11, 212636, 6923473, 148
UNION ALL
    SELECT 660, N'Carta Ao Tom 74', 52, 11, 162560, 5382354, 106
UNION ALL
    SELECT 661, N'Linha de Passe (JoГЈo Bosco)', 53, 7, 230948, 7902328, 181
UNION ALL
    SELECT 662, N'Pela Luz dos Olhos Teus (MiГєcha e Tom Jobim)', 53, 7, 163970, 5399626, 167
UNION ALL
    SELECT 663, N'ChГЈo de Giz (Elba Ramalho)', 53, 7, 274834, 9016916, 113
UNION ALL
    SELECT 664, N'Marina (Dorival Caymmi)', 53, 7, 172643, 5523628, 64
UNION ALL
    SELECT 665, N'Aquarela (Toquinho)', 53, 7, 259944, 8480140, 133
UNION ALL
    SELECT 666, N'CoraГ§ГЈo do Agreste (FafГЎ de BelГ©m)', 53, 7, 258194, 8380320, 99
UNION ALL
    SELECT 667, N'Dona (Roupa Nova)', 53, 7, 243356, 7991295, 200
UNION ALL
    SELECT 668, N'ComeГ§aria Tudo Outra Vez (Maria Creuza)', 53, 7, 206994, 6851151, 62
UNION ALL
    SELECT 669, N'CaГ§ador de Mim (SГЎ & Guarabyra)', 53, 7, 238341, 7751360, 115
UNION ALL
    SELECT 670, N'Romaria (Renato Teixeira)', 53, 7, 244793, 8033885, 199
UNION ALL
    SELECT 671, N'As Rosas NГЈo Falam (Beth Carvalho)', 53, 7, 116767, 3836641, 69
UNION ALL
    SELECT 672, N'Wave (Os Cariocas)', 53, 7, 130063, 4298006, 112
UNION ALL
    SELECT 673, N'Garota de Ipanema (Dick Farney)', 53, 7, 174367, 5767474, 126
UNION ALL
    SELECT 674, N'Preciso Apender a Viver SГі (Maysa)', 53, 7, 143464, 4642359, 174
UNION ALL
    SELECT 675, N'Susie Q', 54, 1, 275565, 9043825, 103
UNION ALL
    SELECT 676, N'I Put A Spell On You', 54, 1, 272091, 8943000, 102
UNION ALL
    SELECT 677, N'Proud Mary', 54, 1, 189022, 6229590, 74
UNION ALL
    SELECT 678, N'Bad Moon Rising', 54, 1, 140146, 4609835, 62
UNION ALL
    SELECT 679, N'Lodi', 54, 1, 191451, 6260214, 141
UNION ALL
    SELECT 680, N'Green River', 54, 1, 154279, 5105874, 132
UNION ALL
    SELECT 681, N'Commotion', 54, 1, 162899, 5354252, 77
UNION ALL
    SELECT 682, N'Down On The Corner', 54, 1, 164858, 5521804, 104
UNION ALL
    SELECT 683, N'Fortunate Son', 54, 1, 140329, 4617559, 50
UNION ALL
    SELECT 684, N'Travelin'' Band', 54, 1, 129358, 4270414, 198
UNION ALL
    SELECT 685, N'Who''ll Stop The Rain', 54, 1, 149394, 4899579, 88
UNION ALL
    SELECT 686, N'Up Around The Bend', 54, 1, 162429, 5368701, 52
UNION ALL
    SELECT 687, N'Run Through The Jungle', 54, 1, 186044, 6156567, 122
UNION ALL
    SELECT 688, N'Lookin'' Out My Back Door', 54, 1, 152946, 5034670, 194
UNION ALL
    SELECT 689, N'Long As I Can See The Light', 54, 1, 213237, 6924024, 182
UNION ALL
    SELECT 690, N'I Heard It Through The Grapevine', 54, 1, 664894, 21947845, 57
UNION ALL
    SELECT 691, N'Have You Ever Seen The Rain?', 54, 1, 160052, 5263675, 62
UNION ALL
    SELECT 692, N'Hey Tonight', 54, 1, 162847, 5343807, 72
UNION ALL
    SELECT 693, N'Sweet Hitch-Hiker', 54, 1, 175490, 5716603, 86
UNION ALL
    SELECT 694, N'Someday Never Comes', 54, 1, 239360, 7945235, 66
UNION ALL
    SELECT 695, N'Walking On The Water', 55, 1, 281286, 9302129, 184
UNION ALL
    SELECT 696, N'Suzie-Q, Pt. 2', 55, 1, 244114, 7986637, 75
UNION ALL
    SELECT 697, N'Born On The Bayou', 55, 1, 316630, 10361866, 193
UNION ALL
    SELECT 698, N'Good Golly Miss Molly', 55, 1, 163604, 5348175, 137
UNION ALL
    SELECT 699, N'Tombstone Shadow', 55, 1, 218880, 7209080, 118
UNION ALL
    SELECT 700, N'Wrote A Song For Everyone', 55, 1, 296385, 9675875, 190
UNION ALL
    SELECT 701, N'Night Time Is The Right Time', 55, 1, 190119, 6211173, 140
UNION ALL
    SELECT 702, N'Cotton Fields', 55, 1, 178181, 5919224, 116
UNION ALL
    SELECT 703, N'It Came Out Of The Sky', 55, 1, 176718, 5807474, 107
UNION ALL
    SELECT 704, N'Don''t Look Now', 55, 1, 131918, 4366455, 128
UNION ALL
    SELECT 705, N'The Midnight Special', 55, 1, 253596, 8297482, 200
UNION ALL
    SELECT 706, N'Before You Accuse Me', 55, 1, 207804, 6815126, 135
UNION ALL
    SELECT 707, N'My Baby Left Me', 55, 1, 140460, 4633440, 143
UNION ALL
    SELECT 708, N'Pagan Baby', 55, 1, 385619, 12713813, 126
UNION ALL
    SELECT 709, N'(Wish I Could) Hideaway', 55, 1, 228466, 7432978, 166
UNION ALL
    SELECT 710, N'It''s Just A Thought', 55, 1, 237374, 7778319, 55
UNION ALL
    SELECT 711, N'Molina', 55, 1, 163239, 5390811, 90
UNION ALL
    SELECT 712, N'Born To Move', 55, 1, 342804, 11260814, 108
UNION ALL
    SELECT 713, N'Lookin'' For A Reason', 55, 1, 209789, 6933135, 130
UNION ALL
    SELECT 714, N'Hello Mary Lou', 55, 1, 132832, 4476563, 192
UNION ALL
    SELECT 715, N'Gatas ExtraordinГЎrias', 56, 7, 212506, 7095702, 181
UNION ALL
    SELECT 716, N'Brasil', 56, 7, 243696, 7911683, 198
UNION ALL
    SELECT 717, N'Eu Sou Neguinha (Ao Vivo)', 56, 7, 251768, 8376000, 108
UNION ALL
    SELECT 718, N'GeraГ§ГЈo Coca-Cola (Ao Vivo)', 56, 7, 228153, 7573301, 138
UNION ALL
    SELECT 719, N'Lanterna Dos Afogados', 56, 7, 204538, 6714582, 119
UNION ALL
    SELECT 720, N'CoronГ© Antonio Bento', 56, 7, 200437, 6713066, 98
UNION ALL
    SELECT 721, N'VocГЄ Passa, Eu Acho GraГ§a (Ao Vivo)', 56, 7, 206733, 6943576, 141
UNION ALL
    SELECT 722, N'Meu Mundo Fica Completo (Com VocГЄ)', 56, 7, 247771, 8322240, 164
UNION ALL
    SELECT 723, N'1В° De Julho', 56, 7, 270262, 9017535, 139
UNION ALL
    SELECT 724, N'MГєsica Urbana 2', 56, 7, 194899, 6383472, 74
UNION ALL
    SELECT 725, N'Vida Bandida (Ao Vivo)', 56, 7, 192626, 6360785, 68
UNION ALL
    SELECT 726, N'Palavras Ao Vento', 56, 7, 212453, 7048676, 115
UNION ALL
    SELECT 727, N'NГЈo Sei O Que Eu Quero Da Vida', 56, 7, 151849, 5024963, 61
UNION ALL
    SELECT 728, N'Woman Is The Nigger Of The World (Ao Vivo)', 56, 7, 298919, 9724145, 129
UNION ALL
    SELECT 729, N'Juventude Transviada (Ao Vivo)', 56, 7, 278622, 9183808, 101
UNION ALL
    SELECT 730, N'Malandragem', 57, 7, 247588, 8165048, 92
UNION ALL
    SELECT 731, N'O Segundo Sol', 57, 7, 252133, 8335629, 96
UNION ALL
    SELECT 732, N'Smells Like Teen Spirit (Ao Vivo)', 57, 7, 316865, 10384506, 138
UNION ALL
    SELECT 733, N'E.C.T.', 57, 7, 227500, 7571834, 82
UNION ALL
    SELECT 734, N'Todo Amor Que Houver Nesta Vida', 57, 7, 227160, 7420347, 188
UNION ALL
    SELECT 735, N'MetrГґ. Linha 743', 57, 7, 174654, 5837495, 190
UNION ALL
    SELECT 736, N'NГіs (Ao Vivo)', 57, 7, 193828, 6498661, 161
UNION ALL
    SELECT 737, N'Na CadГЄncia Do Samba', 57, 7, 196075, 6483952, 190
UNION ALL
    SELECT 738, N'AdmirГЎvel Gado Novo', 57, 7, 274390, 9144031, 115
UNION ALL
    SELECT 739, N'Eleanor Rigby', 57, 7, 189466, 6303205, 120
UNION ALL
    SELECT 740, N'Socorro', 57, 7, 258586, 8549393, 75
UNION ALL
    SELECT 741, N'Blues Da Piedade', 57, 7, 257123, 8472964, 78
UNION ALL
    SELECT 742, N'Rubens', 57, 7, 211853, 7026317, 105
UNION ALL
    SELECT 743, N'NГЈo Deixe O Samba Morrer - Cassia Eller e Alcione', 57, 7, 268173, 8936345, 175
UNION ALL
    SELECT 744, N'Mis Penas Lloraba Yo (Ao Vivo) Soy Gitano (Tangos)', 57, 7, 188473, 6195854, 57
UNION ALL
    SELECT 745, N'Comin'' Home', 58, 1, 235781, 7644604, 106
UNION ALL
    SELECT 746, N'Lady Luck', 58, 1, 168202, 5501379, 149
UNION ALL
    SELECT 747, N'Gettin'' Tighter', 58, 1, 218044, 7176909, 155
UNION ALL
    SELECT 748, N'Dealer', 58, 1, 230922, 7591066, 87
UNION ALL
    SELECT 749, N'I Need Love', 58, 1, 263836, 8701064, 123
UNION ALL
    SELECT 750, N'Drifter', 58, 1, 242834, 8001505, 188
UNION ALL
    SELECT 751, N'Love Child', 58, 1, 188160, 6173806, 147
UNION ALL
    SELECT 752, N'This Time Around / Owed to ''G'' [Instrumental]', 58, 1, 370102, 11995679, 119
UNION ALL
    SELECT 753, N'You Keep On Moving', 58, 1, 319111, 10447868, 60
UNION ALL
    SELECT 754, N'Speed King', 59, 1, 264385, 8587578, 192
UNION ALL
    SELECT 755, N'Bloodsucker', 59, 1, 256261, 8344405, 71
UNION ALL
    SELECT 756, N'Child In Time', 59, 1, 620460, 20230089, 142
UNION ALL
    SELECT 757, N'Flight Of The Rat', 59, 1, 478302, 15563967, 79
UNION ALL
    SELECT 758, N'Into The Fire', 59, 1, 210259, 6849310, 78
UNION ALL
    SELECT 759, N'Living Wreck', 59, 1, 274886, 8993056, 51
UNION ALL
    SELECT 760, N'Hard Lovin'' Man', 59, 1, 431203, 13931179, 108
UNION ALL
    SELECT 761, N'Fireball', 60, 1, 204721, 6714807, 105
UNION ALL
    SELECT 762, N'No No No', 60, 1, 414902, 13646606, 59
UNION ALL
    SELECT 763, N'Strange Kind Of Woman', 60, 1, 247092, 8072036, 72
UNION ALL
    SELECT 764, N'Anyone''s Daughter', 60, 1, 284682, 9354480, 70
UNION ALL
    SELECT 765, N'The Mule', 60, 1, 322063, 10638390, 106
UNION ALL
    SELECT 766, N'Fools', 60, 1, 500427, 16279366, 166
UNION ALL
    SELECT 767, N'No One Came', 60, 1, 385880, 12643813, 107
UNION ALL
    SELECT 768, N'Knocking At Your Back Door', 61, 1, 424829, 13779332, 139
UNION ALL
    SELECT 769, N'Bad Attitude', 61, 1, 307905, 10035180, 165
UNION ALL
    SELECT 770, N'Child In Time (Son Of Aleric - Instrumental)', 61, 1, 602880, 19712753, 175
UNION ALL
    SELECT 771, N'Nobody''s Home', 61, 1, 243017, 7929493, 144
UNION ALL
    SELECT 772, N'Black Night', 61, 1, 368770, 12058906, 180
UNION ALL
    SELECT 773, N'Perfect Strangers', 61, 1, 321149, 10445353, 101
UNION ALL
    SELECT 774, N'The Unwritten Law', 61, 1, 295053, 9740361, 144
UNION ALL
    SELECT 775, N'Call Of The Wild', 61, 1, 293851, 9575295, 182
UNION ALL
    SELECT 776, N'Hush', 61, 1, 213054, 6944928, 168
UNION ALL
    SELECT 777, N'Smoke On The Water', 61, 1, 464378, 15180849, 90
UNION ALL
    SELECT 778, N'Space Trucking', 61, 1, 341185, 11122183, 153
UNION ALL
    SELECT 779, N'Highway Star', 62, 1, 368770, 12012452, 97
UNION ALL
    SELECT 780, N'Maybe I''m A Leo', 62, 1, 290455, 9502646, 110
UNION ALL
    SELECT 781, N'Pictures Of Home', 62, 1, 303777, 9903835, 175
UNION ALL
    SELECT 782, N'Never Before', 62, 1, 239830, 7832790, 113
UNION ALL
    SELECT 783, N'Smoke On The Water', 62, 1, 340871, 11246496, 80
UNION ALL
    SELECT 784, N'Lazy', 62, 1, 442096, 14397671, 131
UNION ALL
    SELECT 785, N'Space Truckin''', 62, 1, 272796, 8981030, 76
UNION ALL
    SELECT 786, N'Vavoom : Ted The Mechanic', 63, 1, 257384, 8510755, 51
UNION ALL
    SELECT 787, N'Loosen My Strings', 63, 1, 359680, 11702232, 135
UNION ALL
    SELECT 788, N'Soon Forgotten', 63, 1, 287791, 9401383, 119
UNION ALL
    SELECT 789, N'Sometimes I Feel Like Screaming', 63, 1, 451840, 14789410, 179
UNION ALL
    SELECT 790, N'Cascades : I''m Not Your Lover', 63, 1, 283689, 9209693, 179
UNION ALL
    SELECT 791, N'The Aviator', 63, 1, 320992, 10532053, 194
UNION ALL
    SELECT 792, N'Rosa''s Cantina', 63, 1, 312372, 10323804, 57
UNION ALL
    SELECT 793, N'A Castle Full Of Rascals', 63, 1, 311693, 10159566, 75
UNION ALL
    SELECT 794, N'A Touch Away', 63, 1, 276323, 9098561, 186
UNION ALL
    SELECT 795, N'Hey Cisco', 63, 1, 354089, 11600029, 132
UNION ALL
    SELECT 796, N'Somebody Stole My Guitar', 63, 1, 249443, 8180421, 100
UNION ALL
    SELECT 797, N'The Purpendicular Waltz', 63, 1, 283924, 9299131, 128
UNION ALL
    SELECT 798, N'King Of Dreams', 64, 1, 328385, 10733847, 96
UNION ALL
    SELECT 799, N'The Cut Runs Deep', 64, 1, 342752, 11191650, 168
UNION ALL
    SELECT 800, N'Fire In The Basement', 64, 1, 283977, 9267550, 150
UNION ALL
    SELECT 801, N'Truth Hurts', 64, 1, 314827, 10224612, 153
UNION ALL
    SELECT 802, N'Breakfast In Bed', 64, 1, 317126, 10323804, 70
UNION ALL
    SELECT 803, N'Love Conquers All', 64, 1, 227186, 7328516, 182
UNION ALL
    SELECT 804, N'Fortuneteller', 64, 1, 349335, 11369671, 184
UNION ALL
    SELECT 805, N'Too Much Is Not Enough', 64, 1, 257724, 8382800, 75
UNION ALL
    SELECT 806, N'Wicked Ways', 64, 1, 393691, 12826582, 133
UNION ALL
    SELECT 807, N'Stormbringer', 65, 1, 246413, 8044864, 54
UNION ALL
    SELECT 808, N'Love Don''t Mean a Thing', 65, 1, 263862, 8675026, 92
UNION ALL
    SELECT 809, N'Holy Man', 65, 1, 270236, 8818093, 155
UNION ALL
    SELECT 810, N'Hold On', 65, 1, 306860, 10022428, 196
UNION ALL
    SELECT 811, N'Lady Double Dealer', 65, 1, 201482, 6554330, 84
UNION ALL
    SELECT 812, N'You Can''t Do it Right (With the One You Love)', 65, 1, 203755, 6709579, 63
UNION ALL
    SELECT 813, N'High Ball Shooter', 65, 1, 267833, 8772471, 169
UNION ALL
    SELECT 814, N'The Gypsy', 65, 1, 242886, 7946614, 180
UNION ALL
    SELECT 815, N'Soldier Of Fortune', 65, 1, 193750, 6315321, 115
UNION ALL
    SELECT 816, N'The Battle Rages On', 66, 1, 356963, 11626228, 89
UNION ALL
    SELECT 817, N'Lick It Up', 66, 1, 240274, 7792604, 93
UNION ALL
    SELECT 818, N'Anya', 66, 1, 392437, 12754921, 149
UNION ALL
    SELECT 819, N'Talk About Love', 66, 1, 247823, 8072171, 113
UNION ALL
    SELECT 820, N'Time To Kill', 66, 1, 351033, 11354742, 83
UNION ALL
    SELECT 821, N'Ramshackle Man', 66, 1, 334445, 10874679, 140
UNION ALL
    SELECT 822, N'A Twist In The Tail', 66, 1, 257462, 8413103, 168
UNION ALL
    SELECT 823, N'Nasty Piece Of Work', 66, 1, 276662, 9076997, 121
UNION ALL
    SELECT 824, N'Solitaire', 66, 1, 282226, 9157021, 112
UNION ALL
    SELECT 825, N'One Man''s Meat', 66, 1, 278804, 9068960, 57
UNION ALL
    SELECT 826, N'Pour Some Sugar On Me', 67, 1, 292519, 9518842, 61
UNION ALL
    SELECT 827, N'Photograph', 67, 1, 248633, 8108507, 117
UNION ALL
    SELECT 828, N'Love Bites', 67, 1, 346853, 11305791, 65
UNION ALL
    SELECT 829, N'Let''s Get Rocked', 67, 1, 296019, 9724150, 50
UNION ALL
    SELECT 830, N'Two Steps Behind [Acoustic Version]', 67, 1, 259787, 8523388, 150
UNION ALL
    SELECT 831, N'Animal', 67, 1, 244741, 7985133, 78
UNION ALL
    SELECT 832, N'Heaven Is', 67, 1, 214021, 6988128, 138
UNION ALL
    SELECT 833, N'Rocket', 67, 1, 247248, 8092463, 153
UNION ALL
    SELECT 834, N'When Love & Hate Collide', 67, 1, 257280, 8364633, 56
UNION ALL
    SELECT 835, N'Action', 67, 1, 220604, 7130830, 184
UNION ALL
    SELECT 836, N'Make Love Like A Man', 67, 1, 255660, 8309725, 193
UNION ALL
    SELECT 837, N'Armageddon It', 67, 1, 322455, 10522352, 62
UNION ALL
    SELECT 838, N'Have You Ever Needed Someone So Bad', 67, 1, 319320, 10400020, 171
UNION ALL
    SELECT 839, N'Rock Of Ages', 67, 1, 248424, 8150318, 198
UNION ALL
    SELECT 840, N'Hysteria', 67, 1, 355056, 11622738, 157
UNION ALL
    SELECT 841, N'Bringin'' On The Heartbreak', 67, 1, 272457, 8853324, 67
UNION ALL
    SELECT 842, N'Roll Call', 68, 2, 321358, 10653494, 51
UNION ALL
    SELECT 843, N'Otay', 68, 2, 423653, 14176083, 175
UNION ALL
    SELECT 844, N'Groovus Interruptus', 68, 2, 319373, 10602166, 117
UNION ALL
    SELECT 845, N'Paris On Mine', 68, 2, 368875, 12059507, 176
UNION ALL
    SELECT 846, N'In Time', 68, 2, 368953, 12287103, 194
UNION ALL
    SELECT 847, N'Plan B', 68, 2, 272039, 9032315, 166
UNION ALL
    SELECT 848, N'Outbreak', 68, 2, 659226, 21685807, 106
UNION ALL
    SELECT 849, N'Baltimore, DC', 68, 2, 346932, 11394473, 53
UNION ALL
    SELECT 850, N'Talkin Loud and Saying Nothin', 68, 2, 360411, 11994859, 124
UNION ALL
    SELECT 851, N'PГ©tala', 69, 7, 270080, 8856165, 71
UNION ALL
    SELECT 852, N'Meu Bem-Querer', 69, 7, 255608, 8330047, 107
UNION ALL
    SELECT 853, N'Cigano', 69, 7, 304692, 10037362, 69
UNION ALL
    SELECT 854, N'Boa Noite', 69, 7, 338755, 11283582, 96
UNION ALL
    SELECT 855, N'Fato Consumado', 69, 7, 211565, 7018586, 115
UNION ALL
    SELECT 856, N'Faltando Um PedaГ§o', 69, 7, 267728, 8788760, 124
UNION ALL
    SELECT 857, N'ГЃlibi', 69, 7, 213237, 6928434, 71
UNION ALL
    SELECT 858, N'Esquinas', 69, 7, 280999, 9096726, 76
UNION ALL
    SELECT 859, N'Se...', 69, 7, 286432, 9413777, 105
UNION ALL
    SELECT 860, N'Eu Te Devoro', 69, 7, 311614, 10312775, 80
UNION ALL
    SELECT 861, N'LilГЎs', 69, 7, 274181, 9049542, 150
UNION ALL
    SELECT 862, N'Acelerou', 69, 7, 284081, 9396942, 52
UNION ALL
    SELECT 863, N'Um Amor Puro', 69, 7, 327784, 10687311, 77
UNION ALL
    SELECT 864, N'Samurai', 70, 7, 330997, 10872787, 174
UNION ALL
    SELECT 865, N'Nem Um Dia', 70, 7, 337423, 11181446, 106
UNION ALL
    SELECT 866, N'Oceano', 70, 7, 217338, 7026441, 125
UNION ALL
    SELECT 867, N'AГ§ai', 70, 7, 270968, 8893682, 185
UNION ALL
    SELECT 868, N'Serrado', 70, 7, 295314, 9842240, 93
UNION ALL
    SELECT 869, N'Flor De Lis', 70, 7, 236355, 7801108, 93
UNION ALL
    SELECT 870, N'Amar Г‰ Tudo', 70, 7, 211617, 7073899, 84
UNION ALL
    SELECT 871, N'Azul', 70, 7, 253962, 8381029, 96
UNION ALL
    SELECT 872, N'Seduzir', 70, 7, 277524, 9163253, 113
UNION ALL
    SELECT 873, N'A Carta', 70, 7, 347297, 11493463, 67
UNION ALL
    SELECT 874, N'Sina', 70, 7, 268173, 8906539, 67
UNION ALL
    SELECT 875, N'Acelerou', 70, 7, 284133, 9391439, 163
UNION ALL
    SELECT 876, N'Um Amor Puro', 70, 7, 327105, 10664698, 109
UNION ALL
    SELECT 877, N'O BГЄbado e a Equilibrista', 71, 7, 223059, 7306143, 118
UNION ALL
    SELECT 878, N'O Mestre-Sala dos Mares', 71, 7, 186226, 6180414, 177
UNION ALL
    SELECT 879, N'AtrГЎs da Porta', 71, 7, 166608, 5432518, 134
UNION ALL
    SELECT 880, N'Dois Pra LГЎ, Dois Pra CГЎ', 71, 7, 263026, 8684639, 98
UNION ALL
    SELECT 881, N'Casa no Campo', 71, 7, 170788, 5531841, 148
UNION ALL
    SELECT 882, N'Romaria', 71, 7, 242834, 7968525, 87
UNION ALL
    SELECT 883, N'AlГґ, AlГґ, Marciano', 71, 7, 241397, 8137254, 116
UNION ALL
    SELECT 884, N'Me Deixas Louca', 71, 7, 214831, 6888030, 181
UNION ALL
    SELECT 885, N'FascinaГ§ГЈo', 71, 7, 180793, 5793959, 182
UNION ALL
    SELECT 886, N'Saudosa Maloca', 71, 7, 278125, 9059416, 87
UNION ALL
    SELECT 887, N'As AparГЄncias Enganam', 71, 7, 247379, 8014346, 199
UNION ALL
    SELECT 888, N'Madalena', 71, 7, 157387, 5243721, 200
UNION ALL
    SELECT 889, N'Maria Rosa', 71, 7, 232803, 7592504, 186
UNION ALL
    SELECT 890, N'Aprendendo A Jogar', 71, 7, 290664, 9391041, 117
UNION ALL
    SELECT 891, N'Layla', 72, 6, 430733, 14115792, 50
UNION ALL
    SELECT 892, N'Badge', 72, 6, 163552, 5322942, 129
UNION ALL
    SELECT 893, N'I Feel Free', 72, 6, 174576, 5725684, 66
UNION ALL
    SELECT 894, N'Sunshine Of Your Love', 72, 6, 252891, 8225889, 138
UNION ALL
    SELECT 895, N'Crossroads', 72, 6, 253335, 8273540, 127
UNION ALL
    SELECT 896, N'Strange Brew', 72, 6, 167810, 5489787, 117
UNION ALL
    SELECT 897, N'White Room', 72, 6, 301583, 9872606, 153
UNION ALL
    SELECT 898, N'Bell Bottom Blues', 72, 6, 304744, 9946681, 136
UNION ALL
    SELECT 899, N'Cocaine', 72, 6, 215928, 7138399, 156
UNION ALL
    SELECT 900, N'I Shot The Sheriff', 72, 6, 263862, 8738973, 84
UNION ALL
    SELECT 901, N'After Midnight', 72, 6, 191320, 6460941, 167
UNION ALL
    SELECT 902, N'Swing Low Sweet Chariot', 72, 6, 208143, 6896288, 80
UNION ALL
    SELECT 903, N'Lay Down Sally', 72, 6, 231732, 7774207, 197
UNION ALL
    SELECT 904, N'Knockin On Heavens Door', 72, 6, 264411, 8758819, 128
UNION ALL
    SELECT 905, N'Wonderful Tonight', 72, 6, 221387, 7326923, 60
UNION ALL
    SELECT 906, N'Let It Grow', 72, 6, 297064, 9742568, 104
UNION ALL
    SELECT 907, N'Promises', 72, 6, 180401, 6006154, 107
UNION ALL
    SELECT 908, N'I Can''t Stand It', 72, 6, 249730, 8271980, 138
UNION ALL
    SELECT 909, N'Signe', 73, 6, 193515, 6475042, 149
UNION ALL
    SELECT 910, N'Before You Accuse Me', 73, 6, 224339, 7456807, 150
UNION ALL
    SELECT 911, N'Hey Hey', 73, 6, 196466, 6543487, 125
UNION ALL
    SELECT 912, N'Tears In Heaven', 73, 6, 274729, 9032835, 65
UNION ALL
    SELECT 913, N'Lonely Stranger', 73, 6, 328724, 10894406, 198
UNION ALL
    SELECT 914, N'Nobody Knows You When You''re Down & Out', 73, 6, 231836, 7669922, 169
UNION ALL
    SELECT 915, N'Layla', 73, 6, 285387, 9490542, 145
UNION ALL
    SELECT 916, N'Running On Faith', 73, 6, 378984, 12536275, 97
UNION ALL
    SELECT 917, N'Walkin'' Blues', 73, 6, 226429, 7435192, 84
UNION ALL
    SELECT 918, N'Alberta', 73, 6, 222406, 7412975, 51
UNION ALL
    SELECT 919, N'San Francisco Bay Blues', 73, 6, 203363, 6724021, 121
UNION ALL
    SELECT 920, N'Malted Milk', 73, 6, 216528, 7096781, 179
UNION ALL
    SELECT 921, N'Old Love', 73, 6, 472920, 15780747, 58
UNION ALL
    SELECT 922, N'Rollin'' And Tumblin''', 73, 6, 251768, 8407355, 186
UNION ALL
    SELECT 923, N'Collision', 74, 4, 204303, 6656596, 112
UNION ALL
    SELECT 924, N'Stripsearch', 74, 4, 270106, 8861119, 187
UNION ALL
    SELECT 925, N'Last Cup Of Sorrow', 74, 4, 251663, 8221247, 78
UNION ALL
    SELECT 926, N'Naked In Front Of The Computer', 74, 4, 128757, 4225077, 185
UNION ALL
    SELECT 927, N'Helpless', 74, 4, 326217, 10753135, 187
UNION ALL
    SELECT 928, N'Mouth To Mouth', 74, 4, 228493, 7505887, 129
UNION ALL
    SELECT 929, N'Ashes To Ashes', 74, 4, 217391, 7093746, 144
UNION ALL
    SELECT 930, N'She Loves Me Not', 74, 4, 209867, 6887544, 183
UNION ALL
    SELECT 931, N'Got That Feeling', 74, 4, 140852, 4643227, 72
UNION ALL
    SELECT 932, N'Paths Of Glory', 74, 4, 257253, 8436300, 164
UNION ALL
    SELECT 933, N'Home Sick Home', 74, 4, 119040, 3898976, 126
UNION ALL
    SELECT 934, N'Pristina', 74, 4, 232698, 7497361, 94
UNION ALL
    SELECT 935, N'Land Of Sunshine', 75, 4, 223921, 7353567, 62
UNION ALL
    SELECT 936, N'Caffeine', 75, 4, 267937, 8747367, 115
UNION ALL
    SELECT 937, N'Midlife Crisis', 75, 4, 263235, 8628841, 102
UNION ALL
    SELECT 938, N'RV', 75, 4, 223242, 7288162, 132
UNION ALL
    SELECT 939, N'Smaller And Smaller', 75, 4, 310831, 10180103, 162
UNION ALL
    SELECT 940, N'Everything''s Ruined', 75, 4, 273658, 9010917, 188
UNION ALL
    SELECT 941, N'Malpractice', 75, 4, 241371, 7900683, 190
UNION ALL
    SELECT 942, N'Kindergarten', 75, 4, 270680, 8853647, 54
UNION ALL
    SELECT 943, N'Be Aggressive', 75, 4, 222432, 7298027, 141
UNION ALL
    SELECT 944, N'A Small Victory', 75, 4, 297168, 9733572, 133
UNION ALL
    SELECT 945, N'Crack Hitler', 75, 4, 279144, 9162435, 72
UNION ALL
    SELECT 946, N'Jizzlobber', 75, 4, 398341, 12926140, 112
UNION ALL
    SELECT 947, N'Midnight Cowboy', 75, 4, 251924, 8242626, 98
UNION ALL
    SELECT 948, N'Easy', 75, 4, 185835, 6073008, 173
UNION ALL
    SELECT 949, N'Get Out', 76, 1, 137482, 4524972, 159
UNION ALL
    SELECT 950, N'Ricochet', 76, 1, 269400, 8808812, 176
UNION ALL
    SELECT 951, N'Evidence', 76, 1, 293590, 9626136, 114
UNION ALL
    SELECT 952, N'The Gentle Art Of Making Enemies', 76, 1, 209319, 6908609, 159
UNION ALL
    SELECT 953, N'Star A.D.', 76, 1, 203807, 6747658, 154
UNION ALL
    SELECT 954, N'Cuckoo For Caca', 76, 1, 222902, 7388369, 147
UNION ALL
    SELECT 955, N'Caralho Voador', 76, 1, 242102, 8029054, 63
UNION ALL
    SELECT 956, N'Ugly In The Morning', 76, 1, 186435, 6224997, 99
UNION ALL
    SELECT 957, N'Digging The Grave', 76, 1, 185129, 6109259, 87
UNION ALL
    SELECT 958, N'Take This Bottle', 76, 1, 298997, 9779971, 147
UNION ALL
    SELECT 959, N'King For A Day', 76, 1, 395859, 13163733, 99
UNION ALL
    SELECT 960, N'What A Day', 76, 1, 158275, 5203430, 87
UNION ALL
    SELECT 961, N'The Last To Know', 76, 1, 267833, 8736776, 53
UNION ALL
    SELECT 962, N'Just A Man', 76, 1, 336666, 11031254, 82
UNION ALL
    SELECT 963, N'Absolute Zero', 76, 1, 181995, 5929427, 171
UNION ALL
    SELECT 964, N'From Out Of Nowhere', 77, 4, 202527, 6587802, 128
UNION ALL
    SELECT 965, N'Epic', 77, 4, 294008, 9631296, 95
UNION ALL
    SELECT 966, N'Falling To Pieces', 77, 4, 316055, 10333123, 99
UNION ALL
    SELECT 967, N'Surprise! You''re Dead!', 77, 4, 147226, 4823036, 70
UNION ALL
    SELECT 968, N'Zombie Eaters', 77, 4, 360881, 11835367, 196
UNION ALL
    SELECT 969, N'The Real Thing', 77, 4, 493635, 16233080, 120
UNION ALL
    SELECT 970, N'Underwater Love', 77, 4, 231993, 7634387, 184
UNION ALL
    SELECT 971, N'The Morning After', 77, 4, 223764, 7355898, 140
UNION ALL
    SELECT 972, N'Woodpecker From Mars', 77, 4, 340532, 11174250, 171
UNION ALL
    SELECT 973, N'War Pigs', 77, 4, 464770, 15267802, 143
UNION ALL
    SELECT 974, N'Edge Of The World', 77, 4, 250357, 8235607, 99
UNION ALL
    SELECT 975, N'Deixa Entrar', 78, 7, 33619, 1095012, 173
UNION ALL
    SELECT 976, N'Falamansa Song', 78, 7, 237165, 7921313, 170
UNION ALL
    SELECT 977, N'Xote Dos Milagres', 78, 7, 269557, 8897778, 89
UNION ALL
    SELECT 978, N'Rindo ГЂ Toa', 78, 7, 222066, 7365321, 74
UNION ALL
    SELECT 979, N'ConfidГЄncia', 78, 7, 222197, 7460829, 147
UNION ALL
    SELECT 980, N'ForrГі De TГіquio', 78, 7, 169273, 5588756, 156
UNION ALL
    SELECT 981, N'Zeca Violeiro', 78, 7, 143673, 4781949, 150
UNION ALL
    SELECT 982, N'Avisa', 78, 7, 355030, 11844320, 90
UNION ALL
    SELECT 983, N'Principiando/Decolagem', 78, 7, 116767, 3923789, 71
UNION ALL
    SELECT 984, N'Asas', 78, 7, 231915, 7711669, 68
UNION ALL
    SELECT 985, N'Medo De Escuro', 78, 7, 213760, 7056323, 71
UNION ALL
    SELECT 986, N'OraГ§ГЈo', 78, 7, 271072, 9003882, 57
UNION ALL
    SELECT 987, N'Minha Gata', 78, 7, 181838, 6039502, 134
UNION ALL
    SELECT 988, N'Desaforo', 78, 7, 174524, 5853561, 108
UNION ALL
    SELECT 989, N'In Your Honor', 79, 1, 230191, 7468463, 135
UNION ALL
    SELECT 990, N'No Way Back', 79, 1, 196675, 6421400, 171
UNION ALL
    SELECT 991, N'Best Of You', 79, 1, 255712, 8363467, 94
UNION ALL
    SELECT 992, N'DOA', 79, 1, 252186, 8232342, 124
UNION ALL
    SELECT 993, N'Hell', 79, 1, 117080, 3819255, 145
UNION ALL
    SELECT 994, N'The Last Song', 79, 1, 199523, 6496742, 113
UNION ALL
    SELECT 995, N'Free Me', 79, 1, 278700, 9109340, 181
UNION ALL
    SELECT 996, N'Resolve', 79, 1, 288731, 9416186, 132
UNION ALL
    SELECT 997, N'The Deepest Blues Are Black', 79, 1, 238419, 7735473, 65
UNION ALL
    SELECT 998, N'End Over End', 79, 1, 352078, 11395296, 111
UNION ALL
    SELECT 999, N'Still', 80, 1, 313182, 10323157, 114
UNION ALL
    SELECT 1000, N'What If I Do?', 80, 1, 302994, 9929799, 55
UNION ALL
    SELECT 1001, N'Miracle', 80, 1, 209684, 6877994, 193
UNION ALL
    SELECT 1002, N'Another Round', 80, 1, 265848, 8752670, 91
UNION ALL
    SELECT 1003, N'Friend Of A Friend', 80, 1, 193280, 6355088, 73
UNION ALL
    SELECT 1004, N'Over And Out', 80, 1, 316264, 10428382, 97
UNION ALL
    SELECT 1005, N'On The Mend', 80, 1, 271908, 9071997, 63
UNION ALL
    SELECT 1006, N'Virginia Moon', 80, 1, 229198, 7494639, 100
UNION ALL
    SELECT 1007, N'Cold Day In The Sun', 80, 1, 200724, 6596617, 198
UNION ALL
    SELECT 1008, N'Razor', 80, 1, 293276, 9721373, 189
UNION ALL
    SELECT 1009, N'All My Life', 81, 4, 263653, 8665545, 142
UNION ALL
    SELECT 1010, N'Low', 81, 4, 268120, 8847196, 76
UNION ALL
    SELECT 1011, N'Have It All', 81, 4, 298057, 9729292, 91
UNION ALL
    SELECT 1012, N'Times Like These', 81, 4, 266370, 8624691, 137
UNION ALL
    SELECT 1013, N'Disenchanted Lullaby', 81, 4, 273528, 8919111, 173
UNION ALL
    SELECT 1014, N'Tired Of You', 81, 4, 311353, 10094743, 150
UNION ALL
    SELECT 1015, N'Halo', 81, 4, 306442, 10026371, 57
UNION ALL
    SELECT 1016, N'Lonely As You', 81, 4, 277185, 9022628, 62
UNION ALL
    SELECT 1017, N'Overdrive', 81, 4, 270550, 8793187, 81
UNION ALL
    SELECT 1018, N'Burn Away', 81, 4, 298396, 9678073, 102
UNION ALL
    SELECT 1019, N'Come Back', 81, 4, 469968, 15371980, 175
UNION ALL
    SELECT 1020, N'Doll', 82, 1, 83487, 2702572, 127
UNION ALL
    SELECT 1021, N'Monkey Wrench', 82, 1, 231523, 7527531, 198
UNION ALL
    SELECT 1022, N'Hey, Johnny Park!', 82, 1, 248528, 8079480, 192
UNION ALL
    SELECT 1023, N'My Poor Brain', 82, 1, 213446, 6973746, 117
UNION ALL
    SELECT 1024, N'Wind Up', 82, 1, 152163, 4950667, 95
UNION ALL
    SELECT 1025, N'Up In Arms', 82, 1, 135732, 4406227, 136
UNION ALL
    SELECT 1026, N'My Hero', 82, 1, 260101, 8472365, 53
UNION ALL
    SELECT 1027, N'See You', 82, 1, 146782, 4888173, 72
UNION ALL
    SELECT 1028, N'Enough Space', 82, 1, 157387, 5169280, 148
UNION ALL
    SELECT 1029, N'February Stars', 82, 1, 289306, 9344875, 155
UNION ALL
    SELECT 1030, N'Everlong', 82, 1, 250749, 8270816, 59
UNION ALL
    SELECT 1031, N'Walking After You', 82, 1, 303856, 9898992, 183
UNION ALL
    SELECT 1032, N'New Way Home', 82, 1, 342230, 11205664, 88
UNION ALL
    SELECT 1033, N'My Way', 83, 12, 275879, 8928684, 172
UNION ALL
    SELECT 1034, N'Strangers In The Night', 83, 12, 155794, 5055295, 87
UNION ALL
    SELECT 1035, N'New York, New York', 83, 12, 206001, 6707993, 143
UNION ALL
    SELECT 1036, N'I Get A Kick Out Of You', 83, 12, 194429, 6332441, 183
UNION ALL
    SELECT 1037, N'Something Stupid', 83, 12, 158615, 5210643, 128
UNION ALL
    SELECT 1038, N'Moon River', 83, 12, 198922, 6395808, 197
UNION ALL
    SELECT 1039, N'What Now My Love', 83, 12, 149995, 4913383, 122
UNION ALL
    SELECT 1040, N'Summer Love', 83, 12, 174994, 5693242, 197
UNION ALL
    SELECT 1041, N'For Once In My Life', 83, 12, 171154, 5557537, 195
UNION ALL
    SELECT 1042, N'Love And Marriage', 83, 12, 89730, 2930596, 168
UNION ALL
    SELECT 1043, N'They Can''t Take That Away From Me', 83, 12, 161227, 5240043, 59
UNION ALL
    SELECT 1044, N'My Kind Of Town', 83, 12, 188499, 6119915, 76
UNION ALL
    SELECT 1045, N'Fly Me To The Moon', 83, 12, 149263, 4856954, 119
UNION ALL
    SELECT 1046, N'I''ve Got You Under My Skin', 83, 12, 210808, 6883787, 148
UNION ALL
    SELECT 1047, N'The Best Is Yet To Come', 83, 12, 173583, 5633730, 70
UNION ALL
    SELECT 1048, N'It Was A Very Good Year', 83, 12, 266605, 8554066, 136
UNION ALL
    SELECT 1049, N'Come Fly With Me', 83, 12, 190458, 6231029, 158
UNION ALL
    SELECT 1050, N'That''s Life', 83, 12, 187010, 6095727, 104
UNION ALL
    SELECT 1051, N'The Girl From Ipanema', 83, 12, 193750, 6410674, 134
UNION ALL
    SELECT 1052, N'The Lady Is A Tramp', 83, 12, 184111, 5987372, 127
UNION ALL
    SELECT 1053, N'Bad, Bad Leroy Brown', 83, 12, 169900, 5548581, 147
UNION ALL
    SELECT 1054, N'Mack The Knife', 83, 12, 292075, 9541052, 89
UNION ALL
    SELECT 1055, N'Loves Been Good To Me', 83, 12, 203964, 6645365, 113
UNION ALL
    SELECT 1056, N'L.A. Is My Lady', 83, 12, 193175, 6378511, 192
UNION ALL
    SELECT 1057, N'Entrando Na Sua (Intro)', 84, 7, 179252, 5840027, 53
UNION ALL
    SELECT 1058, N'Nervosa', 84, 7, 229537, 7680421, 64
UNION ALL
    SELECT 1059, N'Funk De Bamba (Com Fernanda Abreu)', 84, 7, 237191, 7866165, 126
UNION ALL
    SELECT 1060, N'Call Me At CleoВґs', 84, 7, 236617, 7920510, 124
UNION ALL
    SELECT 1061, N'Olhos Coloridos (Com Sandra De SГЎ)', 84, 7, 321332, 10567404, 84
UNION ALL
    SELECT 1062, N'ZambaГ§ГЈo', 84, 7, 301113, 10030604, 179
UNION ALL
    SELECT 1063, N'Funk Hum', 84, 7, 244453, 8084475, 97
UNION ALL
    SELECT 1064, N'Forty Days (Com DJ Hum)', 84, 7, 221727, 7347172, 147
UNION ALL
    SELECT 1065, N'Balada Da Paula', 84, 7, 322821, 10603717, 166
UNION ALL
    SELECT 1066, N'Dujji', 84, 7, 324597, 10833935, 121
UNION ALL
    SELECT 1067, N'Meu Guarda-Chuva', 84, 7, 248528, 8216625, 143
UNION ALL
    SELECT 1068, N'MotГ©is', 84, 7, 213498, 7041077, 147
UNION ALL
    SELECT 1069, N'Whistle Stop', 84, 7, 526132, 17533664, 140
UNION ALL
    SELECT 1070, N'16 Toneladas', 84, 7, 191634, 6390885, 178
UNION ALL
    SELECT 1071, N'Divirta-Se (Saindo Da Sua)', 84, 7, 74919, 2439206, 138
UNION ALL
    SELECT 1072, N'Forty Days Instrumental', 84, 7, 292493, 9584317, 97
UNION ALL
    SELECT 1073, N'Г“ia Eu Aqui De Novo', 85, 10, 219454, 7469735, 108
UNION ALL
    SELECT 1074, N'BaiГЈo Da Penha', 85, 10, 247928, 8393047, 56
UNION ALL
    SELECT 1075, N'Esperando Na Janela', 85, 10, 261041, 8660617, 50
UNION ALL
    SELECT 1076, N'Juazeiro', 85, 10, 222275, 7349779, 79
UNION ALL
    SELECT 1077, N'Гљltimo Pau-De-Arara', 85, 10, 200437, 6638563, 71
UNION ALL
    SELECT 1078, N'Asa Branca', 85, 10, 217051, 7387183, 87
UNION ALL
    SELECT 1079, N'Qui Nem JilГі', 85, 10, 204695, 6937472, 153
UNION ALL
    SELECT 1080, N'Assum Preto', 85, 10, 199653, 6625000, 70
UNION ALL
    SELECT 1081, N'Pau-De-Arara', 85, 10, 191660, 6340649, 147
UNION ALL
    SELECT 1082, N'A Volta Da Asa Branca', 85, 10, 271020, 9098093, 119
UNION ALL
    SELECT 1083, N'O Amor Daqui De Casa', 85, 10, 148636, 4888292, 64
UNION ALL
    SELECT 1084, N'As Pegadas Do Amor', 85, 10, 209136, 6899062, 179
UNION ALL
    SELECT 1085, N'Lamento Sertanejo', 85, 10, 260963, 8518290, 180
UNION ALL
    SELECT 1086, N'Casinha Feliz', 85, 10, 32287, 1039615, 65
UNION ALL
    SELECT 1087, N'IntroduГ§ГЈo (Live)', 86, 7, 154096, 5227579, 53
UNION ALL
    SELECT 1088, N'Palco (Live)', 86, 7, 238315, 8026622, 91
UNION ALL
    SELECT 1089, N'Is This Love (Live)', 86, 7, 295262, 9819759, 146
UNION ALL
    SELECT 1090, N'Stir It Up (Live)', 86, 7, 282409, 9594738, 196
UNION ALL
    SELECT 1091, N'Refavela (Live)', 86, 7, 236695, 7985305, 128
UNION ALL
    SELECT 1092, N'Vendedor De Caranguejo (Live)', 86, 7, 248842, 8358128, 82
UNION ALL
    SELECT 1093, N'Quanta (Live)', 86, 7, 357485, 11774865, 167
UNION ALL
    SELECT 1094, N'Estrela (Live)', 86, 7, 285309, 9436411, 159
UNION ALL
    SELECT 1095, N'Pela Internet (Live)', 86, 7, 263471, 8804401, 70
UNION ALL
    SELECT 1096, N'CГ©rebro EletrГґnico (Live)', 86, 7, 231627, 7805352, 186
UNION ALL
    SELECT 1097, N'OpachorГґ (Live)', 86, 7, 259526, 8596384, 50
UNION ALL
    SELECT 1098, N'Copacabana (Live)', 86, 7, 289671, 9673672, 88
UNION ALL
    SELECT 1099, N'A Novidade (Live)', 86, 7, 316969, 10508000, 116
UNION ALL
    SELECT 1100, N'Ghandi (Live)', 86, 7, 222458, 7481950, 63
UNION ALL
    SELECT 1101, N'De Ouro E Marfim (Live)', 86, 7, 234971, 7838453, 163
UNION ALL
    SELECT 1102, N'Doce De Carnaval (Candy All)', 87, 2, 356101, 11998470, 133
UNION ALL
    SELECT 1103, N'Lamento De Carnaval', 87, 2, 294530, 9819276, 142
UNION ALL
    SELECT 1104, N'Pretinha', 87, 2, 265273, 8914579, 184
UNION ALL
    SELECT 1105, N'A Novidade', 73, 7, 324780, 10765600, 117
UNION ALL
    SELECT 1106, N'Tenho Sede', 73, 7, 261616, 8708114, 101
UNION ALL
    SELECT 1107, N'Refazenda', 73, 7, 218305, 7237784, 118
UNION ALL
    SELECT 1108, N'Realce', 73, 7, 264489, 8847612, 163
UNION ALL
    SELECT 1109, N'EsotГ©rico', 73, 7, 317779, 10530533, 100
UNION ALL
    SELECT 1110, N'DrГЈo', 73, 7, 301453, 9931950, 58
UNION ALL
    SELECT 1111, N'A Paz', 73, 7, 293093, 9593064, 146
UNION ALL
    SELECT 1112, N'Beira Mar', 73, 7, 295444, 9597994, 51
UNION ALL
    SELECT 1113, N'Sampa', 73, 7, 225697, 7469905, 67
UNION ALL
    SELECT 1114, N'ParabolicamarГЎ', 73, 7, 284943, 9543435, 100
UNION ALL
    SELECT 1115, N'Tempo Rei', 73, 7, 302733, 10019269, 149
UNION ALL
    SELECT 1116, N'Expresso 2222', 73, 7, 284760, 9690577, 52
UNION ALL
    SELECT 1117, N'Aquele AbraГ§o', 73, 7, 263993, 8805003, 147
UNION ALL
    SELECT 1118, N'Palco', 73, 7, 270550, 9049901, 121
UNION ALL
    SELECT 1119, N'Toda Menina Baiana', 73, 7, 278177, 9351000, 82
UNION ALL
    SELECT 1120, N'SГ­tio Do Pica-Pau Amarelo', 73, 7, 218070, 7217955, 185
UNION ALL
    SELECT 1121, N'Straight Out Of Line', 88, 3, 259213, 8511877, 106
UNION ALL
    SELECT 1122, N'Faceless', 88, 3, 216006, 6992417, 178
UNION ALL
    SELECT 1123, N'Changes', 88, 3, 260022, 8455835, 119
UNION ALL
    SELECT 1124, N'Make Me Believe', 88, 3, 248607, 8075050, 70
UNION ALL
    SELECT 1125, N'I Stand Alone', 88, 3, 246125, 8017041, 170
UNION ALL
    SELECT 1126, N'Re-Align', 88, 3, 260884, 8513891, 101
UNION ALL
    SELECT 1127, N'I Fucking Hate You', 88, 3, 247170, 8059642, 120
UNION ALL
    SELECT 1128, N'Releasing The Demons', 88, 3, 252760, 8276372, 192
UNION ALL
    SELECT 1129, N'Dead And Broken', 88, 3, 251454, 8206611, 70
UNION ALL
    SELECT 1130, N'I Am', 88, 3, 239516, 7803270, 107
UNION ALL
    SELECT 1131, N'The Awakening', 88, 3, 89547, 3035251, 192
UNION ALL
    SELECT 1132, N'Serenity', 88, 3, 274834, 9172976, 128
UNION ALL
    SELECT 1133, N'American Idiot', 89, 4, 174419, 5705793, 146
UNION ALL
    SELECT 1134, N'Jesus Of Suburbia / City Of The Damned / I Don''t Care / Dearly Beloved / Tales Of Another Broken Home', 89, 4, 548336, 17875209, 199
UNION ALL
    SELECT 1135, N'Holiday', 89, 4, 232724, 7599602, 190
UNION ALL
    SELECT 1136, N'Boulevard Of Broken Dreams', 89, 4, 260858, 8485122, 59
UNION ALL
    SELECT 1137, N'Are We The Waiting', 89, 4, 163004, 5328329, 60
UNION ALL
    SELECT 1138, N'St. Jimmy', 89, 4, 175307, 5716589, 92
UNION ALL
    SELECT 1139, N'Give Me Novacaine', 89, 4, 205871, 6752485, 180
UNION ALL
    SELECT 1140, N'She''s A Rebel', 89, 4, 120528, 3901226, 87
UNION ALL
    SELECT 1141, N'Extraordinary Girl', 89, 4, 214021, 6975177, 175
UNION ALL
    SELECT 1142, N'Letterbomb', 89, 4, 246151, 7980902, 74
UNION ALL
    SELECT 1143, N'Wake Me Up When September Ends', 89, 4, 285753, 9325597, 89
UNION ALL
    SELECT 1144, N'Homecoming / The Death Of St. Jimmy / East 12th St. / Nobody Likes You / Rock And Roll Girlfriend / We''re Coming Home Again', 89, 4, 558602, 18139840, 110
UNION ALL
    SELECT 1145, N'Whatsername', 89, 4, 252316, 8244843, 163
UNION ALL
    SELECT 1146, N'Welcome to the Jungle', 90, 1, 273552, 4538451, 131
UNION ALL
    SELECT 1147, N'It''s So Easy', 90, 1, 202824, 3394019, 175
UNION ALL
    SELECT 1148, N'Nightrain', 90, 1, 268537, 4457283, 108
UNION ALL
    SELECT 1149, N'Out Ta Get Me', 90, 1, 263893, 4382147, 68
UNION ALL
    SELECT 1150, N'Mr. Brownstone', 90, 1, 228924, 3816323, 178
UNION ALL
    SELECT 1151, N'Paradise City', 90, 1, 406347, 6687123, 100
UNION ALL
    SELECT 1152, N'My Michelle', 90, 1, 219961, 3671299, 142
UNION ALL
    SELECT 1153, N'Think About You', 90, 1, 231640, 3860275, 73
UNION ALL
    SELECT 1154, N'Sweet Child O'' Mine', 90, 1, 356424, 5879347, 69
UNION ALL
    SELECT 1155, N'You''re Crazy', 90, 1, 197135, 3301971, 91
UNION ALL
    SELECT 1156, N'Anything Goes', 90, 1, 206400, 3451891, 148
UNION ALL
    SELECT 1157, N'Rocket Queen', 90, 1, 375349, 6185539, 56
UNION ALL
    SELECT 1158, N'Right Next Door to Hell', 91, 1, 182321, 3175950, 142
UNION ALL
    SELECT 1159, N'Dust N'' Bones', 91, 1, 298374, 5053742, 104
UNION ALL
    SELECT 1160, N'Live and Let Die', 91, 1, 184016, 3203390, 167
UNION ALL
    SELECT 1161, N'Don''t Cry (Original)', 91, 1, 284744, 4833259, 194
UNION ALL
    SELECT 1162, N'Perfect Crime', 91, 1, 143637, 2550030, 62
UNION ALL
    SELECT 1163, N'You Ain''t the First', 91, 1, 156268, 2754414, 151
UNION ALL
    SELECT 1164, N'Bad Obsession', 91, 1, 328282, 5537678, 172
UNION ALL
    SELECT 1165, N'Back off Bitch', 91, 1, 303436, 5135662, 188
UNION ALL
    SELECT 1166, N'Double Talkin'' Jive', 91, 1, 203637, 3520862, 145
UNION ALL
    SELECT 1167, N'November Rain', 91, 1, 537540, 8923566, 90
UNION ALL
    SELECT 1168, N'The Garden', 91, 1, 322175, 5438862, 77
UNION ALL
    SELECT 1169, N'Garden of Eden', 91, 1, 161539, 2839694, 140
UNION ALL
    SELECT 1170, N'Don''t Damn Me', 91, 1, 318901, 5385886, 77
UNION ALL
    SELECT 1171, N'Bad Apples', 91, 1, 268351, 4567966, 192
UNION ALL
    SELECT 1172, N'Dead Horse', 91, 1, 257600, 4394014, 119
UNION ALL
    SELECT 1173, N'Coma', 91, 1, 616511, 10201342, 177
UNION ALL
    SELECT 1174, N'Civil War', 92, 3, 461165, 15046579, 129
UNION ALL
    SELECT 1175, N'14 Years', 92, 3, 261355, 8543664, 61
UNION ALL
    SELECT 1176, N'Yesterdays', 92, 3, 196205, 6398489, 195
UNION ALL
    SELECT 1177, N'Knockin'' On Heaven''s Door', 92, 3, 336457, 10986716, 67
UNION ALL
    SELECT 1178, N'Get In The Ring', 92, 3, 341054, 11134105, 129
UNION ALL
    SELECT 1179, N'Shotgun Blues', 92, 3, 203206, 6623916, 153
UNION ALL
    SELECT 1180, N'Breakdown', 92, 3, 424960, 13978284, 83
UNION ALL
    SELECT 1181, N'Pretty Tied Up', 92, 3, 287477, 9408754, 142
UNION ALL
    SELECT 1182, N'Locomotive', 92, 3, 522396, 17236842, 180
UNION ALL
    SELECT 1183, N'So Fine', 92, 3, 246491, 8039484, 69
UNION ALL
    SELECT 1184, N'Estranged', 92, 3, 563800, 18343787, 116
UNION ALL
    SELECT 1185, N'You Could Be Mine', 92, 3, 343875, 11207355, 58
UNION ALL
    SELECT 1186, N'Don''t Cry', 92, 3, 284238, 9222458, 174
UNION ALL
    SELECT 1187, N'My World', 92, 3, 84532, 2764045, 158
UNION ALL
    SELECT 1188, N'Colibri', 93, 2, 361012, 12055329, 175
UNION ALL
    SELECT 1189, N'Love Is The Colour', 93, 2, 251585, 8419165, 139
UNION ALL
    SELECT 1190, N'Magnetic Ocean', 93, 2, 321123, 10720741, 192
UNION ALL
    SELECT 1191, N'Deep Waters', 93, 2, 396460, 13075359, 57
UNION ALL
    SELECT 1192, N'L''Arc En Ciel De Miles', 93, 2, 242390, 8053997, 109
UNION ALL
    SELECT 1193, N'Gypsy', 93, 2, 330997, 11083374, 150
UNION ALL
    SELECT 1194, N'Journey Into Sunlight', 93, 2, 249756, 8241177, 93
UNION ALL
    SELECT 1195, N'Sunchild', 93, 2, 259970, 8593143, 89
UNION ALL
    SELECT 1196, N'Millenium', 93, 2, 379167, 12511939, 80
UNION ALL
    SELECT 1197, N'Thinking ''Bout Tomorrow', 93, 2, 355395, 11865384, 123
UNION ALL
    SELECT 1198, N'Jacob''s Ladder', 93, 2, 367647, 12201595, 144
UNION ALL
    SELECT 1199, N'She Wears Black', 93, 2, 528666, 17617944, 123
UNION ALL
    SELECT 1200, N'Dark Side Of The Cog', 93, 2, 377155, 12491122, 108
UNION ALL
    SELECT 1201, N'Different World', 94, 1, 258692, 4383764, 194
UNION ALL
    SELECT 1202, N'These Colours Don''t Run', 94, 1, 412152, 6883500, 73
UNION ALL
    SELECT 1203, N'Brighter Than a Thousand Suns', 94, 1, 526255, 8721490, 59
UNION ALL
    SELECT 1204, N'The Pilgrim', 94, 1, 307593, 5172144, 96
UNION ALL
    SELECT 1205, N'The Longest Day', 94, 1, 467810, 7785748, 89
UNION ALL
    SELECT 1206, N'Out of the Shadows', 94, 1, 336896, 5647303, 149
UNION ALL
    SELECT 1207, N'The Reincarnation of Benjamin Breeg', 94, 1, 442106, 7367736, 134
UNION ALL
    SELECT 1208, N'For the Greater Good of God', 94, 1, 564893, 9367328, 118
UNION ALL
    SELECT 1209, N'Lord of Light', 94, 1, 444614, 7393698, 81
UNION ALL
    SELECT 1210, N'The Legacy', 94, 1, 562966, 9314287, 165
UNION ALL
    SELECT 1211, N'Hallowed Be Thy Name (Live) [Non Album Bonus Track]', 94, 1, 431262, 7205816, 78
UNION ALL
    SELECT 1212, N'The Number Of The Beast', 95, 3, 294635, 4718897, 141
UNION ALL
    SELECT 1213, N'The Trooper', 95, 3, 235311, 3766272, 150
UNION ALL
    SELECT 1214, N'Prowler', 95, 3, 255634, 4091904, 60
UNION ALL
    SELECT 1215, N'Transylvania', 95, 3, 265874, 4255744, 118
UNION ALL
    SELECT 1216, N'Remember Tomorrow', 95, 3, 352731, 5648438, 78
UNION ALL
    SELECT 1217, N'Where Eagles Dare', 95, 3, 289358, 4630528, 199
UNION ALL
    SELECT 1218, N'Sanctuary', 95, 3, 293250, 4694016, 191
UNION ALL
    SELECT 1219, N'Running Free', 95, 3, 228937, 3663872, 196
UNION ALL
    SELECT 1220, N'Run To The Hilss', 95, 3, 237557, 3803136, 156
UNION ALL
    SELECT 1221, N'2 Minutes To Midnight', 95, 3, 337423, 5400576, 175
UNION ALL
    SELECT 1222, N'Iron Maiden', 95, 3, 324623, 5195776, 170
UNION ALL
    SELECT 1223, N'Hallowed Be Thy Name', 95, 3, 471849, 7550976, 198
UNION ALL
    SELECT 1224, N'Be Quick Or Be Dead', 96, 3, 196911, 3151872, 52
UNION ALL
    SELECT 1225, N'From Here To Eternity', 96, 3, 259866, 4159488, 65
UNION ALL
    SELECT 1226, N'Can I Play With Madness', 96, 3, 282488, 4521984, 147
UNION ALL
    SELECT 1227, N'Wasting Love', 96, 3, 347846, 5566464, 54
UNION ALL
    SELECT 1228, N'Tailgunner', 96, 3, 249469, 3993600, 164
UNION ALL
    SELECT 1229, N'The Evil That Men Do', 96, 3, 325929, 5216256, 60
UNION ALL
    SELECT 1230, N'Afraid To Shoot Strangers', 96, 3, 407980, 6529024, 145
UNION ALL
    SELECT 1231, N'Bring Your Daughter... To The Slaughter', 96, 3, 317727, 5085184, 74
UNION ALL
    SELECT 1232, N'Heaven Can Wait', 96, 3, 448574, 7178240, 62
UNION ALL
    SELECT 1233, N'The Clairvoyant', 96, 3, 269871, 4319232, 197
UNION ALL
    SELECT 1234, N'Fear Of The Dark', 96, 3, 431333, 6906078, 185
UNION ALL
    SELECT 1235, N'The Wicker Man', 97, 1, 275539, 11022464, 149
UNION ALL
    SELECT 1236, N'Ghost Of The Navigator', 97, 1, 410070, 16404608, 60
UNION ALL
    SELECT 1237, N'Brave New World', 97, 1, 378984, 15161472, 127
UNION ALL
    SELECT 1238, N'Blood Brothers', 97, 1, 434442, 17379456, 133
UNION ALL
    SELECT 1239, N'The Mercenary', 97, 1, 282488, 11300992, 142
UNION ALL
    SELECT 1240, N'Dream Of Mirrors', 97, 1, 561162, 22448256, 171
UNION ALL
    SELECT 1241, N'The Fallen Angel', 97, 1, 240718, 9629824, 66
UNION ALL
    SELECT 1242, N'The Nomad', 97, 1, 546115, 21846144, 107
UNION ALL
    SELECT 1243, N'Out Of The Silent Planet', 97, 1, 385541, 15423616, 109
UNION ALL
    SELECT 1244, N'The Thin Line Between Love & Hate', 97, 1, 506801, 20273280, 190
UNION ALL
    SELECT 1245, N'Wildest Dreams', 98, 13, 232777, 9312384, 198
UNION ALL
    SELECT 1246, N'Rainmaker', 98, 13, 228623, 9146496, 137
UNION ALL
    SELECT 1247, N'No More Lies', 98, 13, 441782, 17672320, 140
UNION ALL
    SELECT 1248, N'Montsegur', 98, 13, 350484, 14020736, 63
UNION ALL
    SELECT 1249, N'Dance Of Death', 98, 13, 516649, 20670727, 123
UNION ALL
    SELECT 1250, N'Gates Of Tomorrow', 98, 13, 312032, 12482688, 69
UNION ALL
    SELECT 1251, N'New Frontier', 98, 13, 304509, 12181632, 135
UNION ALL
    SELECT 1252, N'Paschendale', 98, 13, 508107, 20326528, 181
UNION ALL
    SELECT 1253, N'Face In The Sand', 98, 13, 391105, 15648948, 194
UNION ALL
    SELECT 1254, N'Age Of Innocence', 98, 13, 370468, 14823478, 78
UNION ALL
    SELECT 1255, N'Journeyman', 98, 13, 427023, 17082496, 85
UNION ALL
    SELECT 1256, N'Be Quick Or Be Dead', 99, 1, 204512, 8181888, 192
UNION ALL
    SELECT 1257, N'From Here To Eternity', 99, 1, 218357, 8739038, 117
UNION ALL
    SELECT 1258, N'Afraid To Shoot Strangers', 99, 1, 416496, 16664589, 139
UNION ALL
    SELECT 1259, N'Fear Is The Key', 99, 1, 335307, 13414528, 197
UNION ALL
    SELECT 1260, N'Childhood''s End', 99, 1, 280607, 11225216, 82
UNION ALL
    SELECT 1261, N'Wasting Love', 99, 1, 350981, 14041216, 143
UNION ALL
    SELECT 1262, N'The Fugitive', 99, 1, 294112, 11765888, 80
UNION ALL
    SELECT 1263, N'Chains Of Misery', 99, 1, 217443, 8700032, 105
UNION ALL
    SELECT 1264, N'The Apparition', 99, 1, 234605, 9386112, 180
UNION ALL
    SELECT 1265, N'Judas Be My Guide', 99, 1, 188786, 7553152, 185
UNION ALL
    SELECT 1266, N'Weekend Warrior', 99, 1, 339748, 13594678, 98
UNION ALL
    SELECT 1267, N'Fear Of The Dark', 99, 1, 436976, 17483789, 111
UNION ALL
    SELECT 1268, N'01 - Prowler', 100, 6, 236173, 5668992, 115
UNION ALL
    SELECT 1269, N'02 - Sanctuary', 100, 6, 196284, 4712576, 72
UNION ALL
    SELECT 1270, N'03 - Remember Tomorrow', 100, 6, 328620, 7889024, 62
UNION ALL
    SELECT 1271, N'04 - Running Free', 100, 6, 197276, 4739122, 101
UNION ALL
    SELECT 1272, N'05 - Phantom of the Opera', 100, 6, 428016, 10276872, 135
UNION ALL
    SELECT 1273, N'06 - Transylvania', 100, 6, 259343, 6226048, 83
UNION ALL
    SELECT 1274, N'07 - Strange World', 100, 6, 332460, 7981184, 59
UNION ALL
    SELECT 1275, N'08 - Charlotte the Harlot', 100, 6, 252708, 6066304, 194
UNION ALL
    SELECT 1276, N'09 - Iron Maiden', 100, 6, 216058, 5189891, 176
UNION ALL
    SELECT 1277, N'The Ides Of March', 101, 13, 105926, 2543744, 151
UNION ALL
    SELECT 1278, N'Wrathchild', 101, 13, 174471, 4188288, 198
UNION ALL
    SELECT 1279, N'Murders In The Rue Morgue', 101, 13, 258377, 6205786, 145
UNION ALL
    SELECT 1280, N'Another Life', 101, 13, 203049, 4874368, 75
UNION ALL
    SELECT 1281, N'Genghis Khan', 101, 13, 187141, 4493440, 104
UNION ALL
    SELECT 1282, N'Innocent Exile', 101, 13, 232515, 5584861, 127
UNION ALL
    SELECT 1283, N'Killers', 101, 13, 300956, 7227440, 80
UNION ALL
    SELECT 1284, N'Prodigal Son', 101, 13, 372349, 8937600, 155
UNION ALL
    SELECT 1285, N'Purgatory', 101, 13, 200150, 4804736, 69
UNION ALL
    SELECT 1286, N'Drifter', 101, 13, 288757, 6934660, 121
UNION ALL
    SELECT 1287, N'Intro- Churchill S Speech', 102, 13, 48013, 1154488, 188
UNION ALL
    SELECT 1288, N'Aces High', 102, 13, 276375, 6635187, 122
UNION ALL
    SELECT 1289, N'2 Minutes To Midnight', 102, 3, 366550, 8799380, 181
UNION ALL
    SELECT 1290, N'The Trooper', 102, 3, 268878, 6455255, 121
UNION ALL
    SELECT 1291, N'Revelations', 102, 3, 371826, 8926021, 127
UNION ALL
    SELECT 1292, N'Flight Of Icarus', 102, 3, 229982, 5521744, 198
UNION ALL
    SELECT 1293, N'Rime Of The Ancient Mariner', 102, 3, 789472, 18949518, 194
UNION ALL
    SELECT 1294, N'Powerslave', 102, 3, 454974, 10921567, 139
UNION ALL
    SELECT 1295, N'The Number Of The Beast', 102, 3, 275121, 6605094, 53
UNION ALL
    SELECT 1296, N'Hallowed Be Thy Name', 102, 3, 451422, 10836304, 110
UNION ALL
    SELECT 1297, N'Iron Maiden', 102, 3, 261955, 6289117, 79
UNION ALL
    SELECT 1298, N'Run To The Hills', 102, 3, 231627, 5561241, 193
UNION ALL
    SELECT 1299, N'Running Free', 102, 3, 204617, 4912986, 101
UNION ALL
    SELECT 1300, N'Wrathchild', 102, 13, 183666, 4410181, 103
UNION ALL
    SELECT 1301, N'Acacia Avenue', 102, 13, 379872, 9119118, 165
UNION ALL
    SELECT 1302, N'Children Of The Damned', 102, 13, 278177, 6678446, 190
UNION ALL
    SELECT 1303, N'Die With Your Boots On', 102, 13, 314174, 7542367, 93
UNION ALL
    SELECT 1304, N'Phantom Of The Opera', 102, 13, 441155, 10589917, 187
UNION ALL
    SELECT 1305, N'Be Quick Or Be Dead', 103, 1, 233142, 5599853, 60
UNION ALL
    SELECT 1306, N'The Number Of The Beast', 103, 1, 294008, 7060625, 126
UNION ALL
    SELECT 1307, N'Wrathchild', 103, 1, 174106, 4182963, 143
UNION ALL
    SELECT 1308, N'From Here To Eternity', 103, 1, 284447, 6831163, 100
UNION ALL
    SELECT 1309, N'Can I Play With Madness', 103, 1, 213106, 5118995, 75
UNION ALL
    SELECT 1310, N'Wasting Love', 103, 1, 336953, 8091301, 170
UNION ALL
    SELECT 1311, N'Tailgunner', 103, 1, 247640, 5947795, 153
UNION ALL
    SELECT 1312, N'The Evil That Men Do', 103, 1, 478145, 11479913, 167
UNION ALL
    SELECT 1313, N'Afraid To Shoot Strangers', 103, 1, 412525, 9905048, 65
UNION ALL
    SELECT 1314, N'Fear Of The Dark', 103, 1, 431542, 10361452, 69
UNION ALL
    SELECT 1315, N'Bring Your Daughter... To The Slaughter...', 104, 1, 376711, 9045532, 112
UNION ALL
    SELECT 1316, N'The Clairvoyant', 104, 1, 262426, 6302648, 118
UNION ALL
    SELECT 1317, N'Heaven Can Wait', 104, 1, 440555, 10577743, 182
UNION ALL
    SELECT 1318, N'Run To The Hills', 104, 1, 235859, 5665052, 189
UNION ALL
    SELECT 1319, N'2 Minutes To Midnight', 104, 1, 338233, 8122030, 182
UNION ALL
    SELECT 1320, N'Iron Maiden', 104, 1, 494602, 11874875, 81
UNION ALL
    SELECT 1321, N'Hallowed Be Thy Name', 104, 1, 447791, 10751410, 75
UNION ALL
    SELECT 1322, N'The Trooper', 104, 1, 232672, 5588560, 180
UNION ALL
    SELECT 1323, N'Sanctuary', 104, 1, 318511, 7648679, 65
UNION ALL
    SELECT 1324, N'Running Free', 104, 1, 474017, 11380851, 58
UNION ALL
    SELECT 1325, N'Tailgunner', 105, 3, 255582, 4089856, 164
UNION ALL
    SELECT 1326, N'Holy Smoke', 105, 3, 229459, 3672064, 56
UNION ALL
    SELECT 1327, N'No Prayer For The Dying', 105, 3, 263941, 4225024, 159
UNION ALL
    SELECT 1328, N'Public Enema Number One', 105, 3, 254197, 4071587, 136
UNION ALL
    SELECT 1329, N'Fates Warning', 105, 3, 250853, 4018088, 154
UNION ALL
    SELECT 1330, N'The Assassin', 105, 3, 258768, 4141056, 72
UNION ALL
    SELECT 1331, N'Run Silent Run Deep', 105, 3, 275408, 4407296, 175
UNION ALL
    SELECT 1332, N'Hooks In You', 105, 3, 247510, 3960832, 82
UNION ALL
    SELECT 1333, N'Bring Your Daughter... ...To The Slaughter', 105, 3, 284238, 4548608, 59
UNION ALL
    SELECT 1334, N'Mother Russia', 105, 3, 332617, 5322752, 179
UNION ALL
    SELECT 1335, N'Where Eagles Dare', 106, 3, 369554, 5914624, 108
UNION ALL
    SELECT 1336, N'Revelations', 106, 3, 408607, 6539264, 183
UNION ALL
    SELECT 1337, N'Flight Of The Icarus', 106, 3, 230269, 3686400, 97
UNION ALL
    SELECT 1338, N'Die With Your Boots On', 106, 3, 325694, 5212160, 198
UNION ALL
    SELECT 1339, N'The Trooper', 106, 3, 251454, 4024320, 126
UNION ALL
    SELECT 1340, N'Still Life', 106, 3, 294347, 4710400, 114
UNION ALL
    SELECT 1341, N'Quest For Fire', 106, 3, 221309, 3543040, 94
UNION ALL
    SELECT 1342, N'Sun And Steel', 106, 3, 206367, 3306324, 178
UNION ALL
    SELECT 1343, N'To Tame A Land', 106, 3, 445283, 7129264, 141
UNION ALL
    SELECT 1344, N'Aces High', 107, 3, 269531, 6472088, 91
UNION ALL
    SELECT 1345, N'2 Minutes To Midnight', 107, 3, 359810, 8638809, 100
UNION ALL
    SELECT 1346, N'Losfer Words', 107, 3, 252891, 6074756, 87
UNION ALL
    SELECT 1347, N'Flash of The Blade', 107, 3, 242729, 5828861, 82
UNION ALL
    SELECT 1348, N'Duelists', 107, 3, 366471, 8800686, 148
UNION ALL
    SELECT 1349, N'Back in the Village', 107, 3, 320548, 7696518, 148
UNION ALL
    SELECT 1350, N'Powerslave', 107, 3, 407823, 9791106, 74
UNION ALL
    SELECT 1351, N'Rime of the Ancient Mariner', 107, 3, 816509, 19599577, 156
UNION ALL
    SELECT 1352, N'Intro', 108, 3, 115931, 4638848, 98
UNION ALL
    SELECT 1353, N'The Wicker Man', 108, 3, 281782, 11272320, 128
UNION ALL
    SELECT 1354, N'Ghost Of The Navigator', 108, 3, 408607, 16345216, 130
UNION ALL
    SELECT 1355, N'Brave New World', 108, 3, 366785, 14676148, 188
UNION ALL
    SELECT 1356, N'Wrathchild', 108, 3, 185808, 7434368, 114
UNION ALL
    SELECT 1357, N'2 Minutes To Midnight', 108, 3, 386821, 15474816, 123
UNION ALL
    SELECT 1358, N'Blood Brothers', 108, 3, 435513, 17422464, 162
UNION ALL
    SELECT 1359, N'Sign Of The Cross', 108, 3, 649116, 25966720, 59
UNION ALL
    SELECT 1360, N'The Mercenary', 108, 3, 282697, 11309184, 173
UNION ALL
    SELECT 1361, N'The Trooper', 108, 3, 273528, 10942592, 130
UNION ALL
    SELECT 1362, N'Dream Of Mirrors', 109, 1, 578324, 23134336, 160
UNION ALL
    SELECT 1363, N'The Clansman', 109, 1, 559203, 22370432, 163
UNION ALL
    SELECT 1364, N'The Evil That Men Do', 109, 3, 280737, 11231360, 53
UNION ALL
    SELECT 1365, N'Fear Of The Dark', 109, 1, 460695, 18430080, 69
UNION ALL
    SELECT 1366, N'Iron Maiden', 109, 1, 351869, 14076032, 68
UNION ALL
    SELECT 1367, N'The Number Of The Beast', 109, 1, 300434, 12022107, 171
UNION ALL
    SELECT 1368, N'Hallowed Be Thy Name', 109, 1, 443977, 17760384, 142
UNION ALL
    SELECT 1369, N'Sanctuary', 109, 1, 317335, 12695680, 182
UNION ALL
    SELECT 1370, N'Run To The Hills', 109, 1, 292179, 11688064, 154
UNION ALL
    SELECT 1371, N'Moonchild', 110, 3, 340767, 8179151, 119
UNION ALL
    SELECT 1372, N'Infinite Dreams', 110, 3, 369005, 8858669, 198
UNION ALL
    SELECT 1373, N'Can I Play With Madness', 110, 3, 211043, 5067867, 58
UNION ALL
    SELECT 1374, N'The Evil That Men Do', 110, 3, 273998, 6578930, 200
UNION ALL
    SELECT 1375, N'Seventh Son of a Seventh Son', 110, 3, 593580, 14249000, 160
UNION ALL
    SELECT 1376, N'The Prophecy', 110, 3, 305475, 7334450, 143
UNION ALL
    SELECT 1377, N'The Clairvoyant', 110, 3, 267023, 6411510, 152
UNION ALL
    SELECT 1378, N'Only the Good Die Young', 110, 3, 280894, 6744431, 64
UNION ALL
    SELECT 1379, N'Caught Somewhere in Time', 111, 3, 445779, 10701149, 154
UNION ALL
    SELECT 1380, N'Wasted Years', 111, 3, 307565, 7384358, 179
UNION ALL
    SELECT 1381, N'Sea of Madness', 111, 3, 341995, 8210695, 129
UNION ALL
    SELECT 1382, N'Heaven Can Wait', 111, 3, 441417, 10596431, 90
UNION ALL
    SELECT 1383, N'Stranger in a Strange Land', 111, 3, 344502, 8270899, 97
UNION ALL
    SELECT 1384, N'Alexander the Great', 111, 3, 515631, 12377742, 138
UNION ALL
    SELECT 1385, N'De Ja Vu', 111, 3, 296176, 7113035, 124
UNION ALL
    SELECT 1386, N'The Loneliness of the Long Dis', 111, 3, 391314, 9393598, 52
UNION ALL
    SELECT 1387, N'22 Acacia Avenue', 112, 3, 395572, 5542516, 62
UNION ALL
    SELECT 1388, N'Children of the Damned', 112, 3, 274364, 3845631, 111
UNION ALL
    SELECT 1389, N'Gangland', 112, 3, 228440, 3202866, 186
UNION ALL
    SELECT 1390, N'Hallowed Be Thy Name', 112, 3, 428669, 6006107, 96
UNION ALL
    SELECT 1391, N'Invaders', 112, 3, 203180, 2849181, 72
UNION ALL
    SELECT 1392, N'Run to the Hills', 112, 3, 228884, 3209124, 81
UNION ALL
    SELECT 1393, N'The Number Of The Beast', 112, 1, 293407, 11737216, 138
UNION ALL
    SELECT 1394, N'The Prisoner', 112, 3, 361299, 5062906, 153
UNION ALL
    SELECT 1395, N'Sign Of The Cross', 113, 1, 678008, 27121792, 138
UNION ALL
    SELECT 1396, N'Lord Of The Flies', 113, 1, 303699, 12148864, 73
UNION ALL
    SELECT 1397, N'Man On The Edge', 113, 1, 253413, 10137728, 78
UNION ALL
    SELECT 1398, N'Fortunes Of War', 113, 1, 443977, 17760384, 100
UNION ALL
    SELECT 1399, N'Look For The Truth', 113, 1, 310230, 12411008, 167
UNION ALL
    SELECT 1400, N'The Aftermath', 113, 1, 380786, 15233152, 89
UNION ALL
    SELECT 1401, N'Judgement Of Heaven', 113, 1, 312476, 12501120, 136
UNION ALL
    SELECT 1402, N'Blood On The World''s Hands', 113, 1, 357799, 14313600, 197
UNION ALL
    SELECT 1403, N'The Edge Of Darkness', 113, 1, 399333, 15974528, 171
UNION ALL
    SELECT 1404, N'2 A.M.', 113, 1, 337658, 13511087, 187
UNION ALL
    SELECT 1405, N'The Unbeliever', 113, 1, 490422, 19617920, 150
UNION ALL
    SELECT 1406, N'Futureal', 114, 1, 175777, 7032960, 88
UNION ALL
    SELECT 1407, N'The Angel And The Gambler', 114, 1, 592744, 23711872, 108
UNION ALL
    SELECT 1408, N'Lightning Strikes Twice', 114, 1, 290377, 11616384, 103
UNION ALL
    SELECT 1409, N'The Clansman', 114, 1, 539689, 21592327, 124
UNION ALL
    SELECT 1410, N'When Two Worlds Collide', 114, 1, 377312, 15093888, 148
UNION ALL
    SELECT 1411, N'The Educated Fool', 114, 1, 404767, 16191616, 86
UNION ALL
    SELECT 1412, N'Don''t Look To The Eyes Of A Stranger', 114, 1, 483657, 19347584, 119
UNION ALL
    SELECT 1413, N'Como Estais Amigos', 114, 1, 330292, 13213824, 163
UNION ALL
    SELECT 1414, N'Please Please Please', 115, 14, 165067, 5394585, 75
UNION ALL
    SELECT 1415, N'Think', 115, 14, 166739, 5513208, 155
UNION ALL
    SELECT 1416, N'Night Train', 115, 14, 212401, 7027377, 55
UNION ALL
    SELECT 1417, N'Out Of Sight', 115, 14, 143725, 4711323, 54
UNION ALL
    SELECT 1418, N'Papa''s Got A Brand New Bag Pt.1', 115, 14, 127399, 4174420, 66
UNION ALL
    SELECT 1419, N'I Got You (I Feel Good)', 115, 14, 167392, 5468472, 171
UNION ALL
    SELECT 1420, N'It''s A Man''s Man''s Man''s World', 115, 14, 168228, 5541611, 131
UNION ALL
    SELECT 1421, N'Cold Sweat', 115, 14, 172408, 5643213, 185
UNION ALL
    SELECT 1422, N'Say It Loud, I''m Black And I''m Proud Pt.1', 115, 14, 167392, 5478117, 132
UNION ALL
    SELECT 1423, N'Get Up (I Feel Like Being A) Sex Machine', 115, 14, 316551, 10498031, 197
UNION ALL
    SELECT 1424, N'Hey America', 115, 14, 218226, 7187857, 88
UNION ALL
    SELECT 1425, N'Make It Funky Pt.1', 115, 14, 196231, 6507782, 173
UNION ALL
    SELECT 1426, N'I''m A Greedy Man Pt.1', 115, 14, 217730, 7251211, 93
UNION ALL
    SELECT 1427, N'Get On The Good Foot', 115, 14, 215902, 7182736, 73
UNION ALL
    SELECT 1428, N'Get Up Offa That Thing', 115, 14, 250723, 8355989, 147
UNION ALL
    SELECT 1429, N'It''s Too Funky In Here', 115, 14, 239072, 7973979, 171
UNION ALL
    SELECT 1430, N'Living In America', 115, 14, 282880, 9432346, 135
UNION ALL
    SELECT 1431, N'I''m Real', 115, 14, 334236, 11183457, 103
UNION ALL
    SELECT 1432, N'Hot Pants Pt.1', 115, 14, 188212, 6295110, 124
UNION ALL
    SELECT 1433, N'Soul Power (Live)', 115, 14, 260728, 8593206, 129
UNION ALL
    SELECT 1434, N'When You Gonna Learn (Digeridoo)', 116, 1, 230635, 7655482, 82
UNION ALL
    SELECT 1435, N'Too Young To Die', 116, 1, 365818, 12391660, 92
UNION ALL
    SELECT 1436, N'Hooked Up', 116, 1, 275879, 9301687, 173
UNION ALL
    SELECT 1437, N'If I Like It, I Do It', 116, 1, 293093, 9848207, 55
UNION ALL
    SELECT 1438, N'Music Of The Wind', 116, 1, 383033, 12870239, 168
UNION ALL
    SELECT 1439, N'Emergency On Planet Earth', 116, 1, 245263, 8117218, 153
UNION ALL
    SELECT 1440, N'Whatever It Is, I Just Can''t Stop', 116, 1, 247222, 8249453, 198
UNION ALL
    SELECT 1441, N'Blow Your Mind', 116, 1, 512339, 17089176, 179
UNION ALL
    SELECT 1442, N'Revolution 1993', 116, 1, 616829, 20816872, 128
UNION ALL
    SELECT 1443, N'Didgin'' Out', 116, 1, 157100, 5263555, 127
UNION ALL
    SELECT 1444, N'Canned Heat', 117, 14, 331964, 11042037, 68
UNION ALL
    SELECT 1445, N'Planet Home', 117, 14, 284447, 9566237, 116
UNION ALL
    SELECT 1446, N'Black Capricorn Day', 117, 14, 341629, 11477231, 167
UNION ALL
    SELECT 1447, N'Soul Education', 117, 14, 255477, 8575435, 86
UNION ALL
    SELECT 1448, N'Failling', 117, 14, 225227, 7503999, 176
UNION ALL
    SELECT 1449, N'Destitute Illusions', 117, 14, 340218, 11452651, 59
UNION ALL
    SELECT 1450, N'Supersonic', 117, 14, 315872, 10699265, 149
UNION ALL
    SELECT 1451, N'Butterfly', 117, 14, 268852, 8947356, 193
UNION ALL
    SELECT 1452, N'Were Do We Go From Here', 117, 14, 313626, 10504158, 127
UNION ALL
    SELECT 1453, N'King For A Day', 117, 14, 221544, 7335693, 67
UNION ALL
    SELECT 1454, N'Deeper Underground', 117, 14, 281808, 9351277, 151
UNION ALL
    SELECT 1455, N'Just Another Story', 118, 15, 529684, 17582818, 73
UNION ALL
    SELECT 1456, N'Stillness In Time', 118, 15, 257097, 8644290, 99
UNION ALL
    SELECT 1457, N'Half The Man', 118, 15, 289854, 9577679, 135
UNION ALL
    SELECT 1458, N'Light Years', 118, 15, 354560, 11796244, 110
UNION ALL
    SELECT 1459, N'Manifest Destiny', 118, 15, 382197, 12676962, 176
UNION ALL
    SELECT 1460, N'The Kids', 118, 15, 309995, 10334529, 106
UNION ALL
    SELECT 1461, N'Mr. Moon', 118, 15, 329534, 11043559, 147
UNION ALL
    SELECT 1462, N'Scam', 118, 15, 422321, 14019705, 83
UNION ALL
    SELECT 1463, N'Journey To Arnhemland', 118, 15, 322455, 10843832, 61
UNION ALL
    SELECT 1464, N'Morning Glory', 118, 15, 384130, 12777210, 118
UNION ALL
    SELECT 1465, N'Space Cowboy', 118, 15, 385697, 12906520, 98
UNION ALL
    SELECT 1466, N'Last Chance', 119, 4, 112352, 3683130, 139
UNION ALL
    SELECT 1467, N'Are You Gonna Be My Girl', 119, 4, 213890, 6992324, 110
UNION ALL
    SELECT 1468, N'Rollover D.J.', 119, 4, 196702, 6406517, 166
UNION ALL
    SELECT 1469, N'Look What You''ve Done', 119, 4, 230974, 7517083, 114
UNION ALL
    SELECT 1470, N'Get What You Need', 119, 4, 247719, 8043765, 135
UNION ALL
    SELECT 1471, N'Move On', 119, 4, 260623, 8519353, 153
UNION ALL
    SELECT 1472, N'Radio Song', 119, 4, 272117, 8871509, 196
UNION ALL
    SELECT 1473, N'Get Me Outta Here', 119, 4, 176274, 5729098, 84
UNION ALL
    SELECT 1474, N'Cold Hard Bitch', 119, 4, 243278, 7929610, 99
UNION ALL
    SELECT 1475, N'Come Around Again', 119, 4, 270497, 8872405, 127
UNION ALL
    SELECT 1476, N'Take It Or Leave It', 119, 4, 142889, 4643370, 116
UNION ALL
    SELECT 1477, N'Lazy Gun', 119, 4, 282174, 9186285, 136
UNION ALL
    SELECT 1478, N'Timothy', 119, 4, 270341, 8856507, 179
UNION ALL
    SELECT 1479, N'Foxy Lady', 120, 1, 199340, 6480896, 59
UNION ALL
    SELECT 1480, N'Manic Depression', 120, 1, 222302, 7289272, 51
UNION ALL
    SELECT 1481, N'Red House', 120, 1, 224130, 7285851, 169
UNION ALL
    SELECT 1482, N'Can You See Me', 120, 1, 153077, 4987068, 83
UNION ALL
    SELECT 1483, N'Love Or Confusion', 120, 1, 193123, 6329408, 170
UNION ALL
    SELECT 1484, N'I Don''t Live Today', 120, 1, 235311, 7661214, 96
UNION ALL
    SELECT 1485, N'May This Be Love', 120, 1, 191216, 6240028, 169
UNION ALL
    SELECT 1486, N'Fire', 120, 1, 164989, 5383075, 81
UNION ALL
    SELECT 1487, N'Third Stone From The Sun', 120, 1, 404453, 13186975, 71
UNION ALL
    SELECT 1488, N'Remember', 120, 1, 168150, 5509613, 159
UNION ALL
    SELECT 1489, N'Are You Experienced?', 120, 1, 254537, 8292497, 97
UNION ALL
    SELECT 1490, N'Hey Joe', 120, 1, 210259, 6870054, 77
UNION ALL
    SELECT 1491, N'Stone Free', 120, 1, 216293, 7002331, 181
UNION ALL
    SELECT 1492, N'Purple Haze', 120, 1, 171572, 5597056, 90
UNION ALL
    SELECT 1493, N'51st Anniversary', 120, 1, 196388, 6398044, 148
UNION ALL
    SELECT 1494, N'The Wind Cries Mary', 120, 1, 200463, 6540638, 185
UNION ALL
    SELECT 1495, N'Highway Chile', 120, 1, 212453, 6887949, 167
UNION ALL
    SELECT 1496, N'Surfing with the Alien', 121, 1, 263707, 4418504, 63
UNION ALL
    SELECT 1497, N'Ice 9', 121, 1, 239721, 4036215, 189
UNION ALL
    SELECT 1498, N'Crushing Day', 121, 1, 314768, 5232158, 70
UNION ALL
    SELECT 1499, N'Always With Me, Always With You', 121, 1, 202035, 3435777, 68
UNION ALL
    SELECT 1500, N'Satch Boogie', 121, 1, 193560, 3300654, 112
UNION ALL
    SELECT 1501, N'Hill of the Skull', 121, 1, 108435, 1944738, 196
UNION ALL
    SELECT 1502, N'Circles', 121, 1, 209071, 3548553, 167
UNION ALL
    SELECT 1503, N'Lords of Karma', 121, 1, 288227, 4809279, 166
UNION ALL
    SELECT 1504, N'Midnight', 121, 1, 102630, 1851753, 104
UNION ALL
    SELECT 1505, N'Echo', 121, 1, 337570, 5595557, 83
UNION ALL
    SELECT 1506, N'Engenho De Dentro', 122, 7, 310073, 10211473, 134
UNION ALL
    SELECT 1507, N'Alcohol', 122, 7, 355239, 12010478, 75
UNION ALL
    SELECT 1508, N'Mama Africa', 122, 7, 283062, 9488316, 100
UNION ALL
    SELECT 1509, N'Salve Simpatia', 122, 7, 343484, 11314756, 173
UNION ALL
    SELECT 1510, N'W/Brasil (Chama O SГ­ndico)', 122, 7, 317100, 10599953, 197
UNION ALL
    SELECT 1511, N'PaГ­s Tropical', 122, 7, 452519, 14946972, 60
UNION ALL
    SELECT 1512, N'Os Alquimistas EstГЈo Chegando', 122, 7, 367281, 12304520, 179
UNION ALL
    SELECT 1513, N'Charles Anjo 45', 122, 7, 389276, 13022833, 132
UNION ALL
    SELECT 1514, N'SelassiГЄ', 122, 7, 326321, 10724982, 117
UNION ALL
    SELECT 1515, N'Menina SararГЎ', 122, 7, 191477, 6393818, 151
UNION ALL
    SELECT 1516, N'Que Maravilha', 122, 7, 338076, 10996656, 164
UNION ALL
    SELECT 1517, N'Santa Clara Clareou', 122, 7, 380081, 12524725, 199
UNION ALL
    SELECT 1518, N'Filho Maravilha', 122, 7, 227526, 7498259, 91
UNION ALL
    SELECT 1519, N'Taj Mahal', 122, 7, 289750, 9502898, 80
UNION ALL
    SELECT 1520, N'Rapidamente', 123, 7, 252238, 8470107, 127
UNION ALL
    SELECT 1521, N'As Dores do Mundo', 123, 7, 255477, 8537092, 85
UNION ALL
    SELECT 1522, N'Vou Pra Ai', 123, 7, 300878, 10053718, 61
UNION ALL
    SELECT 1523, N'My Brother', 123, 7, 253231, 8431821, 196
UNION ALL
    SELECT 1524, N'HГЎ Quanto Tempo', 123, 7, 270027, 9004470, 182
UNION ALL
    SELECT 1525, N'VГ­cio', 123, 7, 269897, 8887216, 167
UNION ALL
    SELECT 1526, N'Encontrar AlguГ©m', 123, 7, 224078, 7437935, 147
UNION ALL
    SELECT 1527, N'Dance Enquanto Г© Tempo', 123, 7, 229093, 7583799, 183
UNION ALL
    SELECT 1528, N'A Tarde', 123, 7, 266919, 8836127, 131
UNION ALL
    SELECT 1529, N'Always Be All Right', 123, 7, 128078, 4299676, 194
UNION ALL
    SELECT 1530, N'Sem Sentido', 123, 7, 250462, 8292108, 188
UNION ALL
    SELECT 1531, N'Onibusfobia', 123, 7, 315977, 10474904, 84
UNION ALL
    SELECT 1532, N'Pura Elegancia', 124, 16, 284107, 9632269, 75
UNION ALL
    SELECT 1533, N'Choramingando', 124, 16, 190484, 6400532, 83
UNION ALL
    SELECT 1534, N'Por Merecer', 124, 16, 230582, 7764601, 74
UNION ALL
    SELECT 1535, N'No Futuro', 124, 16, 182308, 6056200, 72
UNION ALL
    SELECT 1536, N'Voce Inteira', 124, 16, 241084, 8077282, 74
UNION ALL
    SELECT 1537, N'Cuando A Noite Vai Chegando', 124, 16, 270628, 9081874, 70
UNION ALL
    SELECT 1538, N'Naquele Dia', 124, 16, 251768, 8452654, 167
UNION ALL
    SELECT 1539, N'Equinocio', 124, 16, 269008, 8871455, 158
UNION ALL
    SELECT 1540, N'PapelГЈo', 124, 16, 213263, 7257390, 78
UNION ALL
    SELECT 1541, N'Cuando Eu For Pro Ceu', 124, 16, 118804, 3948371, 87
UNION ALL
    SELECT 1542, N'Do Nosso Amor', 124, 16, 203415, 6774566, 61
UNION ALL
    SELECT 1543, N'Borogodo', 124, 16, 208457, 7104588, 83
UNION ALL
    SELECT 1544, N'Cafezinho', 124, 16, 180924, 6031174, 127
UNION ALL
    SELECT 1545, N'Enquanto O Dia NГЈo Vem', 124, 16, 220891, 7248336, 95
UNION ALL
    SELECT 1546, N'The Green Manalishi', 125, 3, 205792, 6720789, 89
UNION ALL
    SELECT 1547, N'Living After Midnight', 125, 3, 213289, 7056785, 93
UNION ALL
    SELECT 1548, N'Breaking The Law (Live)', 125, 3, 144195, 4728246, 83
UNION ALL
    SELECT 1549, N'Hot Rockin''', 125, 3, 197328, 6509179, 55
UNION ALL
    SELECT 1550, N'Heading Out To The Highway (Live)', 125, 3, 276427, 9006022, 175
UNION ALL
    SELECT 1551, N'The Hellion', 125, 3, 41900, 1351993, 88
UNION ALL
    SELECT 1552, N'Electric Eye', 125, 3, 222197, 7231368, 109
UNION ALL
    SELECT 1553, N'You''ve Got Another Thing Comin''', 125, 3, 305162, 9962558, 192
UNION ALL
    SELECT 1554, N'Turbo Lover', 125, 3, 335542, 11068866, 72
UNION ALL
    SELECT 1555, N'Freewheel Burning', 125, 3, 265952, 8713599, 99
UNION ALL
    SELECT 1556, N'Some Heads Are Gonna Roll', 125, 3, 249939, 8198617, 181
UNION ALL
    SELECT 1557, N'Metal Meltdown', 125, 3, 290664, 9390646, 61
UNION ALL
    SELECT 1558, N'Ram It Down', 125, 3, 292179, 9554023, 153
UNION ALL
    SELECT 1559, N'Diamonds And Rust (Live)', 125, 3, 219350, 7163147, 64
UNION ALL
    SELECT 1560, N'Victim Of Change (Live)', 125, 3, 430942, 14067512, 160
UNION ALL
    SELECT 1561, N'Tyrant (Live)', 125, 3, 282253, 9190536, 99
UNION ALL
    SELECT 1562, N'Comin'' Home', 126, 1, 172068, 5661120, 150
UNION ALL
    SELECT 1563, N'Plaster Caster', 126, 1, 198060, 6528719, 137
UNION ALL
    SELECT 1564, N'Goin'' Blind', 126, 1, 217652, 7167523, 117
UNION ALL
    SELECT 1565, N'Do You Love Me', 126, 1, 193619, 6343111, 79
UNION ALL
    SELECT 1566, N'Domino', 126, 1, 226377, 7488191, 169
UNION ALL
    SELECT 1567, N'Sure Know Something', 126, 1, 254354, 8375190, 114
UNION ALL
    SELECT 1568, N'A World Without Heroes', 126, 1, 177815, 5832524, 72
UNION ALL
    SELECT 1569, N'Rock Bottom', 126, 1, 200594, 6560818, 178
UNION ALL
    SELECT 1570, N'See You Tonight', 126, 1, 146494, 4817521, 115
UNION ALL
    SELECT 1571, N'I Still Love You', 126, 1, 369815, 12086145, 132
UNION ALL
    SELECT 1572, N'Every Time I Look At You', 126, 1, 283898, 9290948, 69
UNION ALL
    SELECT 1573, N'2,000 Man', 126, 1, 312450, 10292829, 187
UNION ALL
    SELECT 1574, N'Beth', 126, 1, 170187, 5577807, 139
UNION ALL
    SELECT 1575, N'Nothin'' To Lose', 126, 1, 222354, 7351460, 151
UNION ALL
    SELECT 1576, N'Rock And Roll All Nite', 126, 1, 259631, 8549296, 113
UNION ALL
    SELECT 1577, N'Immigrant Song', 127, 1, 201247, 6457766, 58
UNION ALL
    SELECT 1578, N'Heartbreaker', 127, 1, 316081, 10179657, 73
UNION ALL
    SELECT 1579, N'Since I''ve Been Loving You', 127, 1, 416365, 13471959, 85
UNION ALL
    SELECT 1580, N'Black Dog', 127, 1, 317622, 10267572, 118
UNION ALL
    SELECT 1581, N'Dazed And Confused', 127, 1, 1116734, 36052247, 70
UNION ALL
    SELECT 1582, N'Stairway To Heaven', 127, 1, 529658, 17050485, 97
UNION ALL
    SELECT 1583, N'Going To California', 127, 1, 234605, 7646749, 185
UNION ALL
    SELECT 1584, N'That''s The Way', 127, 1, 343431, 11248455, 170
UNION ALL
    SELECT 1585, N'Whole Lotta Love (Medley)', 127, 1, 825103, 26742545, 99
UNION ALL
    SELECT 1586, N'Thank You', 127, 1, 398262, 12831826, 159
UNION ALL
    SELECT 1587, N'We''re Gonna Groove', 128, 1, 157570, 5180975, 69
UNION ALL
    SELECT 1588, N'Poor Tom', 128, 1, 182491, 6016220, 144
UNION ALL
    SELECT 1589, N'I Can''t Quit You Baby', 128, 1, 258168, 8437098, 66
UNION ALL
    SELECT 1590, N'Walter''s Walk', 128, 1, 270785, 8712499, 111
UNION ALL
    SELECT 1591, N'Ozone Baby', 128, 1, 215954, 7079588, 80
UNION ALL
    SELECT 1592, N'Darlene', 128, 1, 307226, 10078197, 102
UNION ALL
    SELECT 1593, N'Bonzo''s Montreux', 128, 1, 258925, 8557447, 75
UNION ALL
    SELECT 1594, N'Wearing And Tearing', 128, 1, 330004, 10701590, 190
UNION ALL
    SELECT 1595, N'The Song Remains The Same', 129, 1, 330004, 10708950, 139
UNION ALL
    SELECT 1596, N'The Rain Song', 129, 1, 459180, 15029875, 184
UNION ALL
    SELECT 1597, N'Over The Hills And Far Away', 129, 1, 290089, 9552829, 171
UNION ALL
    SELECT 1598, N'The Crunge', 129, 1, 197407, 6460212, 100
UNION ALL
    SELECT 1599, N'Dancing Days', 129, 1, 223216, 7250104, 192
UNION ALL
    SELECT 1600, N'D''Yer Mak''er', 129, 1, 262948, 8645935, 178
UNION ALL
    SELECT 1601, N'No Quarter', 129, 1, 420493, 13656517, 135
UNION ALL
    SELECT 1602, N'The Ocean', 129, 1, 271098, 8846469, 104
UNION ALL
    SELECT 1603, N'In The Evening', 130, 1, 410566, 13399734, 162
UNION ALL
    SELECT 1604, N'South Bound Saurez', 130, 1, 254406, 8420427, 133
UNION ALL
    SELECT 1605, N'Fool In The Rain', 130, 1, 372950, 12371433, 140
UNION ALL
    SELECT 1606, N'Hot Dog', 130, 1, 197198, 6536167, 145
UNION ALL
    SELECT 1607, N'Carouselambra', 130, 1, 634435, 20858315, 175
UNION ALL
    SELECT 1608, N'All My Love', 130, 1, 356284, 11684862, 150
UNION ALL
    SELECT 1609, N'I''m Gonna Crawl', 130, 1, 329639, 10737665, 175
UNION ALL
    SELECT 1610, N'Black Dog', 131, 1, 296672, 9660588, 190
UNION ALL
    SELECT 1611, N'Rock & Roll', 131, 1, 220917, 7142127, 183
UNION ALL
    SELECT 1612, N'The Battle Of Evermore', 131, 1, 351555, 11525689, 59
UNION ALL
    SELECT 1613, N'Stairway To Heaven', 131, 1, 481619, 15706767, 188
UNION ALL
    SELECT 1614, N'Misty Mountain Hop', 131, 1, 278857, 9092799, 195
UNION ALL
    SELECT 1615, N'Four Sticks', 131, 1, 284447, 9481301, 68
UNION ALL
    SELECT 1616, N'Going To California', 131, 1, 215693, 7068737, 75
UNION ALL
    SELECT 1617, N'When The Levee Breaks', 131, 1, 427702, 13912107, 121
UNION ALL
    SELECT 1618, N'Good Times Bad Times', 132, 1, 166164, 5464077, 138
UNION ALL
    SELECT 1619, N'Babe I''m Gonna Leave You', 132, 1, 401475, 13189312, 165
UNION ALL
    SELECT 1620, N'You Shook Me', 132, 1, 388179, 12643067, 133
UNION ALL
    SELECT 1621, N'Dazed and Confused', 132, 1, 386063, 12610326, 86
UNION ALL
    SELECT 1622, N'Your Time Is Gonna Come', 132, 1, 274860, 9011653, 50
UNION ALL
    SELECT 1623, N'Black Mountain Side', 132, 1, 132702, 4440602, 55
UNION ALL
    SELECT 1624, N'Communication Breakdown', 132, 1, 150230, 4899554, 162
UNION ALL
    SELECT 1625, N'I Can''t Quit You Baby', 132, 1, 282671, 9252733, 172
UNION ALL
    SELECT 1626, N'How Many More Times', 132, 1, 508055, 16541364, 162
UNION ALL
    SELECT 1627, N'Whole Lotta Love', 133, 1, 334471, 11026243, 131
UNION ALL
    SELECT 1628, N'What Is And What Should Never Be', 133, 1, 287973, 9369385, 68
UNION ALL
    SELECT 1629, N'The Lemon Song', 133, 1, 379141, 12463496, 115
UNION ALL
    SELECT 1630, N'Thank You', 133, 1, 287791, 9337392, 96
UNION ALL
    SELECT 1631, N'Heartbreaker', 133, 1, 253988, 8387560, 131
UNION ALL
    SELECT 1632, N'Living Loving Maid (She''s Just A Woman)', 133, 1, 159216, 5219819, 135
UNION ALL
    SELECT 1633, N'Ramble On', 133, 1, 275591, 9199710, 184
UNION ALL
    SELECT 1634, N'Moby Dick', 133, 1, 260728, 8664210, 186
UNION ALL
    SELECT 1635, N'Bring It On Home', 133, 1, 259970, 8494731, 195
UNION ALL
    SELECT 1636, N'Immigrant Song', 134, 1, 144875, 4786461, 167
UNION ALL
    SELECT 1637, N'Friends', 134, 1, 233560, 7694220, 71
UNION ALL
    SELECT 1638, N'Celebration Day', 134, 1, 209528, 6871078, 78
UNION ALL
    SELECT 1639, N'Since I''ve Been Loving You', 134, 1, 444055, 14482460, 195
UNION ALL
    SELECT 1640, N'Out On The Tiles', 134, 1, 246047, 8060350, 143
UNION ALL
    SELECT 1641, N'Gallows Pole', 134, 1, 296228, 9757151, 66
UNION ALL
    SELECT 1642, N'Tangerine', 134, 1, 189675, 6200893, 155
UNION ALL
    SELECT 1643, N'That''s The Way', 134, 1, 337345, 11202499, 69
UNION ALL
    SELECT 1644, N'Bron-Y-Aur Stomp', 134, 1, 259500, 8674508, 176
UNION ALL
    SELECT 1645, N'Hats Off To (Roy) Harper', 134, 1, 219376, 7236640, 117
UNION ALL
    SELECT 1646, N'In The Light', 135, 1, 526785, 17033046, 82
UNION ALL
    SELECT 1647, N'Bron-Yr-Aur', 135, 1, 126641, 4150746, 100
UNION ALL
    SELECT 1648, N'Down By The Seaside', 135, 1, 316186, 10371282, 97
UNION ALL
    SELECT 1649, N'Ten Years Gone', 135, 1, 393116, 12756366, 60
UNION ALL
    SELECT 1650, N'Night Flight', 135, 1, 217547, 7160647, 136
UNION ALL
    SELECT 1651, N'The Wanton Song', 135, 1, 249887, 8180988, 52
UNION ALL
    SELECT 1652, N'Boogie With Stu', 135, 1, 233273, 7657086, 97
UNION ALL
    SELECT 1653, N'Black Country Woman', 135, 1, 273084, 8951732, 97
UNION ALL
    SELECT 1654, N'Sick Again', 135, 1, 283036, 9279263, 71
UNION ALL
    SELECT 1655, N'Achilles Last Stand', 136, 1, 625502, 20593955, 80
UNION ALL
    SELECT 1656, N'For Your Life', 136, 1, 384391, 12633382, 170
UNION ALL
    SELECT 1657, N'Royal Orleans', 136, 1, 179591, 5930027, 181
UNION ALL
    SELECT 1658, N'Nobody''s Fault But Mine', 136, 1, 376215, 12237859, 99
UNION ALL
    SELECT 1659, N'Candy Store Rock', 136, 1, 252055, 8397423, 144
UNION ALL
    SELECT 1660, N'Hots On For Nowhere', 136, 1, 284107, 9342342, 64
UNION ALL
    SELECT 1661, N'Tea For One', 136, 1, 566752, 18475264, 168
UNION ALL
    SELECT 1662, N'Rock & Roll', 137, 1, 242442, 7897065, 152
UNION ALL
    SELECT 1663, N'Celebration Day', 137, 1, 230034, 7478487, 131
UNION ALL
    SELECT 1664, N'The Song Remains The Same', 137, 1, 353358, 11465033, 50
UNION ALL
    SELECT 1665, N'Rain Song', 137, 1, 505808, 16273705, 181
UNION ALL
    SELECT 1666, N'Dazed And Confused', 137, 1, 1612329, 52490554, 95
UNION ALL
    SELECT 1667, N'No Quarter', 138, 1, 749897, 24399285, 199
UNION ALL
    SELECT 1668, N'Stairway To Heaven', 138, 1, 657293, 21354766, 162
UNION ALL
    SELECT 1669, N'Moby Dick', 138, 1, 766354, 25345841, 62
UNION ALL
    SELECT 1670, N'Whole Lotta Love', 138, 1, 863895, 28191437, 143
UNION ALL
    SELECT 1671, N'NatГЎlia', 139, 7, 235728, 7640230, 95
UNION ALL
    SELECT 1672, N'L''Avventura', 139, 7, 278256, 9165769, 111
UNION ALL
    SELECT 1673, N'MГєsica De Trabalho', 139, 7, 260231, 8590671, 62
UNION ALL
    SELECT 1674, N'Longe Do Meu Lado', 139, 7, 266161, 8655249, 74
UNION ALL
    SELECT 1675, N'A Via LГЎctea', 139, 7, 280084, 9234879, 137
UNION ALL
    SELECT 1676, N'MГєsica Ambiente', 139, 7, 247614, 8234388, 97
UNION ALL
    SELECT 1677, N'Aloha', 139, 7, 325955, 10793301, 101
UNION ALL
    SELECT 1678, N'Soul Parsifal', 139, 7, 295053, 9853589, 118
UNION ALL
    SELECT 1679, N'Dezesseis', 139, 7, 323918, 10573515, 196
UNION ALL
    SELECT 1680, N'Mil PedaГ§os', 139, 7, 203337, 6643291, 159
UNION ALL
    SELECT 1681, N'Leila', 139, 7, 323056, 10608239, 86
UNION ALL
    SELECT 1682, N'1Вє De Julho', 139, 7, 290298, 9619257, 140
UNION ALL
    SELECT 1683, N'Esperando Por Mim', 139, 7, 261668, 8844133, 93
UNION ALL
    SELECT 1684, N'Quando VocГЄ Voltar', 139, 7, 173897, 5781046, 86
UNION ALL
    SELECT 1685, N'O Livro Dos Dias', 139, 7, 257253, 8570929, 144
UNION ALL
    SELECT 1686, N'SerГЎ', 140, 7, 148401, 4826528, 60
UNION ALL
    SELECT 1687, N'Ainda Г‰ Cedo', 140, 7, 236826, 7796400, 191
UNION ALL
    SELECT 1688, N'GeraГ§ГЈo Coca-Cola', 140, 7, 141453, 4625731, 104
UNION ALL
    SELECT 1689, N'Eduardo E MГґnica', 140, 7, 271229, 9026691, 89
UNION ALL
    SELECT 1690, N'Tempo Perdido', 140, 7, 302158, 9963914, 57
UNION ALL
    SELECT 1691, N'Indios', 140, 7, 258168, 8610226, 147
UNION ALL
    SELECT 1692, N'Que PaГ­s Г‰ Este', 140, 7, 177606, 5822124, 188
UNION ALL
    SELECT 1693, N'Faroeste Caboclo', 140, 7, 543007, 18092739, 191
UNION ALL
    SELECT 1694, N'HГЎ Tempos', 140, 7, 197146, 6432922, 179
UNION ALL
    SELECT 1695, N'Pais E Filhos', 140, 7, 308401, 10130685, 70
UNION ALL
    SELECT 1696, N'Meninos E Meninas', 140, 7, 203781, 6667802, 158
UNION ALL
    SELECT 1697, N'Vento No Litoral', 140, 7, 366445, 12063806, 53
UNION ALL
    SELECT 1698, N'PerfeiГ§ГЈo', 140, 7, 276558, 9258489, 79
UNION ALL
    SELECT 1699, N'Giz', 140, 7, 202213, 6677671, 74
UNION ALL
    SELECT 1700, N'Dezesseis', 140, 7, 321724, 10501773, 130
UNION ALL
    SELECT 1701, N'Antes Das Seis', 140, 7, 189231, 6296531, 147
UNION ALL
    SELECT 1702, N'Are You Gonna Go My Way', 141, 1, 211591, 6905135, 124
UNION ALL
    SELECT 1703, N'Fly Away', 141, 1, 221962, 7322085, 62
UNION ALL
    SELECT 1704, N'Rock And Roll Is Dead', 141, 1, 204199, 6680312, 156
UNION ALL
    SELECT 1705, N'Again', 141, 1, 228989, 7490476, 67
UNION ALL
    SELECT 1706, N'It Ain''t Over ''Til It''s Over', 141, 1, 242703, 8078936, 105
UNION ALL
    SELECT 1707, N'Can''t Get You Off My Mind', 141, 1, 273815, 8937150, 79
UNION ALL
    SELECT 1708, N'Mr. Cab Driver', 141, 1, 230321, 7668084, 175
UNION ALL
    SELECT 1709, N'American Woman', 141, 1, 261773, 8538023, 111
UNION ALL
    SELECT 1710, N'Stand By My Woman', 141, 1, 259683, 8447611, 114
UNION ALL
    SELECT 1711, N'Always On The Run', 141, 1, 232515, 7593397, 75
UNION ALL
    SELECT 1712, N'Heaven Help', 141, 1, 190354, 6222092, 157
UNION ALL
    SELECT 1713, N'I Belong To You', 141, 1, 257123, 8477980, 105
UNION ALL
    SELECT 1714, N'Believe', 141, 1, 295131, 9661978, 102
UNION ALL
    SELECT 1715, N'Let Love Rule', 141, 1, 342648, 11298085, 196
UNION ALL
    SELECT 1716, N'Black Velveteen', 141, 1, 290899, 9531301, 75
UNION ALL
    SELECT 1717, N'Assim Caminha A Humanidade', 142, 7, 210755, 6993763, 171
UNION ALL
    SELECT 1718, N'Honolulu', 143, 7, 261433, 8558481, 160
UNION ALL
    SELECT 1719, N'DancinВґDays', 143, 7, 237400, 7875347, 136
UNION ALL
    SELECT 1720, N'Um Pro Outro', 142, 7, 236382, 7825215, 178
UNION ALL
    SELECT 1721, N'Aviso Aos Navegantes', 143, 7, 242808, 8058651, 91
UNION ALL
    SELECT 1722, N'Casa', 142, 7, 307591, 10107269, 138
UNION ALL
    SELECT 1723, N'CondiГ§ГЈo', 142, 7, 263549, 8778465, 162
UNION ALL
    SELECT 1724, N'Hyperconectividade', 143, 7, 180636, 5948039, 181
UNION ALL
    SELECT 1725, N'O Descobridor Dos Sete Mares', 143, 7, 225854, 7475780, 131
UNION ALL
    SELECT 1726, N'SatisfaГ§ГЈo', 142, 7, 208065, 6901681, 188
UNION ALL
    SELECT 1727, N'BrumГЎrio', 142, 7, 216241, 7243499, 181
UNION ALL
    SELECT 1728, N'Um Certo AlguГ©m', 143, 7, 194063, 6430939, 103
UNION ALL
    SELECT 1729, N'FullgГЎs', 143, 7, 346070, 11505484, 122
UNION ALL
    SELECT 1730, N'SГЎbado ГЂ Noite', 142, 7, 193854, 6435114, 135
UNION ALL
    SELECT 1731, N'A Cura', 142, 7, 280920, 9260588, 197
UNION ALL
    SELECT 1732, N'Aquilo', 143, 7, 246073, 8167819, 116
UNION ALL
    SELECT 1733, N'AtrГЎs Do Trio ElГ©trico', 142, 7, 149080, 4917615, 155
UNION ALL
    SELECT 1734, N'Senta A Pua', 143, 7, 217547, 7205844, 90
UNION ALL
    SELECT 1735, N'Ro-Que-Se-Da-Ne', 143, 7, 146703, 4805897, 101
UNION ALL
    SELECT 1736, N'Tudo Bem', 142, 7, 196101, 6419139, 89
UNION ALL
    SELECT 1737, N'Toda Forma De Amor', 142, 7, 227813, 7496584, 197
UNION ALL
    SELECT 1738, N'Tudo Igual', 143, 7, 276035, 9201645, 106
UNION ALL
    SELECT 1739, N'Fogo De Palha', 143, 7, 246804, 8133732, 109
UNION ALL
    SELECT 1740, N'Sereia', 142, 7, 278047, 9121087, 198
UNION ALL
    SELECT 1741, N'Assaltaram A GramГЎtica', 143, 7, 261041, 8698959, 152
UNION ALL
    SELECT 1742, N'Se VocГЄ Pensa', 142, 7, 195996, 6552490, 138
UNION ALL
    SELECT 1743, N'LГЎ Vem O Sol (Here Comes The Sun)', 142, 7, 189492, 6229645, 139
UNION ALL
    SELECT 1744, N'O Гљltimo RomГўntico (Ao Vivo)', 143, 7, 231993, 7692697, 57
UNION ALL
    SELECT 1745, N'Pseudo Silk Kimono', 144, 1, 134739, 4334038, 187
UNION ALL
    SELECT 1746, N'Kayleigh', 144, 1, 234605, 7716005, 193
UNION ALL
    SELECT 1747, N'Lavender', 144, 1, 153417, 4999814, 170
UNION ALL
    SELECT 1748, N'Bitter Suite: Brief Encounter / Lost Weekend / Blue Angel', 144, 1, 356493, 11791068, 125
UNION ALL
    SELECT 1749, N'Heart Of Lothian: Wide Boy / Curtain Call', 144, 1, 366053, 11893723, 184
UNION ALL
    SELECT 1750, N'Waterhole (Expresso Bongo)', 144, 1, 133093, 4378835, 106
UNION ALL
    SELECT 1751, N'Lords Of The Backstage', 144, 1, 112875, 3741319, 94
UNION ALL
    SELECT 1752, N'Blind Curve: Vocal Under A Bloodlight / Passing Strangers / Mylo / Perimeter Walk / Threshold', 144, 1, 569704, 18578995, 50
UNION ALL
    SELECT 1753, N'Childhoods End?', 144, 1, 272796, 9015366, 68
UNION ALL
    SELECT 1754, N'White Feather', 144, 1, 143595, 4711776, 60
UNION ALL
    SELECT 1755, N'Arrepio', 145, 7, 136254, 4511390, 54
UNION ALL
    SELECT 1756, N'Magamalabares', 145, 7, 215875, 7183757, 101
UNION ALL
    SELECT 1757, N'Chuva No Brejo', 145, 7, 145606, 4857761, 96
UNION ALL
    SELECT 1758, N'CГ©rebro EletrГґnico', 145, 7, 172800, 5760864, 194
UNION ALL
    SELECT 1759, N'Tempos Modernos', 145, 7, 183066, 6066234, 64
UNION ALL
    SELECT 1760, N'MaraГ§ГЎ', 145, 7, 230008, 7621482, 150
UNION ALL
    SELECT 1761, N'Blanco', 145, 7, 45191, 1454532, 194
UNION ALL
    SELECT 1762, N'Panis Et Circenses', 145, 7, 192339, 6318373, 78
UNION ALL
    SELECT 1763, N'De Noite Na Cama', 145, 7, 209005, 7012658, 126
UNION ALL
    SELECT 1764, N'Beija Eu', 145, 7, 197276, 6512544, 111
UNION ALL
    SELECT 1765, N'Give Me Love', 145, 7, 249808, 8196331, 139
UNION ALL
    SELECT 1766, N'Ainda Lembro', 145, 7, 218801, 7211247, 102
UNION ALL
    SELECT 1767, N'A Menina DanГ§a', 145, 7, 129410, 4326918, 60
UNION ALL
    SELECT 1768, N'DanГ§a Da SolidГЈo', 145, 7, 203520, 6699368, 73
UNION ALL
    SELECT 1769, N'Ao Meu Redor', 145, 7, 275591, 9158834, 183
UNION ALL
    SELECT 1770, N'Bem Leve', 145, 7, 159190, 5246835, 78
UNION ALL
    SELECT 1771, N'Segue O Seco', 145, 7, 178207, 5922018, 161
UNION ALL
    SELECT 1772, N'O Xote Das Meninas', 145, 7, 291866, 9553228, 97
UNION ALL
    SELECT 1773, N'Wherever I Lay My Hat', 146, 14, 136986, 4477321, 52
UNION ALL
    SELECT 1774, N'Get My Hands On Some Lovin''', 146, 14, 149054, 4860380, 96
UNION ALL
    SELECT 1775, N'No Good Without You', 146, 14, 161410, 5259218, 195
UNION ALL
    SELECT 1776, N'You''ve Been A Long Time Coming', 146, 14, 137221, 4437949, 138
UNION ALL
    SELECT 1777, N'When I Had Your Love', 146, 14, 152424, 4972815, 127
UNION ALL
    SELECT 1778, N'You''re What''s Happening (In The World Today)', 146, 14, 142027, 4631104, 151
UNION ALL
    SELECT 1779, N'Loving You Is Sweeter Than Ever', 146, 14, 166295, 5377546, 71
UNION ALL
    SELECT 1780, N'It''s A Bitter Pill To Swallow', 146, 14, 194821, 6477882, 183
UNION ALL
    SELECT 1781, N'Seek And You Shall Find', 146, 14, 223451, 7306719, 173
UNION ALL
    SELECT 1782, N'Gonna Keep On Tryin'' Till I Win Your Love', 146, 14, 176404, 5789945, 188
UNION ALL
    SELECT 1783, N'Gonna Give Her All The Love I''ve Got', 146, 14, 210886, 6893603, 189
UNION ALL
    SELECT 1784, N'I Wish It Would Rain', 146, 14, 172486, 5647327, 194
UNION ALL
    SELECT 1785, N'Abraham, Martin And John', 146, 14, 273057, 8888206, 141
UNION ALL
    SELECT 1786, N'Save The Children', 146, 14, 194821, 6342021, 61
UNION ALL
    SELECT 1787, N'You Sure Love To Ball', 146, 14, 218540, 7217872, 157
UNION ALL
    SELECT 1788, N'Ego Tripping Out', 146, 14, 314514, 10383887, 83
UNION ALL
    SELECT 1789, N'Praise', 146, 14, 235833, 7839179, 159
UNION ALL
    SELECT 1790, N'Heavy Love Affair', 146, 14, 227892, 7522232, 87
UNION ALL
    SELECT 1791, N'Down Under', 147, 1, 222171, 7366142, 92
UNION ALL
    SELECT 1792, N'Overkill', 147, 1, 225410, 7408652, 79
UNION ALL
    SELECT 1793, N'Be Good Johnny', 147, 1, 216320, 7139814, 187
UNION ALL
    SELECT 1794, N'Everything I Need', 147, 1, 216476, 7107625, 198
UNION ALL
    SELECT 1795, N'Down by the Sea', 147, 1, 408163, 13314900, 114
UNION ALL
    SELECT 1796, N'Who Can It Be Now?', 147, 1, 202396, 6682850, 120
UNION ALL
    SELECT 1797, N'It''s a Mistake', 147, 1, 273371, 8979965, 131
UNION ALL
    SELECT 1798, N'Dr. Heckyll & Mr. Jive', 147, 1, 278465, 9110403, 118
UNION ALL
    SELECT 1799, N'Shakes and Ladders', 147, 1, 198008, 6560753, 176
UNION ALL
    SELECT 1800, N'No Sign of Yesterday', 147, 1, 362004, 11829011, 193
UNION ALL
    SELECT 1801, N'Enter Sandman', 148, 3, 332251, 10852002, 73
UNION ALL
    SELECT 1802, N'Sad But True', 148, 3, 324754, 10541258, 96
UNION ALL
    SELECT 1803, N'Holier Than Thou', 148, 3, 227892, 7462011, 191
UNION ALL
    SELECT 1804, N'The Unforgiven', 148, 3, 387082, 12646886, 188
UNION ALL
    SELECT 1805, N'Wherever I May Roam', 148, 3, 404323, 13161169, 52
UNION ALL
    SELECT 1806, N'Don''t Tread On Me', 148, 3, 240483, 7827907, 156
UNION ALL
    SELECT 1807, N'Through The Never', 148, 3, 244375, 8024047, 144
UNION ALL
    SELECT 1808, N'Nothing Else Matters', 148, 3, 388832, 12606241, 105
UNION ALL
    SELECT 1809, N'Of Wolf And Man', 148, 3, 256835, 8339785, 148
UNION ALL
    SELECT 1810, N'The God That Failed', 148, 3, 308610, 10055959, 68
UNION ALL
    SELECT 1811, N'My Friend Of Misery', 148, 3, 409547, 13293515, 162
UNION ALL
    SELECT 1812, N'The Struggle Within', 148, 3, 234240, 7654052, 59
UNION ALL
    SELECT 1813, N'Helpless', 149, 3, 398315, 12977902, 99
UNION ALL
    SELECT 1814, N'The Small Hours', 149, 3, 403435, 13215133, 117
UNION ALL
    SELECT 1815, N'The Wait', 149, 3, 295418, 9688418, 108
UNION ALL
    SELECT 1816, N'Crash Course In Brain Surgery', 149, 3, 190406, 6233729, 113
UNION ALL
    SELECT 1817, N'Last Caress/Green Hell', 149, 3, 209972, 6854313, 105
UNION ALL
    SELECT 1818, N'Am I Evil?', 149, 3, 470256, 15387219, 156
UNION ALL
    SELECT 1819, N'Blitzkrieg', 149, 3, 216685, 7090018, 152
UNION ALL
    SELECT 1820, N'Breadfan', 149, 3, 341551, 11100130, 110
UNION ALL
    SELECT 1821, N'The Prince', 149, 3, 265769, 8624492, 63
UNION ALL
    SELECT 1822, N'Stone Cold Crazy', 149, 3, 137717, 4514830, 100
UNION ALL
    SELECT 1823, N'So What', 149, 3, 189152, 6162894, 114
UNION ALL
    SELECT 1824, N'Killing Time', 149, 3, 183693, 6021197, 140
UNION ALL
    SELECT 1825, N'Overkill', 149, 3, 245133, 7971330, 73
UNION ALL
    SELECT 1826, N'Damage Case', 149, 3, 220212, 7212997, 183
UNION ALL
    SELECT 1827, N'Stone Dead Forever', 149, 3, 292127, 9556060, 66
UNION ALL
    SELECT 1828, N'Too Late Too Late', 149, 3, 192052, 6276291, 87
UNION ALL
    SELECT 1829, N'Hit The Lights', 150, 3, 257541, 8357088, 92
UNION ALL
    SELECT 1830, N'The Four Horsemen', 150, 3, 433188, 14178138, 63
UNION ALL
    SELECT 1831, N'Motorbreath', 150, 3, 188395, 6153933, 66
UNION ALL
    SELECT 1832, N'Jump In The Fire', 150, 3, 281573, 9135755, 166
UNION ALL
    SELECT 1833, N'(Anesthesia) Pulling Teeth', 150, 3, 254955, 8234710, 195
UNION ALL
    SELECT 1834, N'Whiplash', 150, 3, 249208, 8102839, 88
UNION ALL
    SELECT 1835, N'Phantom Lord', 150, 3, 302053, 9817143, 68
UNION ALL
    SELECT 1836, N'No Remorse', 150, 3, 386795, 12672166, 101
UNION ALL
    SELECT 1837, N'Seek & Destroy', 150, 3, 415817, 13452301, 199
UNION ALL
    SELECT 1838, N'Metal Militia', 150, 3, 311327, 10141785, 86
UNION ALL
    SELECT 1839, N'Ain''t My Bitch', 151, 3, 304457, 9931015, 191
UNION ALL
    SELECT 1840, N'2 X 4', 151, 3, 328254, 10732251, 89
UNION ALL
    SELECT 1841, N'The House Jack Built', 151, 3, 398942, 13005152, 69
UNION ALL
    SELECT 1842, N'Until It Sleeps', 151, 3, 269740, 8837394, 106
UNION ALL
    SELECT 1843, N'King Nothing', 151, 3, 328097, 10681477, 93
UNION ALL
    SELECT 1844, N'Hero Of The Day', 151, 3, 261982, 8540298, 69
UNION ALL
    SELECT 1845, N'Bleeding Me', 151, 3, 497998, 16249420, 81
UNION ALL
    SELECT 1846, N'Cure', 151, 3, 294347, 9648615, 104
UNION ALL
    SELECT 1847, N'Poor Twisted Me', 151, 3, 240065, 7854349, 59
UNION ALL
    SELECT 1848, N'Wasted My Hate', 151, 3, 237296, 7762300, 84
UNION ALL
    SELECT 1849, N'Mama Said', 151, 3, 319764, 10508310, 190
UNION ALL
    SELECT 1850, N'Thorn Within', 151, 3, 351738, 11486686, 67
UNION ALL
    SELECT 1851, N'Ronnie', 151, 3, 317204, 10390947, 62
UNION ALL
    SELECT 1852, N'The Outlaw Torn', 151, 3, 588721, 19286261, 113
UNION ALL
    SELECT 1853, N'Battery', 152, 3, 312424, 10229577, 166
UNION ALL
    SELECT 1854, N'Master Of Puppets', 152, 3, 515239, 16893720, 113
UNION ALL
    SELECT 1855, N'The Thing That Should Not Be', 152, 3, 396199, 12952368, 107
UNION ALL
    SELECT 1856, N'Welcome Home (Sanitarium)', 152, 3, 387186, 12679965, 168
UNION ALL
    SELECT 1857, N'Disposable Heroes', 152, 3, 496718, 16135560, 175
UNION ALL
    SELECT 1858, N'Leper Messiah', 152, 3, 347428, 11310434, 172
UNION ALL
    SELECT 1859, N'Orion', 152, 3, 500062, 16378477, 115
UNION ALL
    SELECT 1860, N'Damage Inc.', 152, 3, 330919, 10725029, 200
UNION ALL
    SELECT 1861, N'Fuel', 153, 3, 269557, 8876811, 95
UNION ALL
    SELECT 1862, N'The Memory Remains', 153, 3, 279353, 9110730, 131
UNION ALL
    SELECT 1863, N'Devil''s Dance', 153, 3, 318955, 10414832, 77
UNION ALL
    SELECT 1864, N'The Unforgiven II', 153, 3, 395520, 12886474, 184
UNION ALL
    SELECT 1865, N'Better Than You', 153, 3, 322899, 10549070, 162
UNION ALL
    SELECT 1866, N'Slither', 153, 3, 313103, 10199789, 192
UNION ALL
    SELECT 1867, N'Carpe Diem Baby', 153, 3, 372480, 12170693, 186
UNION ALL
    SELECT 1868, N'Bad Seed', 153, 3, 245394, 8019586, 156
UNION ALL
    SELECT 1869, N'Where The Wild Things Are', 153, 3, 414380, 13571280, 129
UNION ALL
    SELECT 1870, N'Prince Charming', 153, 3, 365061, 12009412, 83
UNION ALL
    SELECT 1871, N'Low Man''s Lyric', 153, 3, 457639, 14855583, 103
UNION ALL
    SELECT 1872, N'Attitude', 153, 3, 315898, 10335734, 113
UNION ALL
    SELECT 1873, N'Fixxxer', 153, 3, 496065, 16190041, 87
UNION ALL
    SELECT 1874, N'Fight Fire With Fire', 154, 3, 285753, 9420856, 71
UNION ALL
    SELECT 1875, N'Ride The Lightning', 154, 3, 397740, 13055884, 129
UNION ALL
    SELECT 1876, N'For Whom The Bell Tolls', 154, 3, 311719, 10159725, 194
UNION ALL
    SELECT 1877, N'Fade To Black', 154, 3, 414824, 13531954, 138
UNION ALL
    SELECT 1878, N'Trapped Under Ice', 154, 3, 244532, 7975942, 121
UNION ALL
    SELECT 1879, N'Escape', 154, 3, 264359, 8652332, 112
UNION ALL
    SELECT 1880, N'Creeping Death', 154, 3, 396878, 12955593, 138
UNION ALL
    SELECT 1881, N'The Call Of Ktulu', 154, 3, 534883, 17486240, 184
UNION ALL
    SELECT 1882, N'Frantic', 155, 3, 350458, 11510849, 74
UNION ALL
    SELECT 1883, N'St. Anger', 155, 3, 441234, 14363779, 125
UNION ALL
    SELECT 1884, N'Some Kind Of Monster', 155, 3, 505626, 16557497, 131
UNION ALL
    SELECT 1885, N'Dirty Window', 155, 3, 324989, 10670604, 114
UNION ALL
    SELECT 1886, N'Invisible Kid', 155, 3, 510197, 16591800, 92
UNION ALL
    SELECT 1887, N'My World', 155, 3, 345626, 11253756, 139
UNION ALL
    SELECT 1888, N'Shoot Me Again', 155, 3, 430210, 14093551, 116
UNION ALL
    SELECT 1889, N'Sweet Amber', 155, 3, 327235, 10616595, 107
UNION ALL
    SELECT 1890, N'The Unnamed Feeling', 155, 3, 429479, 14014582, 80
UNION ALL
    SELECT 1891, N'Purify', 155, 3, 314017, 10232537, 133
UNION ALL
    SELECT 1892, N'All Within My Hands', 155, 3, 527986, 17162741, 130
UNION ALL
    SELECT 1893, N'Blackened', 156, 3, 403382, 13254874, 153
UNION ALL
    SELECT 1894, N'...And Justice For All', 156, 3, 585769, 19262088, 152
UNION ALL
    SELECT 1895, N'Eye Of The Beholder', 156, 3, 385828, 12747894, 112
UNION ALL
    SELECT 1896, N'One', 156, 3, 446484, 14695721, 124
UNION ALL
    SELECT 1897, N'The Shortest Straw', 156, 3, 395389, 13013990, 173
UNION ALL
    SELECT 1898, N'Harvester Of Sorrow', 156, 3, 345547, 11377339, 99
UNION ALL
    SELECT 1899, N'The Frayed Ends Of Sanity', 156, 3, 464039, 15198986, 178
UNION ALL
    SELECT 1900, N'To Live Is To Die', 156, 3, 588564, 19243795, 191
UNION ALL
    SELECT 1901, N'Dyers Eve', 156, 3, 313991, 10302828, 85
UNION ALL
    SELECT 1902, N'Springsville', 157, 2, 207725, 6776219, 178
UNION ALL
    SELECT 1903, N'The Maids Of Cadiz', 157, 2, 233534, 7505275, 87
UNION ALL
    SELECT 1904, N'The Duke', 157, 2, 214961, 6977626, 178
UNION ALL
    SELECT 1905, N'My Ship', 157, 2, 268016, 8581144, 54
UNION ALL
    SELECT 1906, N'Miles Ahead', 157, 2, 209893, 6807707, 139
UNION ALL
    SELECT 1907, N'Blues For Pablo', 157, 2, 318328, 10218398, 129
UNION ALL
    SELECT 1908, N'New Rhumba', 157, 2, 276871, 8980400, 51
UNION ALL
    SELECT 1909, N'The Meaning Of The Blues', 157, 2, 168594, 5395412, 88
UNION ALL
    SELECT 1910, N'Lament', 157, 2, 134191, 4293394, 140
UNION ALL
    SELECT 1911, N'I Don''t Wanna Be Kissed (By Anyone But You)', 157, 2, 191320, 6219487, 58
UNION ALL
    SELECT 1912, N'Springsville (Alternate Take)', 157, 2, 196388, 6382079, 92
UNION ALL
    SELECT 1913, N'Blues For Pablo (Alternate Take)', 157, 2, 212558, 6900619, 101
UNION ALL
    SELECT 1914, N'The Meaning Of The Blues/Lament (Alternate Take)', 157, 2, 309786, 9912387, 136
UNION ALL
    SELECT 1915, N'I Don''t Wanna Be Kissed (By Anyone But You) (Alternate Take)', 157, 2, 192078, 6254796, 182
UNION ALL
    SELECT 1916, N'CoraГ§ГЈo De Estudante', 158, 7, 238550, 7797308, 103
UNION ALL
    SELECT 1917, N'A Noite Do Meu Bem', 158, 7, 220081, 7125225, 140
UNION ALL
    SELECT 1918, N'Paisagem Na Janela', 158, 7, 197694, 6523547, 106
UNION ALL
    SELECT 1919, N'Cuitelinho', 158, 7, 209397, 6803970, 182
UNION ALL
    SELECT 1920, N'CaxangГЎ', 158, 7, 245551, 8144179, 82
UNION ALL
    SELECT 1921, N'Nos Bailes Da Vida', 158, 7, 275748, 9126170, 80
UNION ALL
    SELECT 1922, N'Menestrel Das Alagoas', 158, 7, 199758, 6542289, 164
UNION ALL
    SELECT 1923, N'Brasil', 158, 7, 155428, 5252560, 83
UNION ALL
    SELECT 1924, N'CanГ§ГЈo Do Novo Mundo', 158, 7, 215353, 7032626, 125
UNION ALL
    SELECT 1925, N'Um Gosto De Sol', 158, 7, 307200, 9893875, 67
UNION ALL
    SELECT 1926, N'Solar', 158, 7, 156212, 5098288, 171
UNION ALL
    SELECT 1927, N'Para Lennon E McCartney', 158, 7, 321828, 10626920, 84
UNION ALL
    SELECT 1928, N'Maria, Maria', 158, 7, 72463, 2371543, 195
UNION ALL
    SELECT 1929, N'Minas', 159, 7, 152293, 4921056, 112
UNION ALL
    SELECT 1930, N'FГ© Cega, Faca Amolada', 159, 7, 278099, 9258649, 114
UNION ALL
    SELECT 1931, N'Beijo Partido', 159, 7, 229564, 7506969, 95
UNION ALL
    SELECT 1932, N'Saudade Dos AviГµes Da Panair (Conversando No Bar)', 159, 7, 268721, 8805088, 98
UNION ALL
    SELECT 1933, N'Gran Circo', 159, 7, 251297, 8237026, 119
UNION ALL
    SELECT 1934, N'Ponta de Areia', 159, 7, 272796, 8874285, 163
UNION ALL
    SELECT 1935, N'Trastevere', 159, 7, 265665, 8708399, 70
UNION ALL
    SELECT 1936, N'Idolatrada', 159, 7, 286249, 9426153, 81
UNION ALL
    SELECT 1937, N'Leila (Venha Ser Feliz)', 159, 7, 209737, 6898507, 95
UNION ALL
    SELECT 1938, N'Paula E Bebeto', 159, 7, 135732, 4583956, 126
UNION ALL
    SELECT 1939, N'Simples', 159, 7, 133093, 4326333, 106
UNION ALL
    SELECT 1940, N'Norwegian Wood', 159, 7, 413910, 13520382, 129
UNION ALL
    SELECT 1941, N'Caso VocГЄ Queira Saber', 159, 7, 205688, 6787901, 155
UNION ALL
    SELECT 1942, N'Ace Of Spades', 160, 3, 169926, 5523552, 117
UNION ALL
    SELECT 1943, N'Love Me Like A Reptile', 160, 3, 203546, 6616389, 117
UNION ALL
    SELECT 1944, N'Shoot You In The Back', 160, 3, 160026, 5175327, 73
UNION ALL
    SELECT 1945, N'Live To Win', 160, 3, 217626, 7102182, 175
UNION ALL
    SELECT 1946, N'Fast And Loose', 160, 3, 203337, 6643350, 130
UNION ALL
    SELECT 1947, N'(We Are) The Road Crew', 160, 3, 192600, 6283035, 138
UNION ALL
    SELECT 1948, N'Fire Fire', 160, 3, 164675, 5416114, 169
UNION ALL
    SELECT 1949, N'Jailbait', 160, 3, 213916, 6983609, 155
UNION ALL
    SELECT 1950, N'Dance', 160, 3, 158432, 5155099, 119
UNION ALL
    SELECT 1951, N'Bite The Bullet', 160, 3, 98115, 3195536, 174
UNION ALL
    SELECT 1952, N'The Chase Is Better Than The Catch', 160, 3, 258403, 8393310, 124
UNION ALL
    SELECT 1953, N'The Hammer', 160, 3, 168071, 5543267, 181
UNION ALL
    SELECT 1954, N'Dirty Love', 160, 3, 176457, 5805241, 95
UNION ALL
    SELECT 1955, N'Please Don''t Touch', 160, 3, 169926, 5557002, 114
UNION ALL
    SELECT 1956, N'Emergency', 160, 3, 180427, 5828728, 196
UNION ALL
    SELECT 1957, N'Kir Royal', 161, 16, 234788, 7706552, 152
UNION ALL
    SELECT 1958, N'O Que Vai Em Meu CoraГ§ГЈo', 161, 16, 255373, 8366846, 173
UNION ALL
    SELECT 1959, N'Aos LeГµes', 161, 16, 234684, 7790574, 138
UNION ALL
    SELECT 1960, N'Dois ГЌndios', 161, 16, 219271, 7213072, 173
UNION ALL
    SELECT 1961, N'Noite Negra', 161, 16, 206811, 6819584, 193
UNION ALL
    SELECT 1962, N'Beijo do Olhar', 161, 16, 252682, 8369029, 106
UNION ALL
    SELECT 1963, N'Г‰ Fogo', 161, 16, 194873, 6501520, 122
UNION ALL
    SELECT 1964, N'JГЎ Foi', 161, 16, 245681, 8094872, 154
UNION ALL
    SELECT 1965, N'SГі Se For Pelo Cabelo', 161, 16, 238288, 8006345, 99
UNION ALL
    SELECT 1966, N'No Clima', 161, 16, 249495, 8362040, 120
UNION ALL
    SELECT 1967, N'A MoГ§a e a Chuva', 161, 16, 274625, 8929357, 80
UNION ALL
    SELECT 1968, N'Demorou!', 161, 16, 39131, 1287083, 161
UNION ALL
    SELECT 1969, N'Bitter Pill', 162, 3, 266814, 8666786, 156
UNION ALL
    SELECT 1970, N'Enslaved', 162, 3, 269844, 8789966, 104
UNION ALL
    SELECT 1971, N'Girls, Girls, Girls', 162, 3, 270288, 8874814, 127
UNION ALL
    SELECT 1972, N'Kickstart My Heart', 162, 3, 283559, 9237736, 133
UNION ALL
    SELECT 1973, N'Wild Side', 162, 3, 276767, 9116997, 88
UNION ALL
    SELECT 1974, N'Glitter', 162, 3, 340114, 11184094, 174
UNION ALL
    SELECT 1975, N'Dr. Feelgood', 162, 3, 282618, 9281875, 165
UNION ALL
    SELECT 1976, N'Same Ol'' Situation', 162, 3, 254511, 8283958, 52
UNION ALL
    SELECT 1977, N'Home Sweet Home', 162, 3, 236904, 7697538, 90
UNION ALL
    SELECT 1978, N'Afraid', 162, 3, 248006, 8077464, 73
UNION ALL
    SELECT 1979, N'Don''t Go Away Mad (Just Go Away)', 162, 3, 279980, 9188156, 53
UNION ALL
    SELECT 1980, N'Without You', 162, 3, 268956, 8738371, 83
UNION ALL
    SELECT 1981, N'Smokin'' in The Boys Room', 162, 3, 206837, 6735408, 89
UNION ALL
    SELECT 1982, N'Primal Scream', 162, 3, 286197, 9421164, 148
UNION ALL
    SELECT 1983, N'Too Fast For Love', 162, 3, 200829, 6580542, 116
UNION ALL
    SELECT 1984, N'Looks That Kill', 162, 3, 240979, 7831122, 192
UNION ALL
    SELECT 1985, N'Shout At The Devil', 162, 3, 221962, 7281974, 193
UNION ALL
    SELECT 1986, N'Intro', 163, 1, 52218, 1688527, 145
UNION ALL
    SELECT 1987, N'School', 163, 1, 160235, 5234885, 76
UNION ALL
    SELECT 1988, N'Drain You', 163, 1, 215196, 7013175, 72
UNION ALL
    SELECT 1989, N'Aneurysm', 163, 1, 271516, 8862545, 182
UNION ALL
    SELECT 1990, N'Smells Like Teen Spirit', 163, 1, 287190, 9425215, 195
UNION ALL
    SELECT 1991, N'Been A Son', 163, 1, 127555, 4170369, 159
UNION ALL
    SELECT 1992, N'Lithium', 163, 1, 250017, 8148800, 166
UNION ALL
    SELECT 1993, N'Sliver', 163, 1, 116218, 3784567, 99
UNION ALL
    SELECT 1994, N'Spank Thru', 163, 1, 190354, 6186487, 57
UNION ALL
    SELECT 1995, N'Scentless Apprentice', 163, 1, 211200, 6898177, 109
UNION ALL
    SELECT 1996, N'Heart-Shaped Box', 163, 1, 281887, 9210982, 73
UNION ALL
    SELECT 1997, N'Milk It', 163, 1, 225724, 7406945, 141
UNION ALL
    SELECT 1998, N'Negative Creep', 163, 1, 163761, 5354854, 88
UNION ALL
    SELECT 1999, N'Polly', 163, 1, 149995, 4885331, 170
UNION ALL
    SELECT 2000, N'Breed', 163, 1, 208378, 6759080, 80
UNION ALL
    SELECT 2001, N'Tourette''s', 163, 1, 115591, 3753246, 81
UNION ALL
    SELECT 2002, N'Blew', 163, 1, 216346, 7096936, 163
UNION ALL
    SELECT 2003, N'Smells Like Teen Spirit', 164, 1, 301296, 9823847, 135
UNION ALL
    SELECT 2004, N'In Bloom', 164, 1, 254928, 8327077, 70
UNION ALL
    SELECT 2005, N'Come As You Are', 164, 1, 219219, 7123357, 122
UNION ALL
    SELECT 2006, N'Breed', 164, 1, 183928, 5984812, 170
UNION ALL
    SELECT 2007, N'Lithium', 164, 1, 256992, 8404745, 130
UNION ALL
    SELECT 2008, N'Polly', 164, 1, 177031, 5788407, 74
UNION ALL
    SELECT 2009, N'Territorial Pissings', 164, 1, 143281, 4613880, 155
UNION ALL
    SELECT 2010, N'Drain You', 164, 1, 223973, 7273440, 79
UNION ALL
    SELECT 2011, N'Lounge Act', 164, 1, 156786, 5093635, 137
UNION ALL
    SELECT 2012, N'Stay Away', 164, 1, 212636, 6956404, 70
UNION ALL
    SELECT 2013, N'On A Plain', 164, 1, 196440, 6390635, 53
UNION ALL
    SELECT 2014, N'Something In The Way', 164, 1, 230556, 7472168, 192
UNION ALL
    SELECT 2015, N'Time', 165, 1, 96888, 3124455, 167
UNION ALL
    SELECT 2016, N'P.S.ApareГ§a', 165, 1, 209188, 6842244, 158
UNION ALL
    SELECT 2017, N'Sangue Latino', 165, 1, 223033, 7354184, 62
UNION ALL
    SELECT 2018, N'Folhas Secas', 165, 1, 161253, 5284522, 123
UNION ALL
    SELECT 2019, N'Poeira', 165, 1, 267075, 8784141, 128
UNION ALL
    SELECT 2020, N'MГЎgica', 165, 1, 233743, 7627348, 168
UNION ALL
    SELECT 2021, N'Quem Mata A Mulher Mata O Melhor', 165, 1, 262791, 8640121, 85
UNION ALL
    SELECT 2022, N'MundarГ©u', 165, 1, 217521, 7158975, 142
UNION ALL
    SELECT 2023, N'O BraГ§o Da Minha Guitarra', 165, 1, 258351, 8469531, 109
UNION ALL
    SELECT 2024, N'Deus', 165, 1, 284160, 9188110, 160
UNION ALL
    SELECT 2025, N'MГЈe Terra', 165, 1, 306625, 9949269, 127
UNION ALL
    SELECT 2026, N'ГЂs Vezes', 165, 1, 330292, 10706614, 58
UNION ALL
    SELECT 2027, N'Menino De Rua', 165, 1, 329795, 10784595, 145
UNION ALL
    SELECT 2028, N'Prazer E FГ©', 165, 1, 214831, 7031383, 53
UNION ALL
    SELECT 2029, N'Elza', 165, 1, 199105, 6517629, 182
UNION ALL
    SELECT 2030, N'Requebra', 166, 7, 240744, 8010811, 157
UNION ALL
    SELECT 2031, N'Nossa Gente (Avisa LГ )', 166, 7, 188212, 6233201, 77
UNION ALL
    SELECT 2032, N'Olodum - Alegria Geral', 166, 7, 233404, 7754245, 132
UNION ALL
    SELECT 2033, N'MadagГЎscar Olodum', 166, 7, 252264, 8270584, 185
UNION ALL
    SELECT 2034, N'FaraГі Divindade Do Egito', 166, 7, 228571, 7523278, 124
UNION ALL
    SELECT 2035, N'Todo Amor (Asas Da Liberdade)', 166, 7, 245133, 8121434, 142
UNION ALL
    SELECT 2036, N'DenГєncia', 166, 7, 159555, 5327433, 151
UNION ALL
    SELECT 2037, N'Olodum, A Banda Do PelГґ', 166, 7, 146599, 4900121, 134
UNION ALL
    SELECT 2038, N'Cartao Postal', 166, 7, 211565, 7082301, 59
UNION ALL
    SELECT 2039, N'Jeito Faceiro', 166, 7, 217286, 7233608, 58
UNION ALL
    SELECT 2040, N'Revolta Olodum', 166, 7, 230191, 7557065, 181
UNION ALL
    SELECT 2041, N'Reggae OdoyГЎ', 166, 7, 224470, 7499807, 65
UNION ALL
    SELECT 2042, N'Protesto Do Olodum (Ao Vivo)', 166, 7, 206001, 6766104, 186
UNION ALL
    SELECT 2043, N'Olodum - Smile (Instrumental)', 166, 7, 235833, 7871409, 167
UNION ALL
    SELECT 2044, N'VulcГЈo Dub - Fui Eu', 167, 7, 287059, 9495202, 88
UNION ALL
    SELECT 2045, N'O Trem Da Juventude', 167, 7, 225880, 7507655, 80
UNION ALL
    SELECT 2046, N'Manguetown', 167, 7, 162925, 5382018, 166
UNION ALL
    SELECT 2047, N'Um Amor, Um Lugar', 167, 7, 184555, 6090334, 159
UNION ALL
    SELECT 2048, N'Bora-Bora', 167, 7, 182987, 6036046, 129
UNION ALL
    SELECT 2049, N'Vai Valer', 167, 7, 206524, 6899778, 170
UNION ALL
    SELECT 2050, N'I Feel Good (I Got You) - Sossego', 167, 7, 244976, 8091302, 139
UNION ALL
    SELECT 2051, N'Uns Dias', 167, 7, 240796, 7931552, 133
UNION ALL
    SELECT 2052, N'Sincero Breu', 167, 7, 208013, 6921669, 152
UNION ALL
    SELECT 2053, N'Meu Erro', 167, 7, 188577, 6192791, 172
UNION ALL
    SELECT 2054, N'Selvagem', 167, 7, 148558, 4942831, 90
UNION ALL
    SELECT 2055, N'BrasГ­lia 5:31', 167, 7, 178337, 5857116, 87
UNION ALL
    SELECT 2056, N'Tendo A Lua', 167, 7, 198922, 6568180, 194
UNION ALL
    SELECT 2057, N'Que PaГ­s Г‰ Este', 167, 7, 216685, 7137865, 127
UNION ALL
    SELECT 2058, N'Navegar Impreciso', 167, 7, 262870, 8761283, 95
UNION ALL
    SELECT 2059, N'Feira Moderna', 167, 7, 182517, 6001793, 124
UNION ALL
    SELECT 2060, N'Tequila - Lourinha Bombril (Parate Y Mira)', 167, 7, 255738, 8514961, 129
UNION ALL
    SELECT 2061, N'Vamo BatГЄ Lata', 167, 7, 228754, 7585707, 178
UNION ALL
    SELECT 2062, N'Life During Wartime', 167, 7, 259186, 8543439, 136
UNION ALL
    SELECT 2063, N'Nebulosa Do Amor', 167, 7, 203415, 6732496, 132
UNION ALL
    SELECT 2064, N'CaleidoscГіpio', 167, 7, 256522, 8484597, 196
UNION ALL
    SELECT 2065, N'Trac Trac', 168, 7, 231653, 7638256, 62
UNION ALL
    SELECT 2066, N'Tendo A Lua', 168, 7, 219585, 7342776, 68
UNION ALL
    SELECT 2067, N'Mensagen De Amor (2000)', 168, 7, 183588, 6061324, 131
UNION ALL
    SELECT 2068, N'Lourinha Bombril', 168, 7, 159895, 5301882, 51
UNION ALL
    SELECT 2069, N'La Bella Luna', 168, 7, 192653, 6428598, 114
UNION ALL
    SELECT 2070, N'Busca Vida', 168, 7, 176431, 5798663, 106
UNION ALL
    SELECT 2071, N'Uma Brasileira', 168, 7, 217573, 7280574, 147
UNION ALL
    SELECT 2072, N'Luis Inacio (300 Picaretas)', 168, 7, 198191, 6576790, 183
UNION ALL
    SELECT 2073, N'Saber Amar', 168, 7, 202788, 6723733, 146
UNION ALL
    SELECT 2074, N'Ela Disse Adeus', 168, 7, 226298, 7608999, 128
UNION ALL
    SELECT 2075, N'O Amor Nao Sabe Esperar', 168, 7, 241084, 8042534, 165
UNION ALL
    SELECT 2076, N'Aonde Quer Que Eu Va', 168, 7, 258089, 8470121, 132
UNION ALL
    SELECT 2077, N'CaleidoscГіpio', 169, 7, 211330, 7000017, 155
UNION ALL
    SELECT 2078, N'Г“culos', 169, 7, 219271, 7262419, 57
UNION ALL
    SELECT 2079, N'Cinema Mudo', 169, 7, 227918, 7612168, 165
UNION ALL
    SELECT 2080, N'Alagados', 169, 7, 302393, 10255463, 66
UNION ALL
    SELECT 2081, N'Lanterna Dos Afogados', 169, 7, 190197, 6264318, 129
UNION ALL
    SELECT 2082, N'MelГґ Do Marinheiro', 169, 7, 208352, 6905668, 58
UNION ALL
    SELECT 2083, N'Vital E Sua Moto', 169, 7, 210207, 6902878, 179
UNION ALL
    SELECT 2084, N'O Beco', 169, 7, 189178, 6293184, 200
UNION ALL
    SELECT 2085, N'Meu Erro', 169, 7, 208431, 6893533, 69
UNION ALL
    SELECT 2086, N'Perplexo', 169, 7, 161175, 5355013, 149
UNION ALL
    SELECT 2087, N'Me Liga', 169, 7, 229590, 7565912, 194
UNION ALL
    SELECT 2088, N'Quase Um Segundo', 169, 7, 275644, 8971355, 109
UNION ALL
    SELECT 2089, N'Selvagem', 169, 7, 245890, 8141084, 92
UNION ALL
    SELECT 2090, N'Romance Ideal', 169, 7, 250070, 8260477, 183
UNION ALL
    SELECT 2091, N'SerГЎ Que Vai Chover?', 169, 7, 337057, 11133830, 145
UNION ALL
    SELECT 2092, N'SKA', 169, 7, 148871, 4943540, 183
UNION ALL
    SELECT 2093, N'Bark at the Moon', 170, 1, 257252, 4601224, 103
UNION ALL
    SELECT 2094, N'I Don''t Know', 171, 1, 312980, 5525339, 190
UNION ALL
    SELECT 2095, N'Crazy Train', 171, 1, 295960, 5255083, 158
UNION ALL
    SELECT 2096, N'Flying High Again', 172, 1, 290851, 5179599, 168
UNION ALL
    SELECT 2097, N'Mama, I''m Coming Home', 173, 1, 251586, 4302390, 53
UNION ALL
    SELECT 2098, N'No More Tears', 173, 1, 444358, 7362964, 78
UNION ALL
    SELECT 2099, N'I Don''t Know', 174, 3, 283088, 9207869, 196
UNION ALL
    SELECT 2100, N'Crazy Train', 174, 3, 322716, 10517408, 139
UNION ALL
    SELECT 2101, N'Believer', 174, 3, 308897, 10003794, 169
UNION ALL
    SELECT 2102, N'Mr. Crowley', 174, 3, 344241, 11184130, 175
UNION ALL
    SELECT 2103, N'Flying High Again', 174, 3, 261224, 8481822, 93
UNION ALL
    SELECT 2104, N'Relvelation (Mother Earth)', 174, 3, 349440, 11367866, 127
UNION ALL
    SELECT 2105, N'Steal Away (The Night)', 174, 3, 485720, 15945806, 76
UNION ALL
    SELECT 2106, N'Suicide Solution (With Guitar Solo)', 174, 3, 467069, 15119938, 68
UNION ALL
    SELECT 2107, N'Iron Man', 174, 3, 172120, 5609799, 79
UNION ALL
    SELECT 2108, N'Children Of The Grave', 174, 3, 357067, 11626740, 178
UNION ALL
    SELECT 2109, N'Paranoid', 174, 3, 176352, 5729813, 156
UNION ALL
    SELECT 2110, N'Goodbye To Romance', 174, 3, 334393, 10841337, 135
UNION ALL
    SELECT 2111, N'No Bone Movies', 174, 3, 249208, 8095199, 180
UNION ALL
    SELECT 2112, N'Dee', 174, 3, 261302, 8555963, 109
UNION ALL
    SELECT 2113, N'Shining In The Light', 175, 1, 240796, 7951688, 198
UNION ALL
    SELECT 2114, N'When The World Was Young', 175, 1, 373394, 12198930, 148
UNION ALL
    SELECT 2115, N'Upon A Golden Horse', 175, 1, 232359, 7594829, 112
UNION ALL
    SELECT 2116, N'Blue Train', 175, 1, 405028, 13170391, 54
UNION ALL
    SELECT 2117, N'Please Read The Letter', 175, 1, 262112, 8603372, 199
UNION ALL
    SELECT 2118, N'Most High', 175, 1, 336535, 10999203, 184
UNION ALL
    SELECT 2119, N'Heart In Your Hand', 175, 1, 230896, 7598019, 194
UNION ALL
    SELECT 2120, N'Walking Into Clarksdale', 175, 1, 318511, 10396315, 55
UNION ALL
    SELECT 2121, N'Burning Up', 175, 1, 321619, 10525136, 196
UNION ALL
    SELECT 2122, N'When I Was A Child', 175, 1, 345626, 11249456, 121
UNION ALL
    SELECT 2123, N'House Of Love', 175, 1, 335699, 10990880, 186
UNION ALL
    SELECT 2124, N'Sons Of Freedom', 175, 1, 246465, 8087944, 153
UNION ALL
    SELECT 2125, N'United Colours', 176, 10, 330266, 10939131, 127
UNION ALL
    SELECT 2126, N'Slug', 176, 10, 281469, 9295950, 80
UNION ALL
    SELECT 2127, N'Your Blue Room', 176, 10, 328228, 10867860, 116
UNION ALL
    SELECT 2128, N'Always Forever Now', 176, 10, 383764, 12727928, 200
UNION ALL
    SELECT 2129, N'A Different Kind Of Blue', 176, 10, 120816, 3884133, 143
UNION ALL
    SELECT 2130, N'Beach Sequence', 176, 10, 212297, 6928259, 200
UNION ALL
    SELECT 2131, N'Miss Sarajevo', 176, 10, 340767, 11064884, 156
UNION ALL
    SELECT 2132, N'Ito Okashi', 176, 10, 205087, 6572813, 175
UNION ALL
    SELECT 2133, N'One Minute Warning', 176, 10, 279693, 9335453, 108
UNION ALL
    SELECT 2134, N'Corpse (These Chains Are Way Too Long)', 176, 10, 214909, 6920451, 143
UNION ALL
    SELECT 2135, N'Elvis Ate America', 176, 10, 180166, 5851053, 94
UNION ALL
    SELECT 2136, N'Plot 180', 176, 10, 221596, 7253729, 109
UNION ALL
    SELECT 2137, N'Theme From The Swan', 176, 10, 203911, 6638076, 53
UNION ALL
    SELECT 2138, N'Theme From Let''s Go Native', 176, 10, 186723, 6179777, 77
UNION ALL
    SELECT 2139, N'Wrathchild', 177, 1, 170396, 5499390, 159
UNION ALL
    SELECT 2140, N'Killers', 177, 1, 309995, 10009697, 57
UNION ALL
    SELECT 2141, N'Prowler', 177, 1, 240274, 7782963, 107
UNION ALL
    SELECT 2142, N'Murders In The Rue Morgue', 177, 1, 258638, 8360999, 109
UNION ALL
    SELECT 2143, N'Women In Uniform', 177, 1, 189936, 6139651, 120
UNION ALL
    SELECT 2144, N'Remember Tomorrow', 177, 1, 326426, 10577976, 93
UNION ALL
    SELECT 2145, N'Sanctuary', 177, 1, 198844, 6423543, 107
UNION ALL
    SELECT 2146, N'Running Free', 177, 1, 199706, 6483496, 97
UNION ALL
    SELECT 2147, N'Phantom Of The Opera', 177, 1, 418168, 13585530, 169
UNION ALL
    SELECT 2148, N'Iron Maiden', 177, 1, 235232, 7600077, 195
UNION ALL
    SELECT 2149, N'Corduroy', 178, 1, 305293, 9991106, 179
UNION ALL
    SELECT 2150, N'Given To Fly', 178, 1, 233613, 7678347, 133
UNION ALL
    SELECT 2151, N'Hail, Hail', 178, 1, 223764, 7364206, 181
UNION ALL
    SELECT 2152, N'Daughter', 178, 1, 407484, 13420697, 157
UNION ALL
    SELECT 2153, N'Elderly Woman Behind The Counter In A Small Town', 178, 1, 229328, 7509304, 176
UNION ALL
    SELECT 2154, N'Untitled', 178, 1, 122801, 3957141, 65
UNION ALL
    SELECT 2155, N'MFC', 178, 1, 148192, 4817665, 179
UNION ALL
    SELECT 2156, N'Go', 178, 1, 161541, 5290810, 118
UNION ALL
    SELECT 2157, N'Red Mosquito', 178, 1, 242991, 7944923, 79
UNION ALL
    SELECT 2158, N'Even Flow', 178, 1, 317100, 10394239, 190
UNION ALL
    SELECT 2159, N'Off He Goes', 178, 1, 343222, 11245109, 63
UNION ALL
    SELECT 2160, N'Nothingman', 178, 1, 278595, 9107017, 101
UNION ALL
    SELECT 2161, N'Do The Evolution', 178, 1, 225462, 7377286, 132
UNION ALL
    SELECT 2162, N'Better Man', 178, 1, 246204, 8019563, 131
UNION ALL
    SELECT 2163, N'Black', 178, 1, 415712, 13580009, 149
UNION ALL
    SELECT 2164, N'F*Ckin'' Up', 178, 1, 377652, 12360893, 136
UNION ALL
    SELECT 2165, N'Life Wasted', 179, 4, 234344, 7610169, 65
UNION ALL
    SELECT 2166, N'World Wide Suicide', 179, 4, 209188, 6885908, 80
UNION ALL
    SELECT 2167, N'Comatose', 179, 4, 139990, 4574516, 101
UNION ALL
    SELECT 2168, N'Severed Hand', 179, 4, 270341, 8817438, 161
UNION ALL
    SELECT 2169, N'Marker In The Sand', 179, 4, 263235, 8656578, 90
UNION ALL
    SELECT 2170, N'Parachutes', 179, 4, 216555, 7074973, 55
UNION ALL
    SELECT 2171, N'Unemployable', 179, 4, 184398, 6066542, 50
UNION ALL
    SELECT 2172, N'Big Wave', 179, 4, 178573, 5858788, 120
UNION ALL
    SELECT 2173, N'Gone', 179, 4, 249547, 8158204, 55
UNION ALL
    SELECT 2174, N'Wasted Reprise', 179, 4, 53733, 1731020, 117
UNION ALL
    SELECT 2175, N'Army Reserve', 179, 4, 225567, 7393771, 151
UNION ALL
    SELECT 2176, N'Come Back', 179, 4, 329743, 10768701, 83
UNION ALL
    SELECT 2177, N'Inside Job', 179, 4, 428643, 14006924, 106
UNION ALL
    SELECT 2178, N'Can''t Keep', 180, 1, 219428, 7215713, 119
UNION ALL
    SELECT 2179, N'Save You', 180, 1, 230112, 7609110, 187
UNION ALL
    SELECT 2180, N'Love Boat Captain', 180, 1, 276453, 9016789, 61
UNION ALL
    SELECT 2181, N'Cropduster', 180, 1, 231888, 7588928, 124
UNION ALL
    SELECT 2182, N'Ghost', 180, 1, 195108, 6383772, 197
UNION ALL
    SELECT 2183, N'I Am Mine', 180, 1, 215719, 7086901, 93
UNION ALL
    SELECT 2184, N'Thumbing My Way', 180, 1, 250226, 8201437, 105
UNION ALL
    SELECT 2185, N'You Are', 180, 1, 270863, 8938409, 63
UNION ALL
    SELECT 2186, N'Get Right', 180, 1, 158589, 5223345, 161
UNION ALL
    SELECT 2187, N'Green Disease', 180, 1, 161253, 5375818, 159
UNION ALL
    SELECT 2188, N'Help Help', 180, 1, 215092, 7033002, 169
UNION ALL
    SELECT 2189, N'Bushleager', 180, 1, 237479, 7849757, 138
UNION ALL
    SELECT 2190, N'1/2 Full', 180, 1, 251010, 8197219, 173
UNION ALL
    SELECT 2191, N'Arc', 180, 1, 65593, 2099421, 193
UNION ALL
    SELECT 2192, N'All or None', 180, 1, 277655, 9104728, 126
UNION ALL
    SELECT 2193, N'Once', 181, 1, 231758, 7561555, 106
UNION ALL
    SELECT 2194, N'Evenflow', 181, 1, 293720, 9622017, 151
UNION ALL
    SELECT 2195, N'Alive', 181, 1, 341080, 11176623, 173
UNION ALL
    SELECT 2196, N'Why Go', 181, 1, 200254, 6539287, 149
UNION ALL
    SELECT 2197, N'Black', 181, 1, 343823, 11213314, 173
UNION ALL
    SELECT 2198, N'Jeremy', 181, 1, 318981, 10447222, 158
UNION ALL
    SELECT 2199, N'Oceans', 181, 1, 162194, 5282368, 113
UNION ALL
    SELECT 2200, N'Porch', 181, 1, 210520, 6877475, 152
UNION ALL
    SELECT 2201, N'Garden', 181, 1, 299154, 9740738, 130
UNION ALL
    SELECT 2202, N'Deep', 181, 1, 258324, 8432497, 137
UNION ALL
    SELECT 2203, N'Release', 181, 1, 546063, 17802673, 82
UNION ALL
    SELECT 2204, N'Go', 182, 1, 193123, 6351920, 84
UNION ALL
    SELECT 2205, N'Animal', 182, 1, 169325, 5503459, 165
UNION ALL
    SELECT 2206, N'Daughter', 182, 1, 235598, 7824586, 165
UNION ALL
    SELECT 2207, N'Glorified G', 182, 1, 206968, 6772116, 78
UNION ALL
    SELECT 2208, N'Dissident', 182, 1, 215510, 7034500, 73
UNION ALL
    SELECT 2209, N'W.M.A.', 182, 1, 359262, 12037261, 74
UNION ALL
    SELECT 2210, N'Blood', 182, 1, 170631, 5551478, 58
UNION ALL
    SELECT 2211, N'Rearviewmirror', 182, 1, 284186, 9321053, 167
UNION ALL
    SELECT 2212, N'Rats', 182, 1, 255425, 8341934, 111
UNION ALL
    SELECT 2213, N'Elderly Woman Behind The Counter In A Small Town', 182, 1, 196336, 6499398, 76
UNION ALL
    SELECT 2214, N'Leash', 182, 1, 189257, 6191560, 173
UNION ALL
    SELECT 2215, N'Indifference', 182, 1, 302053, 9756133, 59
UNION ALL
    SELECT 2216, N'Johnny B. Goode', 141, 8, 243200, 8092024, 134
UNION ALL
    SELECT 2217, N'Don''t Look Back', 141, 8, 221100, 7344023, 173
UNION ALL
    SELECT 2218, N'Jah Seh No', 141, 8, 276871, 9134476, 170
UNION ALL
    SELECT 2219, N'I''m The Toughest', 141, 8, 230191, 7657594, 156
UNION ALL
    SELECT 2220, N'Nothing But Love', 141, 8, 221570, 7335228, 155
UNION ALL
    SELECT 2221, N'Buk-In-Hamm Palace', 141, 8, 265665, 8964369, 147
UNION ALL
    SELECT 2222, N'Bush Doctor', 141, 8, 239751, 7942299, 132
UNION ALL
    SELECT 2223, N'Wanted Dread And Alive', 141, 8, 260310, 8670933, 100
UNION ALL
    SELECT 2224, N'Mystic Man', 141, 8, 353671, 11812170, 93
UNION ALL
    SELECT 2225, N'Coming In Hot', 141, 8, 213054, 7109414, 71
UNION ALL
    SELECT 2226, N'Pick Myself Up', 141, 8, 234684, 7788255, 180
UNION ALL
    SELECT 2227, N'Crystal Ball', 141, 8, 309733, 10319296, 110
UNION ALL
    SELECT 2228, N'Equal Rights Downpresser Man', 141, 8, 366733, 12086524, 172
UNION ALL
    SELECT 2229, N'Speak To Me/Breathe', 183, 1, 234213, 7631305, 71
UNION ALL
    SELECT 2230, N'On The Run', 183, 1, 214595, 7206300, 178
UNION ALL
    SELECT 2231, N'Time', 183, 1, 425195, 13955426, 101
UNION ALL
    SELECT 2232, N'The Great Gig In The Sky', 183, 1, 284055, 9147563, 126
UNION ALL
    SELECT 2233, N'Money', 183, 1, 391888, 12930070, 95
UNION ALL
    SELECT 2234, N'Us And Them', 183, 1, 461035, 15000299, 98
UNION ALL
    SELECT 2235, N'Any Colour You Like', 183, 1, 205740, 6707989, 196
UNION ALL
    SELECT 2236, N'Brain Damage', 183, 1, 230556, 7497655, 111
UNION ALL
    SELECT 2237, N'Eclipse', 183, 1, 125361, 4065299, 100
UNION ALL
    SELECT 2238, N'ZeroVinteUm', 184, 17, 315637, 10426550, 147
UNION ALL
    SELECT 2239, N'Queimando Tudo', 184, 17, 172591, 5723677, 99
UNION ALL
    SELECT 2240, N'Hip Hop Rio', 184, 17, 151536, 4991935, 188
UNION ALL
    SELECT 2241, N'Bossa', 184, 17, 29048, 967098, 140
UNION ALL
    SELECT 2242, N'100% HardCore', 184, 17, 165146, 5407744, 198
UNION ALL
    SELECT 2243, N'Biruta', 184, 17, 213263, 7108200, 162
UNION ALL
    SELECT 2244, N'MГЈo Na CabeГ§a', 184, 17, 202631, 6642753, 167
UNION ALL
    SELECT 2245, N'O Bicho TГЎ Pregando', 184, 17, 171964, 5683369, 126
UNION ALL
    SELECT 2246, N'Adoled (Ocean)', 184, 17, 185103, 6009946, 68
UNION ALL
    SELECT 2247, N'Seus Amigos', 184, 17, 100858, 3304738, 145
UNION ALL
    SELECT 2248, N'Paga Pau', 184, 17, 197485, 6529041, 134
UNION ALL
    SELECT 2249, N'Rappers Reais', 184, 17, 202004, 6684160, 144
UNION ALL
    SELECT 2250, N'Nega Do Cabelo Duro', 184, 17, 121808, 4116536, 163
UNION ALL
    SELECT 2251, N'Hemp Family', 184, 17, 205923, 6806900, 111
UNION ALL
    SELECT 2252, N'Quem Me Cobrou?', 184, 17, 121704, 3947664, 88
UNION ALL
    SELECT 2253, N'Se Liga', 184, 17, 410409, 13559173, 196
UNION ALL
    SELECT 2254, N'Bohemian Rhapsody', 185, 1, 358948, 11619868, 129
UNION ALL
    SELECT 2255, N'Another One Bites The Dust', 185, 1, 216946, 7172355, 179
UNION ALL
    SELECT 2256, N'Killer Queen', 185, 1, 182099, 5967749, 181
UNION ALL
    SELECT 2257, N'Fat Bottomed Girls', 185, 1, 204695, 6630041, 83
UNION ALL
    SELECT 2258, N'Bicycle Race', 185, 1, 183823, 6012409, 77
UNION ALL
    SELECT 2259, N'You''re My Best Friend', 185, 1, 172225, 5602173, 144
UNION ALL
    SELECT 2260, N'Don''t Stop Me Now', 185, 1, 211826, 6896666, 186
UNION ALL
    SELECT 2261, N'Save Me', 185, 1, 228832, 7444624, 136
UNION ALL
    SELECT 2262, N'Crazy Little Thing Called Love', 185, 1, 164231, 5435501, 108
UNION ALL
    SELECT 2263, N'Somebody To Love', 185, 1, 297351, 9650520, 144
UNION ALL
    SELECT 2264, N'Now I''m Here', 185, 1, 255346, 8328312, 174
UNION ALL
    SELECT 2265, N'Good Old-Fashioned Lover Boy', 185, 1, 175960, 5747506, 107
UNION ALL
    SELECT 2266, N'Play The Game', 185, 1, 213368, 6915832, 107
UNION ALL
    SELECT 2267, N'Flash', 185, 1, 168489, 5464986, 120
UNION ALL
    SELECT 2268, N'Seven Seas Of Rhye', 185, 1, 170553, 5539957, 138
UNION ALL
    SELECT 2269, N'We Will Rock You', 185, 1, 122880, 4026955, 92
UNION ALL
    SELECT 2270, N'We Are The Champions', 185, 1, 180950, 5880231, 81
UNION ALL
    SELECT 2271, N'We Will Rock You', 186, 1, 122671, 4026815, 101
UNION ALL
    SELECT 2272, N'We Are The Champions', 186, 1, 182883, 5939794, 183
UNION ALL
    SELECT 2273, N'Sheer Heart Attack', 186, 1, 207386, 6642685, 103
UNION ALL
    SELECT 2274, N'All Dead, All Dead', 186, 1, 190119, 6144878, 111
UNION ALL
    SELECT 2275, N'Spread Your Wings', 186, 1, 275356, 8936992, 67
UNION ALL
    SELECT 2276, N'Fight From The Inside', 186, 1, 184737, 6078001, 69
UNION ALL
    SELECT 2277, N'Get Down, Make Love', 186, 1, 231235, 7509333, 106
UNION ALL
    SELECT 2278, N'Sleep On The Sidewalk', 186, 1, 187428, 6099840, 54
UNION ALL
    SELECT 2279, N'Who Needs You', 186, 1, 186958, 6292969, 95
UNION ALL
    SELECT 2280, N'It''s Late', 186, 1, 386194, 12519388, 161
UNION ALL
    SELECT 2281, N'My Melancholy Blues', 186, 1, 206471, 6691838, 87
UNION ALL
    SELECT 2282, N'Shiny Happy People', 187, 4, 226298, 7475323, 74
UNION ALL
    SELECT 2283, N'Me In Honey', 187, 4, 246674, 8194751, 157
UNION ALL
    SELECT 2284, N'Radio Song', 187, 4, 255477, 8421172, 77
UNION ALL
    SELECT 2285, N'Pop Song 89', 188, 4, 185730, 6132218, 189
UNION ALL
    SELECT 2286, N'Get Up', 188, 4, 160235, 5264376, 161
UNION ALL
    SELECT 2287, N'You Are The Everything', 188, 4, 226298, 7373181, 174
UNION ALL
    SELECT 2288, N'Stand', 188, 4, 192862, 6349090, 95
UNION ALL
    SELECT 2289, N'World Leader Pretend', 188, 4, 259761, 8537282, 59
UNION ALL
    SELECT 2290, N'The Wrong Child', 188, 4, 216633, 7065060, 129
UNION ALL
    SELECT 2291, N'Orange Crush', 188, 4, 231706, 7742894, 176
UNION ALL
    SELECT 2292, N'Turn You Inside-Out', 188, 4, 257358, 8395671, 196
UNION ALL
    SELECT 2293, N'Hairshirt', 188, 4, 235911, 7753807, 122
UNION ALL
    SELECT 2294, N'I Remember California', 188, 4, 304013, 9950311, 153
UNION ALL
    SELECT 2295, N'Untitled', 188, 4, 191503, 6332426, 130
UNION ALL
    SELECT 2296, N'How The West Was Won And Where It Got Us', 189, 1, 271151, 8994291, 102
UNION ALL
    SELECT 2297, N'The Wake-Up Bomb', 189, 1, 308532, 10077337, 154
UNION ALL
    SELECT 2298, N'New Test Leper', 189, 1, 326791, 10866447, 138
UNION ALL
    SELECT 2299, N'Undertow', 189, 1, 309498, 10131005, 159
UNION ALL
    SELECT 2300, N'E-Bow The Letter', 189, 1, 324963, 10714576, 101
UNION ALL
    SELECT 2301, N'Leave', 189, 1, 437968, 14433365, 115
UNION ALL
    SELECT 2302, N'Departure', 189, 1, 209423, 6818425, 89
UNION ALL
    SELECT 2303, N'Bittersweet Me', 189, 1, 245812, 8114718, 183
UNION ALL
    SELECT 2304, N'Be Mine', 189, 1, 333087, 10790541, 61
UNION ALL
    SELECT 2305, N'Binky The Doormat', 189, 1, 301688, 9950320, 165
UNION ALL
    SELECT 2306, N'Zither', 189, 1, 154148, 5032962, 187
UNION ALL
    SELECT 2307, N'So Fast, So Numb', 189, 1, 252682, 8341223, 180
UNION ALL
    SELECT 2308, N'Low Desert', 189, 1, 212062, 6989288, 63
UNION ALL
    SELECT 2309, N'Electrolite', 189, 1, 245315, 8051199, 92
UNION ALL
    SELECT 2310, N'Losing My Religion', 187, 4, 269035, 8885672, 140
UNION ALL
    SELECT 2311, N'Low', 187, 4, 296777, 9633860, 111
UNION ALL
    SELECT 2312, N'Near Wild Heaven', 187, 4, 199862, 6610009, 79
UNION ALL
    SELECT 2313, N'Endgame', 187, 4, 230687, 7664479, 126
UNION ALL
    SELECT 2314, N'Belong', 187, 4, 247013, 8219375, 193
UNION ALL
    SELECT 2315, N'Half A World Away', 187, 4, 208431, 6837283, 117
UNION ALL
    SELECT 2316, N'Texarkana', 187, 4, 220081, 7260681, 80
UNION ALL
    SELECT 2317, N'Country Feedback', 187, 4, 249782, 8178943, 149
UNION ALL
    SELECT 2318, N'Carnival Of Sorts', 190, 4, 233482, 7669658, 126
UNION ALL
    SELECT 2319, N'Radio Free Aurope', 190, 4, 245315, 8163490, 190
UNION ALL
    SELECT 2320, N'Perfect Circle', 190, 4, 208509, 6898067, 143
UNION ALL
    SELECT 2321, N'Talk About The Passion', 190, 4, 203206, 6725435, 143
UNION ALL
    SELECT 2322, N'So Central Rain', 190, 4, 194768, 6414550, 166
UNION ALL
    SELECT 2323, N'Don''t Go Back To Rockville', 190, 4, 272352, 9010715, 120
UNION ALL
    SELECT 2324, N'Pretty Persuasion', 190, 4, 229929, 7577754, 79
UNION ALL
    SELECT 2325, N'Green Grow The Rushes', 190, 4, 225671, 7422425, 55
UNION ALL
    SELECT 2326, N'Can''t Get There From Here', 190, 4, 220630, 7285936, 121
UNION ALL
    SELECT 2327, N'Driver 8', 190, 4, 204747, 6779076, 128
UNION ALL
    SELECT 2328, N'Fall On Me', 190, 4, 172016, 5676811, 159
UNION ALL
    SELECT 2329, N'I Believe', 190, 4, 227709, 7542929, 54
UNION ALL
    SELECT 2330, N'Cuyahoga', 190, 4, 260623, 8591057, 140
UNION ALL
    SELECT 2331, N'The One I Love', 190, 4, 197355, 6495125, 133
UNION ALL
    SELECT 2332, N'The Finest Worksong', 190, 4, 229276, 7574856, 70
UNION ALL
    SELECT 2333, N'It''s The End Of The World As We Know It (And I Feel Fine)', 190, 4, 244819, 7998987, 107
UNION ALL
    SELECT 2334, N'Infeliz Natal', 191, 4, 138266, 4503299, 63
UNION ALL
    SELECT 2335, N'A Sua', 191, 4, 142132, 4622064, 54
UNION ALL
    SELECT 2336, N'Papeau Nuky Doe', 191, 4, 121652, 3995022, 155
UNION ALL
    SELECT 2337, N'Merry Christmas', 191, 4, 126040, 4166652, 189
UNION ALL
    SELECT 2338, N'Bodies', 191, 4, 180035, 5873778, 108
UNION ALL
    SELECT 2339, N'Puteiro Em JoГЈo Pessoa', 191, 4, 195578, 6395490, 186
UNION ALL
    SELECT 2340, N'Esporrei Na Manivela', 191, 4, 293276, 9618499, 92
UNION ALL
    SELECT 2341, N'BГЄ-a-BГЎ', 191, 4, 249051, 8130636, 118
UNION ALL
    SELECT 2342, N'Cajueiro', 191, 4, 158589, 5164837, 105
UNION ALL
    SELECT 2343, N'Palhas Do Coqueiro', 191, 4, 133851, 4396466, 96
UNION ALL
    SELECT 2344, N'Maluco Beleza', 192, 1, 203206, 6628067, 150
UNION ALL
    SELECT 2345, N'O Dia Em Que A Terra Parou', 192, 1, 261720, 8586678, 197
UNION ALL
    SELECT 2346, N'No Fundo Do Quintal Da Escola', 192, 1, 177606, 5836953, 84
UNION ALL
    SELECT 2347, N'O Segredo Do Universo', 192, 1, 192679, 6315187, 177
UNION ALL
    SELECT 2348, N'As Profecias', 192, 1, 232515, 7657732, 87
UNION ALL
    SELECT 2349, N'Mata Virgem', 192, 1, 142602, 4690029, 192
UNION ALL
    SELECT 2350, N'Sapato 36', 192, 1, 196702, 6507301, 93
UNION ALL
    SELECT 2351, N'Todo Mundo Explica', 192, 1, 134896, 4449772, 183
UNION ALL
    SELECT 2352, N'Que Luz Г‰ Essa', 192, 1, 165067, 5620058, 105
UNION ALL
    SELECT 2353, N'Diamante De Mendigo', 192, 1, 206053, 6775101, 149
UNION ALL
    SELECT 2354, N'NegГіcio Г‰', 192, 1, 175464, 5826775, 198
UNION ALL
    SELECT 2355, N'Muita Estrela, Pouca ConstelaГ§ГЈo', 192, 1, 268068, 8781021, 167
UNION ALL
    SELECT 2356, N'SГ©culo XXI', 192, 1, 244897, 8040563, 113
UNION ALL
    SELECT 2357, N'Rock Das Aranhas (Ao Vivo) (Live)', 192, 1, 231836, 7591945, 179
UNION ALL
    SELECT 2358, N'The Power Of Equality', 193, 4, 243591, 8148266, 113
UNION ALL
    SELECT 2359, N'If You Have To Ask', 193, 4, 216790, 7199175, 54
UNION ALL
    SELECT 2360, N'Breaking The Girl', 193, 4, 295497, 9805526, 137
UNION ALL
    SELECT 2361, N'Funky Monks', 193, 4, 323395, 10708168, 143
UNION ALL
    SELECT 2362, N'Suck My Kiss', 193, 4, 217234, 7129137, 166
UNION ALL
    SELECT 2363, N'I Could Have Lied', 193, 4, 244506, 8088244, 141
UNION ALL
    SELECT 2364, N'Mellowship Slinky In B Major', 193, 4, 240091, 7971384, 140
UNION ALL
    SELECT 2365, N'The Righteous & The Wicked', 193, 4, 248084, 8134096, 101
UNION ALL
    SELECT 2366, N'Give It Away', 193, 4, 283010, 9308997, 144
UNION ALL
    SELECT 2367, N'Blood Sugar Sex Magik', 193, 4, 271229, 8940573, 162
UNION ALL
    SELECT 2368, N'Under The Bridge', 193, 4, 264359, 8682716, 143
UNION ALL
    SELECT 2369, N'Naked In The Rain', 193, 4, 265717, 8724674, 59
UNION ALL
    SELECT 2370, N'Apache Rose Peacock', 193, 4, 282226, 9312588, 154
UNION ALL
    SELECT 2371, N'The Greeting Song', 193, 4, 193593, 6346507, 70
UNION ALL
    SELECT 2372, N'My Lovely Man', 193, 4, 279118, 9220114, 115
UNION ALL
    SELECT 2373, N'Sir Psycho Sexy', 193, 4, 496692, 16354362, 99
UNION ALL
    SELECT 2374, N'They''re Red Hot', 193, 4, 71941, 2382220, 90
UNION ALL
    SELECT 2375, N'By The Way', 194, 1, 218017, 7197430, 136
UNION ALL
    SELECT 2376, N'Universally Speaking', 194, 1, 259213, 8501904, 72
UNION ALL
    SELECT 2377, N'This Is The Place', 194, 1, 257906, 8469765, 69
UNION ALL
    SELECT 2378, N'Dosed', 194, 1, 312058, 10235611, 105
UNION ALL
    SELECT 2379, N'Don''t Forget Me', 194, 1, 277995, 9107071, 175
UNION ALL
    SELECT 2380, N'The Zephyr Song', 194, 1, 232960, 7690312, 190
UNION ALL
    SELECT 2381, N'Can''t Stop', 194, 1, 269400, 8872479, 130
UNION ALL
    SELECT 2382, N'I Could Die For You', 194, 1, 193906, 6333311, 55
UNION ALL
    SELECT 2383, N'Midnight', 194, 1, 295810, 9702450, 121
UNION ALL
    SELECT 2384, N'Throw Away Your Television', 194, 1, 224574, 7483526, 99
UNION ALL
    SELECT 2385, N'Cabron', 194, 1, 218592, 7458864, 110
UNION ALL
    SELECT 2386, N'Tear', 194, 1, 317413, 10395500, 132
UNION ALL
    SELECT 2387, N'On Mercury', 194, 1, 208509, 6834762, 136
UNION ALL
    SELECT 2388, N'Minor Thing', 194, 1, 217835, 7148115, 178
UNION ALL
    SELECT 2389, N'Warm Tape', 194, 1, 256653, 8358200, 125
UNION ALL
    SELECT 2390, N'Venice Queen', 194, 1, 369110, 12280381, 187
UNION ALL
    SELECT 2391, N'Around The World', 195, 1, 238837, 7859167, 153
UNION ALL
    SELECT 2392, N'Parallel Universe', 195, 1, 270654, 8958519, 135
UNION ALL
    SELECT 2393, N'Scar Tissue', 195, 1, 217469, 7153744, 200
UNION ALL
    SELECT 2394, N'Otherside', 195, 1, 255973, 8357989, 110
UNION ALL
    SELECT 2395, N'Get On Top', 195, 1, 198164, 6587883, 124
UNION ALL
    SELECT 2396, N'Californication', 195, 1, 321671, 10568999, 51
UNION ALL
    SELECT 2397, N'Easily', 195, 1, 231418, 7504534, 53
UNION ALL
    SELECT 2398, N'Porcelain', 195, 1, 163787, 5278793, 86
UNION ALL
    SELECT 2399, N'Emit Remmus', 195, 1, 240300, 7901717, 149
UNION ALL
    SELECT 2400, N'I Like Dirt', 195, 1, 157727, 5225917, 95
UNION ALL
    SELECT 2401, N'This Velvet Glove', 195, 1, 225280, 7480537, 175
UNION ALL
    SELECT 2402, N'Savior', 195, 1, 292493, 9551885, 191
UNION ALL
    SELECT 2403, N'Purple Stain', 195, 1, 253440, 8359971, 159
UNION ALL
    SELECT 2404, N'Right On Time', 195, 1, 112613, 3722219, 115
UNION ALL
    SELECT 2405, N'Road Trippin''', 195, 1, 205635, 6685831, 108
UNION ALL
    SELECT 2406, N'The Spirit Of Radio', 196, 1, 299154, 9862012, 151
UNION ALL
    SELECT 2407, N'The Trees', 196, 1, 285126, 9345473, 149
UNION ALL
    SELECT 2408, N'Something For Nothing', 196, 1, 240770, 7898395, 135
UNION ALL
    SELECT 2409, N'Freewill', 196, 1, 324362, 10694110, 136
UNION ALL
    SELECT 2410, N'Xanadu', 196, 1, 667428, 21753168, 97
UNION ALL
    SELECT 2411, N'Bastille Day', 196, 1, 280528, 9264769, 115
UNION ALL
    SELECT 2412, N'By-Tor And The Snow Dog', 196, 1, 519888, 17076397, 184
UNION ALL
    SELECT 2413, N'Anthem', 196, 1, 264515, 8693343, 102
UNION ALL
    SELECT 2414, N'Closer To The Heart', 196, 1, 175412, 5767005, 198
UNION ALL
    SELECT 2415, N'2112 Overture', 196, 1, 272718, 8898066, 100
UNION ALL
    SELECT 2416, N'The Temples Of Syrinx', 196, 1, 133459, 4360163, 116
UNION ALL
    SELECT 2417, N'La Villa Strangiato', 196, 1, 577488, 19137855, 144
UNION ALL
    SELECT 2418, N'Fly By Night', 196, 1, 202318, 6683061, 130
UNION ALL
    SELECT 2419, N'Finding My Way', 196, 1, 305528, 9985701, 82
UNION ALL
    SELECT 2420, N'Jingo', 197, 1, 592953, 19736495, 120
UNION ALL
    SELECT 2421, N'El Corazon Manda', 197, 1, 713534, 23519583, 150
UNION ALL
    SELECT 2422, N'La Puesta Del Sol', 197, 1, 628062, 20614621, 127
UNION ALL
    SELECT 2423, N'Persuasion', 197, 1, 318432, 10354751, 118
UNION ALL
    SELECT 2424, N'As The Years Go by', 197, 1, 233064, 7566829, 52
UNION ALL
    SELECT 2425, N'Soul Sacrifice', 197, 1, 296437, 9801120, 175
UNION ALL
    SELECT 2426, N'Fried Neckbones And Home Fries', 197, 1, 638563, 20939646, 107
UNION ALL
    SELECT 2427, N'Santana Jam', 197, 1, 882834, 29207100, 151
UNION ALL
    SELECT 2428, N'Evil Ways', 198, 1, 475402, 15289235, 141
UNION ALL
    SELECT 2429, N'We''ve Got To Get Together/Jingo', 198, 1, 1070027, 34618222, 85
UNION ALL
    SELECT 2430, N'Rock Me', 198, 1, 94720, 3037596, 139
UNION ALL
    SELECT 2431, N'Just Ain''t Good Enough', 198, 1, 850259, 27489067, 146
UNION ALL
    SELECT 2432, N'Funky Piano', 198, 1, 934791, 30200730, 94
UNION ALL
    SELECT 2433, N'The Way You Do To Mer', 198, 1, 618344, 20028702, 75
UNION ALL
    SELECT 2434, N'Holding Back The Years', 141, 1, 270053, 8833220, 117
UNION ALL
    SELECT 2435, N'Money''s Too Tight To Mention', 141, 1, 268408, 8861921, 193
UNION ALL
    SELECT 2436, N'The Right Thing', 141, 1, 262687, 8624063, 193
UNION ALL
    SELECT 2437, N'It''s Only Love', 141, 1, 232594, 7659017, 157
UNION ALL
    SELECT 2438, N'A New Flame', 141, 1, 237662, 7822875, 160
UNION ALL
    SELECT 2439, N'You''ve Got It', 141, 1, 235232, 7712845, 163
UNION ALL
    SELECT 2440, N'If You Don''t Know Me By Now', 141, 1, 206524, 6712634, 67
UNION ALL
    SELECT 2441, N'Stars', 141, 1, 248137, 8194906, 79
UNION ALL
    SELECT 2442, N'Something Got Me Started', 141, 1, 239595, 7997139, 183
UNION ALL
    SELECT 2443, N'Thrill Me', 141, 1, 303934, 10034711, 63
UNION ALL
    SELECT 2444, N'Your Mirror', 141, 1, 240666, 7893821, 116
UNION ALL
    SELECT 2445, N'For Your Babies', 141, 1, 256992, 8408803, 131
UNION ALL
    SELECT 2446, N'So Beautiful', 141, 1, 298083, 9837832, 149
UNION ALL
    SELECT 2447, N'Angel', 141, 1, 240561, 7880256, 92
UNION ALL
    SELECT 2448, N'Fairground', 141, 1, 263888, 8793094, 128
UNION ALL
    SELECT 2449, N'ГЃgua E Fogo', 199, 1, 278987, 9272272, 56
UNION ALL
    SELECT 2450, N'TrГЄs Lados', 199, 1, 233665, 7699609, 61
UNION ALL
    SELECT 2451, N'Ela Desapareceu', 199, 1, 250122, 8289200, 86
UNION ALL
    SELECT 2452, N'Balada Do Amor InabalГЎvel', 199, 1, 240613, 8025816, 71
UNION ALL
    SELECT 2453, N'CanГ§ГЈo Noturna', 199, 1, 238628, 7874774, 68
UNION ALL
    SELECT 2454, N'MuГ§ulmano', 199, 1, 249600, 8270613, 167
UNION ALL
    SELECT 2455, N'Maquinarama', 199, 1, 245629, 8213710, 163
UNION ALL
    SELECT 2456, N'RebeliГЈo', 199, 1, 298527, 9817847, 85
UNION ALL
    SELECT 2457, N'A Гљltima Guerra', 199, 1, 314723, 10480391, 136
UNION ALL
    SELECT 2458, N'Fica', 199, 1, 272169, 8980972, 200
UNION ALL
    SELECT 2459, N'Ali', 199, 1, 306390, 10110351, 196
UNION ALL
    SELECT 2460, N'Preto DamiГЈo', 199, 1, 264568, 8697658, 187
UNION ALL
    SELECT 2461, N'Г‰ Uma Partida De Futebol', 200, 1, 1071, 38747, 165
UNION ALL
    SELECT 2462, N'Eu Disse A Ela', 200, 1, 254223, 8479463, 142
UNION ALL
    SELECT 2463, N'ZГ© Trindade', 200, 1, 247954, 8331310, 188
UNION ALL
    SELECT 2464, N'Garota Nacional', 200, 1, 317492, 10511239, 161
UNION ALL
    SELECT 2465, N'TГЈo Seu', 200, 1, 243748, 8133126, 63
UNION ALL
    SELECT 2466, N'Sem Terra', 200, 1, 279353, 9196411, 187
UNION ALL
    SELECT 2467, N'Os Exilados', 200, 1, 245551, 8222095, 62
UNION ALL
    SELECT 2468, N'Um Dia Qualquer', 200, 1, 292414, 9805570, 62
UNION ALL
    SELECT 2469, N'Los Pretos', 200, 1, 239229, 8025667, 50
UNION ALL
    SELECT 2470, N'Sul Da AmГ©rica', 200, 1, 254928, 8484871, 80
UNION ALL
    SELECT 2471, N'PoconГ©', 200, 1, 318406, 10771610, 186
UNION ALL
    SELECT 2472, N'Lucky 13', 201, 4, 189387, 6200617, 84
UNION ALL
    SELECT 2473, N'Aeroplane Flies High', 201, 4, 473391, 15408329, 128
UNION ALL
    SELECT 2474, N'Because You Are', 201, 4, 226403, 7405137, 70
UNION ALL
    SELECT 2475, N'Slow Dawn', 201, 4, 192339, 6269057, 139
UNION ALL
    SELECT 2476, N'Believe', 201, 4, 192940, 6320652, 116
UNION ALL
    SELECT 2477, N'My Mistake', 201, 4, 240901, 7843477, 176
UNION ALL
    SELECT 2478, N'Marquis In Spades', 201, 4, 192731, 6304789, 118
UNION ALL
    SELECT 2479, N'Here''s To The Atom Bomb', 201, 4, 266893, 8763140, 126
UNION ALL
    SELECT 2480, N'Sparrow', 201, 4, 176822, 5696989, 130
UNION ALL
    SELECT 2481, N'Waiting', 201, 4, 228336, 7627641, 86
UNION ALL
    SELECT 2482, N'Saturnine', 201, 4, 229877, 7523502, 163
UNION ALL
    SELECT 2483, N'Rock On', 201, 4, 366471, 12133825, 188
UNION ALL
    SELECT 2484, N'Set The Ray To Jerry', 201, 4, 249364, 8215184, 105
UNION ALL
    SELECT 2485, N'Winterlong', 201, 4, 299389, 9670616, 112
UNION ALL
    SELECT 2486, N'Soot & Stars', 201, 4, 399986, 12866557, 129
UNION ALL
    SELECT 2487, N'Blissed & Gone', 201, 4, 286302, 9305998, 50
UNION ALL
    SELECT 2488, N'Siva', 202, 4, 261172, 8576622, 81
UNION ALL
    SELECT 2489, N'Rhinocerous', 202, 4, 353462, 11526684, 198
UNION ALL
    SELECT 2490, N'Drown', 202, 4, 270497, 8883496, 147
UNION ALL
    SELECT 2491, N'Cherub Rock', 202, 4, 299389, 9786739, 50
UNION ALL
    SELECT 2492, N'Today', 202, 4, 202213, 6596933, 198
UNION ALL
    SELECT 2493, N'Disarm', 202, 4, 198556, 6508249, 185
UNION ALL
    SELECT 2494, N'Landslide', 202, 4, 190275, 6187754, 97
UNION ALL
    SELECT 2495, N'Bullet With Butterfly Wings', 202, 4, 257306, 8431747, 87
UNION ALL
    SELECT 2496, NULL, 202, 4, 263653, 8728470, 149
UNION ALL
    SELECT 2497, N'Zero', 202, 4, 161123, 5267176, 68
UNION ALL
    SELECT 2498, N'Tonight, Tonight', 202, 4, 255686, 8351543, 50
UNION ALL
    SELECT 2499, N'Eye', 202, 4, 294530, 9784201, 72
UNION ALL
    SELECT 2500, N'Ava Adore', 202, 4, 261433, 8590412, 125
UNION ALL
    SELECT 2501, N'Perfect', 202, 4, 203023, 6734636, 87
UNION ALL
    SELECT 2502, N'The Everlasting Gaze', 202, 4, 242155, 7844404, 99
UNION ALL
    SELECT 2503, N'Stand Inside Your Love', 202, 4, 253753, 8270113, 71
UNION ALL
    SELECT 2504, N'Real Love', 202, 4, 250697, 8025896, 155
UNION ALL
    SELECT 2505, N'[Untitled]', 202, 4, 231784, 7689713, 189
UNION ALL
    SELECT 2506, N'Nothing To Say', 203, 1, 238027, 7744833, 61
UNION ALL
    SELECT 2507, N'Flower', 203, 1, 208822, 6830732, 163
UNION ALL
    SELECT 2508, N'Loud Love', 203, 1, 297456, 9660953, 86
UNION ALL
    SELECT 2509, N'Hands All Over', 203, 1, 362475, 11893108, 69
UNION ALL
    SELECT 2510, N'Get On The Snake', 203, 1, 225123, 7313744, 137
UNION ALL
    SELECT 2511, N'Jesus Christ Pose', 203, 1, 352966, 11739886, 93
UNION ALL
    SELECT 2512, N'Outshined', 203, 1, 312476, 10274629, 68
UNION ALL
    SELECT 2513, N'Rusty Cage', 203, 1, 267728, 8779485, 198
UNION ALL
    SELECT 2514, N'Spoonman', 203, 1, 248476, 8289906, 88
UNION ALL
    SELECT 2515, N'The Day I Tried To Live', 203, 1, 321175, 10507137, 109
UNION ALL
    SELECT 2516, N'Black Hole Sun', 203, 1, 320365, 10425229, 105
UNION ALL
    SELECT 2517, N'Fell On Black Days', 203, 1, 282331, 9256082, 174
UNION ALL
    SELECT 2518, N'Pretty Noose', 203, 1, 253570, 8317931, 179
UNION ALL
    SELECT 2519, N'Burden In My Hand', 203, 1, 292153, 9659911, 66
UNION ALL
    SELECT 2520, N'Blow Up The Outside World', 203, 1, 347898, 11379527, 81
UNION ALL
    SELECT 2521, N'Ty Cobb', 203, 1, 188786, 6233136, 126
UNION ALL
    SELECT 2522, N'Bleed Together', 203, 1, 232202, 7597074, 72
UNION ALL
    SELECT 2523, N'Morning Dance', 204, 2, 238759, 8101979, 197
UNION ALL
    SELECT 2524, N'Jubilee', 204, 2, 275147, 9151846, 184
UNION ALL
    SELECT 2525, N'Rasul', 204, 2, 238315, 7854737, 185
UNION ALL
    SELECT 2526, N'Song For Lorraine', 204, 2, 240091, 8101723, 194
UNION ALL
    SELECT 2527, N'Starburst', 204, 2, 291500, 9768399, 126
UNION ALL
    SELECT 2528, N'Heliopolis', 204, 2, 338729, 11365655, 94
UNION ALL
    SELECT 2529, N'It Doesn''t Matter', 204, 2, 270027, 9034177, 148
UNION ALL
    SELECT 2530, N'Little Linda', 204, 2, 264019, 8958743, 97
UNION ALL
    SELECT 2531, N'End Of Romanticism', 204, 2, 320078, 10553155, 56
UNION ALL
    SELECT 2532, N'The House Is Rockin''', 205, 6, 144352, 4706253, 137
UNION ALL
    SELECT 2533, N'Crossfire', 205, 6, 251219, 8238033, 73
UNION ALL
    SELECT 2534, N'Tightrope', 205, 6, 281155, 9254906, 92
UNION ALL
    SELECT 2535, N'Let Me Love You Baby', 205, 6, 164127, 5378455, 92
UNION ALL
    SELECT 2536, N'Leave My Girl Alone', 205, 6, 256365, 8438021, 123
UNION ALL
    SELECT 2537, N'Travis Walk', 205, 6, 140826, 4650979, 109
UNION ALL
    SELECT 2538, N'Wall Of Denial', 205, 6, 336927, 11085915, 94
UNION ALL
    SELECT 2539, N'Scratch-N-Sniff', 205, 6, 163422, 5353627, 191
UNION ALL
    SELECT 2540, N'Love Me Darlin''', 205, 6, 201586, 6650869, 58
UNION ALL
    SELECT 2541, N'Riviera Paradise', 205, 6, 528692, 17232776, 187
UNION ALL
    SELECT 2542, N'Dead And Bloated', 206, 1, 310386, 10170433, 190
UNION ALL
    SELECT 2543, N'Sex Type Thing', 206, 1, 218723, 7102064, 185
UNION ALL
    SELECT 2544, N'Wicked Garden', 206, 1, 245368, 7989505, 150
UNION ALL
    SELECT 2545, N'No Memory', 206, 1, 80613, 2660859, 148
UNION ALL
    SELECT 2546, N'Sin', 206, 1, 364800, 12018823, 164
UNION ALL
    SELECT 2547, N'Naked Sunday', 206, 1, 229720, 7444201, 68
UNION ALL
    SELECT 2548, N'Creep', 206, 1, 333191, 10894988, 169
UNION ALL
    SELECT 2549, N'Piece Of Pie', 206, 1, 324623, 10605231, 199
UNION ALL
    SELECT 2550, N'Plush', 206, 1, 314017, 10229848, 194
UNION ALL
    SELECT 2551, N'Wet My Bed', 206, 1, 96914, 3198627, 50
UNION ALL
    SELECT 2552, N'Crackerman', 206, 1, 194403, 6317361, 136
UNION ALL
    SELECT 2553, N'Where The River Goes', 206, 1, 505991, 16468904, 143
UNION ALL
    SELECT 2554, N'Soldier Side - Intro', 207, 3, 63764, 2056079, 144
UNION ALL
    SELECT 2555, N'B.Y.O.B.', 207, 3, 255555, 8407935, 161
UNION ALL
    SELECT 2556, N'Revenga', 207, 3, 228127, 7503805, 64
UNION ALL
    SELECT 2557, N'Cigaro', 207, 3, 131787, 4321705, 130
UNION ALL
    SELECT 2558, N'Radio/Video', 207, 3, 249312, 8224917, 180
UNION ALL
    SELECT 2559, N'This Cocaine Makes Me Feel Like I''m On This Song', 207, 3, 128339, 4185193, 153
UNION ALL
    SELECT 2560, N'Violent Pornography', 207, 3, 211435, 6985960, 97
UNION ALL
    SELECT 2561, N'Question!', 207, 3, 200698, 6616398, 109
UNION ALL
    SELECT 2562, N'Sad Statue', 207, 3, 205897, 6733449, 113
UNION ALL
    SELECT 2563, N'Old School Hollywood', 207, 3, 176953, 5830258, 96
UNION ALL
    SELECT 2564, N'Lost in Hollywood', 207, 3, 320783, 10535158, 159
UNION ALL
    SELECT 2565, N'The Sun Road', 208, 1, 880640, 29008407, 90
UNION ALL
    SELECT 2566, N'Dark Corners', 208, 1, 513541, 16839223, 153
UNION ALL
    SELECT 2567, N'Duende', 208, 1, 447582, 14956771, 181
UNION ALL
    SELECT 2568, N'Black Light Syndrome', 208, 1, 526471, 17300835, 196
UNION ALL
    SELECT 2569, N'Falling in Circles', 208, 1, 549093, 18263248, 76
UNION ALL
    SELECT 2570, N'Book of Hours', 208, 1, 583366, 19464726, 66
UNION ALL
    SELECT 2571, N'Chaos-Control', 208, 1, 529841, 17455568, 87
UNION ALL
    SELECT 2572, N'Midnight From The Inside Out', 209, 6, 286981, 9442157, 61
UNION ALL
    SELECT 2573, N'Sting Me', 209, 6, 268094, 8813561, 133
UNION ALL
    SELECT 2574, N'Thick & Thin', 209, 6, 222720, 7284377, 93
UNION ALL
    SELECT 2575, N'Greasy Grass River', 209, 6, 218749, 7157045, 71
UNION ALL
    SELECT 2576, N'Sometimes Salvation', 209, 6, 389146, 12749424, 111
UNION ALL
    SELECT 2577, N'Cursed Diamonds', 209, 6, 368300, 12047978, 62
UNION ALL
    SELECT 2578, N'Miracle To Me', 209, 6, 372636, 12222116, 121
UNION ALL
    SELECT 2579, N'Wiser Time', 209, 6, 459990, 15161907, 185
UNION ALL
    SELECT 2580, N'Girl From A Pawnshop', 209, 6, 404688, 13250848, 60
UNION ALL
    SELECT 2581, N'Cosmic Fiend', 209, 6, 308401, 10115556, 189
UNION ALL
    SELECT 2582, N'Black Moon Creeping', 210, 6, 359314, 11740886, 142
UNION ALL
    SELECT 2583, N'High Head Blues', 210, 6, 371879, 12227998, 69
UNION ALL
    SELECT 2584, N'Title Song', 210, 6, 505521, 16501316, 192
UNION ALL
    SELECT 2585, N'She Talks To Angels', 210, 6, 361978, 11837342, 186
UNION ALL
    SELECT 2586, N'Twice As Hard', 210, 6, 275565, 9008067, 131
UNION ALL
    SELECT 2587, N'Lickin''', 210, 6, 314409, 10331216, 111
UNION ALL
    SELECT 2588, N'Soul Singing', 210, 6, 233639, 7672489, 108
UNION ALL
    SELECT 2589, N'Hard To Handle', 210, 6, 206994, 6786304, 164
UNION ALL
    SELECT 2590, N'Remedy', 210, 6, 337084, 11049098, 66
UNION ALL
    SELECT 2591, N'White Riot', 211, 4, 118726, 3922819, 111
UNION ALL
    SELECT 2592, N'Remote Control', 211, 4, 180297, 5949647, 100
UNION ALL
    SELECT 2593, N'Complete Control', 211, 4, 192653, 6272081, 104
UNION ALL
    SELECT 2594, N'Clash City Rockers', 211, 4, 227500, 7555054, 116
UNION ALL
    SELECT 2595, N'(White Man) In Hammersmith Palais', 211, 4, 240640, 7883532, 163
UNION ALL
    SELECT 2596, N'Tommy Gun', 211, 4, 195526, 6399872, 71
UNION ALL
    SELECT 2597, N'English Civil War', 211, 4, 156708, 5111226, 171
UNION ALL
    SELECT 2598, N'I Fought The Law', 211, 4, 159764, 5245258, 78
UNION ALL
    SELECT 2599, N'London Calling', 211, 4, 199706, 6569007, 50
UNION ALL
    SELECT 2600, N'Train In Vain', 211, 4, 189675, 6329877, 156
UNION ALL
    SELECT 2601, N'Bankrobber', 211, 4, 272431, 9067323, 50
UNION ALL
    SELECT 2602, N'The Call Up', 211, 4, 324336, 10746937, 153
UNION ALL
    SELECT 2603, N'Hitsville UK', 211, 4, 261433, 8606887, 154
UNION ALL
    SELECT 2604, N'The Magnificent Seven', 211, 4, 268486, 8889821, 94
UNION ALL
    SELECT 2605, N'This Is Radio Clash', 211, 4, 249756, 8366573, 163
UNION ALL
    SELECT 2606, N'Know Your Rights', 211, 4, 217678, 7195726, 117
UNION ALL
    SELECT 2607, N'Rock The Casbah', 211, 4, 222145, 7361500, 193
UNION ALL
    SELECT 2608, N'Should I Stay Or Should I Go', 211, 4, 187219, 6188688, 58
UNION ALL
    SELECT 2609, N'War (The Process)', 212, 1, 252630, 8254842, 177
UNION ALL
    SELECT 2610, N'The Saint', 212, 1, 216215, 7061584, 91
UNION ALL
    SELECT 2611, N'Rise', 212, 1, 219088, 7106195, 179
UNION ALL
    SELECT 2612, N'Take The Power', 212, 1, 235755, 7650012, 175
UNION ALL
    SELECT 2613, N'Breathe', 212, 1, 299781, 9742361, 194
UNION ALL
    SELECT 2614, N'Nico', 212, 1, 289488, 9412323, 98
UNION ALL
    SELECT 2615, N'American Gothic', 212, 1, 236878, 7739840, 82
UNION ALL
    SELECT 2616, N'Ashes And Ghosts', 212, 1, 300591, 9787692, 116
UNION ALL
    SELECT 2617, N'Shape The Sky', 212, 1, 209789, 6885647, 141
UNION ALL
    SELECT 2618, N'Speed Of Light', 212, 1, 262817, 8563352, 176
UNION ALL
    SELECT 2619, N'True Believers', 212, 1, 308009, 9981359, 144
UNION ALL
    SELECT 2620, N'My Bridges Burn', 212, 1, 231862, 7571370, 62
UNION ALL
    SELECT 2621, N'She Sells Sanctuary', 213, 1, 253727, 8368634, 111
UNION ALL
    SELECT 2622, N'Fire Woman', 213, 1, 312790, 10196995, 152
UNION ALL
    SELECT 2623, N'Lil'' Evil', 213, 1, 165825, 5419655, 116
UNION ALL
    SELECT 2624, N'Spirit Walker', 213, 1, 230060, 7555897, 65
UNION ALL
    SELECT 2625, N'The Witch', 213, 1, 258768, 8725403, 127
UNION ALL
    SELECT 2626, N'Revolution', 213, 1, 256026, 8371254, 120
UNION ALL
    SELECT 2627, N'Wild Hearted Son', 213, 1, 266893, 8670550, 170
UNION ALL
    SELECT 2628, N'Love Removal Machine', 213, 1, 257619, 8412167, 58
UNION ALL
    SELECT 2629, N'Rain', 213, 1, 236669, 7788461, 56
UNION ALL
    SELECT 2630, N'Edie (Ciao Baby)', 213, 1, 241632, 7846177, 139
UNION ALL
    SELECT 2631, N'Heart Of Soul', 213, 1, 274207, 8967257, 195
UNION ALL
    SELECT 2632, N'Love', 213, 1, 326739, 10729824, 69
UNION ALL
    SELECT 2633, N'Wild Flower', 213, 1, 215536, 7084321, 70
UNION ALL
    SELECT 2634, N'Go West', 213, 1, 238158, 7777749, 181
UNION ALL
    SELECT 2635, N'Resurrection Joe', 213, 1, 255451, 8532840, 163
UNION ALL
    SELECT 2636, N'Sun King', 213, 1, 368431, 12010865, 108
UNION ALL
    SELECT 2637, N'Sweet Soul Sister', 213, 1, 212009, 6889883, 107
UNION ALL
    SELECT 2638, N'Earth Mofo', 213, 1, 282200, 9204581, 50
UNION ALL
    SELECT 2639, N'Break on Through', 214, 1, 149342, 4943144, 187
UNION ALL
    SELECT 2640, N'Soul Kitchen', 214, 1, 215066, 7040865, 54
UNION ALL
    SELECT 2641, N'The Crystal Ship', 214, 1, 154853, 5052658, 83
UNION ALL
    SELECT 2642, N'Twentienth Century Fox', 214, 1, 153913, 5069211, 85
UNION ALL
    SELECT 2643, N'Alabama Song', 214, 1, 200097, 6563411, 61
UNION ALL
    SELECT 2644, N'Light My Fire', 214, 1, 428329, 13963351, 71
UNION ALL
    SELECT 2645, N'Back Door Man', 214, 1, 214360, 7035636, 121
UNION ALL
    SELECT 2646, N'I Looked At You', 214, 1, 142080, 4663988, 105
UNION ALL
    SELECT 2647, N'End Of The Night', 214, 1, 172695, 5589732, 91
UNION ALL
    SELECT 2648, N'Take It As It Comes', 214, 1, 137168, 4512656, 194
UNION ALL
    SELECT 2649, N'The End', 214, 1, 701831, 22927336, 64
UNION ALL
    SELECT 2650, N'Roxanne', 215, 1, 192992, 6330159, 73
UNION ALL
    SELECT 2651, N'Can''t Stand Losing You', 215, 1, 181159, 5971983, 123
UNION ALL
    SELECT 2652, N'Message in a Bottle', 215, 1, 291474, 9647829, 93
UNION ALL
    SELECT 2653, N'Walking on the Moon', 215, 1, 302080, 10019861, 190
UNION ALL
    SELECT 2654, N'Don''t Stand so Close to Me', 215, 1, 241031, 7956658, 196
UNION ALL
    SELECT 2655, N'De Do Do Do, De Da Da Da', 215, 1, 247196, 8227075, 162
UNION ALL
    SELECT 2656, N'Every Little Thing She Does is Magic', 215, 1, 261120, 8646853, 198
UNION ALL
    SELECT 2657, N'Invisible Sun', 215, 1, 225593, 7304320, 163
UNION ALL
    SELECT 2658, N'Spirit''s in the Material World', 215, 1, 181133, 5986622, 74
UNION ALL
    SELECT 2659, N'Every Breath You Take', 215, 1, 254615, 8364520, 76
UNION ALL
    SELECT 2660, N'King Of Pain', 215, 1, 300512, 9880303, 109
UNION ALL
    SELECT 2661, N'Wrapped Around Your Finger', 215, 1, 315454, 10361490, 113
UNION ALL
    SELECT 2662, N'Don''t Stand So Close to Me ''86', 215, 1, 293590, 9636683, 186
UNION ALL
    SELECT 2663, N'Message in a Bottle (new classic rock mix)', 215, 1, 290951, 9640349, 108
UNION ALL
    SELECT 2664, N'Time Is On My Side', 216, 1, 179983, 5855836, 191
UNION ALL
    SELECT 2665, N'Heart Of Stone', 216, 1, 164493, 5329538, 185
UNION ALL
    SELECT 2666, N'Play With Fire', 216, 1, 132022, 4265297, 107
UNION ALL
    SELECT 2667, N'Satisfaction', 216, 1, 226612, 7398766, 199
UNION ALL
    SELECT 2668, N'As Tears Go By', 216, 1, 164284, 5357350, 171
UNION ALL
    SELECT 2669, N'Get Off Of My Cloud', 216, 1, 176013, 5719514, 148
UNION ALL
    SELECT 2670, N'Mother''s Little Helper', 216, 1, 167549, 5422434, 180
UNION ALL
    SELECT 2671, N'19th Nervous Breakdown', 216, 1, 237923, 7742984, 168
UNION ALL
    SELECT 2672, N'Paint It Black', 216, 1, 226063, 7442888, 151
UNION ALL
    SELECT 2673, N'Under My Thumb', 216, 1, 221387, 7371799, 129
UNION ALL
    SELECT 2674, N'Ruby Tuesday', 216, 1, 197459, 6433467, 180
UNION ALL
    SELECT 2675, N'Let''s Spend The Night Together', 216, 1, 217495, 7137048, 96
UNION ALL
    SELECT 2676, N'Intro', 217, 1, 49737, 1618591, 72
UNION ALL
    SELECT 2677, N'You Got Me Rocking', 217, 1, 205766, 6734385, 159
UNION ALL
    SELECT 2678, N'Gimmie Shelters', 217, 1, 382119, 12528764, 85
UNION ALL
    SELECT 2679, N'Flip The Switch', 217, 1, 252421, 8336591, 133
UNION ALL
    SELECT 2680, N'Memory Motel', 217, 1, 365844, 11982431, 188
UNION ALL
    SELECT 2681, N'Corinna', 217, 1, 257488, 8449471, 170
UNION ALL
    SELECT 2682, N'Saint Of Me', 217, 1, 325694, 10725160, 66
UNION ALL
    SELECT 2683, N'Wainting On A Friend', 217, 1, 302497, 9978046, 194
UNION ALL
    SELECT 2684, N'Sister Morphine', 217, 1, 376215, 12345289, 89
UNION ALL
    SELECT 2685, N'Live With Me', 217, 1, 234893, 7709006, 109
UNION ALL
    SELECT 2686, N'Respectable', 217, 1, 215693, 7099669, 65
UNION ALL
    SELECT 2687, N'Thief In The Night', 217, 1, 337266, 10952756, 121
UNION ALL
    SELECT 2688, N'The Last Time', 217, 1, 287294, 9498758, 148
UNION ALL
    SELECT 2689, N'Out Of Control', 217, 1, 479242, 15749289, 170
UNION ALL
    SELECT 2690, N'Love Is Strong', 218, 1, 230896, 7639774, 180
UNION ALL
    SELECT 2691, N'You Got Me Rocking', 218, 1, 215928, 7162159, 114
UNION ALL
    SELECT 2692, N'Sparks Will Fly', 218, 1, 196466, 6492847, 81
UNION ALL
    SELECT 2693, N'The Worst', 218, 1, 144613, 4750094, 124
UNION ALL
    SELECT 2694, N'New Faces', 218, 1, 172146, 5689122, 160
UNION ALL
    SELECT 2695, N'Moon Is Up', 218, 1, 222119, 7366316, 154
UNION ALL
    SELECT 2696, N'Out Of Tears', 218, 1, 327418, 10677236, 126
UNION ALL
    SELECT 2697, N'I Go Wild', 218, 1, 264019, 8630833, 122
UNION ALL
    SELECT 2698, N'Brand New Car', 218, 1, 256052, 8459344, 176
UNION ALL
    SELECT 2699, N'Sweethearts Together', 218, 1, 285492, 9550459, 161
UNION ALL
    SELECT 2700, N'Suck On The Jugular', 218, 1, 268225, 8920566, 129
UNION ALL
    SELECT 2701, N'Blinded By Rainbows', 218, 1, 273946, 8971343, 91
UNION ALL
    SELECT 2702, N'Baby Break It Down', 218, 1, 249417, 8197309, 178
UNION ALL
    SELECT 2703, N'Thru And Thru', 218, 1, 375092, 12175406, 135
UNION ALL
    SELECT 2704, N'Mean Disposition', 218, 1, 249155, 8273602, 182
UNION ALL
    SELECT 2705, N'Walking Wounded', 219, 4, 277968, 9184345, 179
UNION ALL
    SELECT 2706, N'Temptation', 219, 4, 205087, 6711943, 106
UNION ALL
    SELECT 2707, N'The Messenger', 219, 4, 212062, 6975437, 178
UNION ALL
    SELECT 2708, N'Psychopomp', 219, 4, 315559, 10295199, 194
UNION ALL
    SELECT 2709, N'Sister Awake', 219, 4, 343875, 11299407, 146
UNION ALL
    SELECT 2710, N'The Bazaar', 219, 4, 222458, 7245691, 96
UNION ALL
    SELECT 2711, N'Save Me (Remix)', 219, 4, 396303, 13053839, 196
UNION ALL
    SELECT 2712, N'Fire In The Head', 219, 4, 306337, 10005675, 138
UNION ALL
    SELECT 2713, N'Release', 219, 4, 244114, 8014606, 106
UNION ALL
    SELECT 2714, N'Heaven Coming Down', 219, 4, 241867, 7846459, 59
UNION ALL
    SELECT 2715, N'The River (Remix)', 219, 4, 343170, 11193268, 139
UNION ALL
    SELECT 2716, N'Babylon', 219, 4, 169795, 5568808, 127
UNION ALL
    SELECT 2717, N'Waiting On A Sign', 219, 4, 261903, 8558590, 101
UNION ALL
    SELECT 2718, N'Life Line', 219, 4, 277786, 9082773, 111
UNION ALL
    SELECT 2719, N'Paint It Black', 219, 4, 214752, 7101572, 109
UNION ALL
    SELECT 2720, N'Temptation', 220, 4, 205244, 6719465, 194
UNION ALL
    SELECT 2721, N'Army Ants', 220, 4, 215405, 7075838, 122
UNION ALL
    SELECT 2722, N'Psychopomp', 220, 4, 317231, 10351778, 81
UNION ALL
    SELECT 2723, N'Gyroscope', 220, 4, 177711, 5810323, 149
UNION ALL
    SELECT 2724, N'Alarum', 220, 4, 298187, 9712545, 99
UNION ALL
    SELECT 2725, N'Release', 220, 4, 266292, 8725824, 51
UNION ALL
    SELECT 2726, N'Transmission', 220, 4, 317257, 10351152, 133
UNION ALL
    SELECT 2727, N'Babylon', 220, 4, 292466, 9601786, 102
UNION ALL
    SELECT 2728, N'Pulse', 220, 4, 250253, 8183872, 142
UNION ALL
    SELECT 2729, N'Emerald', 220, 4, 289750, 9543789, 68
UNION ALL
    SELECT 2730, N'Aftermath', 220, 4, 343745, 11085607, 183
UNION ALL
    SELECT 2731, N'I Can''t Explain', 221, 1, 125152, 4082896, 150
UNION ALL
    SELECT 2732, N'Anyway, Anyhow, Anywhere', 221, 1, 161253, 5234173, 59
UNION ALL
    SELECT 2733, N'My Generation', 221, 1, 197825, 6446634, 110
UNION ALL
    SELECT 2734, N'Substitute', 221, 1, 228022, 7409995, 103
UNION ALL
    SELECT 2735, N'I''m A Boy', 221, 1, 157126, 5120605, 103
UNION ALL
    SELECT 2736, N'Boris The Spider', 221, 1, 149472, 4835202, 151
UNION ALL
    SELECT 2737, N'Happy Jack', 221, 1, 132310, 4353063, 174
UNION ALL
    SELECT 2738, N'Pictures Of Lily', 221, 1, 164414, 5329751, 149
UNION ALL
    SELECT 2739, N'I Can See For Miles', 221, 1, 262791, 8604989, 74
UNION ALL
    SELECT 2740, N'Magic Bus', 221, 1, 197224, 6452700, 92
UNION ALL
    SELECT 2741, N'Pinball Wizard', 221, 1, 181890, 6055580, 168
UNION ALL
    SELECT 2742, N'The Seeker', 221, 1, 204643, 6736866, 143
UNION ALL
    SELECT 2743, N'Baba O''Riley', 221, 1, 309472, 10141660, 128
UNION ALL
    SELECT 2744, N'Won''t Get Fooled Again (Full Length Version)', 221, 1, 513750, 16855521, 96
UNION ALL
    SELECT 2745, N'Let''s See Action', 221, 1, 243513, 8078418, 146
UNION ALL
    SELECT 2746, NULL, 221, 1, 289619, 9458549, 123
UNION ALL
    SELECT 2747, N'Join Together', 221, 1, 262556, 8602485, 152
UNION ALL
    SELECT 2748, N'Squeeze Box', 221, 1, 161280, 5256508, 139
UNION ALL
    SELECT 2749, N'Who Are You (Single Edit Version)', 221, 1, 299232, 9900469, 196
UNION ALL
    SELECT 2750, N'You Better You Bet', 221, 1, 338520, 11160877, 75
UNION ALL
    SELECT 2751, N'Primavera', 222, 7, 126615, 4152604, 189
UNION ALL
    SELECT 2752, N'Chocolate', 222, 7, 194690, 6411587, 116
UNION ALL
    SELECT 2753, N'Azul Da Cor Do Mar', 222, 7, 197955, 6475007, 70
UNION ALL
    SELECT 2754, N'O Descobridor Dos Sete Mares', 222, 7, 262974, 8749583, 113
UNION ALL
    SELECT 2755, N'AtГ© Que Enfim Encontrei VocГЄ', 222, 7, 105064, 3477751, 167
UNION ALL
    SELECT 2756, N'CoronГ© Antonio Bento', 222, 7, 131317, 4340326, 106
UNION ALL
    SELECT 2757, N'New Love', 222, 7, 237897, 7786824, 103
UNION ALL
    SELECT 2758, N'NГЈo Vou Ficar', 222, 7, 172068, 5642919, 167
UNION ALL
    SELECT 2759, N'MГєsica No Ar', 222, 7, 158511, 5184891, 103
UNION ALL
    SELECT 2760, N'Salve Nossa Senhora', 222, 7, 115461, 3827629, 124
UNION ALL
    SELECT 2761, N'VocГЄ Fugiu', 222, 7, 238367, 7971147, 161
UNION ALL
    SELECT 2762, N'Cristina NВє 2', 222, 7, 90148, 2978589, 93
UNION ALL
    SELECT 2763, N'Compadre', 222, 7, 171389, 5631446, 199
UNION ALL
    SELECT 2764, N'Over Again', 222, 7, 200489, 6612634, 140
UNION ALL
    SELECT 2765, N'RГ©u Confesso', 222, 7, 217391, 7189874, 137
UNION ALL
    SELECT 2766, N'O Que Me Importa', 223, 7, 153155, 4977852, 156
UNION ALL
    SELECT 2767, N'Gostava Tanto De VocГЄ', 223, 7, 253805, 8380077, 153
UNION ALL
    SELECT 2768, N'VocГЄ', 223, 7, 242599, 7911702, 62
UNION ALL
    SELECT 2769, N'NГЈo Quero Dinheiro', 223, 7, 152607, 5031797, 97
UNION ALL
    SELECT 2770, N'Eu Amo VocГЄ', 223, 7, 242782, 7914628, 151
UNION ALL
    SELECT 2771, N'A Festa Do Santo Reis', 223, 7, 159791, 5204995, 69
UNION ALL
    SELECT 2772, N'I Don''t Know What To Do With Myself', 223, 7, 221387, 7251478, 199
UNION ALL
    SELECT 2773, N'Padre CГ­cero', 223, 7, 139598, 4581685, 195
UNION ALL
    SELECT 2774, N'Nosso Adeus', 223, 7, 206471, 6793270, 59
UNION ALL
    SELECT 2775, N'CanГЎrio Do Reino', 223, 7, 139337, 4552858, 119
UNION ALL
    SELECT 2776, N'Preciso Ser Amado', 223, 7, 174001, 5618895, 173
UNION ALL
    SELECT 2777, N'BalanГ§o', 223, 7, 209737, 6890327, 153
UNION ALL
    SELECT 2778, N'Preciso Aprender A Ser SГі', 223, 7, 162220, 5213894, 126
UNION ALL
    SELECT 2779, N'Esta Г‰ A CanГ§ГЈo', 223, 7, 184450, 6069933, 179
UNION ALL
    SELECT 2780, N'Formigueiro', 223, 7, 252943, 8455132, 169
UNION ALL
    SELECT 2781, N'Comida', 224, 4, 322612, 10786578, 71
UNION ALL
    SELECT 2782, N'Go Back', 224, 4, 230504, 7668899, 117
UNION ALL
    SELECT 2783, N'PrГЎ Dizer Adeus', 224, 4, 222484, 7382048, 120
UNION ALL
    SELECT 2784, N'FamГ­lia', 224, 4, 218331, 7267458, 57
UNION ALL
    SELECT 2785, N'Os Cegos Do Castelo', 224, 4, 296829, 9868187, 53
UNION ALL
    SELECT 2786, N'O Pulso', 224, 4, 199131, 6566998, 75
UNION ALL
    SELECT 2787, N'Marvin', 224, 4, 264359, 8741444, 145
UNION ALL
    SELECT 2788, N'Nem 5 Minutos Guardados', 224, 4, 245995, 8143797, 95
UNION ALL
    SELECT 2789, N'Flores', 224, 4, 215510, 7148017, 136
UNION ALL
    SELECT 2790, N'Palavras', 224, 4, 158458, 5285715, 98
UNION ALL
    SELECT 2791, N'HereditГЎrio', 224, 4, 151693, 5020547, 176
UNION ALL
    SELECT 2792, N'A Melhor Forma', 224, 4, 191503, 6349938, 144
UNION ALL
    SELECT 2793, N'CabeГ§a Dinossauro', 224, 4, 37120, 1220930, 84
UNION ALL
    SELECT 2794, N'32 Dentes', 224, 4, 184946, 6157904, 113
UNION ALL
    SELECT 2795, N'Bichos Escrotos (Vinheta)', 224, 4, 104986, 3503755, 170
UNION ALL
    SELECT 2796, N'NГЈo Vou Lutar', 224, 4, 189988, 6308613, 100
UNION ALL
    SELECT 2797, N'Homem Primata (Vinheta)', 224, 4, 34168, 1124909, 188
UNION ALL
    SELECT 2798, N'Homem Primata', 224, 4, 195500, 6486470, 160
UNION ALL
    SELECT 2799, N'PolГ­cia (Vinheta)', 224, 4, 56111, 1824213, 67
UNION ALL
    SELECT 2800, N'Querem Meu Sangue', 224, 4, 212401, 7069773, 147
UNION ALL
    SELECT 2801, N'DiversГЈo', 224, 4, 285936, 9531268, 100
UNION ALL
    SELECT 2802, N'TelevisГЈo', 224, 4, 293668, 9776548, 157
UNION ALL
    SELECT 2803, N'Sonifera Ilha', 225, 4, 170684, 5678290, 166
UNION ALL
    SELECT 2804, N'Lugar Nenhum', 225, 4, 195840, 6472780, 80
UNION ALL
    SELECT 2805, N'Sua Impossivel Chance', 225, 4, 246622, 8073248, 85
UNION ALL
    SELECT 2806, N'Desordem', 225, 4, 213289, 7067340, 97
UNION ALL
    SELECT 2807, N'NГЈo Vou Me Adaptar', 225, 4, 221831, 7304656, 186
UNION ALL
    SELECT 2808, N'Domingo', 225, 4, 208613, 6883180, 88
UNION ALL
    SELECT 2809, N'AmanhГЈ NГЈo Se Sabe', 225, 4, 189440, 6243967, 176
UNION ALL
    SELECT 2810, N'Caras Como Eu', 225, 4, 183092, 5999048, 131
UNION ALL
    SELECT 2811, N'Senhora E Senhor', 225, 4, 203702, 6733733, 193
UNION ALL
    SELECT 2812, N'Era Uma Vez', 225, 4, 224261, 7453156, 176
UNION ALL
    SELECT 2813, N'MisГ©ria', 225, 4, 262191, 8727645, 141
UNION ALL
    SELECT 2814, N'InsensГ­vel', 225, 4, 207830, 6893664, 55
UNION ALL
    SELECT 2815, N'Eu E Ela', 225, 4, 276035, 9138846, 175
UNION ALL
    SELECT 2816, N'Toda Cor', 225, 4, 209084, 6939176, 197
UNION ALL
    SELECT 2817, N'Г‰ Preciso Saber Viver', 225, 4, 251115, 8271418, 119
UNION ALL
    SELECT 2818, N'Senhor Delegado/Eu NГЈo Aguento', 225, 4, 156656, 5277983, 53
UNION ALL
    SELECT 2819, N'Battlestar Galactica: The Story So Far', 226, 18, 2622250, 490750393, 142
UNION ALL
    SELECT 2820, N'Occupation / Precipice', 227, 19, 5286953, 1054423946, 148
UNION ALL
    SELECT 2821, N'Exodus, Pt. 1', 227, 19, 2621708, 475079441, 140
UNION ALL
    SELECT 2822, N'Exodus, Pt. 2', 227, 19, 2618000, 466820021, 147
UNION ALL
    SELECT 2823, N'Collaborators', 227, 19, 2626626, 483484911, 79
UNION ALL
    SELECT 2824, N'Torn', 227, 19, 2631291, 495262585, 93
UNION ALL
    SELECT 2825, N'A Measure of Salvation', 227, 18, 2563938, 489715554, 119
UNION ALL
    SELECT 2826, N'Hero', 227, 18, 2713755, 506896959, 63
UNION ALL
    SELECT 2827, N'Unfinished Business', 227, 18, 2622038, 528499160, 196
UNION ALL
    SELECT 2828, N'The Passage', 227, 18, 2623875, 490375760, 174
UNION ALL
    SELECT 2829, N'The Eye of Jupiter', 227, 18, 2618750, 517909587, 114
UNION ALL
    SELECT 2830, N'Rapture', 227, 18, 2624541, 508406153, 61
UNION ALL
    SELECT 2831, N'Taking a Break from All Your Worries', 227, 18, 2624207, 492700163, 70
UNION ALL
    SELECT 2832, N'The Woman King', 227, 18, 2626376, 552893447, 106
UNION ALL
    SELECT 2833, N'A Day In the Life', 227, 18, 2620245, 462818231, 148
UNION ALL
    SELECT 2834, N'Dirty Hands', 227, 18, 2627961, 537648614, 176
UNION ALL
    SELECT 2835, N'Maelstrom', 227, 18, 2622372, 514154275, 69
UNION ALL
    SELECT 2836, N'The Son Also Rises', 227, 18, 2621830, 499258498, 164
UNION ALL
    SELECT 2837, N'Crossroads, Pt. 1', 227, 20, 2622622, 486233524, 174
UNION ALL
    SELECT 2838, N'Crossroads, Pt. 2', 227, 20, 2869953, 497335706, 88
UNION ALL
    SELECT 2839, N'Genesis', 228, 19, 2611986, 515671080, 178
UNION ALL
    SELECT 2840, N'Don''t Look Back', 228, 21, 2571154, 493628775, 96
UNION ALL
    SELECT 2841, N'One Giant Leap', 228, 21, 2607649, 521616246, 94
UNION ALL
    SELECT 2842, N'Collision', 228, 21, 2605480, 526182322, 174
UNION ALL
    SELECT 2843, N'Hiros', 228, 21, 2533575, 488835454, 156
UNION ALL
    SELECT 2844, N'Better Halves', 228, 21, 2573031, 549353481, 142
UNION ALL
    SELECT 2845, N'Nothing to Hide', 228, 19, 2605647, 510058181, 87
UNION ALL
    SELECT 2846, N'Seven Minutes to Midnight', 228, 21, 2613988, 515590682, 117
UNION ALL
    SELECT 2847, N'Homecoming', 228, 21, 2601351, 516015339, 73
UNION ALL
    SELECT 2848, N'Six Months Ago', 228, 19, 2602852, 505133869, 197
UNION ALL
    SELECT 2849, N'Fallout', 228, 21, 2594761, 501145440, 84
UNION ALL
    SELECT 2850, N'The Fix', 228, 21, 2600266, 507026323, 185
UNION ALL
    SELECT 2851, N'Distractions', 228, 21, 2590382, 537111289, 109
UNION ALL
    SELECT 2852, N'Run!', 228, 21, 2602602, 542936677, 167
UNION ALL
    SELECT 2853, N'Unexpected', 228, 21, 2598139, 511777758, 165
UNION ALL
    SELECT 2854, N'Company Man', 228, 21, 2601226, 493168135, 183
UNION ALL
    SELECT 2855, N'Company Man', 228, 21, 2601101, 503786316, 71
UNION ALL
    SELECT 2856, N'Parasite', 228, 21, 2602727, 487461520, 147
UNION ALL
    SELECT 2857, N'A Tale of Two Cities', 229, 19, 2636970, 513691652, 157
UNION ALL
    SELECT 2858, N'Lost (Pilot, Part 1) [Premiere]', 230, 19, 2548875, 217124866, 81
UNION ALL
    SELECT 2859, N'Man of Science, Man of Faith (Premiere)', 231, 19, 2612250, 543342028, 128
UNION ALL
    SELECT 2860, N'Adrift', 231, 19, 2564958, 502663995, 176
UNION ALL
    SELECT 2861, N'Lost (Pilot, Part 2)', 230, 19, 2436583, 204995876, 103
UNION ALL
    SELECT 2862, N'The Glass Ballerina', 229, 21, 2637458, 535729216, 166
UNION ALL
    SELECT 2863, N'Further Instructions', 229, 19, 2563980, 502041019, 195
UNION ALL
    SELECT 2864, N'Orientation', 231, 19, 2609083, 500600434, 129
UNION ALL
    SELECT 2865, N'Tabula Rasa', 230, 19, 2627105, 210526410, 119
UNION ALL
    SELECT 2866, N'Every Man for Himself', 229, 21, 2637387, 513803546, 88
UNION ALL
    SELECT 2867, N'Everybody Hates Hugo', 231, 19, 2609192, 498163145, 180
UNION ALL
    SELECT 2868, N'Walkabout', 230, 19, 2587370, 207748198, 111
UNION ALL
    SELECT 2869, N'...And Found', 231, 19, 2563833, 500330548, 195
UNION ALL
    SELECT 2870, N'The Cost of Living', 229, 19, 2637500, 505647192, 91
UNION ALL
    SELECT 2871, N'White Rabbit', 230, 19, 2571965, 201654606, 85
UNION ALL
    SELECT 2872, N'Abandoned', 231, 19, 2587041, 537348711, 132
UNION ALL
    SELECT 2873, N'House of the Rising Sun', 230, 19, 2590032, 210379525, 191
UNION ALL
    SELECT 2874, N'I Do', 229, 19, 2627791, 504676825, 110
UNION ALL
    SELECT 2875, N'Not In Portland', 229, 21, 2637303, 499061234, 131
UNION ALL
    SELECT 2876, N'Not In Portland', 229, 21, 2637345, 510546847, 78
UNION ALL
    SELECT 2877, N'The Moth', 230, 19, 2631327, 228896396, 120
UNION ALL
    SELECT 2878, N'The Other 48 Days', 231, 19, 2610625, 535256753, 168
UNION ALL
    SELECT 2879, N'Collision', 231, 19, 2564916, 475656544, 140
UNION ALL
    SELECT 2880, N'Confidence Man', 230, 19, 2615244, 223756475, 188
UNION ALL
    SELECT 2881, N'Flashes Before Your Eyes', 229, 21, 2636636, 537760755, 51
UNION ALL
    SELECT 2882, N'Lost Survival Guide', 229, 21, 2632590, 486675063, 74
UNION ALL
    SELECT 2883, N'Solitary', 230, 19, 2612894, 207045178, 161
UNION ALL
    SELECT 2884, N'What Kate Did', 231, 19, 2610250, 484583988, 78
UNION ALL
    SELECT 2885, N'Raised By Another', 230, 19, 2590459, 223623810, 161
UNION ALL
    SELECT 2886, N'Stranger In a Strange Land', 229, 21, 2636428, 505056021, 64
UNION ALL
    SELECT 2887, N'The 23rd Psalm', 231, 19, 2610416, 487401604, 171
UNION ALL
    SELECT 2888, N'All the Best Cowboys Have Daddy Issues', 230, 19, 2555492, 211743651, 103
UNION ALL
    SELECT 2889, N'The Hunting Party', 231, 21, 2611333, 520350364, 151
UNION ALL
    SELECT 2890, N'Tricia Tanaka Is Dead', 229, 21, 2635010, 548197162, 172
UNION ALL
    SELECT 2891, N'Enter 77', 229, 21, 2629796, 517521422, 70
UNION ALL
    SELECT 2892, N'Fire + Water', 231, 21, 2600333, 488458695, 171
UNION ALL
    SELECT 2893, N'Whatever the Case May Be', 230, 19, 2616410, 183867185, 83
UNION ALL
    SELECT 2894, N'Hearts and Minds', 230, 19, 2619462, 207607466, 197
UNION ALL
    SELECT 2895, N'Par Avion', 229, 21, 2629879, 517079642, 104
UNION ALL
    SELECT 2896, N'The Long Con', 231, 19, 2679583, 518376636, 194
UNION ALL
    SELECT 2897, N'One of Them', 231, 21, 2698791, 542332389, 92
UNION ALL
    SELECT 2898, N'Special', 230, 19, 2618530, 219961967, 119
UNION ALL
    SELECT 2899, N'The Man from Tallahassee', 229, 21, 2637637, 550893556, 184
UNION ALL
    SELECT 2900, N'ExposГ©', 229, 21, 2593760, 511338017, 77
UNION ALL
    SELECT 2901, N'Homecoming', 230, 19, 2515882, 210675221, 165
UNION ALL
    SELECT 2902, N'Maternity Leave', 231, 21, 2780416, 555244214, 190
UNION ALL
    SELECT 2903, N'Left Behind', 229, 21, 2635343, 538491964, 88
UNION ALL
    SELECT 2904, N'Outlaws', 230, 19, 2619887, 206500939, 104
UNION ALL
    SELECT 2905, N'The Whole Truth', 231, 21, 2610125, 495487014, 52
UNION ALL
    SELECT 2906, N'...In Translation', 230, 19, 2604575, 215441983, 130
UNION ALL
    SELECT 2907, N'Lockdown', 231, 21, 2610250, 543886056, 62
UNION ALL
    SELECT 2908, N'One of Us', 229, 21, 2638096, 502387276, 177
UNION ALL
    SELECT 2909, N'Catch-22', 229, 21, 2561394, 489773399, 191
UNION ALL
    SELECT 2910, N'Dave', 231, 19, 2825166, 574325829, 180
UNION ALL
    SELECT 2911, N'Numbers', 230, 19, 2609772, 214709143, 118
UNION ALL
    SELECT 2912, N'D.O.C.', 229, 21, 2616032, 518556641, 165
UNION ALL
    SELECT 2913, N'Deus Ex Machina', 230, 19, 2582009, 214996732, 87
UNION ALL
    SELECT 2914, N'S.O.S.', 231, 19, 2639541, 517979269, 113
UNION ALL
    SELECT 2915, N'Do No Harm', 230, 19, 2618487, 212039309, 183
UNION ALL
    SELECT 2916, N'Two for the Road', 231, 21, 2610958, 502404558, 143
UNION ALL
    SELECT 2917, N'The Greater Good', 230, 19, 2617784, 214130273, 197
UNION ALL
    SELECT 2918, N'"?"', 231, 19, 2782333, 528227089, 198
UNION ALL
    SELECT 2919, N'Born to Run', 230, 19, 2618619, 213772057, 50
UNION ALL
    SELECT 2920, N'Three Minutes', 231, 19, 2763666, 531556853, 145
UNION ALL
    SELECT 2921, N'Exodus (Part 1)', 230, 19, 2620747, 213107744, 72
UNION ALL
    SELECT 2922, N'Live Together, Die Alone, Pt. 1', 231, 21, 2478041, 457364940, 164
UNION ALL
    SELECT 2923, N'Exodus (Part 2) [Season Finale]', 230, 19, 2605557, 208667059, 187
UNION ALL
    SELECT 2924, N'Live Together, Die Alone, Pt. 2', 231, 19, 2656531, 503619265, 113
UNION ALL
    SELECT 2925, N'Exodus (Part 3) [Season Finale]', 230, 19, 2619869, 197937785, 140
UNION ALL
    SELECT 2926, N'Zoo Station', 232, 1, 276349, 9056902, 165
UNION ALL
    SELECT 2927, N'Even Better Than The Real Thing', 232, 1, 221361, 7279392, 133
UNION ALL
    SELECT 2928, N'One', 232, 1, 276192, 9158892, 78
UNION ALL
    SELECT 2929, N'Until The End Of The World', 232, 1, 278700, 9132485, 157
UNION ALL
    SELECT 2930, N'Who''s Gonna Ride Your Wild Horses', 232, 1, 316551, 10304369, 180
UNION ALL
    SELECT 2931, N'So Cruel', 232, 1, 349492, 11527614, 128
UNION ALL
    SELECT 2932, N'The Fly', 232, 1, 268982, 8825399, 101
UNION ALL
    SELECT 2933, N'Mysterious Ways', 232, 1, 243826, 8062057, 104
UNION ALL
    SELECT 2934, N'Tryin'' To Throw Your Arms Around The World', 232, 1, 232463, 7612124, 128
UNION ALL
    SELECT 2935, N'Ultraviolet (Light My Way)', 232, 1, 330788, 10754631, 159
UNION ALL
    SELECT 2936, N'Acrobat', 232, 1, 270288, 8824723, 145
UNION ALL
    SELECT 2937, N'Love Is Blindness', 232, 1, 263497, 8531766, 162
UNION ALL
    SELECT 2938, N'Beautiful Day', 233, 1, 248163, 8056723, 196
UNION ALL
    SELECT 2939, N'Stuck In A Moment You Can''t Get Out Of', 233, 1, 272378, 8997366, 112
UNION ALL
    SELECT 2940, N'Elevation', 233, 1, 227552, 7479414, 119
UNION ALL
    SELECT 2941, N'Walk On', 233, 1, 296280, 9800861, 132
UNION ALL
    SELECT 2942, N'Kite', 233, 1, 266893, 8765761, 158
UNION ALL
    SELECT 2943, N'In A Little While', 233, 1, 219271, 7189647, 52
UNION ALL
    SELECT 2944, N'Wild Honey', 233, 1, 226768, 7466069, 111
UNION ALL
    SELECT 2945, N'Peace On Earth', 233, 1, 288496, 9476171, 72
UNION ALL
    SELECT 2946, N'When I Look At The World', 233, 1, 257776, 8500491, 68
UNION ALL
    SELECT 2947, N'New York', 233, 1, 330370, 10862323, 199
UNION ALL
    SELECT 2948, N'Grace', 233, 1, 330657, 10877148, 99
UNION ALL
    SELECT 2949, N'The Three Sunrises', 234, 1, 234788, 7717990, 107
UNION ALL
    SELECT 2950, N'Spanish Eyes', 234, 1, 196702, 6392710, 104
UNION ALL
    SELECT 2951, N'Sweetest Thing', 234, 1, 185103, 6154896, 93
UNION ALL
    SELECT 2952, N'Love Comes Tumbling', 234, 1, 282671, 9328802, 133
UNION ALL
    SELECT 2953, N'Bass Trap', 234, 1, 213289, 6834107, 147
UNION ALL
    SELECT 2954, N'Dancing Barefoot', 234, 1, 287895, 9488294, 142
UNION ALL
    SELECT 2955, N'Everlasting Love', 234, 1, 202631, 6708932, 67
UNION ALL
    SELECT 2956, N'Unchained Melody', 234, 1, 294164, 9597568, 110
UNION ALL
    SELECT 2957, N'Walk To The Water', 234, 1, 289253, 9523336, 88
UNION ALL
    SELECT 2958, N'Luminous Times (Hold On To Love)', 234, 1, 277760, 9015513, 60
UNION ALL
    SELECT 2959, N'Hallelujah Here She Comes', 234, 1, 242364, 8027028, 196
UNION ALL
    SELECT 2960, N'Silver And Gold', 234, 1, 279875, 9199746, 71
UNION ALL
    SELECT 2961, N'Endless Deep', 234, 1, 179879, 5899070, 68
UNION ALL
    SELECT 2962, N'A Room At The Heartbreak Hotel', 234, 1, 274546, 9015416, 150
UNION ALL
    SELECT 2963, N'Trash, Trampoline And The Party Girl', 234, 1, 153965, 5083523, 70
UNION ALL
    SELECT 2964, N'Vertigo', 235, 1, 194612, 6329502, 197
UNION ALL
    SELECT 2965, N'Miracle Drug', 235, 1, 239124, 7760916, 197
UNION ALL
    SELECT 2966, N'Sometimes You Can''t Make It On Your Own', 235, 1, 308976, 10112863, 155
UNION ALL
    SELECT 2967, N'Love And Peace Or Else', 235, 1, 290690, 9476723, 87
UNION ALL
    SELECT 2968, N'City Of Blinding Lights', 235, 1, 347951, 11432026, 174
UNION ALL
    SELECT 2969, N'All Because Of You', 235, 1, 219141, 7198014, 60
UNION ALL
    SELECT 2970, N'A Man And A Woman', 235, 1, 270132, 8938285, 197
UNION ALL
    SELECT 2971, N'Crumbs From Your Table', 235, 1, 303568, 9892349, 155
UNION ALL
    SELECT 2972, N'One Step Closer', 235, 1, 231680, 7512912, 123
UNION ALL
    SELECT 2973, N'Original Of The Species', 235, 1, 281443, 9230041, 125
UNION ALL
    SELECT 2974, N'Yahweh', 235, 1, 262034, 8636998, 189
UNION ALL
    SELECT 2975, N'Discotheque', 236, 1, 319582, 10442206, 184
UNION ALL
    SELECT 2976, N'Do You Feel Loved', 236, 1, 307539, 10122694, 59
UNION ALL
    SELECT 2977, N'Mofo', 236, 1, 349178, 11583042, 161
UNION ALL
    SELECT 2978, N'If God Will Send His Angels', 236, 1, 322533, 10563329, 178
UNION ALL
    SELECT 2979, N'Staring At The Sun', 236, 1, 276924, 9082838, 57
UNION ALL
    SELECT 2980, N'Last Night On Earth', 236, 1, 285753, 9401017, 181
UNION ALL
    SELECT 2981, N'Gone', 236, 1, 266866, 8746301, 66
UNION ALL
    SELECT 2982, N'Miami', 236, 1, 293041, 9741603, 115
UNION ALL
    SELECT 2983, N'The Playboy Mansion', 236, 1, 280555, 9274144, 171
UNION ALL
    SELECT 2984, N'If You Wear That Velvet Dress', 236, 1, 315167, 10227333, 60
UNION ALL
    SELECT 2985, N'Please', 236, 1, 302602, 9909484, 174
UNION ALL
    SELECT 2986, N'Wake Up Dead Man', 236, 1, 292832, 9515903, 190
UNION ALL
    SELECT 2987, N'Helter Skelter', 237, 1, 187350, 6097636, 90
UNION ALL
    SELECT 2988, N'Van Diemen''s Land', 237, 1, 186044, 5990280, 103
UNION ALL
    SELECT 2989, N'Desire', 237, 1, 179226, 5874535, 115
UNION ALL
    SELECT 2990, N'Hawkmoon 269', 237, 1, 382458, 12494987, 164
UNION ALL
    SELECT 2991, N'All Along The Watchtower', 237, 1, 264568, 8623572, 78
UNION ALL
    SELECT 2992, N'I Still Haven''t Found What I''m Looking for', 237, 1, 353567, 11542247, 72
UNION ALL
    SELECT 2993, N'Freedom For My People', 237, 1, 38164, 1249764, 141
UNION ALL
    SELECT 2994, N'Silver And Gold', 237, 1, 349831, 11450194, 186
UNION ALL
    SELECT 2995, N'Pride (In The Name Of Love)', 237, 1, 267807, 8806361, 88
UNION ALL
    SELECT 2996, N'Angel Of Harlem', 237, 1, 229276, 7498022, 91
UNION ALL
    SELECT 2997, N'Love Rescue Me', 237, 1, 384522, 12508716, 150
UNION ALL
    SELECT 2998, N'When Love Comes To Town', 237, 1, 255869, 8340954, 117
UNION ALL
    SELECT 2999, N'Heartland', 237, 1, 303360, 9867748, 80
UNION ALL
    SELECT 3000, N'God Part II', 237, 1, 195604, 6497570, 129
UNION ALL
    SELECT 3001, N'The Star Spangled Banner', 237, 1, 43232, 1385810, 149
UNION ALL
    SELECT 3002, N'Bullet The Blue Sky', 237, 1, 337005, 10993607, 168
UNION ALL
    SELECT 3003, N'All I Want Is You', 237, 1, 390243, 12729820, 165
UNION ALL
    SELECT 3004, N'Pride (In The Name Of Love)', 238, 1, 230243, 7549085, 52
UNION ALL
    SELECT 3005, N'New Year''s Day', 238, 1, 258925, 8491818, 122
UNION ALL
    SELECT 3006, N'With Or Without You', 238, 1, 299023, 9765188, 187
UNION ALL
    SELECT 3007, N'I Still Haven''t Found What I''m Looking For', 238, 1, 280764, 9306737, 178
UNION ALL
    SELECT 3008, N'Sunday Bloody Sunday', 238, 1, 282174, 9269668, 139
UNION ALL
    SELECT 3009, N'Bad', 238, 1, 351817, 11628058, 179
UNION ALL
    SELECT 3010, N'Where The Streets Have No Name', 238, 1, 276218, 9042305, 135
UNION ALL
    SELECT 3011, N'I Will Follow', 238, 1, 218253, 7184825, 77
UNION ALL
    SELECT 3012, N'The Unforgettable Fire', 238, 1, 295183, 9684664, 74
UNION ALL
    SELECT 3013, N'Sweetest Thing', 238, 1, 183066, 6071385, 122
UNION ALL
    SELECT 3014, N'Desire', 238, 1, 179853, 5893206, 149
UNION ALL
    SELECT 3015, N'When Love Comes To Town', 238, 1, 258194, 8479525, 184
UNION ALL
    SELECT 3016, N'Angel Of Harlem', 238, 1, 230217, 7527339, 183
UNION ALL
    SELECT 3017, N'All I Want Is You', 238, 1, 591986, 19202252, 67
UNION ALL
    SELECT 3018, N'Sunday Bloody Sunday', 239, 1, 278204, 9140849, 88
UNION ALL
    SELECT 3019, N'Seconds', 239, 1, 191582, 6352121, 97
UNION ALL
    SELECT 3020, N'New Year''s Day', 239, 1, 336274, 11054732, 146
UNION ALL
    SELECT 3021, N'Like A Song...', 239, 1, 287294, 9365379, 115
UNION ALL
    SELECT 3022, N'Drowning Man', 239, 1, 254458, 8457066, 102
UNION ALL
    SELECT 3023, N'The Refugee', 239, 1, 221283, 7374043, 57
UNION ALL
    SELECT 3024, N'Two Hearts Beat As One', 239, 1, 243487, 7998323, 81
UNION ALL
    SELECT 3025, N'Red Light', 239, 1, 225854, 7453704, 119
UNION ALL
    SELECT 3026, N'Surrender', 239, 1, 333505, 11221406, 169
UNION ALL
    SELECT 3027, N'"40"', 239, 1, 157962, 5251767, 69
UNION ALL
    SELECT 3028, N'Zooropa', 240, 1, 392359, 12807979, 147
UNION ALL
    SELECT 3029, N'Babyface', 240, 1, 241998, 7942573, 200
UNION ALL
    SELECT 3030, N'Numb', 240, 1, 260284, 8577861, 153
UNION ALL
    SELECT 3031, N'Lemon', 240, 1, 418324, 13988878, 89
UNION ALL
    SELECT 3032, N'Stay (Faraway, So Close!)', 240, 1, 298475, 9785480, 78
UNION ALL
    SELECT 3033, N'Daddy''s Gonna Pay For Your Crashed Car', 240, 1, 320287, 10609581, 113
UNION ALL
    SELECT 3034, N'Some Days Are Better Than Others', 240, 1, 257436, 8417690, 136
UNION ALL
    SELECT 3035, N'The First Time', 240, 1, 225697, 7247651, 150
UNION ALL
    SELECT 3036, N'Dirty Day', 240, 1, 324440, 10652877, 71
UNION ALL
    SELECT 3037, N'The Wanderer', 240, 1, 283951, 9258717, 66
UNION ALL
    SELECT 3038, N'Breakfast In Bed', 241, 8, 196179, 6513325, 110
UNION ALL
    SELECT 3039, N'Where Did I Go Wrong', 241, 8, 226742, 7485054, 143
UNION ALL
    SELECT 3040, N'I Would Do For You', 241, 8, 334524, 11193602, 184
UNION ALL
    SELECT 3041, N'Homely Girl', 241, 8, 203833, 6790788, 126
UNION ALL
    SELECT 3042, N'Here I Am (Come And Take Me)', 241, 8, 242102, 8106249, 85
UNION ALL
    SELECT 3043, N'Kingston Town', 241, 8, 226951, 7638236, 174
UNION ALL
    SELECT 3044, N'Wear You To The Ball', 241, 8, 213342, 7159527, 132
UNION ALL
    SELECT 3045, N'(I Can''t Help) Falling In Love With You', 241, 8, 207568, 6905623, 183
UNION ALL
    SELECT 3046, N'Higher Ground', 241, 8, 260179, 8665244, 159
UNION ALL
    SELECT 3047, N'Bring Me Your Cup', 241, 8, 341498, 11346114, 193
UNION ALL
    SELECT 3048, N'C''est La Vie', 241, 8, 270053, 9031661, 85
UNION ALL
    SELECT 3049, N'Reggae Music', 241, 8, 245106, 8203931, 68
UNION ALL
    SELECT 3050, N'Superstition', 241, 8, 319582, 10728099, 137
UNION ALL
    SELECT 3051, N'Until My Dying Day', 241, 8, 235807, 7886195, 68
UNION ALL
    SELECT 3052, N'Where Have All The Good Times Gone?', 242, 1, 186723, 6063937, 197
UNION ALL
    SELECT 3053, N'Hang ''Em High', 242, 1, 210259, 6872314, 55
UNION ALL
    SELECT 3054, N'Cathedral', 242, 1, 82860, 2650998, 160
UNION ALL
    SELECT 3055, N'Secrets', 242, 1, 206968, 6803255, 136
UNION ALL
    SELECT 3056, N'Intruder', 242, 1, 100153, 3282142, 159
UNION ALL
    SELECT 3057, N'(Oh) Pretty Woman', 242, 1, 174680, 5665828, 78
UNION ALL
    SELECT 3058, N'Dancing In The Street', 242, 1, 225985, 7461499, 104
UNION ALL
    SELECT 3059, N'Little Guitars (Intro)', 242, 1, 42240, 1439530, 175
UNION ALL
    SELECT 3060, N'Little Guitars', 242, 1, 228806, 7453043, 135
UNION ALL
    SELECT 3061, N'Big Bad Bill (Is Sweet William Now)', 242, 1, 165146, 5489609, 81
UNION ALL
    SELECT 3062, N'The Full Bug', 242, 1, 201116, 6551013, 188
UNION ALL
    SELECT 3063, N'Happy Trails', 242, 1, 65488, 2111141, 136
UNION ALL
    SELECT 3064, N'Eruption', 243, 1, 102164, 3272891, 67
UNION ALL
    SELECT 3065, N'Ain''t Talkin'' ''bout Love', 243, 1, 228336, 7569506, 193
UNION ALL
    SELECT 3066, N'Runnin'' With The Devil', 243, 1, 215902, 7061901, 146
UNION ALL
    SELECT 3067, N'Dance the Night Away', 243, 1, 185965, 6087433, 101
UNION ALL
    SELECT 3068, N'And the Cradle Will Rock...', 243, 1, 213968, 7011402, 120
UNION ALL
    SELECT 3069, N'Unchained', 243, 1, 208953, 6777078, 161
UNION ALL
    SELECT 3070, N'Jump', 243, 1, 241711, 7911090, 54
UNION ALL
    SELECT 3071, N'Panama', 243, 1, 211853, 6921784, 160
UNION ALL
    SELECT 3072, N'Why Can''t This Be Love', 243, 1, 227761, 7457655, 66
UNION ALL
    SELECT 3073, N'Dreams', 243, 1, 291813, 9504119, 136
UNION ALL
    SELECT 3074, N'When It''s Love', 243, 1, 338991, 11049966, 97
UNION ALL
    SELECT 3075, N'Poundcake', 243, 1, 321854, 10366978, 136
UNION ALL
    SELECT 3076, N'Right Now', 243, 1, 321828, 10503352, 118
UNION ALL
    SELECT 3077, N'Can''t Stop Loving You', 243, 1, 248502, 8107896, 132
UNION ALL
    SELECT 3078, N'Humans Being', 243, 1, 308950, 10014683, 162
UNION ALL
    SELECT 3079, N'Can''t Get This Stuff No More', 243, 1, 315376, 10355753, 167
UNION ALL
    SELECT 3080, N'Me Wise Magic', 243, 1, 366053, 12013467, 177
UNION ALL
    SELECT 3081, N'Runnin'' With The Devil', 244, 1, 216032, 7056863, 165
UNION ALL
    SELECT 3082, N'Eruption', 244, 1, 102556, 3286026, 98
UNION ALL
    SELECT 3083, N'You Really Got Me', 244, 1, 158589, 5194092, 77
UNION ALL
    SELECT 3084, N'Ain''t Talkin'' ''Bout Love', 244, 1, 230060, 7617284, 132
UNION ALL
    SELECT 3085, N'I''m The One', 244, 1, 226507, 7373922, 64
UNION ALL
    SELECT 3086, N'Jamie''s Cryin''', 244, 1, 210546, 6946086, 183
UNION ALL
    SELECT 3087, N'Atomic Punk', 244, 1, 182073, 5908861, 95
UNION ALL
    SELECT 3088, N'Feel Your Love Tonight', 244, 1, 222850, 7293608, 125
UNION ALL
    SELECT 3089, N'Little Dreamer', 244, 1, 203258, 6648122, 68
UNION ALL
    SELECT 3090, N'Ice Cream Man', 244, 1, 200306, 6573145, 195
UNION ALL
    SELECT 3091, N'On Fire', 244, 1, 180636, 5879235, 196
UNION ALL
    SELECT 3092, N'Neworld', 245, 1, 105639, 3495897, 72
UNION ALL
    SELECT 3093, N'Without You', 245, 1, 390295, 12619558, 177
UNION ALL
    SELECT 3094, N'One I Want', 245, 1, 330788, 10743970, 63
UNION ALL
    SELECT 3095, N'From Afar', 245, 1, 324414, 10524554, 77
UNION ALL
    SELECT 3096, N'Dirty Water Dog', 245, 1, 327392, 10709202, 175
UNION ALL
    SELECT 3097, N'Once', 245, 1, 462837, 15378082, 151
UNION ALL
    SELECT 3098, N'Fire in the Hole', 245, 1, 331728, 10846768, 103
UNION ALL
    SELECT 3099, N'Josephina', 245, 1, 342491, 11161521, 52
UNION ALL
    SELECT 3100, N'Year to the Day', 245, 1, 514612, 16621333, 169
UNION ALL
    SELECT 3101, N'Primary', 245, 1, 86987, 2812555, 121
UNION ALL
    SELECT 3102, N'Ballot or the Bullet', 245, 1, 342282, 11212955, 189
UNION ALL
    SELECT 3103, N'How Many Say I', 245, 1, 363937, 11716855, 70
UNION ALL
    SELECT 3104, N'Sucker Train Blues', 246, 1, 267859, 8738780, 70
UNION ALL
    SELECT 3105, N'Do It For The Kids', 246, 1, 235911, 7693331, 159
UNION ALL
    SELECT 3106, N'Big Machine', 246, 1, 265613, 8673442, 65
UNION ALL
    SELECT 3107, N'Illegal I Song', 246, 1, 257750, 8483347, 197
UNION ALL
    SELECT 3108, N'Spectacle', 246, 1, 221701, 7252876, 143
UNION ALL
    SELECT 3109, N'Fall To Pieces', 246, 1, 270889, 8823096, 93
UNION ALL
    SELECT 3110, N'Headspace', 246, 1, 223033, 7237986, 109
UNION ALL
    SELECT 3111, N'Superhuman', 246, 1, 255921, 8365328, 91
UNION ALL
    SELECT 3112, N'Set Me Free', 246, 1, 247954, 8053388, 128
UNION ALL
    SELECT 3113, N'You Got No Right', 246, 1, 335412, 10991094, 149
UNION ALL
    SELECT 3114, N'Slither', 246, 1, 248398, 8118785, 67
UNION ALL
    SELECT 3115, N'Dirty Little Thing', 246, 1, 237844, 7732982, 194
UNION ALL
    SELECT 3116, N'Loving The Alien', 246, 1, 348786, 11412762, 138
UNION ALL
    SELECT 3117, N'Pela Luz Dos Olhos Teus', 247, 7, 119196, 3905715, 90
UNION ALL
    SELECT 3118, N'A Bencao E Outros', 247, 7, 421093, 14234427, 63
UNION ALL
    SELECT 3119, N'Tudo Na Mais Santa Paz', 247, 7, 222406, 7426757, 152
UNION ALL
    SELECT 3120, N'O Velho E Aflor', 247, 7, 275121, 9126828, 199
UNION ALL
    SELECT 3121, N'Cotidiano N 2', 247, 7, 55902, 1805797, 103
UNION ALL
    SELECT 3122, N'Adeus', 247, 7, 221884, 7259351, 156
UNION ALL
    SELECT 3123, N'Samba Pra Endrigo', 247, 7, 259265, 8823551, 65
UNION ALL
    SELECT 3124, N'So Por Amor', 247, 7, 236591, 7745764, 83
UNION ALL
    SELECT 3125, N'Meu Pranto Rolou', 247, 7, 181760, 6003345, 167
UNION ALL
    SELECT 3126, N'Mulher Carioca', 247, 7, 191686, 6395048, 105
UNION ALL
    SELECT 3127, N'Um Homem Chamado Alfredo', 247, 7, 151640, 4976227, 161
UNION ALL
    SELECT 3128, N'Samba Do Jato', 247, 7, 220813, 7357840, 110
UNION ALL
    SELECT 3129, N'Oi, La', 247, 7, 167053, 5562700, 192
UNION ALL
    SELECT 3130, N'Vinicius, Poeta Do Encontro', 247, 7, 336431, 10858776, 141
UNION ALL
    SELECT 3131, N'Soneto Da Separacao', 247, 7, 193880, 6277511, 145
UNION ALL
    SELECT 3132, N'Still Of The Night', 141, 3, 398210, 13043817, 103
UNION ALL
    SELECT 3133, N'Here I Go Again', 141, 3, 233874, 7652473, 198
UNION ALL
    SELECT 3134, N'Is This Love', 141, 3, 283924, 9262360, 63
UNION ALL
    SELECT 3135, N'Love Ain''t No Stranger', 141, 3, 259395, 8490428, 55
UNION ALL
    SELECT 3136, N'Looking For Love', 141, 3, 391941, 12769847, 73
UNION ALL
    SELECT 3137, N'Now You''re Gone', 141, 3, 251141, 8162193, 150
UNION ALL
    SELECT 3138, N'Slide It In', 141, 3, 202475, 6615152, 62
UNION ALL
    SELECT 3139, N'Slow An'' Easy', 141, 3, 367255, 11961332, 129
UNION ALL
    SELECT 3140, N'Judgement Day', 141, 3, 317074, 10326997, 198
UNION ALL
    SELECT 3141, N'You''re Gonna Break My Hart Again', 141, 3, 250853, 8176847, 156
UNION ALL
    SELECT 3142, N'The Deeper The Love', 141, 3, 262791, 8606504, 102
UNION ALL
    SELECT 3143, N'Crying In The Rain', 141, 3, 337005, 10931921, 128
UNION ALL
    SELECT 3144, N'Fool For Your Loving', 141, 3, 250801, 8129820, 82
UNION ALL
    SELECT 3145, N'Sweet Lady Luck', 141, 3, 273737, 8919163, 56
UNION ALL
    SELECT 3146, N'Faixa Amarela', 248, 7, 240692, 8082036, 196
UNION ALL
    SELECT 3147, N'Posso AtГ© Me Apaixonar', 248, 7, 200698, 6735526, 157
UNION ALL
    SELECT 3148, N'NГЈo Sou Mais Disso', 248, 7, 225985, 7613817, 79
UNION ALL
    SELECT 3149, N'Vivo Isolado Do Mundo', 248, 7, 180035, 6073995, 52
UNION ALL
    SELECT 3150, N'CoraГ§ГЈo Em Desalinho', 248, 7, 185208, 6225948, 159
UNION ALL
    SELECT 3151, N'Seu BalancГЄ', 248, 7, 219454, 7311219, 190
UNION ALL
    SELECT 3152, N'Vai Adiar', 248, 7, 270393, 9134882, 129
UNION ALL
    SELECT 3153, N'Rugas', 248, 7, 140930, 4703182, 55
UNION ALL
    SELECT 3154, N'Feirinha da Pavuna/Luz do Repente/BagaГ§o da Laranja', 248, 7, 107206, 3593684, 119
UNION ALL
    SELECT 3155, N'Sem Essa de Malandro Agulha', 248, 7, 158484, 5332668, 119
UNION ALL
    SELECT 3156, N'Chico NГЈo Vai na Corimba', 248, 7, 269374, 9122188, 189
UNION ALL
    SELECT 3157, N'Papel Principal', 248, 7, 217495, 7325302, 145
UNION ALL
    SELECT 3158, N'Saudade Louca', 248, 7, 243591, 8136475, 110
UNION ALL
    SELECT 3159, N'CamarГЈo que Dorme e Onda Leva', 248, 7, 299102, 10012231, 159
UNION ALL
    SELECT 3160, N'Sapopemba e Maxambomba', 248, 7, 245394, 8268712, 155
UNION ALL
    SELECT 3161, N'Minha FГ©', 248, 7, 206994, 6981474, 176
UNION ALL
    SELECT 3162, N'Lua de Ogum', 248, 7, 168463, 5719129, 155
UNION ALL
    SELECT 3163, N'Samba pras moГ§as', 248, 7, 152816, 5121366, 137
UNION ALL
    SELECT 3164, N'Verdade', 248, 7, 332826, 11120708, 169
UNION ALL
    SELECT 3165, N'The Brig', 229, 21, 2617325, 488919543, 88
UNION ALL
    SELECT 3166, N'.07%', 228, 21, 2585794, 541715199, 69
UNION ALL
    SELECT 3167, N'Five Years Gone', 228, 21, 2587712, 530551890, 72
UNION ALL
    SELECT 3168, N'The Hard Part', 228, 21, 2601017, 475996611, 61
UNION ALL
    SELECT 3169, N'The Man Behind the Curtain', 229, 21, 2615990, 493951081, 107
UNION ALL
    SELECT 3170, N'Greatest Hits', 229, 21, 2617117, 522102916, 157
UNION ALL
    SELECT 3171, N'Landslide', 228, 21, 2600725, 518677861, 93
UNION ALL
    SELECT 3172, N'The Office: An American Workplace (Pilot)', 249, 19, 1380833, 290482361, 151
UNION ALL
    SELECT 3173, N'Diversity Day', 249, 19, 1306416, 257879716, 181
UNION ALL
    SELECT 3174, N'Health Care', 249, 19, 1321791, 260493577, 188
UNION ALL
    SELECT 3175, N'The Alliance', 249, 19, 1317125, 266203162, 190
UNION ALL
    SELECT 3176, N'Basketball', 249, 19, 1323541, 267464180, 139
UNION ALL
    SELECT 3177, N'Hot Girl', 249, 19, 1325458, 267836576, 118
UNION ALL
    SELECT 3178, N'The Dundies', 250, 19, 1253541, 246845576, 55
UNION ALL
    SELECT 3179, N'Sexual Harassment', 250, 19, 1294541, 273069146, 195
UNION ALL
    SELECT 3180, N'Office Olympics', 250, 19, 1290458, 256247623, 95
UNION ALL
    SELECT 3181, N'The Fire', 250, 19, 1288166, 266856017, 121
UNION ALL
    SELECT 3182, N'Halloween', 250, 19, 1315333, 249205209, 183
UNION ALL
    SELECT 3183, N'The Fight', 250, 19, 1320028, 277149457, 186
UNION ALL
    SELECT 3184, N'The Client', 250, 19, 1299341, 253836788, 87
UNION ALL
    SELECT 3185, N'Performance Review', 250, 19, 1292458, 256143822, 143
UNION ALL
    SELECT 3186, N'Email Surveillance', 250, 19, 1328870, 265101113, 190
UNION ALL
    SELECT 3187, N'Christmas Party', 250, 19, 1282115, 260891300, 80
UNION ALL
    SELECT 3188, N'Booze Cruise', 250, 19, 1267958, 252518021, 143
UNION ALL
    SELECT 3189, N'The Injury', 250, 19, 1275275, 253912762, 130
UNION ALL
    SELECT 3190, N'The Secret', 250, 19, 1264875, 253143200, 199
UNION ALL
    SELECT 3191, N'The Carpet', 250, 19, 1264375, 256477011, 183
UNION ALL
    SELECT 3192, N'Boys and Girls', 250, 19, 1278333, 255245729, 57
UNION ALL
    SELECT 3193, N'Valentine''s Day', 250, 19, 1270375, 253552710, 82
UNION ALL
    SELECT 3194, N'Dwight''s Speech', 250, 19, 1278041, 255001728, 128
UNION ALL
    SELECT 3195, N'Take Your Daughter to Work Day', 250, 19, 1268333, 253451012, 168
UNION ALL
    SELECT 3196, N'Michael''s Birthday', 250, 19, 1237791, 247238398, 100
UNION ALL
    SELECT 3197, N'Drug Testing', 250, 19, 1278625, 244626927, 94
UNION ALL
    SELECT 3198, N'Conflict Resolution', 250, 19, 1274583, 253808658, 153
UNION ALL
    SELECT 3199, N'Casino Night - Season Finale', 250, 19, 1712791, 327642458, 58
UNION ALL
    SELECT 3200, N'Gay Witch Hunt', 251, 19, 1326534, 276942637, 160
UNION ALL
    SELECT 3201, N'The Convention', 251, 19, 1297213, 255117055, 161
UNION ALL
    SELECT 3202, N'The Coup', 251, 19, 1276526, 267205501, 166
UNION ALL
    SELECT 3203, N'Grief Counseling', 251, 19, 1282615, 256912833, 85
UNION ALL
    SELECT 3204, N'The Initiation', 251, 19, 1280113, 251728257, 174
UNION ALL
    SELECT 3205, N'Diwali', 251, 19, 1279904, 252726644, 198
UNION ALL
    SELECT 3206, N'Branch Closing', 251, 19, 1822781, 358761786, 108
UNION ALL
    SELECT 3207, N'The Merger', 251, 19, 1801926, 345960631, 60
UNION ALL
    SELECT 3208, N'The Convict', 251, 22, 1273064, 248863427, 140
UNION ALL
    SELECT 3209, N'A Benihana Christmas, Pts. 1 & 2', 251, 22, 2519436, 515301752, 74
UNION ALL
    SELECT 3210, N'Back from Vacation', 251, 22, 1271688, 245378749, 83
UNION ALL
    SELECT 3211, N'Traveling Salesmen', 251, 22, 1289039, 250822697, 53
UNION ALL
    SELECT 3212, N'Producer''s Cut: The Return', 251, 22, 1700241, 337219980, 189
UNION ALL
    SELECT 3213, N'Ben Franklin', 251, 22, 1271938, 264168080, 173
UNION ALL
    SELECT 3214, N'Phyllis''s Wedding', 251, 22, 1271521, 258561054, 155
UNION ALL
    SELECT 3215, N'Business School', 251, 22, 1302093, 254402605, 143
UNION ALL
    SELECT 3216, N'Cocktails', 251, 22, 1272522, 259011909, 99
UNION ALL
    SELECT 3217, N'The Negotiation', 251, 22, 1767851, 371663719, 86
UNION ALL
    SELECT 3218, N'Safety Training', 251, 22, 1271229, 253054534, 87
UNION ALL
    SELECT 3219, N'Product Recall', 251, 22, 1268268, 251208610, 197
UNION ALL
    SELECT 3220, N'Women''s Appreciation', 251, 22, 1732649, 338778844, 152
UNION ALL
    SELECT 3221, N'Beach Games', 251, 22, 1676134, 333671149, 129
UNION ALL
    SELECT 3222, N'The Job', 251, 22, 2541875, 501060138, 121
UNION ALL
    SELECT 3223, N'How to Stop an Exploding Man', 228, 21, 2687103, 487881159, 168
UNION ALL
    SELECT 3224, N'Through a Looking Glass', 229, 21, 5088838, 1059546140, 158
UNION ALL
    SELECT 3225, N'Your Time Is Gonna Come', 252, 1, 310774, 5126563, 180
UNION ALL
    SELECT 3226, N'Battlestar Galactica, Pt. 1', 253, 20, 2952702, 541359437, 71
UNION ALL
    SELECT 3227, N'Battlestar Galactica, Pt. 2', 253, 20, 2956081, 521387924, 76
UNION ALL
    SELECT 3228, N'Battlestar Galactica, Pt. 3', 253, 20, 2927802, 554509033, 65
UNION ALL
    SELECT 3229, N'Lost Planet of the Gods, Pt. 1', 253, 20, 2922547, 537812711, 181
UNION ALL
    SELECT 3230, N'Lost Planet of the Gods, Pt. 2', 253, 20, 2914664, 534343985, 195
UNION ALL
    SELECT 3231, N'The Lost Warrior', 253, 20, 2920045, 558872190, 86
UNION ALL
    SELECT 3232, N'The Long Patrol', 253, 20, 2925008, 513122217, 61
UNION ALL
    SELECT 3233, N'The Gun On Ice Planet Zero, Pt. 1', 253, 20, 2907615, 540980196, 112
UNION ALL
    SELECT 3234, N'The Gun On Ice Planet Zero, Pt. 2', 253, 20, 2924341, 546542281, 199
UNION ALL
    SELECT 3235, N'The Magnificent Warriors', 253, 20, 2924716, 570152232, 128
UNION ALL
    SELECT 3236, N'The Young Lords', 253, 20, 2863571, 587051735, 50
UNION ALL
    SELECT 3237, N'The Living Legend, Pt. 1', 253, 20, 2924507, 503641007, 104
UNION ALL
    SELECT 3238, N'The Living Legend, Pt. 2', 253, 20, 2923298, 515632754, 189
UNION ALL
    SELECT 3239, N'Fire In Space', 253, 20, 2926593, 536784757, 54
UNION ALL
    SELECT 3240, N'War of the Gods, Pt. 1', 253, 20, 2922630, 505761343, 124
UNION ALL
    SELECT 3241, N'War of the Gods, Pt. 2', 253, 20, 2923381, 487899692, 152
UNION ALL
    SELECT 3242, N'The Man With Nine Lives', 253, 20, 2956998, 577829804, 153
UNION ALL
    SELECT 3243, N'Murder On the Rising Star', 253, 20, 2935894, 551759986, 50
UNION ALL
    SELECT 3244, N'Greetings from Earth, Pt. 1', 253, 20, 2960293, 536824558, 151
UNION ALL
    SELECT 3245, N'Greetings from Earth, Pt. 2', 253, 20, 2903778, 527842860, 82
UNION ALL
    SELECT 3246, N'Baltar''s Escape', 253, 20, 2922088, 525564224, 65
UNION ALL
    SELECT 3247, N'Experiment In Terra', 253, 20, 2923548, 547982556, 151
UNION ALL
    SELECT 3248, N'Take the Celestra', 253, 20, 2927677, 512381289, 166
UNION ALL
    SELECT 3249, N'The Hand of God', 253, 20, 2924007, 536583079, 145
UNION ALL
    SELECT 3250, N'Pilot', 254, 19, 2484567, 492670102, 126
UNION ALL
    SELECT 3251, N'Through the Looking Glass, Pt. 2', 229, 21, 2617117, 550943353, 128
UNION ALL
    SELECT 3252, N'Through the Looking Glass, Pt. 1', 229, 21, 2610860, 493211809, 91
UNION ALL
    SELECT 3253, N'Instant Karma', 255, 9, 193188, 3150090, 154
UNION ALL
    SELECT 3254, N'#9 Dream', 255, 9, 278312, 4506425, 90
UNION ALL
    SELECT 3255, N'Mother', 255, 9, 287740, 4656660, 81
UNION ALL
    SELECT 3256, N'Give Peace a Chance', 255, 9, 274644, 4448025, 116
UNION ALL
    SELECT 3257, N'Cold Turkey', 255, 9, 281424, 4556003, 198
UNION ALL
    SELECT 3258, N'Whatever Gets You Thru the Night', 255, 9, 215084, 3499018, 129
UNION ALL
    SELECT 3259, N'I''m Losing You', 255, 9, 240719, 3907467, 74
UNION ALL
    SELECT 3260, N'Gimme Some Truth', 255, 9, 232778, 3780807, 109
UNION ALL
    SELECT 3261, N'Oh, My Love', 255, 9, 159473, 2612788, 143
UNION ALL
    SELECT 3262, N'Imagine', 255, 9, 192329, 3136271, 68
UNION ALL
    SELECT 3263, N'Nobody Told Me', 255, 9, 210348, 3423395, 180
UNION ALL
    SELECT 3264, N'Jealous Guy', 255, 9, 239094, 3881620, 92
UNION ALL
    SELECT 3265, N'Working Class Hero', 255, 9, 265449, 4301430, 96
UNION ALL
    SELECT 3266, N'Power to the People', 255, 9, 213018, 3466029, 70
UNION ALL
    SELECT 3267, N'Imagine', 255, 9, 219078, 3562542, 156
UNION ALL
    SELECT 3268, N'Beautiful Boy', 255, 9, 227995, 3704642, 128
UNION ALL
    SELECT 3269, N'Isolation', 255, 9, 156059, 2558399, 133
UNION ALL
    SELECT 3270, N'Watching the Wheels', 255, 9, 198645, 3237063, 58
UNION ALL
    SELECT 3271, N'Grow Old With Me', 255, 9, 149093, 2447453, 129
UNION ALL
    SELECT 3272, N'Gimme Some Truth', 255, 9, 187546, 3060083, 138
UNION ALL
    SELECT 3273, N'[Just Like] Starting Over', 255, 9, 215549, 3506308, 140
UNION ALL
    SELECT 3274, N'God', 255, 9, 260410, 4221135, 168
UNION ALL
    SELECT 3275, N'Real Love', 255, 9, 236911, 3846658, 132
UNION ALL
    SELECT 3276, N'Sympton of the Universe', 256, 1, 340890, 5489313, 148
UNION ALL
    SELECT 3277, N'Snowblind', 256, 1, 295960, 4773171, 75
UNION ALL
    SELECT 3278, N'Black Sabbath', 256, 1, 364180, 5860455, 66
UNION ALL
    SELECT 3279, N'Fairies Wear Boots', 256, 1, 392764, 6315916, 155
UNION ALL
    SELECT 3280, N'War Pigs', 256, 1, 515435, 8270194, 174
UNION ALL
    SELECT 3281, N'The Wizard', 256, 1, 282678, 4561796, 139
UNION ALL
    SELECT 3282, N'N.I.B.', 256, 1, 335248, 5399456, 155
UNION ALL
    SELECT 3283, N'Sweet Leaf', 256, 1, 354706, 5709700, 57
UNION ALL
    SELECT 3284, N'Never Say Die', 256, 1, 258343, 4173799, 171
UNION ALL
    SELECT 3285, N'Sabbath, Bloody Sabbath', 256, 1, 333622, 5373633, 116
UNION ALL
    SELECT 3286, N'Iron Man/Children of the Grave', 256, 1, 552308, 8858616, 107
UNION ALL
    SELECT 3287, N'Paranoid', 256, 1, 189171, 3071042, 124
UNION ALL
    SELECT 3288, N'Rock You Like a Hurricane', 257, 1, 255766, 4300973, 108
UNION ALL
    SELECT 3289, N'No One Like You', 257, 1, 240325, 4050259, 74
UNION ALL
    SELECT 3290, N'The Zoo', 257, 1, 332740, 5550779, 192
UNION ALL
    SELECT 3291, N'Loving You Sunday Morning', 257, 1, 339125, 5654493, 72
UNION ALL
    SELECT 3292, N'Still Loving You', 257, 1, 390674, 6491444, 92
UNION ALL
    SELECT 3293, N'Big City Nights', 257, 1, 251865, 4237651, 77
UNION ALL
    SELECT 3294, N'Believe in Love', 257, 1, 325774, 5437651, 183
UNION ALL
    SELECT 3295, N'Rhythm of Love', 257, 1, 231246, 3902834, 108
UNION ALL
    SELECT 3296, N'I Can''t Explain', 257, 1, 205332, 3482099, 199
UNION ALL
    SELECT 3297, N'Tease Me Please Me', 257, 1, 287229, 4811894, 116
UNION ALL
    SELECT 3298, N'Wind of Change', 257, 1, 315325, 5268002, 74
UNION ALL
    SELECT 3299, N'Send Me an Angel', 257, 1, 273041, 4581492, 193
UNION ALL
    SELECT 3300, N'Jump Around', 258, 17, 217835, 8715653, 112
UNION ALL
    SELECT 3301, N'Salutations', 258, 17, 69120, 2767047, 102
UNION ALL
    SELECT 3302, N'Put Your Head Out', 258, 17, 182230, 7291473, 90
UNION ALL
    SELECT 3303, N'Top O'' The Morning To Ya', 258, 17, 216633, 8667599, 53
UNION ALL
    SELECT 3304, N'Commercial 1', 258, 17, 7941, 319888, 139
UNION ALL
    SELECT 3305, N'House And The Rising Sun', 258, 17, 219402, 8778369, 148
UNION ALL
    SELECT 3306, N'Shamrocks And Shenanigans', 258, 17, 218331, 8735518, 109
UNION ALL
    SELECT 3307, N'House Of Pain Anthem', 258, 17, 155611, 6226713, 155
UNION ALL
    SELECT 3308, N'Danny Boy, Danny Boy', 258, 17, 114520, 4583091, 137
UNION ALL
    SELECT 3309, N'Guess Who''s Back', 258, 17, 238393, 9537994, 135
UNION ALL
    SELECT 3310, N'Commercial 2', 258, 17, 21211, 850698, 108
UNION ALL
    SELECT 3311, N'Put On Your Shit Kickers', 258, 17, 190432, 7619569, 89
UNION ALL
    SELECT 3312, N'Come And Get Some Of This', 258, 17, 170475, 6821279, 166
UNION ALL
    SELECT 3313, N'Life Goes On', 258, 17, 163030, 6523458, 158
UNION ALL
    SELECT 3314, N'One For The Road', 258, 17, 170213, 6810820, 53
UNION ALL
    SELECT 3315, N'Feel It', 258, 17, 239908, 9598588, 129
UNION ALL
    SELECT 3316, N'All My Love', 258, 17, 200620, 8027065, 58
UNION ALL
    SELECT 3317, N'Jump Around (Pete Rock Remix)', 258, 17, 236120, 9447101, 200
UNION ALL
    SELECT 3318, N'Shamrocks And Shenanigans (Boom Shalock Lock Boom/Butch Vig Mix)', 258, 17, 237035, 9483705, 78
UNION ALL
    SELECT 3319, N'Instinto Colectivo', 259, 15, 300564, 12024875, 139
UNION ALL
    SELECT 3320, N'Chapa o Coco', 259, 15, 143830, 5755478, 148
UNION ALL
    SELECT 3321, N'Prostituta', 259, 15, 359000, 14362307, 119
UNION ALL
    SELECT 3322, N'Eu So Queria Sumir', 259, 15, 269740, 10791921, 164
UNION ALL
    SELECT 3323, N'Tres Reis', 259, 15, 304143, 12168015, 125
UNION ALL
    SELECT 3324, N'Um Lugar ao Sol', 259, 15, 212323, 8495217, 162
UNION ALL
    SELECT 3325, N'Batalha Naval', 259, 15, 285727, 11431382, 120
UNION ALL
    SELECT 3326, N'Todo o Carnaval tem seu Fim', 259, 15, 237426, 9499371, 87
UNION ALL
    SELECT 3327, N'O Misterio do Samba', 259, 15, 226142, 9047970, 189
UNION ALL
    SELECT 3328, N'Armadura', 259, 15, 232881, 9317533, 164
UNION ALL
    SELECT 3329, N'Na Ladeira', 259, 15, 221570, 8865099, 128
UNION ALL
    SELECT 3330, N'Carimbo', 259, 15, 328751, 13152314, 131
UNION ALL
    SELECT 3331, N'Catimbo', 259, 15, 254484, 10181692, 104
UNION ALL
    SELECT 3332, N'Funk de Bamba', 259, 15, 237322, 9495184, 54
UNION ALL
    SELECT 3333, N'Chega no Suingue', 259, 15, 221805, 8874509, 103
UNION ALL
    SELECT 3334, N'Mun-Ra', 259, 15, 274651, 10988338, 155
UNION ALL
    SELECT 3335, N'Freestyle Love', 259, 15, 318484, 12741680, 78
UNION ALL
    SELECT 3336, N'War Pigs', 260, 23, 234013, 8052374, 58
UNION ALL
    SELECT 3337, N'Past, Present, and Future', 261, 21, 2492867, 490796184, 60
UNION ALL
    SELECT 3338, N'The Beginning of the End', 261, 21, 2611903, 526865050, 166
UNION ALL
    SELECT 3339, N'LOST Season 4 Trailer', 261, 21, 112712, 20831818, 192
UNION ALL
    SELECT 3340, N'LOST In 8:15', 261, 21, 497163, 98460675, 132
UNION ALL
    SELECT 3341, N'Confirmed Dead', 261, 21, 2611986, 512168460, 141
UNION ALL
    SELECT 3342, N'The Economist', 261, 21, 2609025, 516934914, 173
UNION ALL
    SELECT 3343, N'Eggtown', 261, 19, 2608817, 501061240, 105
UNION ALL
    SELECT 3344, N'The Constant', 261, 21, 2611569, 520209363, 97
UNION ALL
    SELECT 3345, N'The Other Woman', 261, 21, 2605021, 513246663, 148
UNION ALL
    SELECT 3346, N'Ji Yeon', 261, 19, 2588797, 506458858, 194
UNION ALL
    SELECT 3347, N'Meet Kevin Johnson', 261, 19, 2612028, 504132981, 177
UNION ALL
    SELECT 3348, N'The Shape of Things to Come', 261, 21, 2591299, 502284266, 80
UNION ALL
    SELECT 3349, N'Amanda', 262, 2, 246503, 4011615, 147
UNION ALL
    SELECT 3350, N'Despertar', 262, 2, 307385, 4821485, 178
UNION ALL
    SELECT 3351, N'Din Din Wo (Little Child)', 263, 16, 285837, 4615841, 114
UNION ALL
    SELECT 3352, N'Distance', 264, 15, 327122, 5327463, 55
UNION ALL
    SELECT 3353, N'I Guess You''re Right', 265, 1, 212044, 3453849, 171
UNION ALL
    SELECT 3354, N'I Ka Barra (Your Work)', 263, 16, 300605, 4855457, 126
UNION ALL
    SELECT 3355, N'Love Comes', 265, 1, 199923, 3240609, 109
UNION ALL
    SELECT 3356, N'Muita Bobeira', 266, 7, 172710, 2775071, 169
UNION ALL
    SELECT 3357, N'OAM''s Blues', 267, 2, 266936, 4292028, 87
UNION ALL
    SELECT 3358, N'One Step Beyond', 264, 15, 366085, 6034098, 65
UNION ALL
    SELECT 3359, N'Symphony No. 3 in E-flat major, Op. 55, "Eroica" - Scherzo: Allegro Vivace', 268, 24, 356426, 5817216, 106
UNION ALL
    SELECT 3360, N'Something Nice Back Home', 261, 21, 2612779, 484711353, 65
UNION ALL
    SELECT 3361, N'Cabin Fever', 261, 21, 2612028, 477733942, 124
UNION ALL
    SELECT 3362, N'There''s No Place Like Home, Pt. 1', 261, 21, 2609526, 522919189, 181
UNION ALL
    SELECT 3363, N'There''s No Place Like Home, Pt. 2', 261, 21, 2497956, 523748920, 54
UNION ALL
    SELECT 3364, N'There''s No Place Like Home, Pt. 3', 261, 21, 2582957, 486161766, 153
UNION ALL
    SELECT 3365, N'Say Hello 2 Heaven', 269, 23, 384497, 6477217, 179
UNION ALL
    SELECT 3366, N'Reach Down', 269, 23, 672773, 11157785, 140
UNION ALL
    SELECT 3367, N'Hunger Strike', 269, 23, 246292, 4233212, 141
UNION ALL
    SELECT 3368, N'Pushin Forward Back', 269, 23, 225278, 3892066, 135
UNION ALL
    SELECT 3369, N'Call Me a Dog', 269, 23, 304458, 5177612, 51
UNION ALL
    SELECT 3370, N'Times of Trouble', 269, 23, 342539, 5795951, 191
UNION ALL
    SELECT 3371, N'Wooden Jesus', 269, 23, 250565, 4302603, 135
UNION ALL
    SELECT 3372, N'Your Savior', 269, 23, 244226, 4199626, 82
UNION ALL
    SELECT 3373, N'Four Walled World', 269, 23, 414474, 6964048, 61
UNION ALL
    SELECT 3374, N'All Night Thing', 269, 23, 231803, 3997982, 145
UNION ALL
    SELECT 3375, N'No Such Thing', 270, 23, 224837, 3691272, 51
UNION ALL
    SELECT 3376, N'Poison Eye', 270, 23, 237120, 3890037, 115
UNION ALL
    SELECT 3377, N'Arms Around Your Love', 270, 23, 214016, 3516224, 101
UNION ALL
    SELECT 3378, N'Safe and Sound', 270, 23, 256764, 4207769, 139
UNION ALL
    SELECT 3379, N'She''ll Never Be Your Man', 270, 23, 204078, 3355715, 78
UNION ALL
    SELECT 3380, N'Ghosts', 270, 23, 231547, 3799745, 80
UNION ALL
    SELECT 3381, N'Killing Birds', 270, 23, 218498, 3588776, 194
UNION ALL
    SELECT 3382, N'Billie Jean', 270, 23, 281401, 4606408, 107
UNION ALL
    SELECT 3383, N'Scar On the Sky', 270, 23, 220193, 3616618, 65
UNION ALL
    SELECT 3384, N'Your Soul Today', 270, 23, 205959, 3385722, 98
UNION ALL
    SELECT 3385, N'Finally Forever', 270, 23, 217035, 3565098, 168
UNION ALL
    SELECT 3386, N'Silence the Voices', 270, 23, 267376, 4379597, 95
UNION ALL
    SELECT 3387, N'Disappearing Act', 270, 23, 273320, 4476203, 50
UNION ALL
    SELECT 3388, N'You Know My Name', 270, 23, 240255, 3940651, 92
UNION ALL
    SELECT 3389, N'Revelations', 271, 23, 252376, 4111051, 147
UNION ALL
    SELECT 3390, N'One and the Same', 271, 23, 217732, 3559040, 181
UNION ALL
    SELECT 3391, N'Sound of a Gun', 271, 23, 260154, 4234990, 183
UNION ALL
    SELECT 3392, N'Until We Fall', 271, 23, 230758, 3766605, 155
UNION ALL
    SELECT 3393, N'Original Fire', 271, 23, 218916, 3577821, 158
UNION ALL
    SELECT 3394, N'Broken City', 271, 23, 228366, 3728955, 126
UNION ALL
    SELECT 3395, N'Somedays', 271, 23, 213831, 3497176, 69
UNION ALL
    SELECT 3396, N'Shape of Things to Come', 271, 23, 274597, 4465399, 115
UNION ALL
    SELECT 3397, N'Jewel of the Summertime', 271, 23, 233242, 3806103, 60
UNION ALL
    SELECT 3398, N'Wide Awake', 271, 23, 266308, 4333050, 125
UNION ALL
    SELECT 3399, N'Nothing Left to Say But Goodbye', 271, 23, 213041, 3484335, 59
UNION ALL
    SELECT 3400, N'Moth', 271, 23, 298049, 4838884, 52
UNION ALL
    SELECT 3401, N'Show Me How to Live (Live at the Quart Festival)', 271, 23, 301974, 4901540, 95
UNION ALL
    SELECT 3402, N'Band Members Discuss Tracks from "Revelations"', 271, 23, 294294, 61118891, 75
UNION ALL
    SELECT 3403, N'Intoitus: Adorate Deum', 272, 24, 245317, 4123531, 177
UNION ALL
    SELECT 3404, N'Miserere mei, Deus', 273, 24, 501503, 8285941, 56
UNION ALL
    SELECT 3405, N'Canon and Gigue in D Major: I. Canon', 274, 24, 271788, 4438393, 128
UNION ALL
    SELECT 3406, N'Concerto No. 1 in E Major, RV 269 "Spring": I. Allegro', 275, 24, 199086, 3347810, 65
UNION ALL
    SELECT 3407, N'Concerto for 2 Violins in D Minor, BWV 1043: I. Vivace', 276, 24, 193722, 3192890, 50
UNION ALL
    SELECT 3408, N'Aria Mit 30 VerГ¤nderungen, BWV 988 "Goldberg Variations": Aria', 277, 24, 120463, 2081895, 64
UNION ALL
    SELECT 3409, N'Suite for Solo Cello No. 1 in G Major, BWV 1007: I. PrГ©lude', 278, 24, 143288, 2315495, 96
UNION ALL
    SELECT 3410, N'The Messiah: Behold, I Tell You a Mystery... The Trumpet Shall Sound', 279, 24, 582029, 9553140, 94
UNION ALL
    SELECT 3411, N'Solomon HWV 67: The Arrival of the Queen of Sheba', 280, 24, 197135, 3247914, 81
UNION ALL
    SELECT 3412, N'"Eine Kleine Nachtmusik" Serenade In G, K. 525: I. Allegro', 281, 24, 348971, 5760129, 148
UNION ALL
    SELECT 3413, N'Concerto for Clarinet in A Major, K. 622: II. Adagio', 282, 24, 394482, 6474980, 120
UNION ALL
    SELECT 3414, N'Symphony No. 104 in D Major "London": IV. Finale: Spiritoso', 283, 24, 306687, 10085867, 147
UNION ALL
    SELECT 3415, N'Symphony No.5 in C Minor: I. Allegro con brio', 284, 24, 392462, 6419730, 122
UNION ALL
    SELECT 3416, N'Ave Maria', 285, 24, 338243, 5605648, 113
UNION ALL
    SELECT 3417, N'Nabucco: Chorus, "Va, Pensiero, Sull''ali Dorate"', 286, 24, 274504, 4498583, 103
UNION ALL
    SELECT 3418, N'Die WalkГјre: The Ride of the Valkyries', 287, 24, 189008, 3114209, 126
UNION ALL
    SELECT 3419, N'Requiem, Op.48: 4. Pie Jesu', 288, 24, 258924, 4314850, 125
UNION ALL
    SELECT 3420, N'The Nutcracker, Op. 71a, Act II: Scene 14: Pas de deux: Dance of the Prince & the Sugar-Plum Fairy', 289, 24, 304226, 5184289, 191
UNION ALL
    SELECT 3421, N'Nimrod (Adagio) from Variations On an Original Theme, Op. 36 "Enigma"', 290, 24, 250031, 4124707, 132
UNION ALL
    SELECT 3422, N'Madama Butterfly: Un Bel DГ¬ Vedremo', 291, 24, 277639, 4588197, 70
UNION ALL
    SELECT 3423, N'Jupiter, the Bringer of Jollity', 292, 24, 522099, 8547876, 199
UNION ALL
    SELECT 3424, N'Turandot, Act III, Nessun dorma!', 293, 24, 176911, 2920890, 139
UNION ALL
    SELECT 3425, N'Adagio for Strings from the String Quartet, Op. 11', 294, 24, 596519, 9585597, 92
UNION ALL
    SELECT 3426, N'Carmina Burana: O Fortuna', 295, 24, 156710, 2630293, 194
UNION ALL
    SELECT 3427, N'Fanfare for the Common Man', 296, 24, 198064, 3211245, 183
UNION ALL
    SELECT 3428, N'Branch Closing', 251, 22, 1814855, 360331351, 51
UNION ALL
    SELECT 3429, N'The Return', 251, 22, 1705080, 343877320, 147
UNION ALL
    SELECT 3430, N'Toccata and Fugue in D Minor, BWV 565: I. Toccata', 297, 24, 153901, 2649938, 114
UNION ALL
    SELECT 3431, N'Symphony No.1 in D Major, Op.25 "Classical", Allegro Con Brio', 298, 24, 254001, 4195542, 83
UNION ALL
    SELECT 3432, N'Scheherazade, Op. 35: I. The Sea and Sindbad''s Ship', 299, 24, 545203, 8916313, 86
UNION ALL
    SELECT 3433, N'Concerto No.2 in F Major, BWV1047, I. Allegro', 300, 24, 307244, 5064553, 143
UNION ALL
    SELECT 3434, N'Concerto for Piano No. 2 in F Minor, Op. 21: II. Larghetto', 301, 24, 560342, 9160082, 51
UNION ALL
    SELECT 3435, N'Cavalleria Rusticana \ Act \ Intermezzo Sinfonico', 302, 24, 243436, 4001276, 126
UNION ALL
    SELECT 3436, N'Karelia Suite, Op.11: 2. Ballade (Tempo Di Menuetto)', 303, 24, 406000, 5908455, 158
UNION ALL
    SELECT 3437, N'Piano Sonata No. 14 in C Sharp Minor, Op. 27, No. 2, "Moonlight": I. Adagio sostenuto', 304, 24, 391000, 6318740, 75
UNION ALL
    SELECT 3438, N'Fantasia On Greensleeves', 280, 24, 268066, 4513190, 54
UNION ALL
    SELECT 3439, N'Das Lied Von Der Erde, Von Der Jugend', 305, 24, 223583, 3700206, 63
UNION ALL
    SELECT 3440, N'Concerto for Cello and Orchestra in E minor, Op. 85: I. Adagio - Moderato', 306, 24, 483133, 7865479, 61
UNION ALL
    SELECT 3441, N'Two Fanfares for Orchestra: II. Short Ride in a Fast Machine', 307, 24, 254930, 4310896, 62
UNION ALL
    SELECT 3442, N'Wellington''s Victory or the Battle Symphony, Op.91: 2. Symphony of Triumph', 308, 24, 412000, 6965201, 85
UNION ALL
    SELECT 3443, N'Missa Papae Marcelli: Kyrie', 309, 24, 240666, 4244149, 80
UNION ALL
    SELECT 3444, N'Romeo et Juliette: No. 11 - Danse des Chevaliers', 310, 24, 275015, 4519239, 187
UNION ALL
    SELECT 3445, N'On the Beautiful Blue Danube', 311, 24, 526696, 8610225, 163
UNION ALL
    SELECT 3446, N'Symphonie Fantastique, Op. 14: V. Songe d''une nuit du sabbat', 312, 24, 561967, 9173344, 114
UNION ALL
    SELECT 3447, N'Carmen: Overture', 313, 24, 132932, 2189002, 119
UNION ALL
    SELECT 3448, N'Lamentations of Jeremiah, First Set \ Incipit Lamentatio', 314, 24, 69194, 1208080, 59
UNION ALL
    SELECT 3449, N'Music for the Royal Fireworks, HWV351 (1749): La RГ©jouissance', 315, 24, 120000, 2193734, 126
UNION ALL
    SELECT 3450, N'Peer Gynt Suite No.1, Op.46: 1. Morning Mood', 316, 24, 253422, 4298769, 64
UNION ALL
    SELECT 3451, N'Die ZauberflГ¶te, K.620: "Der HГ¶lle Rache Kocht in Meinem Herze"', 317, 25, 174813, 2861468, 155
UNION ALL
    SELECT 3452, N'SCRIABIN: Prelude in B Major, Op. 11, No. 11', 318, 24, 101293, 3819535, 172
UNION ALL
    SELECT 3453, N'Pavan, Lachrimae Antiquae', 319, 24, 253281, 4211495, 98
UNION ALL
    SELECT 3454, N'Symphony No. 41 in C Major, K. 551, "Jupiter": IV. Molto allegro', 320, 24, 362933, 6173269, 168
UNION ALL
    SELECT 3455, N'Rehab', 321, 14, 213240, 3416878, 166
UNION ALL
    SELECT 3456, N'You Know I''m No Good', 321, 14, 256946, 4133694, 157
UNION ALL
    SELECT 3457, N'Me & Mr. Jones', 321, 14, 151706, 2449438, 163
UNION ALL
    SELECT 3458, N'Just Friends', 321, 14, 191933, 3098906, 175
UNION ALL
    SELECT 3459, N'Back to Black', 321, 14, 240320, 3852953, 165
UNION ALL
    SELECT 3460, N'Love Is a Losing Game', 321, 14, 154386, 2509409, 84
UNION ALL
    SELECT 3461, N'Tears Dry On Their Own', 321, 14, 185293, 2996598, 113
UNION ALL
    SELECT 3462, N'Wake Up Alone', 321, 14, 221413, 3576773, 59
UNION ALL
    SELECT 3463, N'Some Unholy War', 321, 14, 141520, 2304465, 98
UNION ALL
    SELECT 3464, N'He Can Only Hold Her', 321, 14, 166680, 2666531, 152
UNION ALL
    SELECT 3465, N'You Know I''m No Good (feat. Ghostface Killah)', 321, 14, 202320, 3260658, 178
UNION ALL
    SELECT 3466, N'Rehab (Hot Chip Remix)', 321, 14, 418293, 6670600, 144
UNION ALL
    SELECT 3467, N'Intro / Stronger Than Me', 322, 9, 234200, 3832165, 121
UNION ALL
    SELECT 3468, N'You Sent Me Flying / Cherry', 322, 9, 409906, 6657517, 126
UNION ALL
    SELECT 3469, N'F**k Me Pumps', 322, 9, 200253, 3324343, 185
UNION ALL
    SELECT 3470, N'I Heard Love Is Blind', 322, 9, 129666, 2190831, 87
UNION ALL
    SELECT 3471, N'(There Is) No Greater Love (Teo Licks)', 322, 9, 167933, 2773507, 161
UNION ALL
    SELECT 3472, N'In My Bed', 322, 9, 315960, 5211774, 99
UNION ALL
    SELECT 3473, N'Take the Box', 322, 9, 199160, 3281526, 195
UNION ALL
    SELECT 3474, N'October Song', 322, 9, 204846, 3358125, 191
UNION ALL
    SELECT 3475, N'What Is It About Men', 322, 9, 209573, 3426106, 56
UNION ALL
    SELECT 3476, N'Help Yourself', 322, 9, 300884, 5029266, 76
UNION ALL
    SELECT 3477, N'Amy Amy Amy (Outro)', 322, 9, 663426, 10564704, 69
UNION ALL
    SELECT 3478, N'Slowness', 323, 23, 215386, 3644793, 178
UNION ALL
    SELECT 3479, N'Prometheus Overture, Op. 43', 324, 24, 339567, 10887931, 132
UNION ALL
    SELECT 3480, N'Sonata for Solo Violin: IV: Presto', 325, 24, 299350, 9785346, 127
UNION ALL
    SELECT 3481, N'A Midsummer Night''s Dream, Op.61 Incidental Music: No.7 Notturno', 326, 24, 387826, 6497867, 85
UNION ALL
    SELECT 3482, N'Suite No. 3 in D, BWV 1068: III. Gavotte I & II', 327, 24, 225933, 3847164, 88
UNION ALL
    SELECT 3483, N'Concert pour 4 Parties de V**les, H. 545: I. Prelude', 328, 24, 110266, 1973559, 194
UNION ALL
    SELECT 3484, N'Adios nonino', 329, 24, 289388, 4781384, 67
UNION ALL
    SELECT 3485, N'Symphony No. 3 Op. 36 for Orchestra and Soprano "Symfonia Piesni Zalosnych" \ Lento E Largo - Tranquillissimo', 330, 24, 567494, 9273123, 193
UNION ALL
    SELECT 3486, N'Act IV, Symphony', 331, 24, 364296, 5987695, 65
UNION ALL
    SELECT 3487, N'3 GymnopГ©dies: No.1 - Lent Et Grave, No.3 - Lent Et Douloureux', 332, 24, 385506, 6458501, 192
UNION ALL
    SELECT 3488, N'Music for the Funeral of Queen Mary: VI. "Thou Knowest, Lord, the Secrets of Our Hearts"', 333, 24, 142081, 2365930, 173
UNION ALL
    SELECT 3489, N'Symphony No. 2: III. Allegro vivace', 334, 24, 376510, 6129146, 56
UNION ALL
    SELECT 3490, N'Partita in E Major, BWV 1006A: I. Prelude', 335, 24, 285673, 4744929, 88
UNION ALL
    SELECT 3491, N'Le Sacre Du Printemps: I.iv. Spring Rounds', 336, 24, 234746, 4072205, 59
UNION ALL
    SELECT 3492, N'Sing Joyfully', 314, 24, 133768, 2256484, 184
UNION ALL
    SELECT 3493, N'Metopes, Op. 29: Calypso', 337, 24, 333669, 5548755, 53
UNION ALL
    SELECT 3494, N'Symphony No. 2, Op. 16 -  "The Four Temperaments": II. Allegro Comodo e Flemmatico', 338, 24, 286998, 4834785, 162
UNION ALL
    SELECT 3495, N'24 Caprices, Op. 1, No. 24, for Solo Violin, in A Minor', 339, 24, 265541, 4371533, 106
UNION ALL
    SELECT 3496, N'Г‰tude 1, In C Major - Preludio (Presto) - Liszt', 340, 24, 51780, 2229617, 61
UNION ALL
    SELECT 3497, N'Erlkonig, D.328', 341, 24, 261849, 4307907, 179
UNION ALL
    SELECT 3498, N'Concerto for Violin, Strings and Continuo in G Major, Op. 3, No. 9: I. Allegro', 342, 24, 493573, 16454937, 195
UNION ALL
    SELECT 3499, N'Pini Di Roma (Pinien Von Rom) \ I Pini Della Via Appia', 343, 24, 286741, 4718950, 161
UNION ALL
    SELECT 3500, N'String Quartet No. 12 in C Minor, D. 703 "Quartettsatz": II. Andante - Allegro assai', 344, 24, 139200, 2283131, 80
UNION ALL
    SELECT 3501, N'L''orfeo, Act 3, Sinfonia (Orchestra)', 345, 24, 66639, 1189062, 133
UNION ALL
    SELECT 3502, N'Quintet for Horn, Violin, 2 Violas, and Cello in E Flat Major, K. 407/386c: III. Allegro', 346, 24, 221331, 3665114, 94
UNION ALL
    SELECT 3503, N'Koyaanisqatsi', 347, 10, 206005, 3305164, 90;
GO
