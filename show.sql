USE kb301_dolgorukov;
GO

-- Цена самого дешевого альбома группы «Red Hot Chili Peppers».
DECLARE @cost DECIMAL(8, 2) = 0;
EXEC @cost = ind.cost_album_cheapest @name_author=N'Red Hot Chili Peppers';
PRINT @cost;
GO

-- Исполнители в жанре «Rock» с наименьшей суммарной длительностью песен в этом
-- жанре.
SELECT * FROM ind.artists_with_duration_minimum(N'Rock');
GO

-- Итоги по жанрам.
SELECT id_genre as N'Идентификатор жанра',
       count_track as N'Количество треков',
       size_summa as N'Информационная масса треков, MiB'
FROM ind.summary_genres;
GO
